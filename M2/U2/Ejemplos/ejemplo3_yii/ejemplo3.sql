﻿--  bbdd
DROP DATABASE IF EXISTS Ejmeplo3Yii;
CREATE DATABASE Ejmeplo3Yii;
USE Ejmeplo3Yii;

CREATE TABLE naciones (
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  continente varchar(50),
  victorias int
);


CREATE TABLE tenistas (
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  correo varchar(50),
  altura int,
  peso int,
  fechBaja date,
  activo boolean,
  naciones int,
  CONSTRAINT fktenistaNaciones FOREIGN KEY (naciones) REFERENCES naciones(id) ON DELETE CASCADE ON UPDATE CASCADE

);


