﻿CREATE DATABASE db2018;

USE db2018;

CREATE TABLE datos (
  id_auto int AUTO_INCREMENT PRIMARY KEY,
  datos varchar(127)
);

CREATE TABLE usuarios(
  id_usuario int AUTO_INCREMENT PRIMARY KEY,
  ip varchar(15),
  nombre  varchar(31)
);

SELECT
  *
FROM usuarios;

INSERT INTO datos (datos)
  VALUES (NOW());

ALTER TABLE datos ADD COLUMN ip varchar(15); 


ALTER TABLE usuarios ADD COLUMN email varchar(127); 
ALTER TABLE datos DROP email;

SELECT COUNT(DISTINCT ip) FROM datos;

SELECT * FROM usuarios;
SELECT * FROM datos;

SELECT DISTINCT
       nombre FROM usuarios WHERE email is NULL ;



SELECT DISTINCT
       nombre FROM usuarios JOIN datos USING(ip) WHERE email is NOT NULL AND MIN(datos.datos) ;


SELECT INTERVAL(datos,NOW()) FROM datos;

ALTER TABLE datos CHANGE datos datos datetime;

ALTER TABLE datos ADD INDEX(ip);


SELECT COUNT(*), ip FROM datos GROUP BY ip;



SELECT MAX(c1.n) FROM (SELECT COUNT(*) n, ip FROM datos WHERE ip IS NOT null GROUP BY ip)c1;


SELECT * FROM (
    SELECT ip,COUNT(*) n FROM datos
      WHERE ip IS NOT NULL
      AND ip<>'172.31.128.189'
      GROUP BY 1
     HAVING COUNT(*)=(
        SELECT MAX(n) FROM (
           SELECT ip,COUNT(*) n FROM datos
            WHERE ip IS NOT NULL
            AND ip<>'172.31.128.189'
          GROUP BY 1
        ) c1
    )  
  ) c2 JOIN usuarios USING(ip);



--  insertar datos del formulario

  UPDATE usuarios set email='ima@gmail.com' WHERE ip='172.31.128.189';



  --  añadir campo

    ALTER TABLE usuarios ADD ip_ok bool;

SELECT COUNT(*) FROM usuarios WHERE ip_ok is NOT null;


SELECT * FROM mysql.user;

DROP USER ''@'localhost';

CREATE USER 'alpe'@'%' IDENTIFIED BY 'hola';

SELECT CURRENT_USER();

CREATE USER 'alpe'@'%' IDENTIFIED BY 'hola';

SELECT * FROM information_schema.SCHEMA_PRIVILEGES;

INSERT INTO visitas value ('imanol');

CREATE DATABASE alpe_db2018;
USE alpe_db2018;

CREATE TABLE visitas(
nombre varchar(31)
);
GRANT ALL ON alpe_db2018.* TO alpe;
FLUSH PRIVILEGES;

SELECT * FROM usuarios;


SELECT * FROM visitas;


REVOKE ALL ON *.* from alpe;

FLUSH PRIVILEGES;

DROP USER alpe@`%`;