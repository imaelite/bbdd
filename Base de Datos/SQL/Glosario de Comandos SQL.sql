º/*COMANDOS DE SQL****************************************************************************************************************************************/



/*DECLARAR VARIABLE LOCAL***********************************************************************************************************/


BEGIN
/*Siempre despues del begin*/

declare comensales tinyint(3) unsigned;/*El nombre de la variable y el tipo de dato*/



SELECT 
    ncomensales into comensales /*se aplica de esta manera*/
FROM
    mesas
WHERE
    codigoc ='COM03'
     AND codigom = codigo_mesa
    
;



/*Escribir en Pantalla*****************************************************************************************************************/

THEN signal sqlstate '45000' 
set message_text ='no se puede aumentar el sueldo de un empleado en  mas de un 20%';



/*FUINCIONES SINTAXIS**************************************************************************************************************************/


set global log_bin_trust_function_creators=1;/*Para que valla*/
drop function if exists sueldo_de;

DELIMITER //
CREATE FUNCTION  sueldo_de (nombre_camarero varCHAR(5))/*aki se crea la funcion en si*/

RETURNS decimal(6,2)/*el tipo de dato que va adevolver*/
BEGIN

DECLARE sueldo decimal(6,2);/*declaracion de la variable a devolver*/



SELECT salariomes INTO sueldo from empleados where nombre=nombre_camarero ;/*la consulta realizada con la variable 
y el argumento pasado por teclado*/


RETURN sueldo;/* devolvermos el valor de la variable local*/
END //

DELIMITER ;


SELECT /*le pasamos como argumento el nombre de un trabajador, y nos devuelve su sueldo*/
    sueldo_de('ROSA')
	limit 1;


/*Las funciones podrán ser llamadas desde cualquier sentencia SQL como
SELECT,UPDATE, INSERT, DELETE.*/
	
	
	

/*VISTAS SINTAXIS***********************************************************************************************************************************/



CREATE OR REPLACE VIEW ejercicio12 (continente,gobierno,numero) AS /*Los "atributos" que tendra la vista en si*/
    (SELECT DISTINCT /* se muestran las formas de gobierno de y el número de los países con esa forma de gobierno*/

          country.continent,country.GovernmentForm,COUNT(*)
    FROM
        country
    group BY country.GovernmentForm)
      
;



/*Aki veo el tipo de gobierno y los paises que lo tienen*/

SELECT gobierno,numero
    
FROM
    ejercicio12;
    
/* Aki veo el tipo de gobierno por continente*/
    
 SELECT DISTINCT
    gobierno, continente
FROM
    ejercicio12
ORDER BY continente
;	
	
	
/*Muestra la sentencia que se utilizó para crear la vista.*/

SHOW CREATE VIEW nombre_vista;



/*PROCEDIMIENTOS SINTAXIS*****************************************************************************************************************************/


drop procedure if exists buscar_ciudades_variable_usuario;
DELIMITER **
CREATE PROCEDURE buscar_ciudades_variable_usuario(IN nombre_pais CHAR(52))
/*Declaramos una variable donde almacenaremos el nombre del pais*/

BEGIN

DECLARE codigo char(3);/*Variables locales*/

DECLARE poblacion int(11);

/* Consultamos el codigo del pais con el nombre que le pasamos */

SELECT Code into codigo FROM Country WHERE Name=nombre_pais;

/*Damos un valor a la variable población.*/

SET poblacion='150000';

/*Saco la consulta de las ciudades de ese pais, donde la poblacion sea mayor que 150000*/

SELECT * FROM City WHERE CountryCode=codigo and Population>poblacion;

END **

DELIMITER ;


CALL buscar_ciudades_variable_usuario('Zambia');/*El pais en cuestion es zambia*/



/*Los procedimientos puedes ser:*/

IN: el procedimiento recibe el parámetro y no le modifica. Le usa para
consultar y utilizar su valor.

OUT: el procedimiento únicamente puede escribir en el parámetro, no
puede consultarle.

INOUT: el procedimiento recibe el parámetro y puede consultarle y
modificarle.

OUT o INOUT se usan cuando se desea que un procedimiento
nos devuelva valores en determinadas variables.

/*Estructuras de Control********************************************************************************************************************/

/*Estructura IF*/



DELIMITER // /*En el caso de una funcion*/
CREATE FUNCTION  funcion_categoria (nombre_categoria varCHAR(15))/*aki se crea la funcion en si*/

RETURNS decimal(6,2)/*el tipo de dato que va adevolver, la propia funcion*/
BEGIN

DECLARE media decimal(6,2);/*declaracion de la variable a devolver*/

DECLARE oficio varCHAR(15);/*Esta es la variable que variara su valor en el if segun lo que introduzca el usuario*/

IF nombre_categoria='camarero' THEN set oficio='CAMARERO';/*Aki empiezael if las condiciones*/
ELSEIF nombre_categoria='COCINERO' THEN  set oficio='COCINERO';
ELSEIF nombre_categoria='ADMINISTRATIVO'THEN  set oficio='ADMINISTRATIVO';
ELSE THEN set oficio='nada';/*Si no es nada de todo lo anterior*/
END IF;/*Aki finaliza el if*/



SELECT avg(salariomes) into media from empleados where categoria=oficio;/*la consulta realizada con la variable 
y el argumento pasado por teclado*/

RETURN media;/* devolvermos el valor de la variable local*/
END //

DELIMITER ;

SELECT /*le pasamos como argumento el nombre de un trabajador, y nos devuelve su sueldo*/
    funcion_categoria('CAMARERO')
	;





/*Estructura CASE*/



set global log_bin_trust_function_creators=1;/*Esto para que funcione*/
drop function if exists DiaSemana;
DELIMITER $$
CREATE FUNCTION DiaSemana(d DATE)
RETURNS VARCHAR(10)
BEGIN
DECLARE n INT;
SET n=DAYOFWEEK(d);/*Metodo externo que convierte la fecha en un numero entero del 1 al 6*/
CASE/*Empieza el case aki*/
WHEN n=1 THEN RETURN 'Domingo';/*Las condiciones se ponen igual que el if per en ved de ELSEIF se pone when*/
WHEN n=2 THEN RETURN 'Lunes';
WHEN n=3 THEN RETURN 'Martes';
WHEN n=4 THEN RETURN 'Miercoles';
WHEN n=5 THEN RETURN 'Jueves';
WHEN n=6 THEN RETURN 'Viernes';
ELSE RETURN 'Sabado';/*Si no es nada de todo lo anterior*/
END CASE;/*Aki termina el case*/
END $$
DELIMITER ;


SELECT DiaSemana(CURDATE()) as 'Hoy es-->';




/*Estructura While*/

set global log_bin_trust_function_creators=1;/*Para que valla*/
drop function if exists palabra_reves;

DELIMITER //
CREATE FUNCTION palabra_reves (palabra_derecho varchar(80) )

RETURNS varchar(80)
BEGIN

DECLARE palabra_resultado varchar(80) default '';
DECLARE letra_reves varchar(80);
DECLARE longitud int default 0;

select LENGTH (palabra_derecho) into longitud;

WHILE longitud>0 do  


select SUBSTR(palabra_derecho,longitud, 1) into letra_reves;

select concat(palabra_resultado,letra_reves) into palabra_resultado;

set letra_reves ='';

SET longitud = longitud - 1;

END WHILE;

RETURN palabra_resultado;
END // 

DELIMITER ;

SELECT palabra_reves('imanol');



/*TRIGGERS----------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

/*Estructura basica*/

drop trigger if exists impedirAumento;
delimiter //
create trigger impedirAumento after insert on empleados /*se ejecuta despues de insertar*/
for each row 
begin


IF new.salariomes=2000  THEN signal sqlstate '45000' 
set message_text ='no se puede insertar un empleado que cobre mas de 2000';

END IF;

end //

delimiter ;


/*pa ver los trigers*/

show triggers;



/*para borrarlo*/

drop trigger nombre_trigger;


/*OLD Y NEW*/

old.CODIGOPEDIDO/*Hace referencia al argumento que le metomos al principio*/

new.salariomes/*Hace referencia al argumento ya modificado que se obtiene por ejemplo al actualizar*/



/*CREACION DE BASE DE DATOS******************************************************************************************************************/

DROP DATABASES IF EXISTS NOMBRE;/*Crear una Base de Datos*/

CREATE DATABASES NOMBRE;/*Crear una base de datos*/

USE NOMBRE;/* Usar la base de datos*/



/*Crear una Tabla*/

CREATE TABLE NOMBRE;/*Crear una tabla dentro de una base de datos*/


create table propietarios(/*Crear tabla con sus campos*/
DNI VARCHAR(9),
Nombre VARCHAR(60)) ENGINE=INNODB;



/*Cambiar el motor de las tablas, pueden ser:ISAM,InnoDB,Memory o MyISAM*/

alter table City  ENGINE=INNODB;



/*Añadir la fk*/

ALTER TABLE animales ADD FOREIGN KEY (propietario)

REFERENCES 

propietarios(DNI) ON DELETE CASCADE ON UPDATE CASCADE;


/*O con constreint, para hacerla mas restrictiva*/


ALTER TABLE CountryLanguage ADD CONSTRAINT FK2 FOREIGN KEY (CountryCode)

REFERENCES 

Country(Code) ON DELETE CASCADE ON UPDATE CASCADE;



/*JOIN*/


/*La diferencia esta que em el from para relacionar las tablas usamos join y nos ahorramos el where,

usamos on tras el from y luego todo con and*/
FROM
    empleados join
    clientes join
	pedidos
on
clientes.codigoempleadorepventas=empleados.codigoempleado
and
clientes.codigocliente=pedidos.codigopedido
group by nombre;




/*ENSEÑAR*/

SHOW DATABASES;/*ver bases de datos*/

SHOW TABLES;/*mirar las tablas de la base de datos*/

SHOW CREATE TABLE NOMBRE;/*como se ha creado la tabla*/

select * from country limit 0,2 ;/*Enseñar valores de 2 en 2*/

SELECT columna FROM tabla WHERE EXISTS (subconsulta)/*si existen filas en la subconsulta asociada*/



/*VER*/

DESC NOMBRE;/*ver la estructura de la tabla*/




/*ELIMINAR POR COMPLETO*/

DROP TABLE NOMBRE;/*eliminar tabla*/

DROP DATABASE NOMBRE;/*eliminar una base de datos*/





/*MODIFICAR*/

ALTER TABLE NOMBRE RENAME NOMBRE;/*cambiar el nombre de una tabla*/

ALTER TABLE NOMBRE ADD CONSTRAINT PK PRIMARY KEY (CAMPO);/*añadir clave primaria*/

ALTER TABLE NOMBRE ADD CAMPO VARCHAR(20);/*Añadir una columna a una tabla*/

ALTER TABLE NOMBRE ADD CAMPO VARCHAR(20) NOT NULL BEFORE CAMPO; /*Añadir una columna a una tabla en una determinada posicion*/

ALTER TABLE clientes CHANGE NombreClientes NombreCliente VARCHAR(50);/*renombrar una tabla*/

ALTER TABLE NOMBRE DROP CAMPO;/*quitar un campo de una tabla*/

ALTER TABLE estaciones ADD helados VARCHAR(20) COMMENT 'helados ice cream';/*añadir comentarios en un campo*/

ALTER TABLE NOMBRE DROP PRIMARY KEY;/*quitar la clave primaria de una tabla*/

ALTER TABLE CITY MODIFY ID INT(11)AUTO_INCREMENT;/*Modificar un dato para que su valor se auto incremente.*/

ALTER TABLE estaciones MODIFY id INT(11)UNIQUE;/*Modificar un dato para que su valor sea unico.*/

ALTER TABLE NOMBRE MODIFY CAMPO VARCHAR(70) not null default '';/*cambiar el dato de un campo, para hacerlo not null*/

ALTER TABLE NOMBRE MODIFY SALARIO INT(11)UNSIGNED zerofill;/*hacer que un campo sea positivo,y que rellene el tamaño del 
campo con 0s por la izquierda*/

ALTER TABLE NOMBRE MODIFY SALARIO INT(11) SIGNED ;/*hacer que un campo pueda tener valores 
negativos*/

ALTER TABLE NOMBRE MODIFY CAMPO ENUM ('DATO','DATO');/*crear un campo que sea una selección*/


/*USO DEL LIMIT*/


WHILE numero<numero_total DO

select codemple into codigo from empleados limit numero,1;/*De esta manera cojo uno a uno los codigos de la tabla, empezando por el uno 
y a continuacion*/

set numero=numero+1;

end while;


/*TRABAJAR CON CADENAS*/


select LENGTH (palabra_derecho) into longitud;/*Cojer la longitud de una cadena y guardarla en una variable*/


select SUBSTR(palabra_derecho,longitud, 1) into letra_reves; /*Cojer letra a letra de una cadena y guardarla*/


select concat(palabra_resultado,letra_reves) into palabra_resultado;/*sumar una palabra a otra cadena*/



set letra_reves ='';/*limpiar una cadena*/





/*ACTUALIZAR*/

UPDATE Country SET Name=concat(Name,'min') order by population LIMIT 1; /*Cambia el Name al país con menor población, 
por el nombre que tenia añadiendo la palabra ‘min’ al final.*/

UPDATE Country SET Language='ingles' WHERE Language='English' And IsOfficial='T';/*cambiar el valor de un campo 
de una tabla,si si idioma es ingles y es el lenguaje oficial del pais)*/

UPDATE Country SET Population=Population*1.10 WHERE Continent ='Europe';/*aumentar la poblacion un 10% de un pais
que este en europa*/





/*INTRODUCIR DATOS*/

INSERT INTO cuidadores VALUES ('1','juan','1000.00','12/09/2004','T','Mañana');/*insertar datos en una tabla, 
en función del orden de los campos que haya en la tabla)*/

INSERT INTO Country (Code,Continent,Region)VALUES ('SLV','North America','Central America');/*insertar datos en una 
tabla, en función de que campos concretos te interesa rellenar)*/





/*BORRAR*/

DELETE FROM TABLA WHERE Name='El Salvador';/*borrar un registro de la tabla que cumple la clausula where)*/

DELETE FROM CountryLanguage WHERE Language='Dutch';/*borrar los lenguages que sean 'Dutch')*/

DELETE FROM TABLA;/*Borrar todo el contenido de la tabla)*/








/*OPERADORES*/

/*Logicos*/

>:Mayor que

<:Menor que

>=:Mayor o igual

<=:Menor o igual.

=:igual.

<>:distinto.

!=:distinto.




/*Aritméticos*/

+,-,*,/,%




/*De comparacion*/

=,>=,<=,<>,>,<.



/*Otros Operadores*/


like:como,comparador de cadenas.

and:devuelve verdadero si las expresiones de derecha e izquierda son verdaeras.

or: devuelve verdadero si cualquiera de las expresiones de derecha e izquierda son verdaeras.

not:invierte la logica de la expresion que esta a su derecha.

%:el simbolo del porcentaje sustituye a cualquier cadena de caracteres, como un comodin.

as: se usa para atribuir a un campo un aleas.

concat: concatenar datos, como nombre + apellidos.

in: comprueba si una columna tiene un valor igual que cualquiera de los que esten el el parentesis, como en 

una subconsulta.

EXISTS:Se utiliza para filtrar los resultados de una consulta si existen filas en la subconsulta asociada.

BETWEEN:Permite seleccionar los registros que estén incluidos en un rango.

IS e IS NOT:Permiten verificar si un campo es o no nulo respectivamente.


GROUP BY:agrupar filas en la salida de una sentencia.

ORDER BY para obtener resultados ordenados por la columna que queramos ascendente o
descendente, ASC o DESC.

LIMIT: permite limitar el número de filas devueltas.

DISTINCT:nhace que no haya repeticiones en los resultados del campo.

COUNT():sirve para contar las filas de cada grupo.

HAVING: permite hacer selecciones en situaciones en las que no es posible usar WHERE.

all:calcular la relación entre una expresión y todos los registros de la subconsulta.

any:calcular la relación entre una expresión y algunos registros de la subconsulta.




/*FUNCIONES*/

/*SIN:Tras el select se usa para hacer formulas matematicas:*/ select sin (3.14/2);


/*AS: Asignar un alias a cualquier expresion select:*/select
 nombre,fecha,datediff(current_date(),fecha)/365 as edad from gente;
 

/*distinct:solo se muestran filas diferentes:*/ select distinct fecha from gente;


/*WHERE:datos de tablas que cumplan condiciones, se puede usar con (<,>,<>,>=,<=,=)o
(AND,OR,NOT) where nombre=”Mengano” and nombre=”Fulano”;


/*GROUP BY:Ordenar según valores de la columna indicada, y eliminan valores dulicados,
 permite usar funciones de resumen o reunion:*/select nombre,fecha from gente group by fecha.
 
 
/*HAVING:selecciona en situaciones donde no es posible usar el where, se usa con las sentencias de arriba.


/*ORDER BY:Ordenar unos datos desde la columna que queramos:*/select * from gente order by fecha;


/*ASC O DESC: Elegir el orden de agrupamiento:*/ select * from gente order by fecha desc;


/*LIMIT:limitar el numero de filas devueltas, admite dos parametros:*/ select * from gente limit 3;
/*Si se usan los dos parametros el primero indica la primera fila a recuperar 
y el segundo el numero de filas a recuperar:*/select * from gente limit 0.2;


/*COUNT():Cuenta las filas de cada grupo, se puede usar >1:*/ select fecha, count('') As cuenta from gente group by fecha;



/*MAX(),MIN(),SUM(),AVG(),STD():Muestran el valor según la sentencia elegida:*/

SUM (Expresion) : /*Suma los valores indicados en el argumento*/

AVG (Expresion) : /*Calcula la media de los valores*/

MIN (Expresion) : /*Calcula el mínimo*/

MAX (Expresion) : /*Calcula el máximo*/

POWER(radio_argumento, 3):/*Elevar un argumento a una potencia*/

COUNT(*) : /*Cuenta el número de valores de una fila ,incluyendo los nulos. */
  
STD (Expresion):/*Obtiene la desviación estándar a partir de los valores 
de una columna con respecto a una tabla de la base de datos.  */





/*Tipos de Datos SQL


*Tipo de dato************Sinónimos*************Tamaño****************Descripción

-BINARY                  VARBINARY             1 byte por carácter    cualquier tipo de datos

-BIT                     BOOLEAN               1 byte                 Valores Sí y No,

-TINYINT                 INTEGER1/BYTE         1 byte                 Un número entero entre 0 y 255.

-COUNTER                 AUTOINCREMENT                                Contadores cuyo valor se incrementa automáticamente

-MONEY                   CURRENCY              8 bytes                Un número entero Enorme

-DATETIME                DATE/TIME             8 bytes                Una valor de fecha u hora entre los años 100 y 9999

-TIMESTAMP                                                            Combinación de fecha y hora.

-YEAR                                          tamaño 2 o 4           si el año con dos o cuatro dígitos.

-UNIQUEIDENTIFIER        GUID                  128 bits  	          Un número de identificación único 

-DECIMAL                 NUMERIC/DEC           17 bytes               valores comprendidos entre 1028 - 1 y - 1028 - 1. 

-REAL                    SINGLE/FLOAT4         4 bytes                Un valor para valores negativos y positivos

-FLOAT                   DOUBLE/FLOAT8         8 bytes                Un valor para valores negativos y positivos

-SMALLINT                SHORT/INTEGER2        2 bytes                Un entero corto entre – 32.768 y 32.767.

-INTEGER                 LONG/INT/INTEGER4     4 bytes                Un entero largo entre – 2.147.483.648 y 2.147.483.647. 

-IMAGE                   LONGBINARY/GENERAL    Lo que se requiera     Se utiliza para objetos OLE.

-TEXT                    LONGTEXT/LONGCHAR     2 bytes por carácter   Desde cero hasta un máximo de 2.14 gigabytes. 

-CHAR                    NCHAR/STRING/VARCHAR  2 bytes por carácter   Desde cero a 255 caracteres




*/











