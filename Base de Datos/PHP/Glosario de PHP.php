	
<?php
//GLOSARIO DE PHP///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

<?php//Zonja PhP


/*
COMENTARIO
*/

//RUTA
//localhost/dwes17

//RUTA A phpMyAdmin
//localhost/phpmyadmin/


//LINK /////////////////////////////////////////////////////////////////////////////////////////////

//union entre paginas php, se pone en el php y la ruta de la otra página php	
include("proporciona_datos.php");
require("proporciona_datos.php");//si el required es erroneo, no se ejecuta nada en adelante por eso pone requerido
require_once('DB.php');

		


//VARIABLE//////////////////////////////////////////////////////////////////////////////////////////


$nombre="ima";

$edo=28;

static $contador=0;//Solo se activa una vez esta linea y conserva su valor al salir de su anbito o linea

var $nombre="ima";//Local,fuera de una funcion

global $nombre;	  //Global, siempre dentro de la funcion, accesible para todo el codigo
$a = 1;
function Prueba2(){
 global $a;
 echo $a;}//esto devuleve 1, ya que hemos convertido a $a en global dentro de la funcion.
Prueba2();


$_POST["enviando"]//Super Global, declarada como array, accesible desde fuera del archivo php, pero dentro del pakete

$num1=12.4444;//variable en este caso decimal




//CONSTANTE//////////////////////////////////////////////////////////////////////////////////////////////


define("AUTOR", "Ima", true);//el true lo hace insensible a mayusculas y minusculas

echo "el autor es: ".AUTOR;//no se puede meter entre las comillas como las variables
//Otras constantes predefinidas son __LINE__:me da el numero de linea o __FILE__:me da la ruta del fichero

define("pi", 3.1416);//te guarda el valor de pi en este caso

$pi=pi();//o se saca de esta manera




//CADENAS////////////////////////////////////////////////////////////////////////////////////////////////

echo "<p class='resaltar'>Esto es un Ejemplo de Frase</p>";//Para meter estilos en un parrafo, con diferentes comillas
echo "<p class=\"resaltar\">Esto es un Ejemplo de Frase</p>";//con "\" delante y detras de las comillas similares

//Imprimir la palabra en minusculas
echo (strtolower($nombre));

//Imprimir la palabra en mayusculas
echo (strtoupper($nombre));

$nombre="imanol";
echo $nombre[2];//Devuelve la letra "a"

//Poner en mayusculas la primera letra de cada palabra.
$resultado=ucwords($cadena);

$str = 'abcdef';

// longitud de la cadena
echo strlen($str);



//Esto me da el numero entero menos el último campo
$numero = substr($dni, 0, $dni_longitud - 1);

//de aqui saco la letra del dni
$letra_del_dni=substr($dni, $dni_longitud-1, $dni_longitud);

//Revertir una cadena
strrev($cadena);//con función 
$cadenareves="";//con bucle
 for ($i = 0; $i <= strlen($cadena); $i++) {
     $cadenareves=$cadenareves+substr($cadena, $longitud - $i, 1);
}

//te cuenta el número de caracteres hasta que encuentra una g en este caso
strcspn($str, "g");

//Devuelve la cadena desde que encuentra el caracter que le indicamos inclusive
echo strstr("carmenduran@domenicoscarlatti.es","@");///@domenicoscarlatti.es

// Pone en mayúscula solo la 1ª letra de $texto
echo  ucfirst($texto)."</br>";

// Pone en mayúscula la 1ª letra de cada palabra de $texto
echo  ucwords($texto)."</br>";



//PRINT////////////////////////////////////////////////////////////////////////////////////////////////////////////////

print "Bienvenidos al Curso de PHP <br/>" ;

printf("El numero pi es: " . "%.4f", " $pi");//para en este caso sacar solo 4 decimales

//Para concatenar variables es con "."  se puede poner de estas dos formas y si lo pongo con comillas simples 
//no reconoce las variables es decir que inclulle el $

print "Mi nombre es " . $nombre;
print "Mi nombre es  $nombre";

echo $nombre,$edad; //eco puedo unir variables a pelo pero print no, se suele usar casi siempre el echo.
		

		
		
//ARRAYS/////////////////////////////////////////////////////////////////////////////////////////////////


//Declaracion 
$arreglo1 = array();//array vacia


//array indexado, utiliza números para ordenar las posiciones del array empezando por 0
      
$semana=array("Lunes","Martes","Miercoles");


//añadir un dato al array respetando la posicion

$semana[3]="jueves";



         
//array asociativo utiliza nombres para referirse a los valores
         
$datos=array("Nombre"=>"Juan","Edad"=>29);//clave=>valor

//añadir un dato al array, con su nombre

$datos["apellido"]="osuna";

echo  "Mi edad es ".$datos["Edad"];//te pintaria 29


//Recorrer Arrays


//Array asociativo
foreach($datos as $clave=>$valor){
     echo "A ". $clave ." le corresponde ". $valor ."<br>";
}

//Array indexado
for ($i=0; $i < count($array); $i++) {
	echo $array[$i];
}
   
   

//Funciones para las Arrays

// Pregunat si en un array hay un dato
if (!in_array($dato, $array)) {
}

//mostrar el último elemento del array
$fruits = array('manzana', 'plátano', 'arándano');
echo end($fruits); // arándano


//contar el número de elementos del array
count($paises);

//Introducir datos en un array asociativo
$paises["Alemania"] = "Berlin";

//Introducir datos en un array normal
array_push($array, $dato);

//O Asi
$array[] = $dato;

//en este caso el elemento final es un array normal de cadenas
 $usuarios[0]['mascotas'][] = "pato";


//borrar un valor del array
unset($paises["España"]);


//mostrar una array entera
echo var_dump($ordenada);


//Mostrarla de manera horizontal
print_r($arreglo);


//Asignar a un array una serie de valores
$numeros = range(0, 4); // array(0, 1, 2, 3, 4)
$letras = range('a', 'd'); // array('a', 'b', 'c', 'd',);
		
		
//Asignar a unos valores de array un nombre
$arreglo = array("Juan Daniel", "23", "Mexico", "55010");
list($nombre, $edad, $ciudad, $codigo_postal) = $arreglo;


//Ordena array asociativos, en orden ascendente("a" =>,"b" =>...)
asort($arreglo);

//convertir un array a cadena poniendo entre los elementos del array en este caso "de"
$matriz=array(7,'julio',2011);
echo implode(' de ',$matriz);

$idiomas = implode(",", $extras);//o de esta manera para separar los elementos de un array por comas.


//comprueba si esta variable es una array o no 
if (is_array($datos)) {
    echo "si es arrray";
} else {
    echo "no es una array";
}

 //devuelve la primera clave que corresponda con esa valor
        
$array = array('azul' => 0, 'rojo' => 1, 'verde' => 2, 'rosa' => 3);
        
$clave = array_search(1, $array); // $clave = 2;
echo $clave;
$clave = array_search(3, $array);  // $clave = 1;
echo $clave;


//CLAVES

        
//comprueba si en un array, hay una clave 


//con in_array
if (in_array("España", $paises, true)){
    echo "si esta españa<br/>";
} else {
    echo "No esta españa<br/>";
}


//con array_key_exists
 $search_array = array('primero' => 1, 'segundo' => 4);
 if (array_key_exists('primero', $search_array)) {
       echo "Si existe esa clave";
} else {
       echo "No existe esa clave";
}

//con array_serch

$array = array(0 => 'azul', 1 => 'rojo', 2 => 'verde', 3 => 'rojo');

$clave = array_search('verde', $array); // $clave = 2;
$clave = array_search('rojo', $array);  // $clave = 1;	

	

//Encontar una clave pasandole el valor en bucle, apliar en el proyecto: Formulario_clase
function ver_ciudad($distancia, $rutas) {
        if ($distancia == "") {
              echo "Falta Distancia";
        } else {
      foreach ($rutas as $clave => $valor) {
        if ($valor == $distancia) {
              echo $clave . "</br>";
        }
}
}
}

//de este array
$array = array(
    'fruta1' => 'manzana',
    'fruta2' => 'naranja',
    'fruta3' => 'uva',
    'fruta4' => 'manzana',
    'fruta5' => 'manzana');


// Este ciclo muestra todas las claves del array asociativo
// donde el valor equivale a "manzana".

while ($nombre_fruta = current($array)) {
    if ($nombre_fruta == 'manzana') {
        echo key($array).'<br />';
    }
    next($array);
}



//VALORES


//Encontrar una valor pasandole la clave 

  function ver_distancia($ciudad, $rutas) {//este array está en el proyecto Formulario_clase
  $existe2 = comprobar_ciudad($ciudad, $rutas);
     if ($existe2) {
       $valor = $rutas[$ciudad];
       echo $valor . " KMs";
     }
}


//array asociativa, multidimensional, un array compuesto de 3 arrays asociativas

 $cursos = array("ASIR" => array("Primero" => array("BD" => "Basde de Datos", "SI" => "Sistemas Informáticos", "Redes" => "Administración de Redes"),
                                 "Segundo" => array("SI" => "Servicios de Internet", "SAD" => "Seguridad y alta disponibilidad", "EIE" => "Empresa e Iniciativa Emprendedora")),
                 "DAW" => array("Primero" => array("FOL" => "Formación y orientación laboral", "PROG" => "Programación", "ED" => "Entorno de Desarrollo"),
                                "Segundo" => array("JS" => "JavaScript", "PHP" => "Entorno Servidor", "DIW" => "Diseño de interfaces Web")),
                 "DAM" => array("Primero" => array("BD" => "Basde de Datos", "SI" => "Sistemas Informáticos", "LM" => "Lenguaje de Marcas"),
                                "Segundo" => array("PMM" => "Programación Multiplataforma y Dispositivos", "SGE" => "Sistemas Gestión Empresariales", "DI" => "Desarrollo Interfaces")));


echo $cursos["ASIR"]["Primero"]["BD"];//acceder individualmente, te daria: Basde de Datos


//bucle que recorre la array multidimensional, pintandola


  function visualiza_todo($cursos) {
  $curso = "";
  echo "<h2>Visualizar Todos Los Cursos</h2>";
       foreach ($cursos as $modulo => $cursoss) {//aqui simplemento cojo todo lo que hay en el array
           echo "</br><h3>$modulo:<h3><br><br>";//aqui muestro el módulo concreto DAW,ASIR....
				foreach ($cursoss as $curso => $asignaturas) {
                    echo "<table  border='4px' width='50' cellspacing='2' cellpadding='5' align='center'>";
                    echo "<tr><th>Año</th><th>$curso</th></tr>";//Aqui pongo el año concreto
                    foreach ($asignaturas as $siglas => $asignatura) {//el array concreto de cada año
                        echo "<tr><td>" . $siglas . "</td><td>" . $asignatura . "</td></tr>";
                    }
          echo "</table></br>";
 }
 }
 }

 
//convertir un array en un array global accesible desde cualquier parte del archivo, donde rutas sería el nombre del array

 $GLOBALS["rutas"][$ciudad] = $distancia;




//FUNCIONES///////////////////////////////////////////////////////////////////////////////////////////////////////


//llenar una array, con numeros aleatorios

function llenar_array($array) {

   for ($i = 0; $i < 10; $i++) {
        $num1 = rand(1, 10);
        $array[$i] = $num1;
		}
        return $array;
}




//parametro por referencia "& + $variable" , es decir lo que realize esta funcion tambien afectara

function incrementa(&$valor1){
$valor1++;//al valor de la variable que le hemos pasado por argumento.
return $valor1;
}


$numero=5;//es decir $numero pasaria a tener un valor de 6
echo incrementa($numero);



static function descuento(){//metodo statico
	self::$ayuda=4500;
}

Compra_Vehiculo::descuento();//llamada a metodo statico desde fuera de la clase


//Factorial recursivo

function factor($n) {///factorial recursivo que llama a si mismo
   $resultado = 0;
if ($n == 0) {
   $resultado = 1;
} else {

   $resultado = $n * factor($n - 1);
}
  return $resultado;
}

//otra funcion recursiva
function recursividad($a) {
   if ($a < 20) {
       echo "$a\n";
      recursividad($a + 1);
   }
}

//EXPRESIONES REGULARES//////////////////////////////////////////////////////////////////////////////

ereg // Coincidencia de expresiones regulares
eregi //Coincidencia de expresiones regulares sin diferenciar mayúsculas y minúsculas


//comprobar un correo electrónico
$cadena = "imanol_elite@hotmail.com";
echo preg_match("/[a-z0-9]@[a-z0-9].[a-z]/",$cadena);


//comprobar que en una cadena hay unas letras
$abecedario = "abcdefghijklmnñopqrstuvwz"; 
if (preg_match("/abc/", $abecedario)) {
       echo "Encuentro 'abc'";
   } else {
       echo "Noencuentro abc";
}


//comprobar que en una cadena hay unas letras al fianl
$direccion = "Calle Cuéllar";
if (preg_match("/^calle/i", $direccion)) {//la i del final ignora minusulas y mallusculas
    echo "Es una calle";
} else {
    echo "No es una calle";
}
 
//Validar DNI

      function validar_dni($dni) {
            //letras del dni
            $letras = array("T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E", "T");

            $dni_longitud = strlen($dni); //longitud del dni

            $numero = substr($dni, 0, $dni_longitud - 1); //el numero del dni que meto
            $letra_del_dni = substr($dni, $dni_longitud - 1, $dni_longitud); //la letra del dni

            $numero = (int) $numero;//convierto el nímero a entero
            $resultado = $numero % 23;//obtengo el resto de la operacion para sacar la letra que el dni devería tener

           if (strcasecmp($letras[$resultado], $letra_del_dni) == 0) {//aqui comparo las letras
                echo "El dni es correcto</br></br>";
            } else {
                echo "El dni no es correcto</br></br>";
            }
            echo "El dni " . $dni . " con los numeros " . $numero . " tiene la letra " . $letras [$resultado];
        }

//comprobar que en una cadena hay unas letras al final
$cadena = "Esto es un saludo: hola";
if (preg_match("/hola$/", $cadena)) {
    echo "Se encunentra 'hola' al final";
} else {
    echo "No se encunentra 'hola' al final";
}


 
//CONDICIONALES//////////////////////////////////////////////////////////////////////////////////////


Strcmp//compara teniendo en cuenta mayuscula o minuscula
strcasecmp //esta ignora mayusculas o minusculas
		
$var1="Casa";
$var2="CASA";

if (strcmp ($cadena1 , $cadena2 ) == 0) { //Devuelve 0 si son iguales o 1 si no son iguales 

	echo "Son iguales";
	
}else{
	
	echo "No son iguales";
	
}


/*
El resto de comparadores como <,>,<=... estos son como en java salvo el "===" que compara
si el valor y el tipo de dato es identico
*/	
	


$var1=8;
$var3="Ima";

//if

if($var1!=$var3){//Los Comparadores son igual que java pero inclullen el "===" que compara el dato
// y el tipo de dato a la vez, la prioridad es "&&", "and" y por ultimo "="
	echo "Diferentes";
}else{
	echo "No son Diferentes";
}


//if y else if

if($edad<=18){//else if
     echo "Eres menor de edad";
    }else if($edad<=40){
     echo "Eres Joven";
    }else if($edad<=65){
     echo "Eres Maduro";
    }else{
         echo "Cu&iacutedate";
    }
	

//switch case
switch ($aleatorio) {

                case 1:
                    $cont_1++;
                    break;
                case 2:
                    $cont_2++;
                    break;
                case 3:
                    $cont_3++;
                    break;
                case 4:
                    $cont_4++;
                    break;
                case 5:
                    $cont_5++;
                    break;

                default :
                    $cont_6++;
                    break;
            }
	
	
//Operador ternario

 echo $edad<18 ? "Eres menor de eddad no puedes acceder" : "Puedes acceder";
 
 //Se pone la condicion si se cumple se dice lo primero , sino lo segundo
 
 
 
//BUCLES//////////////////////////////////////////////////////////////////////////////////////////


//while
 while ($var1 <= 10) {
         echo "El numero es: " . $var1 . "<br>";
         $var1++;
}


//do - while
do{
  $var1++;
  echo "El numero es: " . $var1 . "<br>";
}while ($var1 <= 10);


//for
for ($i = 0; $i <= 10; $i++) {
   echo $i . " Hemos entrado en el bucle<br>";
if ($i == 5) {
   echo "Bucle Fin";
   break;//asi salimos del bucle, de manera brsuca
}
}


for ($i = 10; $i >= -10; $i--) {
if ($i == 0) {
   echo "Division por 0 no es posible" . "<br>";
   continue;//de esta manera evitamos la operacion
}
echo "9 / $i = " . 9 / $i . "<br>";
}

//for-each

$todoPizzas = DB::obtienePizzas();
foreach ($todoPizzas as $pizza) {
      if (!in_array($pizza, $_SESSION["cesta"]->getCesta())) {
}




//OPERADORES LOGICOS/////////////////////////////////////////////////////////////////////////////////////////////

$a=valores[1];//Previamente dar valor a las variables
$b=valores[5];

$num4="5";//Casting Implicito	
$num4=$num4+4;

$num5="5";//Casting Explicito	
$resultado=(int)$num5;

$suma=a+b;
$resta=a-b;
$multiplicacion=a*b;
$division=a/b;
$num1=rand(1,6);//numero aleatorio, solo sacara del 1 al 6 inclusive
$potencia=pow(b, a);//Potencia
$resultado=numeros%23;//Resto de una Division
$num3=round(5.7556454,2);//opcional redondear con 2 decimales, esto daria 5.76

echo "Suma: ".suma."</br>";//poner la variable con la operacion hecha


//FORMULARIOS/////////////////////////////////////////////////////////////////////////////////////////////////////////////

//GET
//No se puede enviar información binaria (archivos, imágenes,etc.) => necesario el método POST.
//Los datos se ven en la URL del navegador.

//POST
//Rompe la funcionalidad del botón “Atrás” del navegador
//El botón actualizar repite la operación


//LLevar varios datos de una página a otra atraves de un enlace
echo "<a href='cancelaciones.php?numeroEliminar=".$row['numero']."&numeroCentro=".$row['id_centro']."'>ANULAR</a>";
 echo "<a href='descripciones.php?imagen=".$texto."'>";


//Tipos de entrada de datos por formulario



//Los input normales se accede a ellos con:
<input type="text"  name="nombre"  size="30" maxlength="40" >
$nombre = isset($_REQUEST['nombre']) ? $_REQUEST['nombre'] : NULL;




//los input de cuadradito se declaran como un array
<input type="checkbox" name="extras[]" value="aleman" >aleman
<input type="checkbox" name="extras[]" value="musica" >musica


//se obtiene de esta manera, para mostrarlo
 if (isset($_REQUEST["extras"])) {//primero se comprueba que alguno de estos esté seleccionado
//luego se pinta de esta manera sean los que sean
$extras = isset($_REQUEST['extras']) ? $_REQUEST['extras'] : NULL;

 foreach ($extras as $extra) {
         echo "$extra ";
}

//o de esta manera si quieres llenar un campo set de una tabla
if (isset($_REQUEST["extras"])) {
		$extras = isset($_REQUEST['extras']) ? $_REQUEST['extras'] : NULL;
        $idiomas = implode(",", $extras);
}




//Los inputs tipo boton se acceden de esta manera

<input type="button" name="actualizar" value="Actualizar Datos">
$actualizar = isset($_REQUEST['actualizar']) ? $_REQUEST['actualizar'] : NULL;
 
 if ($actualizar){
 print ("Se han actualizado los datos");
 }
 
 

//los imput de circulo se les pone un name normal pero uno siempre tiene que esta seleccionado
<input type="radio" name="sexo" value="M" CHECKED>Masculino
<input type="radio" name="sexo" value="F">Femenino


//se obtiene de esta manera
$sexo = isset($_REQUEST['sexo']) ? $_REQUEST['sexo'] : NULL;

 
 
//Los campos select se obtiene asi
<select name="raza"></br></br>
     <option value="Pequines">Pequines</option>
     <option value="Bulldog">Bulldog</option>
</select>

$raza = isset($_REQUEST['raza']) ? $_REQUEST['raza'] : NULL;

 
//El textarea se comprueba asi
 if ($_REQUEST["comentario"] !== "") {
    echo "<tr><td> Comentario</td><td>" .$_REQUEST["comentario"] . "</td></tr>";
}



//Los input hidden son para obtener datos de un formulario sin que el usuario lo vea
echo "<input type='hidden' name='dni' value='" . $fila["usuario_ID"] . " '  required>";
$dni_usuario = isset($_REQUEST['dni']) ? $_REQUEST['dni'] : NULL;



//El input de borrar se usa asi
<input type="reset" value="Limpiar" name="borrar"/>
$borrar = isset($_REQUEST['borrar']) ? $_REQUEST['borrar'] : NULL;

 if ($borrar){
 echo "Se ha pulsado el botón de limpiar datos";
 }


//llenar un campo select dinámicamnete tirando de un array


<select name="annio">
  
   include ("array.php");

   foreach ($cursos["ASIR"] as $clave => $valor) {
   echo '<option value="' . $valor . '">' . $valor . "</option>";
   }
  
</select>



//Llenar un campo de cuadraditos dinámicamente

  Idiomas</br></br>
 
   include ("array.php");

   foreach ($tablas["Duennios"] as $clave => $valor) {
   foreach ($valor as $clav => $valo) {
        echo " <input type='checkbox' name='extras[]' value='" . $valo . "'>$valo";
}
}




//Llenar un campo de circulitos dinámicamente

$query2 = "SELECT * FROM centro";
$resultado = mysqli_query($conexion, $query2);

if ($resultado) {
    while ($row = mysqli_fetch_array($resultado)) {
            echo "<input type='radio' name='centro' required value=" . $row['cod'] . ">" . $row["cod"];
   }	
   } else {
			echo "Error al recuperar los datos";
  }



//comprovar si se pulsa el boton con name:enviar, por ejemplo el submit
if (isset($_GET["enviar"])) {
	

//comprobar si los campos estan vacios
if ($_GET["nombre"] === "" || $_GET["apellido"] === "") {
	 echo "Error Datos vacios</br>";
    if ($_GET["nombre"] === "") {
     echo "Nombre vacio</br>";
                    }
    if ($_GET["apellido"] === "") {
    echo "Apellido vacio</br>";
                    }
}else//aqui ya, pinto los campos en una tabla o lo que sea

//FECHAS//////////////////////////////////////////////////////////////////////////////////////////////



//Obtener la fecha en formato 19-11-2016 
 
$hoy = getdate();
$fecha = date("d-m-Y", $hoy[0]);
echo $fecha;



//Convertir una fecha europea a formato americano año-mes-dia
$hoy = getdate();
$fecha = date("Y-m-d", $hoy[0]);



//Obtener la fecha en formato americano
$fecha = date("Y-m-d", $hoy[0]);

//Convertir a formato europeo lo que te devuelve la BD
$query = "SELECT *, DATE_FORMAT(f_solicitud,'%d-%m-%Y') AS niceDate FROM inscripcion WHERE numero = '$numeroEliminar'";

$resultado = mysqli_query($conexion, $query);

while ($row = mysqli_fetch_array($resultado)) {
        echo $row['niceDate'];//OJO aqui se saca el aleas
}


//SENTENCIAS SQL//////////////////////////////////////////////////////////////////////////////////


//Crear BD
$consulta = "CREATE DATABASE $basephp";


//Crear Tablas
     $tabla2 = "CREATE TABLE Duennios  (
  usuario_ID int(11) NOT NULL  UNIQUE ,
  usuarioNombre varchar(20) NOT NULL,
  Hora TIME DEFAULT '00:00:00', 
  Sexo Enum('M','F') DEFAULT 'M' not null,
  Nacimiento DATE DEFAULT '1970-12-31', 
  Fumador char(1),
  Idiomas SET('Castellano','Frances','Ingles','Aleman','Bulgaro','Chino'), 
  PRIMARY KEY  (usuario_ID)
) ENGINE=InnoDB";


//Relacionar tablas
$actualizacion = "ALTER TABLE Perrera 
  ADD FOREIGN KEY (CodigoUsuarioID) REFERENCES  Duennios (usuario_ID) ON DELETE CASCADE ON UPDATE CASCADE  ";
		
		
//Insertar Datos
$consulta1 = "INSERT INTO  Perrera (Perro_ID,CodigoUsuarioID) "
             . "VALUES ('$codigo_perro','$codigo_usuario')";		
		
//Borrar Registros
$consulta = "DELETE FROM Duennios WHERE usuario_ID='$dni'";


//Actualizar el Registros
$consulta = "UPDATE Duennios SET usuarioNombre='$nombre',Hora='$hora',Sexo='$sexo' , Nacimiento='$nacimiento' ,Fumador ='$fumador',Idiomas='$idiomas'"
          . "WHERE usuario_ID='$dni_usuario';";
		  
		  
//Mostrar tablas relacionadas
$consulta4 = "SELECT 
    *
FROM
    Perrera,
    Duennios,
    Celda
WHERE
	Perrera.CodigoUsuarioID=Duennios.usuario_ID
    and Celda.UsuarioID=Duennios.usuario_ID
 order by usuario_ID";
		
		
//Autocomit

mysqli_autocommit($conexion, false);

 $resultado = mysqli_query($conexion, $query3);
 $actualizacionSQL = mysqli_query($conexion, $actualizacion);
 
if ($resultado && $actualizacionSQL) {//operaciones que se realicen
                
} else {
	print "Se produjo un error, se ha vuelto a los valores iniciales";
	mysqli_rollback($conexion);
}
		
mysqli_autocommit($conexion, true);
		
		
//CONSULTAS PREPARADAS////////////////////////////////////////////////////////////////////////////////


//Caracteres indicativos del tipo de los parámetros en una consulta preparada.
//Carácter. Tipo del parámetro.

I. Número entero.
D. Número real (doble precisión).
S. Cadena de texto.
B. Contenido en formato binario (BLOB).




//Insertar Datos


$consulta = "insert into centro (cod,nombre,tlf) values (?,?,?)";
$resultado = mysqli_prepare($conexion, $consulta);

$codigo = 4;
$nombre = "Linares";
$tlf = "888654333";

mysqli_stmt_bind_param($resultado, "iss", $codigo, $nombre, $tlf);
$resultado = mysqli_stmt_execute($resultado);

if ($resultado) {
     echo "Inserción correcta";
}		
		
		
		
//Insercion con Array Genérica

	
$centros = [["Cinco", "980458604"], ["Seis", "980458604"], ["Siete", "980458604"]];
		
$consulta = "insert into centro (nombre,tlf) values (?,?)";
        
if ($resultado = mysqli_prepare($conexion,  $consulta)) {
     foreach ($centros as $centro) {
           mysqli_stmt_bind_param($resultado, "ss", $centro[0], $centro[1]);
           mysqli_stmt_execute($resultado);
           // echo "Centro Insertado, nombre: " . $centro[0] . " tlf: " . $centro[1] . " </br>";
            }
} else {
     echo "Error en La insercion";
}


//Insercion con Array Asociativa


$centros=["Ima"=>"34534534345","nol"=>"23423432423","ito"=>"2342343"];

$consulta = "insert into centro (nombre,tlf) values (?,?)";
        
$resultado = mysqli_prepare($conexion, $consulta);

foreach($centros as $clave=>$valor){
        mysqli_stmt_bind_param($resultado, "ss", $clave, $valor);
        mysqli_stmt_execute($resultado);
        //echo "Centro Insertado, nombre: " . $clave . " tlf: " . $valor . " </br>";
        }
		
mysqli_stmt_close($resultado);
		
		
		
//Actualización de Datos

$consulta = "update centro set nombre=?, tlf=? where cod=?";

$resultado = mysqli_prepare($conexion, $consulta);

$nombre = "Maria Imanola";
$tlf = "777777777";
$codigo = 2;

mysqli_stmt_bind_param($resultado, "ssi", $nombre, $tlf, $codigo);

$resultado = mysqli_stmt_execute($resultado);

if ($resultado) {
    echo "actualizacion correcta";
} else {
    echo "actualización incorrecta";
}
		

//Listado de Datos

 $consulta = "select * from centro";
 
if (!$resultado = mysqli_prepare($conexion, $consulta )) {
       echo "Fallo en la culsulta o conexion";
       exit();
}
else{
	
mysqli_stmt_execute($resultado);
 
//Ojo al orden en el que se ponen estas variables por que corresponde al orden de la tabla OJO
mysqli_stmt_bind_result($resultado, $codigo,$nombre,$tlf);
        
		while (mysqli_stmt_fetch($resultado)) {
            echo "Centro: " . $codigo . " ,Nombre: " . $nombre . " ,Telefono: " . $tlf . "<br/><br/>";
        }
        }		
		
		
		
		
		
//TRY CATCH//////////////////////////////////////////////////////////////////////////////////////////////////		
		
		
		
//Funcion que verifica si un dato es un entero

function entero($entero) {
     if (!intval($entero)) {
            throw new Exception('No es un número Entero');
     } 
    return $entero;
}

try {
//en función de lo que le enviemos si no es un entero salta por el catch, si no nos muestra el return

    echo entero(3);
	
} catch (Exception $ex) {
    echo "Excepción capturada: " . $ex->getMessage();
}		
		
//MOVERSE ENTRE PÁGINAS//////////////////////////////////////////////////////////////////////////////////


//este al pulsar un boton confirmar
if (isset($_REQUEST["confirmar"])) {
    header("Location:registro.php");
}

//Este desde html con un temporizador
echo "<meta http-equiv='refresh' content='1;url=inicio.php'>";




//ENLACES///////////////////////////////////////////////////////////////////////////////////////////////

//quitar los platos seleccionados mediante de un enlace
if (isset($_GET['numeroEliminar'])) {
    $numeroEliminar = isset($_REQUEST['numeroEliminar']) ? $_REQUEST['numeroEliminar'] : 0;
    $_SESSION["cesta"]->eliminar($numeroEliminar);
    header("location: registro.php");
}


//en enlace en si
 $enlace = "<a href='registro.php?numeroEliminar=" . $informacion . "'>quitar</a>";



//SESIONES///////////////////////////////////////////////////////////////////////////////////////////////

//esto al principio de cada archivo
session_start();
		

//Destruir la sesión
if (session_destroy()) {
     echo "<h4>Sesión Destruida</h4><br>";
     echo "<meta http-equiv='refresh' content='1;url=inicio.php'>";
}


//Meter los datos de inicio.php en la sesion
if (isset($_REQUEST["componer"])) {
    $_SESSION['numero_consumiciones'] = isset($_REQUEST['numero_consumiciones']) ? $_REQUEST['numero_consumiciones'] : 0;
    $_SESSION['pago'] = isset($_REQUEST['pago']) ? $_REQUEST['pago'] : 0;
    $_SESSION['hora'] = isset($_REQUEST['hora']) ? $_REQUEST['hora'] : NULL;
}

//mostrar el contenido de una sesión
echo isset($_SESSION['hora']) ? $_SESSION['hora'] : NULL;


//Recorrer un array en una sesion
foreach (@$_SESSION["cesta"]->getCesta() as $pizza) {
    $precio += $pizza->getPrecio();
}


//Limpiar de cotenido variables de sesion
unset($_SESSION['lugar']);
unset($_SESSION['topics']);
unset($_SESSION['forma_pago']);
unset($_SESSION['extras']);
		
		
//OBJETOS//////////////////////////////////////////////////////////////////////////////////////////

//Instanciaciones de objetos

 $clasica1 = new clasica($array_datos);

//crear un array de golpe con datos de un formulario, para crear un objeto con ella
   $array_datos = array(
                    "codigo" => $_REQUEST["cod"],
                    "descripcion" => $_REQUEST["desc"],
                    "precio" => $_REQUEST["precio"],
                    "tipo" => $_REQUEST["tipo"],                        
                    "foto" => $destino,
                         "ciudad" => $_REQUEST["ciudad"],
                      "ingredientes" => $_REQUEST["ingredientes"]
                        
                        ); 

$clasica = new clasica($array_datos);


//Declaración de clases

//clase padre instanciable
class Pizza {

    protected $codigo;
    protected $descripcion;
    protected $precio;
    protected $tipo;
    protected $foto;

    //Constructor antiguo
    public function __construct($codigo, $descripcion, $precio, $tipo, $foto) {
        $this->codigo = $codigo;
        $this->descripcion = $descripcion;
        $this->precio = $precio;
        $this->tipo = $tipo;
        $this->foto = $foto;
    }

    //constructor que trabajo con una array
    public function __construct($row) {
        $this->codigo = $row['codigo'];
        $this->descripcion = $row['descripcion'];
        $this->precio = $row['precio'];
        $this->tipo = $row['tipo'];
        $this->foto = $row['foto'];
    }

//Aqui irian los gets y sets
}


//clase hija que hereda del padre
class Clasica extends Pizza {

    private $ciudad;
    private $ingredientes;

    public function __construct($array_datos) {
        //constructor del padre
        parent::__construct($array_datos);
        //constructor de la propia clase clasica
        $this->ciudad = $array_datos['ciudad'];
        $this->ingredientes = $array_datos['ingredientes'];
      
    }

    //Aqui irian los gets y sets

  }


//Funciones dentro de la clase de un objeto


    //Método para añadir pizzas una a una
    public function anadir($pizza) {
      if(!in_array($pizza,$this->cesta)){
        $this->cesta[] = $pizza;
      }
    }

    //Metodo para eliminar pizza según el código
    public function eliminar($pizzaCodigo) {
        for ($i = 0; $i < count($this->cesta); $i++) {
           // echo $this->cesta[$i]->getCodigo();
            if ($pizzaCodigo === $this->cesta[$i]->getCodigo()) {
                array_splice($this->cesta, $i, 1);
            }
        }
    }

    //Metodo para calcular el precio total de la seleccion
   public function calcularPrecio() {
        $precio = 0;
        foreach ($this->cesta as $pizza_in) {
            $precio += $pizza_in->getPrecio();
        }
        return $precio;
    }

    //vaciar la cesta de un objeto
   public function vaciar_cesta() {
        unset($this->cesta);
        $this->cesta = array();
    }


//SERIALIZAR UN OBJETO////////////////////////////////////////////////////////////////////////////////////////////////////


    //Para introducirlo en un value de algun input por ejemplo
    base64_encode(serialize($objeto));

    //o el objeto codificado en si
    $objeto_normal = unserialize(base64_decode($_REQUEST["pizza"]));


//SUBIR UN ARCHIVO A LA BD//////////////////////////////////////////////////////////////////////////////////////////////


//De 1 en 1
  Foto<input type="file" name="archivo"><br/><br/>

  if (isset($_REQUEST["enviar"])) {
                //Parte del fichero a subir
                //obtenemos los datos del archivo
                $tamano = $_FILES["archivo"]['size'];
                $tipo = $_FILES["archivo"]['type'];
                $archivo = $_FILES["archivo"]['name'];
                $prefijo = substr(md5(uniqid(rand())), 0, 6); // --- construimos un prefijo--
                if ($archivo != "") {
                    // guardamos el archivo a la carpeta imagenes
                    $destino = "imagenes/" . $prefijo . "_" . $archivo;
                    if (copy($_FILES['archivo']['tmp_name'], $destino)) {
                        $status = "Archivo subido: <b>" . $archivo . "</b>";
                        echo "<img src='" . $destino . "'><br>";
                        // mostramos la imagen subida ------------
                    } else {
                        $status = "Error al subir el archivo";
                    }
                } else {
                    $status = "Error al subir archivo";
                }

                //lo que luego meteriamos a la BD seía el 
                $destino;
                

                //Luego en otro lugar para mostrar esa foto, no hace falta ni extensión ni carpeta
                  echo "<img src='" . $pizza->getFoto() . "' align='center' height='80px' width='80px'><br>";
}





//FUNCIONES ESTÁTICAS///////////////////////////////////////////////////////////////////////////////////////////////////

  require_once('Pizza.php');

  const INT_QUERY = 0;
  const INT_STATEMENT = 1;


//la función de ejecutar consultas inicial
      private static function ejecutaSentencias($statementArray) {
        require ("conexion.php");

        try {
            $opc = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
            $dsn = "mysql:host=$servidor;dbname=$dbname";
            $dwes = new PDO($dsn, $usuario, $contrasena, $opc);
            $resultado;
            $dwes->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dwes->beginTransaction();
            foreach ($statementArray as $statement) {
                $resultado = $dwes->exec($sql);
            }
            if ($resultado) {
                $dwes->commit();
            } else {
                $dwes->rollback();
            }
        } catch (PDOException $e) {
            echo "Se ha producido un error: " . $e->getMessage();
        }
        return $resultado;
    }


   //Funcion ejecuta sentencias según sean de select o de insert
    private static function ejecutaConsulta($sql, $type) {
        require ("conexion.php");
        try {
            $opc = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
            $dsn = "mysql:host=$servidor;dbname=$dbname";
            $dwes = new PDO($dsn, $usuario, $contrasena, $opc);
            $resultado = true;
            $dwes->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $dwes->beginTransaction();
            if ($type == DB::INT_QUERY) {
                $resultado = $dwes->query($sql);
            } else if ($type == DB::INT_STATEMENT) {
                $resultado = $dwes->exec($sql);
                if ($resultado) {
                    $dwes->commit();
                } else {
                    $dwes->rollback();
                }
            }
        } catch (PDOException $e) {
            echo "Se ha producido un error: " . $e->getMessage();
        }
        return $resultado;
    }

//Listar todas las pizzas de una tabla y las devuelve en forma de array de objetos pizza

     $todoPizzas = DB::obtienePizzas();//se la llama desde donde sea asi

    public static function obtienePizzas() {

        $sql = "SELECT * FROM pizzas";
        $resultado = self::ejecutaConsulta($sql, DB::INT_QUERY);//ojo esta condición hay que definirla previamente en la clase
        $pizzas = array();
        if ($resultado) {
            $row = $resultado->fetch();
            while ($row != null) {
                $pizzas[] = new Pizza($row);
                $row = $resultado->fetch();
            }
        }
        return $pizzas;
    }

//Listar una pizza por codigo, devuelve solo 1 pizza

    $pizza = DB::obtienePizza($codigo);//se llama asi

    public static function obtienePizza($codigo) {
        $sql = "SELECT * FROM pizzas where codigo='$codigo'";
        $resultado = self::ejecutaConsulta($sql, DB::INT_QUERY);//ojo esta condición hay que definirla previamente en la clase
        if ($resultado) {
            $row = $resultado->fetch();
            $pizzas = new Pizza($row);
        }
        return $pizzas;
    }


//obtener el número total de pedidos de la BD

$numero_total=DB::pedido();//se llama asi

    public static function pedido() {
        $sql = "SELECT max(numero) from pedido";
        $resultado = self::ejecutaConsulta($sql, DB::INT_QUERY);//ojo esta condición hay que definirla previamente en la clase
        return $resultado->fetch()[0];
        return $resultado;
    }


    //Inserciones en pedidos y en detalle Normales sin preparada

    $ok = DB::insertar_pedido($_SESSION['pago'], $_SESSION['hora'], $_SESSION['precio']);//se le llama asi

//insertar pedidos
    public static function insertar_pedido($f_pago, $fecha, $importe) {
        $sql = "insert into pedido (f_pago,fecha,importe) values ('$f_pago','$fecha','$importe')";
        $resultado = self::ejecutaConsulta($sql, DB::INT_STATEMENT);//ojo esta condición hay que definirla previamente en la clase
        if ($resultado) {
            echo "<p>Inserción Correcta en Tabla Pedidos</p>";
        } else {
            throw new Exception('<p>Inserción InCorrecta en Tabla Pedidos</p>');
}
}


//insertar detalles
    public static function insertar_detalle($f_pago, $importe) {
        $sql = "insert into detalle (id_pedido,id_pizza) values ('$f_pago','$importe')";
        $resultado = self::ejecutaConsulta($sql, DB::INT_STATEMENT);
        if ($resultado) {
            echo "<p>Inserción Correcta en Tabla Detalles</p><br>";
        } else {
            throw new Exception('<p>Inserción InCorrecta en Tabla Detalles</p>');
        }
        return $resultado;
    }
		
    //se les llama asi
     $ok = DB::insertar_detalle(DB::pedido(), $pizza->getCodigo());



 //Inserciones con preparada en la tabla pedido y detalles
    public static function anadirPedido_detalle($codigo, $pago, $fecha, $importe) {
      require ("conexion.php");
        try {
            $cesta = $_SESSION["cesta"]->getCesta();
            $opc = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
            $dsn = "mysql:host=$servidor;dbname=$dbname";
            $conexion = new PDO($dsn, $usuario, $contrasena, $opc);

            $conexion->beginTransaction();

            $sentencia = $conexion->prepare("INSERT INTO pedido (numero,f_pago,fecha,importe) VALUES (:numero,:f_pago,:fecha,:importe)");


            $codigo++;
            $sentencia->bindParam(':numero', $codigo);
            $sentencia->bindParam(':f_pago', $pago);
            $sentencia->bindParam(':fecha', $fecha);
            $sentencia->bindParam(':importe', $importe);

            if ($sentencia->execute()) {
                echo "<p>Inserción Correcta en Tabla Pedidos</p>";
            } else {
                echo "<p>Inserción Incoorrecta en Tabla Pedidos</p>";
            }

            foreach ($cesta as $datos) {
                $sentencia2 = $conexion->prepare("INSERT INTO detalle (id_pedido, id_pizza) VALUES (:id_pedido, :id_pizza)");

                $codigoPizza = $datos->getCodigo();
                $sentencia2->bindParam(':id_pedido', $codigo);
                $sentencia2->bindParam(':id_pizza', $codigoPizza);

                if ($sentencia2->execute()) {
                    echo "<p>Inserción Correcta en Tabla Detalles</p>";
                } else {
                    echo "<p>Inserción Incorrecta en Tabla Detalles</p>";
                }
            }

            $conexion->commit();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            $conexion->rollback();
        }
        return $sentencia;
    }


    //se la llama asi
    $ok = DB::anadirPedido_detalle(DB::pedido(), $_SESSION['pago'], $_SESSION['hora'], $_SESSION['precio']);



    //localizar ua pizza clasica concreta
    public static function obtieneClasica($codigo) {
        $sql = "SELECT * FROM pizzas,clasica where pizzas.codigo='$codigo'";
 
        $resultado = self::ejecutaConsulta($sql, DB::INT_QUERY);
        if ($resultado) {
            $row = $resultado->fetch();
            $pizzas = new Clasica($row);
        }
        return $pizzas;
    }

//se le llama asi, se le pasa un codigo
$pizza = DB::obtieneClasica($numeroEliminar);


    //Añadir una pizza en tabla pizza y clasica

 public static function anadirPizzas($objeto) {
        require ("conexion.php");
        try {

            $opc = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
            $dsn = "mysql:host=$servidor;dbname=$dbname";
            $conexion = new PDO($dsn, $usuario, $contrasena, $opc);

            $conexion->beginTransaction();

            //Inserción en Pizzas
            $sentencia = $conexion->prepare("INSERT INTO pizzas (codigo,descripcion,precio,tipo,foto) VALUES (:cod,:des,:precio,:tipo,:foto)");

            //Para evitar que caske

            $codigo1 = $objeto->getCodigo();
            $desc1 = $objeto->getDescripcion();
            $prec1 = $objeto->getPrecio();
            $tipo1 = $objeto->getTipo();
            $foto1 = $objeto->getFoto();

            $sentencia->bindParam(':cod',$codigo1);
            $sentencia->bindParam(':des',$desc1);
            $sentencia->bindParam(':precio',$prec1);
            $sentencia->bindParam(':tipo',$tipo1);
            $sentencia->bindParam(':foto',$foto1);

            if ($sentencia->execute()) {
                echo "<p>Inserción Correcta en Tabla Pizzas</p>";
            } else {
                echo "<p>Inserción Incoorrecta en Tabla Pizzas</p>";
            }
            //Inserción en clásca

            $sentencia1 = $conexion->prepare("INSERT INTO clasica (cod,ciudad,ingredientes) VALUES (?,?,?)");


                 //Para evitar que caske
            
            $codigo2 = $objeto->getCodigo();
            $ciudad1 = $objeto->getCiudad();
            $ingre1 = $objeto->getIngredientes();
            
            
            
            $sentencia1->bindParam(1,$codigo2);
            $sentencia1->bindParam(2,$ciudad1);
            $sentencia1->bindParam(3,$ingre1);

            if ($sentencia1->execute()) {
                echo "<p>Inserción Correcta en Tabla Clasica</p>";
            } else {
                echo "<p>Inserción Incoorrecta en Tabla Clasica</p>";
            }



            $conexion->commit();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            $conexion->rollback();
        }
        return $sentencia;
    }

  //se le pasa una pizza clasica como argumento
  $clasica = new clasica($array_datos);

  $ok = DB::anadirPizzas($clasica);

//obtener todos los códigos de las pizzas

      public static function saberPizzas() {

        $sql = "SELECT codigo FROM pizzas";
        $resultado = self::ejecutaConsulta($sql, DB::INT_QUERY);
        $pizzas;
        $resultadp = array();
        if ($resultado) {
            $row = $resultado->fetch();
            while ($row != null) {
                $pizzas = new Pizza($row);
                $row = $resultado->fetch();
                $resultadp[] = $pizzas->getCodigo();
            }
        }
        return $resultadp;
    }
    

  //Esta función no recibe ningún parámetro devolverá un array con el nombre de las pizzas que nadie la ha pedido.
    public static function saberImpopular() {

        $sql = "SELECT codigo,descripcion,precio,tipo,foto FROM pizzas p1 left join detalle d1 on d1.id_pizza=p1.codigo where d1.id_pizza is null";
        $resultado = self::ejecutaConsulta($sql, DB::INT_QUERY);
        $pizzas;
        $resultadp = array();
        if ($resultado) {
            $row = $resultado->fetch();
            while ($row != null) {
                $pizzas = new Pizza($row);
                $row = $resultado->fetch();
                $resultadp[] = $pizzas->getCodigo();
            }
        }
        return $resultadp;
    }



//Esta función recibe como parámetro el código de un PIZZA y devolverá un array con la descripción de todas las pizzas que son del mismo tipo que la elegida.
    public static function saberSimilares($codigo) {
        $sql = "SELECT codigo,descripcion,precio,tipo,foto FROM pizzas where tipo=(select tipo from pizzas where codigo='" . $codigo . "')";
        $resultado = self::ejecutaConsulta($sql, DB::INT_QUERY);
        $pizzas;
        $resultadp = array();
        if ($resultado) {
            $row = $resultado->fetch();
            while ($row != null) {
                $pizzas = new Pizza($row);
                $row = $resultado->fetch();
                $resultadp[] = $pizzas->getDescripcion();
            }
        }
        return $resultadp;
    }



//comprobar si un tipo concreto existe
    public static function obtenerTipo($tipoEntrada) {
        $sql = "SELECT * FROM pizzas WHERE tipo='$tipoEntrada'";
        $resultado = self::ejecutaConsulta($sql);
        $tipo = null;
        if ($resultado) {
            $row = $resultado->fetch();
            $tipo = $row['tipo'];
        }
        return $tipo;
    }

//se comprueba asi
   $nombre = DB::obtenerTipo($tipo);
 if ($nombre != "") {
    $salida = "<br/><p>El Tipo Existe, pero el Codigo de Pizza No</p>";
 }



		?>