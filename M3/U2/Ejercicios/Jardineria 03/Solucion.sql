﻿--  (4.10.18) Sacar los nombres de los clientes que han pedido más de 200 unidades de cualquier producto.

SELECT DISTINCT
  NombreCliente
FROM pedidos
  JOIN detallepedidos
    ON pedidos.CodigoPedido = detallepedidos.CodigoPedido
  JOIN clientes
    ON pedidos.CodigoCliente = clientes.CodigoCliente
WHERE Cantidad > 200
GROUP BY NombreCliente;

--  (4.10.40) Códigos de los frutales no vendidos



SELECT
  CodigoProducto
FROM productos
WHERE Gama = 'frutales';


SELECT DISTINCT
  c1.CodigoProducto
FROM detallepedidos
  RIGHT JOIN (SELECT
      CodigoProducto
    FROM productos
    WHERE Gama = 'frutales') c1
    ON c1.CodigoProducto = detallepedidos.CodigoProducto
WHERE detallepedidos.CodigoProducto IS NULL;

--  DAVID
SELECT
  *
FROM (SELECT
    codigoproducto
  FROM productos
  WHERE gama = 'frutales') c1
  LEFT JOIN (SELECT DISTINCT
      codigoproducto
    FROM (SELECT
        codigoproducto
      FROM productos
      WHERE gama = 'frutales') c1
      JOIN detallepedidos USING (codigoproducto)) c2 USING (codigoproducto)
WHERE c2.codigoproducto IS NULL;


--  (4.10.09) Sacar el nombre, apellidos, oficina (ciudad) y cargo del empleado que no represente a ningún cliente


SELECT DISTINCT
  CodigoEmpleadoRepVentas
FROM clientes;

SELECT
  *
FROM empleados
  LEFT JOIN (SELECT DISTINCT
      CodigoEmpleadoRepVentas
    FROM clientes) c1
    ON c1.CodigoEmpleadoRepVentas = CodigoEmpleado
WHERE c1.CodigoEmpleadoRepVentas IS NULL;

SELECT
  Ciudad,
  CodigoOficina
FROM oficinas;

SELECT
  c1.Nombre,
  c1.Apellido1,
  c1.Apellido2,
  c2.Ciudad,
  c1.Puesto
FROM (SELECT
    *
  FROM empleados
    LEFT JOIN (SELECT DISTINCT
        CodigoEmpleadoRepVentas
      FROM clientes) c1
      ON c1.CodigoEmpleadoRepVentas = CodigoEmpleado
  WHERE c1.CodigoEmpleadoRepVentas IS NULL) c1
  JOIN (SELECT
      Ciudad,
      CodigoOficina
    FROM oficinas) c2 USING (codigooficina); 

  --  david ???????????

    select c2.Nombre,c2.apellido1,c2.apellido2,ciudad,c2.puesto from(select nombre,apellido1,apellido2,codigooficina,puesto, from empleados left join (select distinct codigoempleadorepventas, codigoempleado from clientes)c1 using(codigoempleado) where c1.codigoempleado is null  )c2 join oficinas using(codigooficina);


--  (4.08.03) Obtener los nombres de los clientes cuya línea de crédito sea mayor que los pagos que haya realizado

SELECT
  CodigoCliente,

  SUM(Cantidad)
FROM pagos
GROUP BY CodigoCliente;

SELECT
  NombreCliente

FROM (SELECT
    CodigoCliente,

    SUM(Cantidad) total
  FROM pagos
  GROUP BY CodigoCliente) c1
  JOIN clientes USING (CodigoCliente)
WHERE LimiteCredito > total;


--  (4.08.09) ¿Qué cliente(s) ha rechazado más pedidos?

SELECT DISTINCT
  CodigoCliente,
  COUNT(*) total
FROM pedidos
WHERE Estado = 'Rechazado'
GROUP BY CodigoCliente;


SELECT
  MAX(c1.total)
FROM (SELECT DISTINCT
    CodigoCliente,
    COUNT(*) total
  FROM pedidos
  WHERE Estado = 'Rechazado'
  GROUP BY CodigoCliente) c1;


SELECT
  clientes.NombreCliente
FROM (SELECT DISTINCT
    CodigoCliente,
    COUNT(*) total
  FROM pedidos
  WHERE Estado = 'Rechazado'
  GROUP BY CodigoCliente) c1
  JOIN clientes USING (CodigoCliente)
WHERE c1.total = (SELECT
    MAX(c1.total)
  FROM (SELECT DISTINCT
      CodigoCliente,
      COUNT(*) total
    FROM pedidos
    WHERE Estado = 'Rechazado'
    GROUP BY CodigoCliente) c1);

--  david

SELECT
  nombrecliente
FROM (SELECT
    codigocliente
  FROM pedidos
  WHERE estado = 'rechazado'
  GROUP BY codigocliente
  HAVING COUNT(*) = (SELECT
      MAX(n)
    FROM (SELECT
        codigocliente,
        COUNT(*) n
      FROM pedidos
      WHERE estado = 'rechazado'
      GROUP BY codigocliente) c1)) c2
  JOIN clientes USING (codigocliente);



--  (4.10.02b) Sacar el listado con los nombres de los clientes, el total pedido y el total pagado por cada uno de ellos. ???????????????????????????


SELECT
  CodigoPedido,
  SUM(Cantidad * PrecioUnidad) total_pagado
FROM detallepedidos
GROUP BY CodigoPedido;

--  c2
SELECT

  CodigoCliente,
  SUM(total_pagado) pedidos_total
FROM pedidos
  JOIN (SELECT
      CodigoPedido,
      SUM(Cantidad * PrecioUnidad) total_pagado
    FROM detallepedidos
    GROUP BY CodigoPedido) c1 USING (CodigoPedido)
GROUP BY CodigoCliente;
--  c3
SELECT
  CodigoCliente,
  SUM(Cantidad) AS pagos_total
FROM pagos
GROUP BY CodigoCliente;

--  c4

SELECT
  NombreCliente,
  pedidos_total,
  c3.pagos_total
FROM (SELECT

    CodigoCliente,
    SUM(total_pagado) pedidos_total
  FROM pedidos
    JOIN (SELECT
        CodigoPedido,
        SUM(Cantidad * PrecioUnidad) total_pagado
      FROM detallepedidos
      GROUP BY CodigoPedido) c1 USING (CodigoPedido)
  GROUP BY CodigoCliente) c2
  JOIN (SELECT
      CodigoCliente,
      SUM(Cantidad) AS pagos_total
    FROM pagos
    GROUP BY CodigoCliente) c3 USING (CodigoCliente)
  JOIN clientes USING (CodigoCliente);


--  david
  select NombreCliente,pedido,pagos from 
    (select codigocliente,sum(s) pedido from (select codigopedido,sum(cantidad*preciounidad) s from detallepedidos group by codigopedido) c1 join pedidos using(codigopedido) group by codigocliente) 
    c2 join (select codigocliente,sum(cantidad) pagos from pagos group by codigocliente) c3 using(codigocliente)
    join (select codigocliente,NombreCliente from clientes) clientes using(codigocliente); 










SELECT
  NombreCliente,
  c1.total_pedido,
  c2.total_pagado
FROM clientes
  JOIN (SELECT
      CodigoCliente,
      COUNT(*) total_pedido,
      CodigoPedido
    FROM pedidos
    GROUP BY CodigoCliente) c1 USING (CodigoCliente)
  JOIN (SELECT
      detallepedidos.CodigoPedido,
      SUM(Cantidad * PrecioUnidad) total_pagado

    FROM detallepedidos
    GROUP BY detallepedidos.CodigoPedido) c2 USING (CodigoPedido);



--  (4.09.02) Sacar el nombre de los clientes que no hayan hecho pagos, junto al nombre de sus representantes (sin apellidos) y 

--  la ciudad de la oficina la que pertenece el representante.


--  clientes que no han hecho ningun pago

SELECT
  NombreCliente,
  CodigoEmpleadoRepVentas
FROM clientes
  LEFT JOIN (SELECT DISTINCT
      CodigoCliente
    FROM pagos) c1 USING (CodigoCliente)
WHERE c1.CodigoCliente IS NULL;


-- c2 join empleados


SELECT
  NombreCliente,
  Nombre,
  Ciudad
FROM (SELECT DISTINCT
    NombreCliente,
    CodigoEmpleadoRepVentas
  FROM clientes
    LEFT JOIN (SELECT DISTINCT
        CodigoCliente
      FROM pagos) c1 USING (CodigoCliente)
  WHERE c1.CodigoCliente IS NULL) c2
  JOIN empleados
    ON c2.CodigoEmpleadoRepVentas = CodigoEmpleado
  JOIN oficinas USING (CodigoOficina);


SELECT
  *
FROM clientes;
--  david

SELECT
  NombreCliente,
  nombre,
  ciudad
FROM (SELECT DISTINCT
    nombrecliente,
    CodigoEmpleadoRepVentas codigoempleado
  FROM clientes
    LEFT
    JOIN (SELECT DISTINCT
        codigocliente
      FROM pagos) c1 USING (codigocliente)
  WHERE c1.codigocliente IS NULL) c2
  JOIN empleados
  USING (codigoempleado)
  JOIN oficinas USING (codigooficina);











-- nombre de clientes y nombre de su representante
SELECT
  clientes.NombreCliente,
  c1.NombreCliente repre
FROM clientes
  JOIN (SELECT
      *
    FROM clientes) c1
    ON clientes.CodigoCliente = c1.CodigoEmpleadoRepVentas;

--  oficina del representante

SELECT
  oficinas.Ciudad,
  CodigoEmpleado
FROM oficinas
  JOIN empleados USING (CodigoOficina);

--  resultado

SELECT DISTINCT
  clientes.NombreCliente,
  c1.NombreCliente repre,
  c2.Ciudad
FROM clientes
  JOIN (SELECT
      *
    FROM clientes) c1
    ON clientes.CodigoCliente = c1.CodigoEmpleadoRepVentas
  JOIN (SELECT
      oficinas.Ciudad,
      CodigoEmpleado
    FROM oficinas
      JOIN empleados USING (CodigoOficina)) c2
    ON c2.CodigoEmpleado = c1.CodigoEmpleadoRepVentas;



  --  (4.10.20) Obtener los nombre de clientes que no estén al día con sus pagos. (Sin excluir los pedidos rechazados)


SELECT CodigoCliente,
       FechaPago
        FROM pagos;

SELECT 
       FechaPedido,
     
       CodigoCliente FROM pedidos;

SELECT * FROM (SELECT CodigoCliente,
       FechaPago
        FROM pagos) c1 JOIN (SELECT 
       FechaPedido,
     
       CodigoCliente FROM pedidos) c2 USING(CodigoCliente);


