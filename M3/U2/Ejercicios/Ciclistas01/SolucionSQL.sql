﻿--  (b) Obtener las poblaciones que tienen la meta de alguna etapa, pero desde las que no se realiza ninguna salida. Resuélvela utilizando NOT IN

SELECT
DISTINCT
  llegada
FROM etapa
WHERE llegada NOT IN (SELECT DISTINCT
    salida
  FROM etapa);

--  (l) Obtener el nombre de los puertos de montaña que tienen una altura superior a la altura media de todos los puertos

SELECT
  nompuerto
FROM puerto
WHERE altura > (SELECT
    AVG(
    altura)
  FROM puerto);

--  (08) ¿Cuántos maillots diferentes ha llevado Induráin?

SELECT
  dorsal
FROM ciclista
WHERE nombre = 'Miguel Induráin';

SELECT
  COUNT(DISTINCT código)
FROM lleva
WHERE dorsal = (SELECT
    dorsal
  FROM ciclista
  WHERE nombre = 'Miguel Induráin');

--  (a) Obtener los datos de las etapas que pasan por algún puerto de montaña y que tienen salida y llegada en la misma población


SELECT DISTINCT
  numetapa
FROM puerto;


SELECT
  *
FROM etapa
WHERE numetapa IN (SELECT DISTINCT
    numetapa
  FROM puerto)
AND salida = llegada;

-- david

SELECT
  *
FROM (SELECT DISTINCT
    numetapa
  FROM puerto) c1
  JOIN etapa USING (numetapa)
WHERE salida = llegada;


--  (c) Obtener el nombre y el equipo de los ciclistas que han ganado alguna etapa llevando el maillot amarillo, mostrando también el número de etapa     


--  c1 



SELECT código FROM maillot WHERE color='amarillo';

-- C2

SELECT numetapa,
       dorsal FROM etapa;

-- c3

--   ganadores que han llevado el dorsal amarillo 
SELECT DISTINCT dorsal, numetapa FROM lleva JOIN (SELECT código FROM maillot WHERE color='amarillo') c1 USING(código);




--  dorsales que han llevada el maillot amarillo y han ganado alguna etapa
  SELECT DISTINCT c1.dorsal FROM (SELECT DISTINCT dorsal,numetapa FROM lleva JOIN (SELECT código FROM maillot WHERE color='amarillo') c1 USING(código)) c1 JOIN (SELECT numetapa,
       dorsal FROM etapa)c2 ON c2.dorsal=c1.dorsal AND c2.numetapa=c1.numetapa;


-- final

  SELECT DISTINCT ciclista.nombre, ciclista.nomequipo, c1.numetapa FROM ciclista JOIN (SELECT DISTINCT c1.dorsal,c1.numetapa FROM (SELECT DISTINCT dorsal,numetapa FROM lleva JOIN (SELECT código FROM maillot WHERE color='amarillo') c1 USING(código)) c1 JOIN (SELECT numetapa,
       dorsal FROM etapa)c2 ON c2.dorsal=c1.dorsal AND c2.numetapa=c1.numetapa) c1 USING(dorsal);




--  (e) Obtener el número de las etapas que tienen algún puerto de montaña, indicando cuántos tiene cada una de ellas

SELECT
  numetapa,
  COUNT(*)
FROM puerto
GROUP BY numetapa;

--  (g) Obtener el nombre y el equipo de los ciclistas que han llevado algún maillot o que han ganado algún puerto ???????????????????????????


-- c1


SELECT  DISTINCT dorsal FROM lleva
  union
SELECT  DISTINCT dorsal FROM puerto;


SELECT 
       nombre,
       nomequipo FROM (SELECT  dorsal FROM lleva
  union
SELECT  dorsal FROM puerto)c1 JOIN ciclista USING(dorsal);



  --  (k) Obtener la edad media de los ciclistas que han ganado alguna etapa


  SELECT AVG(edad) FROM ciclista JOIN (SELECT DISTINCT dorsal FROM etapa) c1 ON ciclista.dorsal = c1.dorsal;

  -- david
    SELECT AVG(edad) FROM (SELECT DISTINCT dorsal FROM etapa) c1 JOIN  ciclista using(dorsal);


 --   (03) Cuántos puertos ha ganado Induráin llevando algún maillot

  SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin';


  SELECT c1.dorsal FROM (SELECT DISTINCT dorsal, numetapa
                        
                         FROM lleva) c1 JOIN (SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin') c2 USING (dorsal);


  SELECT COUNT(*) FROM (SELECT c1.dorsal, c1.numetapa FROM (SELECT DISTINCT dorsal, numetapa
                        
                         FROM lleva) c1 JOIN (SELECT dorsal FROM ciclista WHERE nombre='Miguel Induráin') c2 USING (dorsal)) c1 JOIN puerto USING(dorsal,numetapa);


  --  david

    SELECT COUNT(*) FROM (select dorsal from ciclista where nombre='miguel induráin') c1 join puerto using(dorsal) join lleva using (dorsal,numetapa);


    --  (d) Obtener los datos de las etapas que no comienzan en la misma ciudad en que acaba la etapa anterior




SELECT e1.* FROM etapa e1 JOIN etapa e2 ON e1.numetapa-1 = e2.numetapa AND e2.llegada<>e1.salida;




select nombre, nomequipo, etapa.numetapa FROM
 (select código from maillot where color='amarillo')c1 JOIN lleva using(código) join etapa 
on etapa.dorsal=lleva.dorsal and etapa.numetapa=lleva.numetapa join ciclista on etapa.dorsal=ciclista.dorsal;