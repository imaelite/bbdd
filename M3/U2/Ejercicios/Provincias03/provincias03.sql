﻿-- Consulta 1 ¿Qué autonomías tienen nombre simple? Ordena el resultado alfabéticamente en orden inverso

SELECT DISTINCT
  autonomia
FROM provincias
WHERE autonomia NOT LIKE '% %'
ORDER BY autonomia DESC;

-- Consulta 2 ¿Qué autonomías tienen provincias con nombre compuesto? Ordenar el resultado alfabéticamente

SELECT DISTINCT
  autonomia
FROM provincias
WHERE provincia LIKE '% %'
ORDER BY autonomia;


-- Consulta 3 Listado de provincias y autonomías que contengan la letra ñ

SELECT
  provincia
FROM provincias
WHERE provincia LIKE '%ñ%' COLLATE utf8_bin
UNION
SELECT
  autonomia
FROM provincias
WHERE autonomia LIKE '%ñ%' COLLATE utf8_bin;

-- Consulta 4 (22) Superficie del país

SELECT
  SUM(superficie)
FROM provincias;


-- Consulta 5 En un listado alfabético, ¿qué provincia estaría la primera?


SELECT
  provincia
FROM provincias
ORDER BY provincia LIMIT 1;

SELECT
  MIN(provincia)
FROM provincias;


-- Consulta 6 ¿Qué provincias tienen un nombre más largo que el de su autonomía?


SELECT
  provincia
FROM provincias
WHERE CHAR_LENGTH(provincia) > CHAR_LENGTH(autonomia);


-- Consulta 7 ¿Cuántas comunidades autónomas hay?


SELECT
  COUNT(DISTINCT autonomia)
FROM provincias;


-- Consulta 8 (29) ¿Cuánto mide el nombre de autonomía más corto?

SELECT
  MIN(CHAR_LENGTH(autonomia))
FROM provincias;


SELECT DISTINCT
  provincia,
  CHAR_LENGTH(provincia) AS longitud
FROM provincias
ORDER BY longitud LIMIT 1;


SELECT
  MIN(longitud)
FROM (SELECT DISTINCT
    provincia,
    CHAR_LENGTH(autonomia) AS longitud
  FROM provincias) c1;


-- Consulta 9 (30) ¿Cuánto mide el nombre de provincia más largo?


SELECT
  MAX(CHAR_LENGTH(provincia))
FROM provincias;


-- Consultas 10 (28) Población media de las provincias entre 2 y 3 millones de habitantes sin decimales

SELECT
  ROUND(AVG(poblacion))
FROM provincias
WHERE poblacion BETWEEN 2000000 AND 3000000;

SELECT
  ROUND(AVG(poblacion))
FROM provincias
WHERE poblacion BETWEEN 2e6 AND 3e6;

