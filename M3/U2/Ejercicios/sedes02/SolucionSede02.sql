﻿
-- (02) Indicar el nombre de las ciudades que tengan una población por encima de la población media

SELECT
  nombre
FROM ciudad
WHERE población > (SELECT
    AVG(población)
  FROM ciudad);


--  (03) Indicar el nombre de las ciudades que tengan una población por debajo de la población media

SELECT
  nombre
FROM ciudad
WHERE población < (SELECT
    AVG(población)
  FROM ciudad);

--  (04) Indicar el nombre de la ciudad con la población máxima

SELECT
  nombre
FROM ciudad
WHERE población = (SELECT
    MAX(población)
  FROM ciudad);

--  (05) Indicar el nombre de la ciudad con la población mínima
SELECT
  nombre
FROM ciudad
WHERE población = (SELECT
    MIN(población)
  FROM ciudad);

--  (16) Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos. Ordena por el nombre del supervisado
--                          
SELECT

  persona,
    supervisor,
  p1.ciudad, p2.ciudad
FROM supervisa
  JOIN persona p1
    ON persona = p1.nombre 
  JOIN persona p2 ON supervisor = p2.nombre ORDER BY persona
;



--  (6) Indicar el número de ciudades que tengan una población por encima de la población media

SELECT
  COUNT(*)
FROM ciudad
WHERE población > (SELECT
    AVG(población)
  FROM ciudad);

--  (07) Indicarme el número de personas (de la tabla personas) que viven en cada ciudad. No se refiere a la población total de la ciudad.

SELECT
  ciudad,
  COUNT(*) n
FROM persona
GROUP BY ciudad;

--  (08) Utilizando la tabla trabaja, indicar cuantas personas trabajan en cada una de las compañías
SELECT
  compañia,
  COUNT(*) n
FROM trabaja
GROUP BY compañia;