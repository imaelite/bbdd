﻿-- 1 (41) Autonomías uniprovinciales

SELECT
  autonomia,
  COUNT(provincia) AS n
FROM provincias
GROUP BY autonomia; -- c1

SELECT
  c1.autonomia
FROM (SELECT
    autonomia,
    COUNT(provincia) AS n
  FROM provincias
  GROUP BY autonomia) c1
WHERE c1.n = 1;

-- -----------------

SELECT
  autonomia
FROM provincias
GROUP BY autonomia
HAVING COUNT(*) = 1;




-- 2  (42) ¿Qué autonomía tiene 5 provincias?

SELECT
  autonomia
FROM provincias
GROUP BY autonomia
HAVING COUNT(*) = 5;

-- 3 (43) Población de la autonomía más poblada

SELECT
  autonomia,

  SUM(poblacion) AS suma
FROM provincias
GROUP BY autonomia;-- c1


SELECT
  c1.suma
FROM (SELECT
    autonomia,
    SUM(poblacion) AS suma
  FROM provincias
  GROUP BY autonomia) c1
ORDER BY c1.suma DESC LIMIT 1;

-- ------------------
SELECT
  MAX(pob)
FROM (SELECT
    autonomia,
    SUM(poblacion) AS pob
  FROM provincias
  GROUP BY autonomia) c1;



-- 4 (44) ¿Qué porcentaje del total nacional representa Cantabria en población y en superficie?????????????????????????

SELECT
  SUM(poblacion) pob_total,
  SUM(superficie) sup_total
FROM provincias;-- c1

SELECT
  c1.pob_total,
  c1.sup_total
FROM (SELECT
    SUM(poblacion) pob_total,
    SUM(superficie) sup_total
  FROM provincias) c1;

SELECT poblacion/(SELECT SUM(poblacion) FROM provincias)*100,
  superficie/(SELECT SUM(superficie) FROM provincias)*100
 FROM provincias WHERE provincia='cantabria';

-- 5 (45) Automía más extensa 

SELECT
  SUM(superficie),
  autonomia
FROM provincias
GROUP BY autonomia; -- c1

SELECT
  autonomia
FROM (SELECT
    autonomia,
    SUM(superficie) sup
  FROM provincias
  GROUP BY autonomia) c1
WHERE sup = (SELECT
    MAX(sup)
  FROM (SELECT
      autonomia,
      SUM(superficie) sup

    FROM provincias
    GROUP BY autonomia) c1);


-- (47) ¿En qué posición del ranking autonómico por población de mayor a menor está Cantabria?

-- poblacion de cantabria

SELECT
  poblacion
FROM provincias
WHERE provincia = 'cantabria';

SELECT
  COUNT(*) + 1
FROM provincias
WHERE poblacion > (SELECT
    poblacion
  FROM provincias
  WHERE provincia = 'cantabria');


--  (48) Provincia más despoblada de la autonomía más poblada

-- provincias de la autonomia mas poblada
SELECT 
       provincia,
       poblacion
        FROM provincias WHERE autonomia=(SELECT
  c1.autonomia
FROM (SELECT
    autonomia,
    SUM(poblacion) AS suma
  FROM provincias
  GROUP BY autonomia) c1
ORDER BY c1.suma DESC LIMIT 1)

;
-- poblacion maxima
SELECT MAX(c1.poblacion) FROM (SELECT 
       provincia,
       poblacion
        FROM provincias WHERE autonomia=(SELECT
  c1.autonomia
FROM (SELECT
    autonomia,
    SUM(poblacion) AS suma
  FROM provincias
  GROUP BY autonomia) c1
ORDER BY c1.suma DESC LIMIT 1)) c1;

SELECT 
       provincia FROM provincias WHERE poblacion=(SELECT MIN(c1.poblacion) FROM (SELECT 
       provincia,
       poblacion
        FROM provincias WHERE autonomia=(SELECT
  c1.autonomia
FROM (SELECT
    autonomia,
    SUM(poblacion) AS suma
  FROM provincias
  GROUP BY autonomia) c1
ORDER BY c1.suma DESC LIMIT 1)) c1);









  SELECT
  c1.autonomia
FROM (SELECT
    autonomia,
    SUM(poblacion) AS suma
  FROM provincias
  GROUP BY autonomia) c1
ORDER BY c1.suma DESC LIMIT 1;


  --  (46) Autonomía con más provincias. Utiliza como alias n y c1.

SELECT autonomia,COUNT(*) n FROM provincias GROUP BY autonomia;

-- (47) ¿En qué posición del ranking autonómico por población de mayor a menor está Cantabria?

  SELECT autonomia,SUM(poblacion) n FROM provincias GROUP BY autonomia ORDER BY n DESC;


  --  (44) ¿Qué porcentaje del total nacional representa Cantabria en población y en superficie?


  SELECT autonomia,SUM(poblacion) np, SUM(superficie) s FROM provincias GROUP BY autonomia;

    SELECT SUM(poblacion) n, SUM(superficie) s FROM provincias;

      SELECT autonomia,SUM(poblacion) p, SUM(superficie) s FROM provincias WHERE autonomia='cantabria';


      SELECT (c1.n*c2.p/100) FROM ( SELECT SUM(poblacion) n, SUM(superficie) s FROM provincias) c1,
        (SELECT autonomia,SUM(poblacion) p, SUM(superficie) s FROM provincias WHERE autonomia='cantabria') c2;



      select provincia from provincias where poblacion=
      (select min(poblacion) from provincias where autonomia=
      (select autonomia from provincias group by autonomia having sum(poblacion)=
      (select max(habs) from (select autonomia, sum(poblacion) habs from provincias group by autonomia) c1) ) ) 

      and autonomia= (select autonomia from provincias group by autonomia having sum(poblacion)= 

      (select max(habs) from (select autonomia, sum(poblacion) habs from provincias group by autonomia) c1 ));