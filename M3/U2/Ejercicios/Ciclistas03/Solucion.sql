﻿--  (01) En cuántas etapas ha llevado maillot Induráin con puerto y además las ha ganado


SELECT dorsal
       FROM ciclista WHERE nombre='Miguel Induráin';


SELECT DISTINCT
       etapa.numetapa, etapa.dorsal FROM puerto JOIN etapa USING(numetapa);

SELECT * FROM (SELECT dorsal
       FROM ciclista WHERE nombre='Miguel Induráin')c1 JOIN (SELECT DISTINCT
       etapa.numetapa, etapa.dorsal FROM puerto JOIN etapa USING(numetapa)) c2 USING(dorsal);


--  david
  select count(*) FROM (select dorsal from ciclista where nombre='miguel indurain')c1 join (select distinct numetapa,dorsal from etapa
  join(select numetapa from puerto)p using(numetapa)) c2 using(dorsal) join lleva on c2.dorsal=lleva.dorsal and c2.numetapa=lleva.numetapa;

--  (06) Código de los maillots que han sido llevados por más de un equipo


  --  (o) Obtener los datos de las etapas cuyos puertos (todos) superan los 1300 metros de altura


  --  etapas y sus puertos
  SELECT 
         numetapa, COUNT(*)
          FROM puerto GROUP BY numetapa;


--  puertos de más de 1300 metros

  SELECT numetapa,COUNT(*) n FROM puerto WHERE altura >1300 GROUP BY numetapa;




  SELECT c1.numetapa FROM ( SELECT 
         numetapa, COUNT(*) n
          FROM puerto GROUP BY numetapa)c1 JOIN (SELECT numetapa,COUNT(*) n FROM puerto WHERE altura >1300 GROUP BY numetapa)c2
    USING(numetapa) WHERE c1.n=c2.n;



  SELECT * FROM etapa JOIN ( SELECT c1.numetapa FROM ( SELECT 
         numetapa, COUNT(*) n
          FROM puerto GROUP BY numetapa)c1 JOIN (SELECT numetapa,COUNT(*) n FROM puerto WHERE altura >1300 GROUP BY numetapa)c2
    USING(numetapa) WHERE c1.n=c2.n) c1 USING(numetapa);




  --  (05) Nombre de los ciclistas que han ganado todos los puertos de alguna etapa

    SELECT
           dorsal,numetapa, nompuerto
            FROM puerto ;



  --  (h) Obtener los datos de los ciclistas que han vestido todos los maillots (no necesariamente en la misma etapa)


    SELECT DISTINCT
           código FROM lleva;

    SELECT dorsal, código FROM lleva GROUP BY dorsal;

    --  (04) Cuántas etapas sin puerto ha ganado Induráin llevando algún maillot


      SELECT dorsal
       FROM ciclista WHERE nombre='Miguel Induráin';


      SELECT DISTINCT
             numetapa
              FROM puerto;

      SELECT DISTINCT numetapa, dorsal FROM etapa LEFT JOIN (SELECT DISTINCT
             numetapa
              FROM puerto)c1 USING(numetapa) WHERE c1.numetapa IS NULL;


      SELECT DISTINCT
             lleva.dorsal,
             numetapa FROM lleva JOIN (SELECT dorsal
       FROM ciclista WHERE nombre='Miguel Induráin')c1 USING(dorsal);


      SELECT count(*) FROM (SELECT DISTINCT numetapa, dorsal FROM etapa LEFT JOIN (SELECT DISTINCT
             numetapa
              FROM puerto)c1 USING(numetapa) WHERE c1.numetapa IS NULL) c1 JOIN (SELECT DISTINCT
             lleva.dorsal,
             numetapa FROM lleva JOIN (SELECT dorsal
       FROM ciclista WHERE nombre='Miguel Induráin')c1 USING(dorsal)) c2 ON c1.numetapa=c2.numetapa AND c1.dorsal=c2.dorsal;

      --  david
        select count(DISTINCT dorsal,numetapa) from (select dorsal from ciclista where nombre='miguel induráin') c1 join ( select * from ( select numetapa, dorsal from etapa) c01 left join ( select distinct numetapa from puerto ) c02 using(numetapa) where c02.numetapa is null ) c2 using(dorsal) join lleva using(numetapa,dorsal);



--  (p) Obtener el nombre de los ciclistas que pertenecen a un equipo de más de cinco ciclistas y que han ganado alguna etapa, indicando también cuántas etapas han ganado
  

  SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>5;


  SELECT dorsal FROM ciclista WHERE nomequipo IN (SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>5);


  SELECT c1.nombre, COUNT(etapa.numetapa) FROM etapa JOIN (SELECT dorsal, nombre FROM ciclista WHERE nomequipo IN (SELECT nomequipo FROM ciclista GROUP BY nomequipo HAVING COUNT(*)>5))c1 USING(dorsal) GROUP BY c1.nombre;

  --  dorsal

    select nombre, n from(select dorsal,nombre from (select nomequipo from ciclista group by nomequipo having count(*)>5) c1 join ciclista using(nomequipo)) c2 join(select dorsal, count(*) n from etapa group by dorsal) c3 using(dorsal) order by nombre;



    --  (06) Código de los maillots que han sido llevados por más de un equipo

      SELECT DISTINCT
             nomequipo, dorsal FROM ciclista;

      SELECT código, COUNT(DISTINCT c1.nomequipo) FROM (SELECT DISTINCT
             nomequipo, dorsal FROM ciclista) c1 JOIN lleva USING(dorsal) GROUP BY código;


      --  david

        select código from (select distinct código, nomequipo from lleva join ciclista using(dorsal)) c2 group by código having count(*)>1;


        --  (h) Obtener los datos de los ciclistas que han vestido todos los maillots (no necesariamente en la misma etapa)

          SELECT   dorsal FROM lleva GROUP BY dorsal HAVING COUNT(DISTINCT código)=6;

  --  david

    select * from (select dorsal from (select distinct dorsal, código from lleva) c1 group by dorsal having count(*)=(select count(*) from maillot)) c4 join ciclista using(dorsal);
  

    --  (05) Nombre de los ciclistas que han ganado todos los puertos de alguna etapa     26,1,2

      SELECT 
             numetapa, nompuerto, dorsal
             FROM puerto ORDER BY numetapa;

      SELECT 
             nombre FROM ciclista WHERE dorsal IN (26,1,42,2) ORDER BY nombre;

      --  debo
        select nombre from(select distinct dorsal from ( select numetapa, count(*) n from puerto group by numetapa) c1 join (select dorsal, numetapa, count(*) n from puerto group by dorsal, numetapa)c2 using(n,numetapa))c3 join ciclista using(dorsal) order by nombre;



        --  (i) Obtener el código y el color de aquellos maillots que sólo han sido llevados por ciclistas de un mismo equipo


          SELECT  código, color FROM maillot;

          --  david
            select c3.código, color from (select código from (select distinct código, nomequipo from lleva join ciclista using(dorsal))c1 group by código having count(*)=1) c3 join maillot using(código);



         
                 
              --  (o2) Obtener los datos de las etapas cuyos puertos (todos) superan los 1300 metros de altura

                SELECT nompuerto, numetapa FROM puerto WHERE altura > 1300;


                SELECT nompuerto,
                       numetapa
                        FROM puerto;

           -- 2 11 19

            SELECT * FROM etapa WHERE numetapa IN (2,11,19);


