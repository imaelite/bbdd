﻿--  (4.10.28) ¿Cuál es la descripción de la gama del producto más rechazado?


SELECT
  CodigoPedido
FROM pedidos
WHERE Estado = 'rechazado';


SELECT
  CodigoProducto
FROM detallepedidos
WHERE CodigoPedido IN (SELECT
    CodigoPedido
  FROM pedidos
  WHERE Estado = 'rechazado');



SELECT
  Gama,
  COUNT(*) n,
  Descripcion
FROM productos
WHERE CodigoProducto IN (SELECT
    CodigoProducto
  FROM detallepedidos
  WHERE CodigoPedido IN (SELECT
      CodigoPedido
    FROM pedidos
    WHERE Estado = 'rechazado'))
GROUP BY Gama
ORDER BY n DESC LIMIT 1;


SELECT
  *
FROM productos
WHERE Gama = 'Ornamentales';

SELECT
  *
FROM gamasproductos;


SELECT
  descripciontexto
FROM (SELECT
    codigoproducto
  FROM (SELECT
      codigopedido
    FROM pedidos
    WHERE estado = 'rechazado') c1
    JOIN detallepedidos USING (codigopedido)
  GROUP BY codigoproducto
  HAVING SUM(cantidad) = (SELECT
      MAX(s)
    FROM (SELECT
        codigoproducto,
        SUM(cantidad) s
      FROM (SELECT
          codigopedido
        FROM pedidos
        WHERE estado = 'rechazado') c1
        JOIN detallepedidos USING (codigopedido)
      GROUP BY codigoproducto) c2)) c4
  JOIN productos USING (codigoproducto)
  JOIN gamasproductos USING (gama);



--  (4.10.34) Nombre del cliente que más frutales ha pedido

SELECT DISTINCT
  NombreCliente
FROM clientes;



SELECT
  nombrecliente
FROM (SELECT
    codigocliente
  FROM (SELECT
      codigoproducto
    FROM productos
    WHERE gama = 'frutales') c1
    JOIN detallepedidos USING (codigoproducto)
    JOIN pedidos USING (codigopedido)
  GROUP BY codigocliente
  HAVING SUM(cantidad) = (SELECT
      MAX(s)
    FROM (SELECT
        codigocliente,
        SUM(cantidad) s
      FROM (SELECT
          codigoproducto
        FROM productos
        WHERE gama = 'Frutales') c1
        JOIN detallepedidos USING (codigoproducto)
        JOIN pedidos USING (codigopedido)
      GROUP BY codigocliente) c2)) c3
  JOIN clientes USING (codigocliente);



SELECT
  *
FROM gamasproductos;

SELECT
  descripciontexto
FROM (SELECT
    gama
  FROM (SELECT
      codigopedido
    FROM pedidos
    WHERE estado = 'rechazado') c1
    JOIN detallepedidos USING (codigopedido)
    JOIN productos USING (codigoproducto)
  GROUP BY gama
  HAVING SUM(cantidad) = (SELECT
      MAX(s)
    FROM (SELECT
        gama,
        SUM(cantidad) s
      FROM (SELECT
          codigopedido
        FROM pedidos
        WHERE estado = 'rechazado') c1
        JOIN detallepedidos USING (codigopedido)
        JOIN productos USING (codigoproducto)
      GROUP BY gama) c2)) c4
  JOIN gamasproductos USING (gama);


SELECT DISTINCT
  Ciudad
FROM oficinas;

    select ciudad FROM( ( select codigooficina, ciudad from oficinas) c1 join ( select codigooficina, codigoempleado from empleados) c2 using(codigooficina) join ( select codigocliente, codigoempleadorepventas from clientes ) c3 on codigoempleadorepventas = codigoempleado join (select codigopedido, codigocliente from pedidos) c4 using (codigocliente) join (select codigopedido, contidad, preciounidad from detallepedidos) c5 using(codigopedido) group by ciudad, codigooficina having sum(cantidad*preciounidad)=(select max(s) from ( select codigooficina, ciudad from oficinas) c1 join (select codigooficina, ciudad from oficinas) c2 join ( select codigooficina, codigoempleado from empleados) c3 using(codigooficina) join (select codigocliente, codigoempleadorepventas from clientes ) c4 on codigoempleadorepventas= codigoempleado join (select codigopedido, codigocliente from pedidos) c5 using(codigocliente) join (select codigopedido, cantidad, preciounidad from detallepedidos) c6 using(codigopedido) group by codigooficina) ))c6 ;


SELECT
  *
FROM clientes;



--  (4.10.25) ¿Con qué cliente la empresa gana más dinero? (Sin excluir los pedidos rechazados)


--  (4.10.25) ¿Con qué cliente la empresa gana más dinero? (Sin excluir los pedidos rechazados)

SELECT
  *
FROM (SELECT
    codigocliente
  FROM (SELECT
      codigocliente,
      MAX(fechapago) paga
    FROM pagos
    GROUP BY codigocliente) c1
    JOIN (SELECT
        codigocliente,
        MAX(fechaentrega) compra
      FROM pedidos
      GROUP BY codigocliente) c2 USING (codigocliente)
  WHERE DATEDIFF(paga, compra) = (SELECT
      MIN(retardo)
    FROM (SELECT
        codigocliente,
        DATEDIFF(paga, compra) retardo
      FROM (SELECT
          codigocliente,
          MAX(fechapago) paga
        FROM pagos
        GROUP BY codigocliente) c1
        JOIN (SELECT
            codigocliente,
            MAX(fechaentrega) compra
          FROM pedidos
          GROUP BY codigocliente) c2 USING (codigocliente)) c3)) c5
  JOIN clientes USING (codigocliente);



--  

SELECT
  NombreCliente,
  CodigoCliente
FROM clientes;

SELECT DISTINCT
  Nombre,
  Apellido1,
  Apellido2
FROM empleados;


--  (4.10.32) Ciudad de la oficina que más frutales vende

SELECT DISTINCT
  Ciudad
FROM oficinas;


  
SELECT e2.Nombre,e2.Apellido1,e2.Apellido2 FROM empleados e1 JOIN ( SELECT CodigoEmpleadoRepVentas CodigoEmpleado FROM ( SELECT CodigoCliente,SUM(Cantidad*PrecioUnidad) total FROM ( SELECT CodigoCliente,CodigoPedido FROM pedidos WHERE Estado='entregado' ) pedidos JOIN detallepedidos USING(CodigoPedido) GROUP BY 1 ) c1 JOIN ( SELECT CodigoCliente,CodigoEmpleadoRepVentas FROM clientes ) clientes USING(CodigoCliente) GROUP BY 1 HAVING SUM(total)=( SELECT MAX(total) FROM ( SELECT CodigoEmpleadoRepVentas CodigoEmpleado,SUM(total) total FROM ( SELECT CodigoCliente,SUM(Cantidad*PrecioUnidad) total FROM ( SELECT CodigoCliente,CodigoPedido FROM pedidos WHERE Estado='entregado' ) pedidos JOIN detallepedidos USING(CodigoPedido) GROUP BY 1 ) c1 JOIN ( SELECT CodigoCliente,CodigoEmpleadoRepVentas FROM clientes ) clientes USING(CodigoCliente) GROUP BY 1 ) c2 ) ) c3 USING(CodigoEmpleado) JOIN empleados e2 ON e1.CodigoJefe=e2.CodigoEmpleado;




SELECT DISTINCT NombreCliente,CodigoCliente FROM ( SELECT CodigoProducto FROM ( SELECT CodigoProducto,PrecioVenta,PrecioProveedor FROM productos ) productos WHERE PrecioVenta-PrecioProveedor=( SELECT MIN(PrecioVenta-PrecioProveedor) FROM productos ) ) c1 JOIN ( SELECT CodigoProducto,CodigoPedido FROM detallepedidos ) detallepedidos USING(CodigoProducto) JOIN ( SELECT CodigoPedido,CodigoCliente FROM pedidos ) pedidos USING(CodigoPedido) JOIN ( SELECT CodigoCliente,NombreCliente FROM clientes ) clientes USING(CodigoCliente) ORDER BY NombreCliente;



SELECT Ciudad FROM ( SELECT CodigoProducto FROM productos WHERE Gama='Frutales') c1 JOIN ( SELECT CodigoProducto,CodigoPedido,Cantidad FROM detallepedidos ) c2 USING(CodigoProducto) JOIN ( SELECT CodigoPedido,CodigoCliente FROM pedidos ) c3 USING(CodigoPedido) JOIN ( SELECT CodigoCliente,CodigoEmpleadoRepVentas CodigoEmpleado FROM clientes ) c4 USING(CodigoCliente) JOIN ( SELECT CodigoEmpleado,CodigoOficina FROM empleados ) c5 USING(CodigoEmpleado) JOIN ( SELECT CodigoOficina,Ciudad FROM oficinas ) c6 USING(CodigoOficina) GROUP BY Ciudad,CodigoOficina HAVING SUM(Cantidad)=( SELECT MAX(s) FROM ( SELECT CodigoOficina,SUM(Cantidad) s FROM ( SELECT CodigoProducto FROM productos WHERE Gama='Frutales') c1 JOIN ( SELECT CodigoProducto,CodigoPedido,Cantidad FROM detallepedidos ) c2 USING(CodigoProducto) JOIN ( SELECT CodigoPedido,CodigoCliente FROM pedidos ) c3 USING(CodigoPedido) JOIN ( SELECT CodigoCliente, CodigoEmpleadoRepVentas CodigoEmpleado FROM clientes ) c4 USING(CodigoCliente) JOIN ( SELECT CodigoEmpleado,CodigoOficina FROM empleados ) c5 USING(CodigoEmpleado) JOIN ( SELECT CodigoOficina,Ciudad FROM oficinas ) c6 USING(CodigoOficina) GROUP BY CodigoOficina ) c2 );


SELECT CodigoOficina, IFNULL(n_local,0) n_local, n_total-IFNULL(n_local,0) n_foraneos FROM ( SELECT CodigoOficina,COUNT(*) n_total FROM oficinas JOIN empleados USING(CodigoOficina) JOIN clientes ON empleados.CodigoEmpleado=clientes.CodigoEmpleadoRepVentas GROUP BY CodigoOficina ) c2 LEFT JOIN ( SELECT CodigoOficina,COUNT(*) n_local FROM oficinas JOIN empleados USING(CodigoOficina) JOIN clientes ON empleados.CodigoEmpleado=clientes.CodigoEmpleadoRepVentas WHERE oficinas.Ciudad=clientes.Ciudad GROUP BY CodigoOficina ) c1 USING(CodigoOficina);


SELECT CodigoProducto,Nombre FROM ( SELECT CodigoProducto FROM ( SELECT CodigoPedido FROM pedidos WHERE Estado='Rechazado') c1 JOIN detallepedidos USING(CodigoPedido) GROUP BY CodigoProducto HAVING SUM(Cantidad)=( SELECT MAX(s) FROM ( SELECT CodigoProducto,SUM(Cantidad) s FROM ( SELECT CodigoPedido FROM pedidos WHERE Estado='Rechazado') c1 JOIN detallepedidos USING(CodigoPedido) GROUP BY CodigoProducto ) c2 ) ) c3 JOIN productos USING(CodigoProducto);


SELECT SUM(Cantidad*PrecioUnidad) FROM ( SELECT CodigoProducto FROM ( SELECT Gama FROM detallepedidos JOIN productos USING(CodigoProducto) GROUP BY Gama HAVING SUM(Cantidad)=( SELECT MAX(s) FROM ( SELECT Gama,SUM(Cantidad) s FROM detallepedidos JOIN productos USING(CodigoProducto) GROUP BY Gama ) c1 ) ) c2 JOIN productos USING(Gama) ) c3 JOIN detallepedidos USING(CodigoProducto);



SELECT * FROM ( SELECT CodigoCliente,NombreCliente,total,pago,LimiteCredito limite FROM ( SELECT CodigoCliente,SUM(total) total FROM ( SELECT CodigoPedido,SUM(Cantidad*PrecioUnidad) total FROM detallepedidos GROUP BY CodigoPedido ) c1 JOIN pedidos USING(CodigoPedido) GROUP BY CodigoCliente ) c2 JOIN clientes USING(CodigoCliente) JOIN ( SELECT CodigoCliente,SUM(Cantidad) pago FROM pagos GROUP BY CodigoCliente ) c3 USING(CodigoCliente) ) c3 WHERE total-pago>limite;


SELECT CodigoProducto,Nombre FROM ( SELECT CodigoProducto FROM ( SELECT CodigoPedido FROM pedidos WHERE Estado='Rechazado') c1 JOIN detallepedidos USING(CodigoPedido) GROUP BY CodigoProducto HAVING SUM(Cantidad*PrecioUnidad)=( SELECT MAX(s) FROM ( SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad) s FROM ( SELECT CodigoPedido FROM pedidos WHERE Estado='Rechazado') c1 JOIN detallepedidos USING(CodigoPedido) GROUP BY CodigoProducto ) c2 ) ) c3 JOIN productos USING(CodigoProducto);

SELECT DISTINCT NombreCliente,CodigoCliente FROM ( SELECT CodigoProducto FROM productos WHERE PrecioVenta-PrecioProveedor=( SELECT MIN(PrecioVenta-PrecioProveedor) FROM productos ) ) c1 JOIN detallepedidos USING(CodigoProducto) JOIN pedidos USING(CodigoPedido) JOIN clientes USING(CodigoCliente) ORDER BY NombreCliente;

SELECT DescripcionTexto FROM ( SELECT CodigoProducto FROM detallepedidos GROUP BY CodigoProducto HAVING SUM(Cantidad*PrecioUnidad)=( SELECT MAX(s) FROM ( SELECT CodigoProducto,SUM(Cantidad*PrecioUnidad) s FROM detallepedidos GROUP BY CodigoProducto ) c1 ) ) c2 JOIN productos USING(CodigoProducto) JOIN gamasproductos USING(Gama);

SELECT CodigoProducto FROM ( SELECT CodigoProducto FROM productos WHERE Gama='Frutales') c1 JOIN detallepedidos USING(CodigoProducto) GROUP BY CodigoProducto HAVING SUM(Cantidad)=( SELECT MAX(s) FROM ( SELECT CodigoProducto,SUM(Cantidad) s FROM ( SELECT CodigoProducto FROM productos WHERE Gama='Frutales') c1 JOIN detallepedidos USING(CodigoProducto) GROUP BY CodigoProducto ) c2
);









