﻿-- (28d) ¿Qué comercios sólo distribuyen Windows? ??????

--  c1
SELECT
  codigo

FROM programa
WHERE nombre = 'Windows';

--  cif que venden windows
SELECT
  cif
FROM distribuye
  JOIN (SELECT
      codigo

    FROM programa
    WHERE nombre = 'Windows') c1
    ON distribuye.codigo = c1.codigo;



-- los programas que venden los de arriba

SELECT
  cif,
  codigo
FROM distribuye
WHERE cif IN (SELECT
    cif
  FROM distribuye
    JOIN (SELECT
        codigo
      FROM programa
      WHERE nombre = 'Windows') c1
      ON distribuye.codigo = c1.codigo);


-- cif que no venden windows

SELECT DISTINCT
  cif
FROM distribuye
WHERE codigo NOT IN (SELECT
    codigo

  FROM programa
  WHERE nombre = 'Windows');

-- c3



SELECT
  *
FROM (SELECT
    cif
  FROM distribuye
    JOIN (SELECT
        codigo

      FROM programa
      WHERE nombre = 'Windows') c1
      ON distribuye.codigo = c1.codigo) c1
  LEFT JOIN (SELECT DISTINCT
      cif
    FROM distribuye
    WHERE codigo NOT IN (SELECT
        codigo

      FROM programa
      WHERE nombre = 'Windows')) c2
    ON c1.cif = c2.cif;




--  (28f) ¿Qué comercios sólo distribuyen un programa?

SELECT
  cif,
  COUNT(*) n
FROM distribuye
GROUP BY cif;

SELECT
  *
FROM (SELECT
    cif,
    COUNT(*) n
  FROM distribuye
  GROUP BY cif) c1
WHERE c1.n = 1;


--  (44) Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente Pepe Pérez (Subconsulta)

-- c1 

SELECT
  dni
FROM cliente
WHERE nombre = 'Pepe Pérez';


-- c2 

SELECT
  medio
FROM registra
WHERE dni = (SELECT
    dni
  FROM cliente
  WHERE nombre = 'Pepe Pérez');

--  c3
SELECT DISTINCT
  dni
FROM registra
WHERE medio IN (SELECT
    medio
  FROM registra
  WHERE dni = (SELECT
      dni
    FROM cliente
    WHERE nombre = 'Pepe Pérez'))
AND dni NOT LIKE (SELECT
    dni
  FROM cliente
  WHERE nombre = 'Pepe Pérez');

--  c4
SELECT
  nombre
FROM (SELECT DISTINCT
    dni
  FROM registra
  WHERE medio IN (SELECT
      medio
    FROM registra
    WHERE dni = (SELECT
        dni
      FROM cliente
      WHERE nombre = 'Pepe Pérez'))
  AND dni NOT LIKE (SELECT
      dni
    FROM cliente
    WHERE nombre = 'Pepe Pérez')) c1
  JOIN cliente
    ON c1.dni = cliente.dni ORDER BY nombre;


--  (58) Listado de la cantidad de versiones de cada programa ordenados por el nombre del programa

  SELECT  nombre, COUNT(*) FROM programa GROUP BY nombre ORDER BY nombre;

  --  david
    select nombre,count(*) n from programa group by 1 order by 1;


    --  (59) Nombre de los programas que sólo dispongan de una única versión

        SELECT  nombre  FROM programa GROUP BY nombre HAVING COUNT(*)=1;

        --  (63) Nombres de comercios (no locales comerciales) que tengan exclusivas de distribución. Es decir, que distribuyan programas que sólo ellos distribuyen.


          SELECT  cif,
                  codigo
                   FROM distribuye;

   SELECT cif, nombre FROM comercio;

  --  (64) Ciudades donde se puedan adquirir programas distribuidos en exclusiva por los comercios (no los locales comerciales, aunque sí que hay que tener en cuenta el stock)

    SELECT * FROM comercio;


    --  david
     select distinct ciudad from (select programa from(select distinct programa.nombre programa,comercio.nombre comercio from programa join distribuye using(codigo) join comercio using(cif)) c1 group by 1 having count(*)=1) c2 join programa on programa=nombre join distribuye using(codigo) join comercio using(cif);


    --  (57) Nombre de los comercios que sólo distribuyen un único programa además de Windows

      SELECT codigo FROM programa WHERE nombre='Windows';


      SELECT * FROM distribuye WHERE codigo IN (SELECT codigo FROM programa WHERE nombre='Windows');
      SELECT * FROM distribuye;

      SELECT * FROM comercio;

      --  david

        select nombre from (select cif from distribuye join (select codigo from programa where nombre='windows')c1 using(codigo))c3 join ( select cif from (select distinct cif,nombre from programa join distribuye using(codigo))c2 group by 1 having count(*)=2)c4 using(cif) join comercio using(cif);



        --  (61) Nombre de los programas que se distribuyen en un único local, entendiendo como el mismo programa a todas sus versiones

          SELECT DISTINCT
                 nombre
                  FROM programa;

          SELECT codigo,COUNT( DISTINCT cif) n FROM distribuye GROUP BY codigo;

          SELECT * FROM programa JOIN (SELECT codigo,COUNT( DISTINCT cif) n FROM distribuye GROUP BY codigo)c1 USING(codigo) WHERE c1.n=1;

          SELECT DISTINCT nombre, programa.codigo FROM distribuye JOIN programa USING(codigo);

          SELECT c1.nombre, COUNT(DISTINCT cif) FROM distribuye JOIN (SELECT DISTINCT nombre, programa.codigo FROM distribuye JOIN programa USING(codigo)) c1 USING(codigo) GROUP BY c1.nombre;

          --  david
            
            select nombre from (select distinct cif, nombre from programa join distribuye using(codigo)) c1 group by 1 having count(*)=1;

            --  (60) Nombre de los locales que distribuyen únicamente 2 programas. Entendiendo por un único programa a todas sus versiones.

           --   Application Server
       -- Database
   -- Norton Internet Security
   -- Windows
              SELECT * FROM comercio;

                SELECT c1.nombre, COUNT( cif) FROM distribuye JOIN (SELECT DISTINCT nombre, programa.codigo FROM distribuye JOIN programa USING(codigo)) c1 USING(codigo) GROUP BY c1.nombre;


                --  (62) Nombre de los programas que se distribuyen en un único comercio, entendiendo como el mismo programa a todas sus versiones

  SELECT c1.nombre, cif FROM distribuye JOIN (SELECT DISTINCT nombre, programa.codigo FROM distribuye JOIN programa USING(codigo)) c1 USING(codigo);


  SELECT cif,
         nombre
          FROM comercio;


  SELECT * FROM (SELECT cif,
         nombre
          FROM comercio) c1 JOIN ( SELECT c1.nombre, cif FROM distribuye JOIN (SELECT DISTINCT nombre, programa.codigo FROM distribuye JOIN programa USING(codigo)) c1 USING(codigo)) c2 USING(cif);

  --  Application Server Database Application Server Freddy Hardest La prisión Paradox C++ Builder DB/2 JBuilder

    /**
Application Server
Database
Freddy Hardest
La prisión
Paradox
C++ Builder
DB/2 
JBuilder

    */

        /**
Application Server
Database
Freddy Hardest
La prisión
Paradox
C++ Builder
DB/2 
JBuilder

    */

    SELECT DISTINCT programa.nombre programa, comercio.nombre comercio  FROM programa JOIN distribuye USING(codigo) JOIN comercio USING(cif);

    SELECT c2.programa FROM (SELECT DISTINCT programa.nombre programa, comercio.nombre comercio  FROM programa JOIN distribuye USING(codigo) JOIN comercio USING(cif))c2 GROUP BY c2.programa HAVING COUNT(*)=1;



select programa from (select distinct p.nombre programa, c.nombre comercio from programa p join distribuye using(codigo) join comercio c using(cif)) c1 group by 1 having count(*)=1;