﻿--  (7) A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento cuyo oficio sea EMPLEADO (utilizar GROUP BY para agrupar por departamento.
  --  En la cláusula WHERE habrá que indicar que el oficio es EMPLEADO).

  select DISTINCT dept_no, COUNT(*) from emple where  oficio='EMPLEADO' GROUP BY dept_no;

  --  david
    select dept_no,count(*) from emple where oficio='empleado' group by dept_no;