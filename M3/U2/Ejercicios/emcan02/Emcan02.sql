﻿
--  (04) ¿Qué centro impartirá la mayor cantidad de cursos?

SELECT
  cod_centro,
  COUNT(*) n
FROM programacion
GROUP BY cod_centro
ORDER BY n DESC;

--  3900026767


SELECT
  centro
FROM centros
WHERE cod_centro = '3900026767';

--  david


--  (14) ¿De qué familia profesional han subvencionado más cursos?

SELECT
  *
FROM cursos;

SELECT
  *
FROM programacion;

SELECT
  SUBSTR(codigo, 1, 3),
  COUNT(*)
FROM programacion
GROUP BY 1;


--  (05) ¿Qué centro impartirá la mayor cantidad de cursos en Santander?

SELECT DISTINCT
  codigo

FROM inicios
WHERE municipio = 'Santander';
   SELECT * FROM centros WHERE cod_centro=();

SELECT
  cod_centro,
  COUNT(*) n
FROM programacion
WHERE cod_centro IN (SELECT DISTINCT
    cod_centro

  FROM centros
  WHERE municipio = 'Santander')
GROUP BY cod_centro
ORDER BY n DESC;


--  3900026705

SELECT
  *
FROM centros
WHERE cod_centro = '3900026705';


--  david

SELECT
  centro
FROM (SELECT
    cod_centro
  FROM (SELECT
      cod_centro
    FROM centros
    WHERE municipio = 'santander') c1
    JOIN programacion USING (cod_centro)
  GROUP BY 1
  HAVING COUNT(*) = (SELECT
      MAX(n)
    FROM (SELECT
        cod_centro,
        COUNT(*) n
      FROM (SELECT
          cod_centro
        FROM centros
        WHERE municipio = 'santander') c1
        JOIN programacion USING (cod_Centro)
      GROUP BY 1) c1)) c2
  JOIN centros USING (cod_centro);




--  (11) ¿Cuál es el curso de informática que más se impartirá?

SELECT
  especialidad,
  COUNT(*)
FROM cursos
WHERE codigo = 'IFCI17'
GROUP BY especialidad;

SELECT
  *
FROM cursos;


SELECT
  codigo,
  COUNT(*) n
FROM programacion
WHERE codigo IN (SELECT DISTINCT
    codigo
  FROM cursos
  WHERE codigo LIKE '%IFC%')
GROUP BY codigo
ORDER BY n DESC;


--  david
SELECT
  especialidad
FROM (SELECT
    codigo
  FROM (SELECT
      fml
    FROM familias
    WHERE familia LIKE '%informática%') c1
    JOIN programacion
      ON fml = SUBSTR(codigo, 1, 3)
  GROUP BY 1
  HAVING COUNT(*) = (SELECT
      MAX(n)
    FROM (SELECT
        codigo,
        COUNT(*) n
      FROM (SELECT
          fml
        FROM familias
        WHERE familia LIKE '%informática%') c1
        JOIN programacion
          ON fml = SUBSTR(codigo, 1, 3)
      GROUP BY 1) c2)) c3
  JOIN cursos USING (codigo);



--  (06) ¿Qué centro impartirá la mayor cantidad de cursos de informática en Cantabria?


SELECT
  cod_centro,
  COUNT(*) n
FROM programacion
WHERE codigo IN (SELECT DISTINCT
    codigo
  FROM cursos
  WHERE codigo LIKE '%IFC%')
GROUP BY cod_centro
ORDER BY n DESC;

--  3900026690

SELECT
  *
FROM centros
WHERE cod_centro = 3900026690;




--  (07) ¿Qué centro impartirá la mayor cantidad de cursos de informática en Santander?

SELECT
  programacion.cod_centro,
  COUNT(*) n
FROM programacion
WHERE codigo IN (SELECT DISTINCT
    codigo
  FROM cursos
  WHERE codigo LIKE '%IFC%')
GROUP BY programacion.cod_centro;

SELECT
  *
FROM centros;




SELECT
  centro,
  cod_centro
FROM centros
WHERE municipio = 'Santander';


SELECT
  *
FROM (SELECT
    centro,
    cod_centro
  FROM centros
  WHERE municipio = 'Santander') c1
  JOIN (SELECT
      programacion.cod_centro,
      COUNT(*) n
    FROM programacion
    WHERE codigo IN (SELECT DISTINCT
        codigo
      FROM cursos
      WHERE codigo LIKE '%IFC%')
    GROUP BY programacion.cod_centro) c2 USING (cod_centro);



SELECT
  *
FROM centros
WHERE cod_centro = '3900026623';


SELECT
  cod_centro,
  centro
FROM centros
WHERE municipio = 'Santander';


SELECT
  *
FROM programacion
  JOIN (SELECT
      fml
    FROM familias
    WHERE familia LIKE '%infor%') c1
    ON SUBSTR(codigo, 1, 3) = fml;

SELECT
  c1.centro,
  c1.cod_centro,
  COUNT(*) n
FROM (SELECT
    cod_centro,
    centro
  FROM centros
  WHERE municipio = 'Santander') c1
  JOIN (SELECT
      *
    FROM programacion
      JOIN (SELECT
          fml
        FROM familias
        WHERE familia LIKE '%infor%') c1
        ON SUBSTR(codigo, 1, 3) = fml) c2 USING (cod_centro)
GROUP BY c1.cod_centro
ORDER BY n DESC;


--  david
SELECT
  centro
FROM (SELECT
    cod_centro
  FROM (SELECT
      fml
    FROM familias
    WHERE familia LIKE '%informática%') c1
    JOIN programacion
      ON fml = SUBSTR(codigo, 1, 3)
    JOIN (SELECT
        cod_centro
      FROM centros) c2 USING (cod_centro)
  GROUP BY 1
  HAVING COUNT(*) = (SELECT
      MAX(n)
    FROM (SELECT
        cod_centro,
        COUNT(*) n
      FROM (SELECT
          fml
        FROM familias
        WHERE familia LIKE '%informática%') c1
        JOIN programacion
          ON fml = SUBSTR(codigo, 1, 3)
        JOIN (SELECT
            cod_centro
          FROM centros
          WHERE municipio = 'santander') c2 USING (cod_centro)
      GROUP BY 1) c3)) c4
  JOIN centros USING (cod_centro);


--  (08) ¿Qué centro impartirá la mayor cantidad de cursos de informática de nivel 3 en Cantabria?


SELECT
  centro
FROM (SELECT
    cod_centro
  FROM (SELECT
      fml
    FROM familias
    WHERE familia LIKE '%informática%') c1
    JOIN programacion
      ON fml = SUBSTR(codigo, 1, 3)
    JOIN (SELECT
        cod_centro
      FROM centros) c2 USING (cod_centro)
  GROUP BY 1
  HAVING COUNT(*) = (SELECT
      MAX(n)
    FROM (SELECT
        cod_centro,
        COUNT(*) n
      FROM (SELECT
          fml
        FROM familias
        WHERE familia LIKE '%informática%') c1
        JOIN programacion
          ON fml = SUBSTR(codigo, 1, 3)
        JOIN (SELECT
            cod_centro
          FROM centros
          WHERE municipio = 'santander') c2 USING (cod_centro)
      GROUP BY 1) c3)) c4
  JOIN centros USING (cod_centro);

--   SELECT * FROM 

SELECT
  *
FROM centros;
SELECT
  *
FROM cursos
WHERE nivel = 3
AND especialidad LIKE '%informática%';


--  david
SELECT
  centro
FROM (SELECT
    cod_centro
  FROM (SELECT
      fml
    FROM familias
    WHERE familia LIKE '%informática%') c1
    JOIN cursos
      ON fml = SUBSTR(codigo, 1, 3)
      AND nivel = 3
    JOIN programacion USING (codigo)
  GROUP BY 1
  HAVING COUNT(*) = (SELECT
      MAX(n)
    FROM (SELECT
        cod_centro,
        COUNT(*) n
      FROM (SELECT
          fml
        FROM familias
        WHERE familia LIKE '%informática%') c1
        JOIN cursos
          ON fml = SUBSTR(codigo, 1, 3)
          AND nivel = 3
        JOIN programacion USING (codigo)
      GROUP BY 1) c2)) c3
  JOIN centros USING (cod_centro);


--  (14) ¿De qué familia profesional han subvencionado más cursos?




SELECT
  cod_centro,
  COUNT(SUBSTR(programacion.codigo, 1, 3))
FROM programacion
GROUP BY cod_centro;

SELECT
  *
FROM familias;



--  david

SELECT
  familia
FROM (SELECT
    SUBSTR(codigo, 1, 3) fml
  FROM programacion
  GROUP BY 1
  HAVING COUNT(*) = (SELECT
      MAX(n)
    FROM (SELECT
        SUBSTR(codigo, 1, 3),
        COUNT(*) n
      FROM programacion
      GROUP BY 1) c1)) c2
  JOIN familias USING (fml);


--  (15) ¿En qué familia profesional está especializado cada centro? Indíquese centro, municipio y familia profesional; y ordénese por municipio y especialidad




    --  (16) ¿En qué familia profesional está especializado cada municipio? Muestra y ordena por familia y municipio

  SELECT id_curso,cod_centro FROM programacion;



  SELECT codigo,
         especialidad
         FROM cursos;


  SELECT * FROM (SELECT id_curso,cod_centro, codigo FROM programacion) c1 JOIN ( SELECT codigo,
         especialidad
         FROM cursos) c2 USING(codigo);


  SELECT municipio, c1.especialidad, COUNT(*) n  FROM centros JOIN (SELECT * FROM (SELECT id_curso,cod_centro, codigo FROM programacion) c1 JOIN ( SELECT codigo,
         especialidad
         FROM cursos) c2 USING(codigo)) c1 USING(cod_centro) GROUP BY  municipio, c1.especialidad;