﻿
--  1- Crear una vista que nos permita ver los productos cuyo precio es mayor que 100. Mostrar los campos CODIGO ARTICULO, SECCION Y PRECIO. Guardar la vista como CONSULTA1.
-- 2- La vista anterior seria la siguiente. Modificarla para ordenar por precio de forma descendente 

CREATE OR REPLACE VIEW CONSULTA1
AS
SELECT
  `CÓDIGO ARTÍCULO`,
  SECCIÓN,
  PRECIO
FROM productos
WHERE PRECIO > 100
ORDER BY PRECIO;

SELECT
  *
FROM CONSULTA1;

--  3- Crear una vista que utilice como tabla la vista CONSULTA1 y que nos muestre todos productos cuya sección es DEPORTES. 

--  Mostrar los campos CODIGO ARTICULO, SECCION Y PRECIO. Guardar la vista como CONSULTA2. 4- La vista anterior seria la siguiente 

CREATE OR REPLACE VIEW CONSULTA2
AS
SELECT
  `CÓDIGO ARTÍCULO`,
  SECCIÓN,
  PRECIO
FROM CONSULTA1
WHERE SECCIÓN = 'DEPORTES';

SELECT
  *
FROM CONSULTA2;


--  5-  Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA1. a. Código articulo: AR90 b. Sección: Novedades c. Precio: 5; 

INSERT INTO CONSULTA1
  VALUE ('AR90', 'Novedades', 5);

--  6- Modificar la vista CONSULTA1 y activar el check en la vista en modo local.
CREATE OR REPLACE VIEW CONSULTA1
AS
SELECT
  `CÓDIGO ARTÍCULO`,
  SECCIÓN,
  PRECIO
FROM productos
WHERE PRECIO > 100
ORDER BY PRECIO WITH LOCAL CHECK OPTION;



--  7 Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA1. a. Código articulo: AR91 b. Sección: Novedades c. Precio: 5;

INSERT INTO CONSULTA1
  VALUE ('AR91', 'Novedades', 5); --  este falla por el CHECK OPTION


--  ¿Qué es lo que ocurre?. Modificar el precio a 110

INSERT INTO CONSULTA1
  VALUE ('AR91', 'Novedades', 110);  --  este si entra


--  8- Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA2. a. Código articulo: AR92 b. Sección: Novedades c. Precio: 5; 

INSERT INTO CONSULTA2
  VALUE ('AR92', 'Novedades', 5);

--   9- Modificar la vista CONSULTA2 y activar el check en la vista en modo local.

CREATE OR REPLACE VIEW CONSULTA2
AS
SELECT
  `CÓDIGO ARTÍCULO`,
  SECCIÓN,
  PRECIO
FROM CONSULTA1
WHERE SECCIÓN = 'DEPORTES' WITH LOCAL CHECK OPTION;

--  10- Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA2. a. Código articulo: AR93 b. Sección: Novedades c. Precio: 5; 

INSERT INTO CONSULTA2
  VALUE ('AR93', 'Novedades', 5);  -- falla por el check


--   Modificar la sección a DEPORTES

INSERT INTO CONSULTA2
  VALUE ('AR93', 'DEPORTES', 5);


--   11- Modificar la vista CONSULTA2 y activar el check en la vista en modo CASCADA. 

CREATE OR REPLACE VIEW CONSULTA2
AS
SELECT
  `CÓDIGO ARTÍCULO`,
  SECCIÓN,
  PRECIO
FROM CONSULTA1
WHERE SECCIÓN = 'DEPORTES' WITH CASCADED CHECK OPTION;
                      
                      
-- 12- Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA2. d. Código articulo: AR94 e. Sección: DEPORTES f. Precio: 5; 

INSERT INTO CONSULTA2
  VALUE ('AR94', 'DEPORTES', 5);  -- falla por que hereda las restricciones de la consulta 1 que tiene WHERE PRECIO > 100

--  ¿Qué es lo que ocurre?. Modificar el precio a 200 

  INSERT INTO CONSULTA2
  VALUE ('AR94', 'DEPORTES', 200);  --  este si cunple las restricciones de ambas vistas 