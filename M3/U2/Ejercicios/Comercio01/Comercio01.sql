﻿--  (2) Consulta todos los datos de todos los programas

SELECT * FROM programa;

--  (1) Averigua el DNI de todos los clientes

  SELECT dni FROM cliente;

  --  (26) Obtén un listado de programas por orden de nombre y versión ????????????????????????

    SELECT 
           *
            FROM programa ORDER BY nombre, version;

    --  (7) Obtén el DNI más 4 de todos los clientes
      SELECT dni + 4 FROM cliente;

      --  (8) Haz un listado con los códigos de los programas multiplicados por 7

        SELECT codigo * 7 FROM programa;

        --  (10) ¿Cuáles son todos los datos del programa cuyo código es 11?

          SELECT * FROM programa WHERE codigo=11;

          --  (21) Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A o por una W

            SELECT *
                    FROM programa WHERE version LIKE '%i' OR nombre LIKE 'A%'  OR nombre LIKE 'W%';


            --  (23) Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, y cuyo nombre no comience por una A


               SELECT *
                    FROM programa WHERE version LIKE '%i' AND nombre NOT LIKE 'A%';


              --  (3) Obtén un listado con los nombres de todos los programas

                SELECT DISTINCT
                       nombre
                        FROM programa;

                --  (4) Genera una lista con todos los comercios

                  SELECT DISTINCT
                         nombre
                          FROM comercio;

   