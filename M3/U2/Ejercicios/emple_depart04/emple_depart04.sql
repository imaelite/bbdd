﻿
-- (20) Contar el número de empleados cuyo oficio sea VENDEDOR

SELECT
  COUNT(*)
FROM emple
WHERE oficio = 'Vendedor';


-- (29) Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece

SELECT DISTINCT
  apellido,
  dnombre
FROM emple
  JOIN depart
    ON emple.dept_no = depart.dept_no;

-- -------------- 

SELECT
  apellido,
  dnombre
FROM emple
  JOIN depart USING (dept_no);


-- (30) Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece.

-- Ordenar los resultados por apellido de forma descendente.


SELECT
  apellido,
  oficio,
  dnombre
FROM emple
  JOIN depart USING (dept_no)
ORDER BY apellido DESC;

-- (23) Mostrar los apellidos del empleado que más gana

SELECT
  MAX(salario)
FROM emple; -- c1

SELECT
  emple.apellido
FROM (SELECT
         MAX(salario) AS maximo,
         apellido
       FROM emple) c1,
     emple
WHERE emple.salario = c1.maximo;

SELECT
  apellido
FROM emple
WHERE salario = (SELECT
    MAX(salario)
  FROM emple);


-- (10) Indicar el número de empleados más el número de departamentos

SELECT
  COUNT(*) + (SELECT
      COUNT(*)
    FROM depart)
FROM emple;

SELECT
  (SELECT
      COUNT(*)
    FROM emple) + (SELECT
      COUNT(*)
    FROM depart);

-- (31) Listar el número de departamento de aquellos departamentos que tengan empleados

--  y el número de empleados que tengan.

SELECT
  dept_no DEPT_NO,
  COUNT(*) NUMERO_DE_EMPLEADOS
FROM emple
GROUP BY dept_no;


-- (32) Listar el número de empleados por departamento, ordenados de más a menos empleados, ????????????????????????

-- incluyendo los departamentos que no tengan empleados. El encabezado de la tabla será dnombre y NUMERO_DE_EMPLEADOS

-- Serán necesario utilizar LEFT JOIN e IFNULL

SELECT
  depart.dept_no,
  COUNT(*) n
FROM emple
  JOIN depart USING (dept_no)
GROUP BY dnombre;  -- C1

SELECT
  dnombre,
  IFNULL(n, 0) NUMERO_DE_EMPLEADOS
FROM depart
  LEFT JOIN (SELECT
      dept_no,
      COUNT(*) n
    FROM emple
      JOIN depart USING (dept_no)
    GROUP BY dept_no) c1 USING(dept_no) ORDER BY NUMERO_DE_EMPLEADOS DESC;