﻿--  (18) Realizar una consulta en la que se muestre por cada código hospital el nombre de las especialidades que tiene. Ordenar por código de hospital y especialidad


SELECT DISTINCT cod_hospital,
       especialidad FROM medicos ORDER BY cod_hospital, especialidad;


--  (6) Visualizar los oficios de los empleados del departamento VENTAS.

  SELECT DISTINCT emple.oficio FROM emple JOIN depart USING(dept_no) WHERE dnombre='VENTAS';

  --  david
    select distinct oficio from emple where dept_no=(select depart.dept_no from depart where dnombre='ventas');


   -- (4) Visualizar el nombre de los empleados vendedores del departamento VENTAS (Nombre del departamento=VENTAS, oficio=VENDEDOR).


      select distinct emple.apellido from emple where dept_no=(select depart.dept_no from depart where dnombre='ventas') AND oficio='VENDEDOR';

      SELECT * FROM emple;

      --  david
        select apellido from emple where dept_no=(SELECT depart.dept_no from depart where dnombre='ventas') and oficio='vendedor';


            --  (13) Mostrar el número de oficios distintos de cada departamento.

              SELECT  dept_no, COUNT(DISTINCT oficio) FROM emple GROUP BY dept_no;

              --  (10) Para cada oficio obtener la suma de salarios.

                SELECT  oficio, sum(salario) FROM emple GROUP BY oficio;


                --  (3) Hallar la media de los salarios de cada código de departamento con empleados (utilizar la función AVG y GROUP BY).

SELECT  dept_no, avg(salario) FROM emple GROUP BY dept_no;


--  (15) Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades.

  SELECT  estanteria, sum( unidades) FROM herramientas GROUP BY estanteria;

  --  (1) Visualizar el número de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamento.
    SELECT dept_no,COUNT(*) FROM emple GROUP BY dept_no;

    -- (19) Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos
      --  (tendrás que partir de la consulta anterior y utilizar GROUP BY).

      SELECT cod_hospital, especialidad, COUNT(*) FROM medicos GROUP BY cod_hospital, especialidad;


      --  (5) Visualizar el número de vendedores del departamento VENTAS (utilizar la función COUNT sobre la consulta (4)).

        select count(*) from emple where dept_no=(select depart.dept_no from depart where dnombre='ventas') AND oficio='VENDEDOR';

        --  david