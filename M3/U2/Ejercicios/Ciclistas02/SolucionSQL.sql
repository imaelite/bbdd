﻿--  (02) Cuántas etapas con puerto ha ganado Induráin llevando algún maillot

--  ;

  SELECT DISTINCT
         ciclista.dorsal
         FROM lleva JOIN ciclista USING(dorsal) WHERE nombre='Miguel Induráin';


  SELECT * FROM etapa JOIN puerto using(numetapa);


 -- david

  select count(*) from (select numetapa,dorsal from puerto where dorsal=(select dorsal from ciclista where nombre='miguel induráin')) c2 join lleva using (numetapa,dorsal);


  --  (n) Obtener el dorsal y el nombre de los ciclistas que han ganado los puertos de mayor altura


    SELECT 
           MAX(altura) FROM puerto;

    SELECT 
           dorsal, nombre FROM puerto JOIN ciclista USING(dorsal) WHERE  altura=(SELECT 
           MAX(altura) FROM puerto);

    --  david

      select dorsal,nombre from (select dorsal from puerto where altura=(select max(altura) from puerto)) c1 join ciclista using(dorsal);



      --  (m) Obtener las poblaciones de salida y de llegada de las etapas donde se encuentran los puertos con mayor pendiente

SELECT MAX(pendiente) FROM puerto;

SELECT 
       numetapa
        FROM puerto WHERE pendiente=(SELECT MAX(pendiente) FROM puerto);


SELECT 
       salida,
       llegada
        FROM etapa WHERE numetapa=(SELECT 
       numetapa
        FROM puerto WHERE pendiente=(SELECT MAX(pendiente) FROM puerto));

--  david

  select salida, llegada from(select numetapa from puerto where pendiente=(select max(pendiente) from puerto)) c1 join etapa using(numetapa);


  --  (f) Obtener el nombre y la edad de los ciclistas que han llevado dos o más maillots en una misma etapa

   SELECT dorsal,numetapa FROM lleva GROUP BY dorsal, numetapa HAVING COUNT(*)>2 ;


  SELECT 
         nombre,
         edad
          FROM ciclista JOIN (SELECT dorsal,numetapa FROM lleva GROUP BY dorsal, numetapa HAVING COUNT(*)>=2) c1 USING(dorsal);

  --  david
    select nombre,edad from (select dorsal from lleva group by dorsal,numetapa having count(*)>1) c1 join ciclista using(dorsal);

    --  (j) Obtener los números de las etapas que no tienen puertos de montaña

      SELECT 
             numetapa
              FROM puerto;

      SELECT etapa.numetapa
       FROM etapa LEFT JOIN (SELECT 
             numetapa
              FROM puerto) c1 USING(numetapa) WHERE c1.numetapa IS NULL;

      --  david

        select c1.numetapa from (select numetapa from etapa) c1 left join (select distinct numetapa from puerto) c2 on c1.numetapa=c2.numetapa where c2.numetapa is null;

        --  (b2) Obtener las poblaciones que tienen la meta de alguna etapa, pero desde las que no se realiza ninguna salida. Resuélvela con un producto externo

SELECT DISTINCT
       salida
       FROM etapa;


          SELECT DISTINCT
                 llegada
                  FROM etapa WHERE llegada NOT IN (SELECT DISTINCT
       salida
       FROM etapa);


          --  david 
            select llegada from (select distinct llegada from etapa ) c1 left join (select distinct salida from etapa ) c2 on llegada=salida where salida is null;

            --  (07) Qué maillots ha llevado Induráin en etapas con puerto cuya etapa haya ganado


SELECT DISTINCT numetapa FROM etapa JOIN puerto USING(numetapa);

SELECT dorsal
        FROM ciclista WHERE nombre='Miguel Induráin';

SELECT DISTINCT
       código FROM lleva JOIN
  
  (SELECT DISTINCT numetapa, etapa.dorsal FROM etapa JOIN puerto USING(numetapa)) c1 USING(numetapa) 
  
  
  JOIN 
  (SELECT dorsal
        FROM ciclista WHERE nombre='Miguel Induráin') c2  ON lleva.dorsal=c2.dorsal AND c2.dorsal=c1.dorsal;

--  david
  select código from (select distinct numetapa from puerto) c2 join (select * from etapa where dorsal=(select dorsal from ciclista where nombre='miguel induráin'))c2b using(numetapa) join lleva using(dorsal,numetapa);






--  (09) Cuántas etapas con puerto ha ganado Induráin llevando algún maillot
    select count(*) from (select c2.numetapa from (select distinct dorsal,numetapa from (select dorsal from ciclista where nombre='miguel induráin') c1 join lleva using(dorsal)) c2 join etapa on c2.dorsal=etapa.dorsal and c2.numetapa=etapa.numetapa) c3 join puerto using(numetapa);