﻿--  (01) ¿Cuántos centros hay censados en Cantabria?
SELECT COUNT(*) FROM  centros;


--  (02) ¿Cuántos cursos se han programado?

  SELECT COUNT(*) FROM programacion;


  --  (03) ¿Cuántos centros han resultado adjudicatarios de estos cursos?

    SELECT COUNT(DISTINCT centro) FROM centros;

    SELECT COUNT(DISTINCT cod_centro) FROM programacion;


    --  (17) ¿En cuántos municipios se imparten cursos?


      SELECT COUNT(DISTINCT municipio) FROM inicios;


      select count(distinct municipio) from programacion join centros using(cod_centro);


      --  (09) ¿Cuántos cursos de informática se imparten en Cantabria?

        SELECT COUNT(codigo) FROM programacion WHERE codigo LIKE '%IFC%';

        SELECT * FROM programacion;

        SELECT * FROM centros ;
        SELECT * FROM cursos;

        SELECT * FROM inicios;

        --  david
          select count(codigo) from (SELECT fml
                                             from familias WHERE familia like '%informática%') c1 join programacion on fml=substr(codigo,1,3);


          --  (10) ¿Cuántos cursos de informática diferentes se imparten en Cantabria?

        SELECT COUNT(DISTINCT codigo) FROM programacion WHERE codigo LIKE '%IFC%';

        --  david
          SELECT COUNT(DISTINCT codigo) from (select fml from familias where familia like '%informática%') c1 join programacion on fml=SUBSTR(codigo,1,3);


          --  (12) ¿Cuántos cursos de informática de nivel 3 se imparten en Cantabria?

             SELECT COUNT(DISTINCT cursos.codigo) n FROM programacion JOIN cursos ON programacion.codigo = cursos.codigo WHERE programacion.codigo LIKE '%IFC%' AND cursos.nivel=3 ;


            SELECT * FROM cursos;


           SELECT * FROM programacion WHERE codigo  LIKE '%IFC%';


          SELECT * FROM cursos WHERE nivel=3;

          SELECT COUNT(*) FROM (SELECT * FROM programacion WHERE codigo  LIKE '%IFC%') c1 JOIN (SELECT * FROM cursos WHERE nivel=3) c2 USING(codigo);


          --  david

            select count(*) from ( select fml from familias where familia like '%informática%') c1 join cursos on fml=substr(codigo,1,3) and nivel=3 join programacion using(codigo);


            --  (13) ¿Cuántos cursos de informática de nivel 3 diferentes se imparten en Cantabria?

 select count(DISTINCT cursos.codigo) from (select fml from familias where familia like '%informática%') c1 join cursos on fml=substr(codigo,1,3) and nivel=3 join programacion using(codigo);



--  (18) En qué municipio hay más centros de formación

  SELECT municipio,COUNT(*) n FROM centros GROUP BY municipio ORDER BY n desc;


  select municipio from centros group by 1 having count(*)=(select max(n) from ( select municipio, count(*) n from centros group by 1) c1 );



