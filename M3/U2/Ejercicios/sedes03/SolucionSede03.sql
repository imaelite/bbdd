﻿--  (10) Indicarme el salario medio de cada una de las compañías

  SELECT
         compañia,
         AVG(salario) FROM trabaja GROUP BY compañia;

  --  (13) Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja

    SELECT persona.nombre,
      persona.ciudad,
        
           compania.ciudad
          
            FROM trabaja JOIN compania ON trabaja.compañia=compania.nombre JOIN persona ON persona.nombre=trabaja.persona;

    --  (20) Indicar el nombre de las personas que NO trabajan para FAGOR

      --  Sólo es necesario el alias c1 y la resta la realizaremos con un RIGHT JOIN en vez con un NOT IN. Ten en cuenta a los desempleados.


      SELECT persona
              FROM trabaja WHERE compañia='FAGOR'; -- C1

      SELECT 
             nombre
              FROM (SELECT persona
              FROM trabaja WHERE compañia='FAGOR') c1 RIGHT JOIN  persona  ON persona=nombre WHERE persona IS NULL;

      --  (09) Indicar la compañía que más trabajadores tiene

        --  Utilizar n como alias para el número de empleados, no uses alias para el máximo ni combines en Access


        SELECT 
               compañia, COUNT(*) n
                FROM trabaja GROUP BY compañia;  -- numero de personas por compañia

        SELECT MAX(n) FROM (SELECT 
               compañia, COUNT(*) n
                FROM trabaja GROUP BY compañia)c1;



          SELECT 
               compañia
                FROM trabaja GROUP BY compañia HAVING COUNT(*) =(SELECT MAX(n) FROM (SELECT 
               compañia, COUNT(*) n
                FROM trabaja GROUP BY compañia)c1);


          SELECT persona, persona.ciudad, compania.ciudad from trabaja join persona join compania on trabaja.persona=persona.nombre and trabaja.compañia = compania.nombre;