﻿

-- 1 Crear una vista que debe mostrar los pedidos de los clientes que son de Madrid, Barcelona o Zaragoza. 
--  Visualizar los campos: CÓDIGO CLIENTE, NUMERO DE PEDIDO, EMPRESA y POBLACIÓN.  Llamar a la vista CONSULTA1

SELECT
  *
FROM clientes;

SELECT
  *
FROM pedidos;

SELECT
  *
FROM clientes
WHERE POBLACIÓN IN ('Madrid', 'Barcelona', 'Zaragoza');

CREATE OR REPLACE VIEW sub_consulta1_optimizada
AS
SELECT
  *
FROM clientes
WHERE POBLACIÓN IN ('Madrid', 'Barcelona', 'Zaragoza');

--  2  Quiero modificarla para que la consulta este optimizada. Para optimizar la consulta primero deberíamos seleccionar los registros en clientes y
--  después combinarlos con pedidos. 

CREATE OR REPLACE VIEW CONSULTA1OPTIMIZADA
AS
SELECT
  sub_consulta1_optimizada.`CÓDIGO CLIENTE`,
  `NÚMERO DE PEDIDO`,
  EMPRESA,
  POBLACIÓN
FROM sub_consulta1_optimizada
  JOIN pedidos
    ON sub_consulta1_optimizada.`CÓDIGO CLIENTE` = pedidos.`CÓDIGO CLIENTE`;


--  5- Crear una vista que nos permita realizar una consulta que muestre que clientes han realizado algún pedido en el año 2002 y su forma de pago fue tarjeta.
--  Utilizamos los campos EMPRESA y POBLACIÓN de la tabla CLIENTES, y NÚMERO DE PEDIDO, FECHA DE PEDIDO y FORMA DE PAGO de la tabla PEDIDOS.
--  Ordenar los registros por el campo FECHA DE PEDIDO de forma ascendente. Guardamos la vista con el nombre CONSULTA 2.
--  La vista debe tener la consulta optimizada. Si necesitas crear más de una vista colócalas el nombre que desees. 


CREATE OR REPLACE VIEW sub_CONSULTA2
AS

SELECT
  `CÓDIGO CLIENTE`,
  `NÚMERO DE PEDIDO`,
  `FECHA DE PEDIDO`,
  `FORMA DE PAGO`
FROM pedidos
WHERE YEAR(`FECHA DE PEDIDO`) = 2002
AND `FORMA DE PAGO` = 'tarjeta';

--  6- Las consultas utilizadas en la vista anterior son las siguientes 
CREATE OR REPLACE VIEW CONSULTA2
AS
SELECT
  sub_CONSULTA2.`NÚMERO DE PEDIDO`,
  sub_CONSULTA2.`FECHA DE PEDIDO`,
  sub_CONSULTA2.`FORMA DE PAGO`,
  EMPRESA,
  POBLACIÓN
FROM clientes
  JOIN sub_CONSULTA2 USING (`CÓDIGO CLIENTE`) ORDER BY sub_CONSULTA2.`FECHA DE PEDIDO`;



-- 7- Crear una vista nueva que nos permita realizar una consulta que nos permita saber los clientes que han realizado más  pedidos.
  --  Mostrar el código y el teléfono. Guardamos la vista con el nombre de CONSULTA3. 
 
  SELECT 
         `CÓDIGO CLIENTE`,COUNT(*) AS numero_pedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`;

  SELECT MAX(c1.numero_pedidos) AS maximi FROM (SELECT 
         `CÓDIGO CLIENTE`,COUNT(*) AS numero_pedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`) c1 ;


  SELECT * FROM ( SELECT 
         `CÓDIGO CLIENTE`,COUNT(*) AS numero_pedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`) c1 JOIN (SELECT MAX(c1.numero_pedidos) AS maximi FROM (SELECT 
         `CÓDIGO CLIENTE`,COUNT(*) AS numero_pedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`) c1 ) c2 ON c1.numero_pedidos=c2.maximi;


  SELECT `CÓDIGO CLIENTE`,
         TELÉFONO FROM clientes JOIN (SELECT * FROM ( SELECT 
         `CÓDIGO CLIENTE`,COUNT(*) AS numero_pedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`) c1 JOIN (SELECT MAX(c1.numero_pedidos) AS maximi FROM (SELECT 
         `CÓDIGO CLIENTE`,COUNT(*) AS numero_pedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`) c1 ) c2 ON c1.numero_pedidos=c2.maximi) c1 USING(`CÓDIGO CLIENTE`);


  -- con vistas

CREATE OR REPLACE VIEW sub_consulta_1_consulta3
AS
 SELECT 
         `CÓDIGO CLIENTE`,COUNT(*) AS numero_pedidos FROM pedidos GROUP BY `CÓDIGO CLIENTE`;

CREATE OR REPLACE VIEW sub_consulta_2_consulta3
AS
  SELECT MAX(sub_consulta_1_consulta3.numero_pedidos) AS maximi FROM sub_consulta_1_consulta3;


CREATE OR REPLACE VIEW sub_consulta_3_consulta3 AS

  SELECT * FROM sub_consulta_1_consulta3 c1 JOIN sub_consulta_2_consulta3 c2 ON c1.numero_pedidos=c2.maximi;

CREATE OR REPLACE VIEW CONSULTA3 AS
SELECT `CÓDIGO CLIENTE`,
         TELÉFONO  FROM clientes JOIN sub_consulta_3_consulta3 USING(`CÓDIGO CLIENTE`);
