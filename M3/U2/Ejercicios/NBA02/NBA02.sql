﻿--  (10) ¿En cuántos equipos ha jugado Gasol?

SELECT
  *
FROM jugadores
WHERE Nombre = 'Pau Gasol'
GROUP BY Nombre
;

--  (9) ¿En qué temporada ha anotado más puntos Gasol?


SELECT
  codigo
FROM jugadores
WHERE Nombre = 'Pau Gasol'
;


SELECT
  *
FROM estadisticas
WHERE estadisticas.jugador = 66;


--  david

SELECT
  temporada
FROM (SELECT
    *
  FROM jugadores
  WHERE nombre LIKE '%gasol%') c1
  JOIN estadisticas
    ON codigo = jugador
    AND puntos_por_partido = (SELECT
        MAX(puntos_por_partido)
      FROM (SELECT
          *
        FROM jugadores
        WHERE nombre LIKE '%gasol%') c1
        JOIN estadisticas
          ON codigo = jugador);



--  (16) Temporada en la que se anotaron más puntos por la totalidad de los equipos

SELECT
  temporada,
  SUM(puntos_local + puntos_visitante)
FROM partidos
GROUP BY temporada;

SELECT
  *
FROM partidos;

SELECT
  temporada
FROM partidos
GROUP BY 1
HAVING SUM(puntos_local + puntos_visitante) = (SELECT
    MAX(s)
  FROM (SELECT
      temporada,
      SUM(puntos_local + puntos_visitante) s
    FROM partidos
    GROUP BY 1) c1);


--  (8) ¿En qué ciudad hay más equipos de basket?

SELECT
  *
FROM equipos;

SELECT
  Ciudad,
  COUNT(Nombre)
FROM equipos
GROUP BY Ciudad;


--  (17) Nombre de los equipos que anotan más puntos jugando fuera que en casa, ordenados alfabéticamente


SELECT

  equipo_visitante,
  SUM(puntos_visitante),
  SUM(puntos_local)
FROM partidos
GROUP BY equipo_visitante
ORDER BY equipo_local;


--  (14) Equipo que ha ganado más partidos

SELECT DISTINCT
  equipos.Nombre
FROM equipos;

SELECT
  *
FROM partidos;


--  (13) ¿Qué equipo tiene a los jugadores más ligeros?

SELECT
  SUM(Peso),
  Nombre_equipo
FROM jugadores
GROUP BY Nombre_equipo;

--  david
SELECT
  nombre_equipo
FROM jugadores
GROUP BY 1
HAVING ROUND(AVG(peso)) = (SELECT
    MIN(p)
  FROM (SELECT
      nombre_equipo,
      ROUND(AVG(peso)) p
    FROM jugadores
    GROUP BY 1) c1);

--  (15) ¿Qué equipo ha anotado más puntos?

SELECT
  equipo_local,
  SUM(puntos_local) s
FROM partidos
GROUP BY equipo_local;

SELECT
  equipo_visitante,
  SUM(puntos_visitante) s
FROM partidos
GROUP BY equipo_visitante;

SELECT
  c1.equipo_local,
  (c1.s + c2.s)
FROM (SELECT
    equipo_local,
    SUM(puntos_local) s
  FROM partidos
  GROUP BY equipo_local) c1
  JOIN (SELECT
      equipo_visitante,
      SUM(puntos_visitante) s
    FROM partidos
    GROUP BY equipo_visitante) c2
    ON c1.equipo_local = c2.equipo_visitante;


--  david
SELECT
  equipo
FROM (SELECT
    equipo_local equipo,
    puntos_local puntos
  FROM partidos
  UNION ALL
  SELECT
    equipo_visitante equipo,
    puntos_visitante puntos
  FROM partidos) c1
GROUP BY 1
HAVING SUM(puntos) = (SELECT
    MAX(s)
  FROM (SELECT
      equipo,
      SUM(puntos) s



    FROM (SELECT
        equipo_local equipo,
        puntos_local puntos
      FROM partidos
      UNION ALL
      SELECT
        equipo_visitante equipo,
        puntos_visitante puntos
      FROM partidos) c1
    GROUP BY 1) c2);


--  (17) Nombre de los equipos que anotan en en total más puntos jugando fuera que en casa, ordenados alfabéticamente

SELECT
  equipo_local,
  SUM(puntos_local) l
FROM partidos
GROUP BY equipo_local;

SELECT
  equipo_visitante,
  SUM(puntos_visitante) l
FROM partidos
GROUP BY equipo_visitante;


SELECT DISTINCT
  c1.equipo_local
FROM (SELECT
    equipo_local,
    SUM(puntos_local) l
  FROM partidos
  GROUP BY equipo_local) c1
  JOIN (SELECT
      equipo_visitante,
      SUM(puntos_visitante) l
    FROM partidos
    GROUP BY equipo_visitante) c2
    ON c1.equipo_local = c2.equipo_visitante

WHERE c1.l < c2.l
ORDER BY c1.equipo_local
;

--  david

SELECT
  equipo
FROM (SELECT
    equipo_local equipo,
    SUM(puntos_local) s_local
  FROM partidos
  GROUP BY 1) c1
  JOIN (SELECT
      equipo_visitante equipo,
      SUM(puntos_visitante) s_visitante
    FROM partidos
    GROUP BY 1) c2
  USING (equipo)
WHERE s_local
ORDER BY equipo;


--  (18) ¿En qué posición juegan los jugadores más pesados?

SELECT
  MAX(peso)
FROM jugadores;

SELECT
  *
FROM jugadores
WHERE Peso >= (SELECT
    MAX(peso)
  FROM jugadores);

--  david

SELECT
  posicion
FROM jugadores
GROUP BY 1
HAVING ROUND(AVG(peso)) = (SELECT
    MAX(p)
  FROM (SELECT
      posicion,
      ROUND(AVG(peso)) p
    FROM jugadores
    GROUP BY 1) c1);



--  (14) Equipo que ha ganado más partidos

SELECT
  *
FROM estadisticas;

SELECT DISTINCT
  Nombre
FROM equipos;


SELECT
  *
FROM information_schema.TABLES
WHERE TABLE_SCHEMA = 'NBA';

SELECT
  equipo_local
FROM partidos
ORDER BY puntos_local DESC LIMIT 1;


SELECT
  MIN(temporada)
FROM partidos;

SELECT
  MAX(puntos_local)
FROM partidos;

SELECT DISTINCT
  equipo_local
FROM partidos
WHERE puntos_local = (SELECT
    MAX(puntos_local)
  FROM partidos)
AND temporada = (SELECT
    MAX(temporada)
  FROM partidos);


DROP FUNCTION madrid2;
CREATE FUNCTION madrid2 ()
RETURNS varchar(31)
BEGIN
  RETURN (SELECT
      provincia
    FROM provincias
    ORDER BY poblacion DESC LIMIT 1);
END;

DROP FUNCTION nba_func1;
CREATE FUNCTION nba_func1 ()
RETURNS varchar(31)
BEGIN
  RETURN (SELECT
      codigo
    FROM partidos
    WHERE puntos_local = (SELECT
        MIN(puntos_local)
      FROM partidos)
    AND puntos_visitante = (SELECT
        MIN(puntos_visitante)
      FROM partidos)
    AND temporada = '06/07');
END;


DROP FUNCTION nba_func2;
CREATE FUNCTION nba_func2 ()
RETURNS varchar(31)
BEGIN
  RETURN (SELECT
      codigo
    FROM partidos
    WHERE temporada = '06/07'
    ORDER BY puntos_local, puntos_visitante
    LIMIT 1);
END;




SELECT
  BENCHMARK(1e2, nba_func1());

SELECT
  BENCHMARK(1e2, nba_func2()); 



