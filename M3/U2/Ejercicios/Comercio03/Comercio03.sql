﻿--  (20) ¿Qué clientes terminan su nombre en la letra o y, además, son mayores de 30 años?

SELECT DISTINCT
  nombre
FROM cliente
WHERE nombre LIKE '%o'
AND edad > 30;

--  (24) Obtén una lista de empresas por orden alfabético ascendente
SELECT
  nombre
FROM comercio
UNION
SELECT
  nombre
FROM fabricante
ORDER BY nombre;



--  (25) Genera un listado de empresas por orden alfabético descendente

SELECT
  nombre
FROM comercio
UNION
SELECT
  nombre
FROM fabricante
ORDER BY nombre DESC;

--  (15) ¿Qué otros comercios hay, además de El Corte Inglés? Utilizar el operador IN

SELECT DISTINCT
  nombre
FROM comercio
WHERE nombre NOT IN ('El Corte Inglés');

--  (17) Obtén un listado que incluya los nombres de los clientes de edades comprendidas entre 10 y 25 y de los mayores de 50 años. Utiliza BETWEEN

-- con union
SELECT
  nombre
FROM cliente
WHERE edad BETWEEN 10 AND 25
UNION
SELECT
  nombre
FROM cliente
WHERE edad > 50;

-- más simple
SELECT
  nombre
FROM cliente
WHERE (edad BETWEEN 10 AND 25)
OR edad > 50;

--  (18) Saca un listado con los comercios de Sevilla y Madrid. No se admiten valores duplicados

SELECT DISTINCT
  nombre
FROM comercio
WHERE ciudad = 'Sevilla'
OR ciudad = 'Madrid';

-- mas corta

SELECT DISTINCT
  nombre
FROM comercio
WHERE ciudad IN ('Sevilla', 'Madrid');

--   (45) Obtener el número de programas que hay en la tabla programas

SELECT
  COUNT(DISTINCT nombre)
FROM programa;

--  (34) ¿Qué usuarios han optado por Internet como medio de registro?

SELECT
  dni
FROM registra
WHERE medio = 'Internet';

SELECT
  nombre
FROM cliente
WHERE dni IN (SELECT DISTINCT
    dni
  FROM registra
  WHERE medio = 'Internet');

          --  (46) Calcula el número de clientes cuya edad es mayor de 40 años

           SELECT COUNT(*) FROM cliente WHERE edad>40;


          --  (47) Calcula el número de productos que ha registrado el establecimiento cuyo CIF es 1

            -- Entendemos que los comercios anotan sus compran en la tabla distribuye y sus ventas en registra. 
            
             -- Lo distribuido y no registrado, permanece en sus almacenes.

            SELECT COUNT(*) FROM registra WHERE cif=1;