﻿--  (4.09.04) Obtener el nombre de los clientes a los que no se les ha entregado a tiempo un pedido. Es decir, FechaEntrega mayor que FechaEsperada   ?????


SELECT
  NombreCliente
FROM (SELECT DISTINCT
    CodigoCliente
  FROM pedidos
  WHERE FechaEntrega > FechaEsperada) c1
  JOIN clientes USING (CodigoCliente);


--  (4.10.36) Código del frutal que más margen de beneficios proporciona

SELECT
  CodigoProducto,
  MAX(PrecioVenta - PrecioProveedor) beneficio
FROM productos
WHERE Gama = 'Frutales';


SELECT
  CodigoProducto
FROM productos
WHERE Gama = 'Frutales'
AND PrecioVenta - PrecioProveedor = (SELECT
    MAX(beneficio)
  FROM (SELECT
      CodigoProducto,
      PrecioVenta - PrecioProveedor beneficio
    FROM productos
    WHERE Gama = 'Frutales') c1);


--  (4.10.03) Sacar el nombre de los clientes que hayan hecho pedidos en 2008


SELECT DISTINCT
  CodigoCliente
FROM pedidos
WHERE YEAR(FechaPedido) = 2008;

SELECT
  NombreCliente
FROM (SELECT DISTINCT
    CodigoCliente
  FROM pedidos
  WHERE YEAR(FechaPedido) = 2008) c1
  JOIN clientes USING (CodigoCliente);

--  (4.07.06b) Muestra el nombre de cliente repetido, ¿puede ser esto posible?


SELECT
  NombreCliente,
  COUNT(*) AS cuenta
FROM clientes
GROUP BY NombreCliente;



SELECT
  c1.NombreCliente
FROM (SELECT
    NombreCliente,
    COUNT(*) AS cuenta
  FROM clientes
  GROUP BY NombreCliente) c1
WHERE c1.cuenta = 2;

--  david

SELECT
  NombreCliente
FROM clientes
GROUP BY NombreCliente
HAVING COUNT(*) > 1;

--  (4.08.04) Sacar el códido de el/los producto/s que más unidades tiene en stock y el que menos


SELECT
  MIN(CantidadEnStock)
FROM productos;


SELECT
  MAX(CantidadEnStock)
FROM productos;


SELECT
  CodigoProducto
FROM productos
WHERE CantidadEnStock = (SELECT
    MAX(CantidadEnStock)
  FROM productos)
UNION
SELECT
  CodigoProducto
FROM productos
WHERE CantidadEnStock = (SELECT
    MIN(CantidadEnStock)
  FROM productos);


--  (4.08.02) Obtener el nombre del producto del que más unidades se hayan vendido en un mismo pedido

SELECT
  CodigoPedido,
  CodigoProducto,
  SUM(Cantidad) cant
FROM detallepedidos
GROUP BY CodigoPedido,
         CodigoProducto
HAVING cant = (SELECT
    MAX(c1.cant)
  FROM (SELECT
      CodigoPedido,
      CodigoProducto,
      SUM(Cantidad) cant
    FROM detallepedidos
    GROUP BY CodigoPedido,
             CodigoProducto) c1);

SELECT
  MAX(c1.cant)
FROM (SELECT
    CodigoPedido,
    CodigoProducto,
    SUM(Cantidad) cant
  FROM detallepedidos
  GROUP BY CodigoPedido,
           CodigoProducto) c1;

SELECT
  Nombre

FROM (SELECT
    CodigoPedido,
    CodigoProducto,
    SUM(Cantidad) cant
  FROM detallepedidos
  GROUP BY CodigoPedido,
           CodigoProducto
  HAVING cant = (SELECT
      MAX(c1.cant)
    FROM (SELECT
        CodigoPedido,
        CodigoProducto,
        SUM(Cantidad) cant
      FROM detallepedidos
      GROUP BY CodigoPedido,
               CodigoProducto) c1)) c1
  JOIN productos USING (CodigoProducto);

-- david
SELECT
  nombre
FROM (SELECT DISTINCT
    codigoproducto
  FROM detallepedidos
  WHERE cantidad = (SELECT
      MAX(cantidad)
    FROM detallepedidos)) c1
  JOIN productos USING (codigoproducto);

--  (4.08.07) Facturación total del producto más caro   

  --  El artículo más caro, no necesariamente ha tenido siempre el mismo precio

  SELECT 
         MAX(PrecioVenta)
          FROM productos;

  SELECT CodigoProducto FROM productos WHERE PrecioVenta=( SELECT 
         MAX(PrecioVenta)
          FROM productos);

  SELECT SUM(PrecioUnidad*Cantidad) FROM pedidos JOIN detallepedidos ON pedidos.CodigoPedido = detallepedidos.CodigoPedido WHERE CodigoProducto=( SELECT CodigoProducto FROM productos WHERE PrecioVenta=( SELECT 
         MAX(PrecioVenta)
          FROM productos));



  --  (4.10.02) Sacar el listado con los nombres de los clientes y el total pagado por cada uno de ellos.    

    SELECT 
           CodigoCliente, SUM(Cantidad) FROM pagos  GROUP BY CodigoCliente;


    SELECT 
           NombreCliente,
         c1.suma_total FROM(select CodigoCliente,SUM(Cantidad) suma_total FROM pagos  GROUP BY CodigoCliente) c1 JOIN clientes USING(CodigoCliente);

--  david

  select nombrecliente,pagos from (select codigocliente,sum(cantidad) pagos from pagos group by codigocliente)c1 join clientes using(codigocliente);

   


    --  (4.10.13) Sacar el número de clientes que tiene asignado cada representante de ventas mostrando su nombre.

     SELECT 
            empleados.Nombre, COUNT(*)
             FROM clientes  JOIN empleados ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado GROUP BY CodigoEmpleadoRepVentas;

    --  david

  select nombre,n from (select codigoempleadorepventas codigoempleado, count(*) n from clientes group by codigoempleadorepventas) c1 join empleados using(codigoempleado);

      --  (4.08.07) Facturación total del producto más caro

        --  c1
        SELECT 
               MAX(PrecioVenta)
                FROM productos;

        SELECT CodigoProducto FROM productos WHERE PrecioVenta=(SELECT 
               MAX(PrecioVenta)
                FROM productos);

        SELECT 
               SUM(
               Cantidad *
               PrecioUnidad)
                FROM detallepedidos WHERE CodigoProducto=(SELECT CodigoProducto FROM productos WHERE PrecioVenta=(SELECT 
               MAX(PrecioVenta)
                FROM productos));

        --  david
          select sum(cantidad*preciounidad) from (select distinct codigoproducto from detallepedidos where preciounidad=(select max(preciounidad) FROM detallepedidos)) c1 join detallepedidos using(codigoproducto);


          --  (4.10.44) Con objeto de acortar distancias entre comerciales y sus clientes, obtener un listado de los códigos de los comerciales que están
            --  atendiendo clientes de fuera de su ciudad pero con oficina en la misma ciudad que el cliente. La idea es enviarles una circular y 
            --  puedan realizar las gestiones oportunas para que sus clientes puedan ser atendidos por otro comercial más próximo geográficamente

            SELECT 
                   Ciudad ciudad_cliente FROM clientes;

            SELECT CodigoEmpleado,
                   CodigoOficina FROM empleados JOIN oficinas USING(CodigoOficina)     WHERE Puesto='Representante Ventas';

            --  david

              SELECT DISTINCT codigoempleado FROM (SELECT DISTINCT codigoempleadorepventas, ciudad FROM clientes JOIN oficinas USING(ciudad)) c1 join empleados

                ON codigoempleadorepventas = codigoempleado JOIN oficinas USING(codigooficina) WHERE c1.ciudad!=oficinas.ciudad;