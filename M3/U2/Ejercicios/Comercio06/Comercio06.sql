﻿--  (53) Obtener el número total de programas que se han distribuido en Sevilla

SELECT
  cif
FROM comercio
WHERE ciudad = 'sevilla'; -- c1

SELECT
  SUM(cantidad)
FROM (SELECT
    cif
  FROM comercio
  WHERE ciudad = 'sevilla') c1
  JOIN distribuye USING (cif);

--  (40) Obtén el nombre de los usuarios que han registrado Access XP

SELECT DISTINCT
  cliente.nombre
FROM cliente
  JOIN registra USING (dni)
  JOIN programa
    ON registra.codigo = programa.codigo
WHERE programa.nombre = 'Access'
AND version = 'xp';

-- david
SELECT DISTINCT
  nombre
FROM (SELECT
    codigo
  FROM programa
  WHERE nombre = 'Access'
  AND version = 'xp') c1
  JOIN registra USING (codigo)
  JOIN cliente USING (dni);

--  (28) ¿Qué comercios distribuyen Windows?

-- mio

SELECT
  *
FROM (SELECT DISTINCT
    cif
  FROM programa
    JOIN distribuye USING (codigo)
  WHERE nombre = 'Windows') c1
  JOIN comercio USING (cif);

-- david  
SELECT
  *
FROM (SELECT
    cif
  FROM (SELECT
      codigo
    FROM programa
    WHERE nombre = 'windows') c1
    JOIN distribuye USING (codigo)) c2
  JOIN comercio USING (cif);
--  (28b) ¿Qué comercios NO distribuyen Windows?


SELECT 
       comercio.*
        FROM (SELECT codigo FROM programa WHERE nombre='windows') c1 JOIN distribuye USING(codigo) RIGHT JOIN comercio USING(cif) WHERE c1.codigo IS NULL;


select comercio.* from comercio left join (select distinct cif from (select codigo from programa where nombre='windows')c1 join distribuye using(codigo))c2 on comercio.cif=c2.cif where c2.cif is null;


--  (29) Genera un listado del nombre de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid
-- Muestra todos los datos del programa pero, en cada subconsulta, arrastra únicamente los campo estrictamente necesarios


SELECT
  programa.*,
  cantidad
FROM programa
  JOIN distribuye USING (codigo)
  JOIN comercio USING (cif)
WHERE comercio.nombre = 'El Corte Inglés'
AND ciudad = 'Madrid';

-- david

SELECT
  programa.*,
  cantidad from (select codigo, cantidad from distribuye where cif=(select cif from comercio where
 nombre ='el corte inglés' and ciudad ='madrid')) c1 join programa using (codigo);


--  (39) Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle

  SELECT id_fab FROM fabricante WHERE nombre='Oracle';

 -- desarrlo distribuye Y comercio

  SELECT DISTINCT
         ciudad FROM (SELECT id_fab FROM fabricante WHERE nombre='Oracle') c1 JOIN desarrolla USING(id_fab) JOIN distribuye USING(codigo) JOIN comercio USING(cif);

  --  (54) Calcular el número total de programas que han desarrollado los fabricantes cuyo país es Estados Unidos

    SELECT COUNT(*) FROM desarrolla JOIN fabricante USING(id_fab) WHERE pais='Estados Unidos';

    -- david
      select count(distinct codigo) from (select id_fab from fabricante where pais='estados unidos') c1 join desarrolla using(id_fab);


      --  (26b) Obtén un listado de aquellos programas que tengan más de una versión, ordenando la lista por programa y por la versión de más a menos moderna
      --  Utiliza el operador IN y muestra todos los campos de cada programa



   SELECT * FROM programa WHERE nombre IN(SELECT nombre
               FROM programa GROUP BY nombre HAVING count(*)>1) ORDER BY nombre,version DESC;

        
     -- (28e) ¿Qué programas sólo se distribuyen en un comercio? Entendiendo como programas diferentes las distintas versiones de un programa.



SELECT 
       nombre,
       version FROM (SELECT 
       codigo
        FROM distribuye GROUP BY codigo HAVING COUNT(*)=1) c1 JOIN programa USING(codigo);

-- david
  SELECT distinct
       nombre FROM (SELECT 
       codigo, count(*) n
        FROM distribuye GROUP BY codigo HAVING n=1) c1 JOIN programa USING(codigo);