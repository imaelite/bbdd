﻿--  (56) Con una consulta concatena los campos nombre y versión de la tabla PROGRAMA

SELECT
  CONCAT(nombre, '', version)
FROM programa;

--  (13) Obtén un listado con los códigos de las distintas versiones de Windows

SELECT
  codigo
FROM programa
WHERE nombre = 'Windows';


--  (22) Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A y termine por una S

SELECT
  *
FROM programa
WHERE version LIKE '%i'
OR (nombre LIKE 'A%'
AND nombre LIKE '%S');


--  (55) Visualiza el nombre de todos los clientes en mayúscula. En el resultado de la consulta debe aparecer también la longitud de la cadena nombre

SELECT
  UPPER(nombre),
  CHAR_LENGTH(nombre)
FROM cliente;

--  (11) ¿Qué fabricantes son de Estados Unidos?

SELECT DISTINCT
  nombre
FROM fabricante
WHERE pais = 'Estados Unidos';


--  (12) ¿Cuáles son los fabricantes no españoles? Utilizar el operador IN



SELECT
  nombre
FROM fabricante
WHERE pais NOT IN (SELECT
    pais
  FROM fabricante
  WHERE pais = 'España');

-- más corto
SELECT
  nombre
FROM fabricante
WHERE pais NOT IN ('España');


--  (14) ¿En qué ciudades comercializa programas El Corte Inglés?

  SELECT distinct
         ciudad FROM comercio WHERE nombre='El Corte Inglés';

  --  (16) Genera una lista con los códigos de las distintas versiones de Windows y Access. Utiliza el operador IN

    SELECT codigo FROM programa WHERE nombre IN ('Windows','Access');


    --  (19) ¿Qué clientes terminan su nombre en la letra o?


      SELECT DISTINCT
             nombre
              FROM cliente  WHERE nombre  LIKE '%o';


    --  (9) ¿Cuáles son los nombres de los programas cuyo código es inferior o igual a 10?

        SELECT DISTINCT
               nombre
                FROM programa WHERE codigo <=10;