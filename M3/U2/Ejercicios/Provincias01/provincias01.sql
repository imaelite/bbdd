﻿-- Consulta 1 Listado de provincias
SELECT
  provincia
FROM provincias;

-- Consulta 2 ¿Cuánto suman 2 y 3?

SELECT
  2 + 3;

-- Consultas 3  Densidades de población de las provincias

SELECT
  provincia,
  poblacion / superficie AS Densidad
FROM provincias;

-- Consultas 4  ¿Cuánto vale la raíz cuadrada de 2?


SELECT
  SQRT(2);


-- Consultas 5  ¿Cuántos caracteres tiene cada nombre de provincia?

SELECT
  provincia,
  CHAR_LENGTH(provincia)
FROM provincias;

-- Consulta 6   Listado de autonomías  

SELECT DISTINCT
  autonomia
FROM provincias;


-- Consulta 7 Provincias con el mismo nombre que su comunidad autónoma

SELECT
  provincia
FROM provincias
WHERE autonomia = provincia;

-- Consulta 8 Provincias que contienen el diptongo 'ue'

SELECT
  provincia
FROM provincias
WHERE provincia LIKE '%ue%';


-- Consulta 9 Provincias que empiezan por A


SELECT
  provincia
FROM provincias
WHERE provincia LIKE 'a%';


-- Consulta 10 Autonomías terminadas en 'ana'

SELECT DISTINCT
  autonomia
FROM provincias
WHERE autonomia LIKE '%ana';



--  

  SELECT CURRENT_USER();


  SELECT
         
         MAX(poblacion)
          FROM provincias;


EXPLAIN
  SELECT provincia FROM provincias WHERE poblacion=(SELECT
         
         MAX(poblacion)
          FROM provincias);


ALTER TABLE provincias ADD INDEX (poblacion);
ALTER TABLE provincias DROP INDEX (poblacion);

EXPLAIN provincias;




DROP FUNCTION madrid;
CREATE FUNCTION madrid()
  RETURNS varchar(31)
  BEGIN
    RETURN (
      SELECT provincia FROM provincias WHERE poblacion=(
        SELECT MAX(poblacion) FROM provincias
      )
    );     
  END;


DROP FUNCTION madrid2;
CREATE FUNCTION madrid2()
  RETURNS varchar(31)
  BEGIN
    RETURN (
      SELECT provincia FROM provincias ORDER BY  poblacion DESC LIMIT 1
    );     
  END;


SELECT BENCHMARK(1e4,madrid()); 

SELECT BENCHMARK(1e4,madrid2()); 


