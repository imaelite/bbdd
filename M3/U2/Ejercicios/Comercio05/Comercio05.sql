﻿--  (43) Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio FNAC (Subconsulta) excluyendo al FNAC


SELECT DISTINCT
  nombre
FROM (SELECT
    cif,
    ciudad
  FROM comercio
  WHERE nombre = 'FNAC') c1
  JOIN comercio
    ON c1.ciudad = comercio.ciudad
    AND c1.cif <> comercio.cif;


--  (27) Genera un listado de los programas que desarrolla Oracle

SELECT
  programa.codigo,
  programa.nombre,
  programa.version
FROM (SELECT
    *
  FROM fabricante
  WHERE nombre = 'oracle') c1
  JOIN desarrolla
    ON c1.id_fab = desarrolla.id_fab
  JOIN programa
    ON desarrolla.codigo = programa.codigo;

SELECT
  programa.*
FROM desarrolla
  JOIN (SELECT
      id_fab
    FROM fabricante
    WHERE nombre = 'oracle') c1 USING (id_fab)
  JOIN programa USING (codigo);


--   (30) ¿Qué fabricante ha desarrollado Freddy Hardest?????????????????????????

  -- mio

SELECT
  nombre
FROM fabricante
  JOIN (SELECT

      id_fab
    FROM programa
      JOIN desarrolla
        ON programa.codigo = desarrolla.codigo
    WHERE nombre = 'Freddy Hardest') c1
    ON c1.id_fab = fabricante.id_fab;

        --   david

          select nombre from (select codigo from programa where nombre='freddy hardest') c1 join desarrolla using(codigo) join fabricante using(id_fab);

          --  (33) ¿Qué medios ha utilizado para registrarse Pepe Pérez?

            SELECT dni FROM cliente WHERE nombre='Pepe Pérez';

            SELECT 
                   medio FROM registra WHERE dni IN(SELECT dni FROM cliente WHERE nombre='Pepe Pérez');


            --  (31) Selecciona el nombre de los programas que se registran por Internet

              SELECT  codigo
                      FROM registra WHERE medio='Internet';

         

              SELECT DISTINCT
                     nombre
                      FROM ( SELECT  codigo
                      FROM registra WHERE medio='Internet')c1 JOIN programa USING(codigo);

              --  david
                select distinct nombre from (select codigo from registra where medio='internet') c1 join programa using(codigo);


              --  (32) Selecciona el nombre de las personas que se registran por Internet

                SELECT
                       dni
                        FROM registra WHERE medio='Internet';

                SELECT 
                       nombre
                        FROM cliente WHERE dni IN (SELECT
                       dni
                        FROM registra WHERE medio='Internet') ORDER BY nombre;

                --  select nombre from(select distinct dni from registra where medio='internet') c1 join cliente using(dni) order by nombre;


                  --  (35) ¿Qué programas han recibido registros por tarjeta postal?

                  SELECT DISTINCT
                         codigo
                          FROM registra WHERE medio='tarjeta postal';

                  SELECT
                         nombre
                          FROM programa JOIN (SELECT DISTINCT
                         codigo
                          FROM registra WHERE medio='tarjeta postal')c1 USING(codigo);

                  --  david

                    SELECT DISTINCT  nombre from (select codigo from registra where medio='tarjeta postal') c1 join programa using(codigo);



                    --  (36) ¿En qué localidades se han registrado productos por Internet?

                      select cif from registra where medio='Internet';

                      SELECT DISTINCT
                             ciudad FROM (select cif from registra where medio='Internet') c1 JOIN comercio USING(cif);

                      --  david
                        select distinct ciudad from (select cif from registra where medio='internet')c1 join comercio using(cif);


                        --  (27) Genera un listado de los programas que desarrolla Oracle

                          SELECT id_fab
                                  FROM fabricante WHERE nombre='Oracle';

                          SELECT 
                                 codigo FROM desarrolla JOIN (SELECT id_fab
                                  FROM fabricante WHERE nombre='Oracle')c1 USING(id_fab);


                          SELECT * FROM (SELECT 
                                 codigo FROM desarrolla JOIN (SELECT id_fab
                                  FROM fabricante WHERE nombre='Oracle')c1 USING(id_fab))c1 JOIN programa USING(codigo);

                          --  david
                            select * from (select codigo from desarrolla where id_fab=(select id_fab from fabricante where nombre='Oracle')) c1 join programa using(codigo);



                            --  (37) Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para los que ha efectuado el registro

                              SELECT 
                                     dni,
                                     codigo
                                      FROM registra WHERE medio='Internet';


                              SELECT 
                                     cliente.nombre,
                                     
                                     programa.nombre
                                      FROM (SELECT 
                                     dni,
                                     codigo
                                      FROM registra WHERE medio='Internet')c1 JOIN cliente USING(dni) JOIN programa ON c1.codigo=programa.codigo;

                              --  david
                                select distinct cliente.nombre, programa.nombre programa from (select dni,codigo from registra where medio='internet')c1 join cliente using(dni) join programa using(codigo);
