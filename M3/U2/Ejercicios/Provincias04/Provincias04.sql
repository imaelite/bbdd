﻿-- Consulta 1 (33) Listado de autonomías cuyas provincias lleven alguna tilde en su nombre

SELECT
DISTINCT
  autonomia
FROM provincias
WHERE LOWER(provincia) LIKE '%á%' COLLATE utf8_bin
OR LOWER(provincia) LIKE '%é%' COLLATE utf8_bin
OR LOWER(provincia) LIKE '%í%' COLLATE utf8_bin
OR LOWER(provincia) LIKE '%ó%' COLLATE utf8_bin
OR LOWER(provincia) LIKE '%ú%' COLLATE utf8_bin;


-- Consulta 2 (31) Provincia más poblada

SELECT
  MAX(poblacion)
FROM provincias;

SELECT
  provincia
FROM provincias
WHERE poblacion = (SELECT
    MAX(poblacion)
  FROM provincias);


-- Consultas 3 (32) Provincia más poblada de las inferiores a 1 millón de habitantes

SELECT
  provincia
FROM provincias
WHERE poblacion = (SELECT
    MAX(poblacion)
  FROM provincias
  WHERE poblacion < 1e6);


-- Consultas 4 (34) Provincia menos poblada de las superiores al millón de habitantes

SELECT
  provincia
FROM provincias
WHERE poblacion = (SELECT
    MIN(poblacion)
  FROM provincias
  WHERE poblacion > 1e6);


-- Consulta 5 (35) ¿En qué autonomía está la provincia más extensa?

SELECT
  autonomia
FROM provincias
WHERE superficie = (SELECT
    MAX(superficie)
  FROM provincias);

-- Consulta 6 (36) ¿Qué provincias tienen una población por encima de la media nacional?

SELECT
  provincia
FROM provincias
WHERE poblacion > (SELECT
    AVG(poblacion)
  FROM provincias);

-- Consulta 7 (37) Densidad de población del país

SELECT
  SUM(poblacion) / SUM(superficie)
FROM provincias;

SELECT
  (SELECT
      SUM(poblacion)
    FROM provincias) / (SELECT
      SUM(superficie)
    FROM provincias);


-- Consulta 8 (38) ¿Cuántas provincias tiene cada comunidad autónoma?

SELECT
  autonomia,
  COUNT(provincia)
FROM provincias
GROUP BY autonomia;

SELECT
  autonomia,
  COUNT(*) n
FROM provincias
GROUP BY autonomia;

-- Consulta 9 (39) Listado del número de provincias por autonomía ordenadas de más a menos provincias y por autonomía en caso de coincidir

SELECT
  COUNT(*) n, autonomia
FROM provincias GROUP BY autonomia ORDER BY n DESC, autonomia;


-- Consulta 10 (40) ¿Cuántas provincias con nombre compuesto tiene cada comunidad autónoma?

SELECT
  autonomia ,COUNT(*)
FROM provincias WHERE provincia LIKE '% %' GROUP BY autonomia;