﻿DROP DATABASE IF EXISTS sedes;
CREATE DATABASE sedes;
USE sedes;

/* creando las bases de datos */

CREATE TABLE ciudad (
  nombre Varchar(30) PRIMARY KEY,
  población Integer
);

CREATE TABLE persona (
  nombre Varchar(30) PRIMARY KEY,
  calle Varchar(30),
  ciudad Varchar(30)
 );

CREATE TABLE compania (
  nombre Varchar(30) PRIMARY KEY,
  ciudad Varchar(30)
);

CREATE TABLE trabaja (
  persona Varchar(30) PRIMARY KEY,
  compañia Varchar(30),
  salario Integer
);

CREATE TABLE supervisa (
  supervisor Varchar(30),
  persona Varchar(30),
  PRIMARY KEY (supervisor,persona)
);

/* DATOS DE LA PRACTICA 2 */

INSERT INTO ciudad VALUES ('CR', 60000);
INSERT INTO ciudad VALUES ('TO', 80000);
INSERT INTO ciudad VALUES ('AB', 70000);
INSERT INTO ciudad VALUES ('CU', 50000);
INSERT INTO ciudad VALUES ('GU', 75000);

INSERT INTO persona VALUES ('Juan', 'Calle1', 'CR');
INSERT INTO persona VALUES ('Eva', 'Calle2', 'CR');
INSERT INTO persona VALUES ('Jose', 'Calle3', 'TO');
INSERT INTO persona VALUES ('Maria', 'Calle4', 'TO');
INSERT INTO persona VALUES ('Ana', 'Calle5', 'AB');
INSERT INTO persona VALUES ('Blas', 'Calle6', 'AB');
INSERT INTO persona VALUES ('Antonio', 'Calle7', 'CU');
INSERT INTO persona VALUES ('Wendy', 'Calle8', 'CU');
INSERT INTO persona VALUES ('Paco', 'Calle9', 'GU');
INSERT INTO persona VALUES ('Marga', 'Calle9', 'GU');
INSERT INTO persona VALUES ('Casimiro', 'Calle11', 'CR');

INSERT INTO compania VALUES ('INDRA', 'CR');
INSERT INTO compania VALUES ('FAGOR', 'TO');
INSERT INTO compania VALUES ('NESTLE', 'AB');

INSERT INTO trabaja VALUES ('Juan', 'INDRA', 40000);
INSERT INTO trabaja VALUES ('Eva', 'INDRA', 30000);
INSERT INTO trabaja VALUES ('Jose', 'FAGOR', 50000);
INSERT INTO trabaja VALUES ('Maria', 'FAGOR', 40000);
INSERT INTO trabaja VALUES ('Ana', 'FAGOR', 18000);
INSERT INTO trabaja VALUES ('Blas', 'NESTLE', 25000);
INSERT INTO trabaja VALUES ('Antonio', 'NESTLE', 15000);
INSERT INTO trabaja VALUES ('Wendy', 'INDRA', 50000);
INSERT INTO trabaja VALUES ('Paco', 'NESTLE', 25000);
INSERT INTO trabaja VALUES ('Marga', 'NESTLE', 50000);

INSERT INTO supervisa VALUES ('Wendy', 'Juan');
INSERT INTO supervisa VALUES ('Wendy', 'Eva');
INSERT INTO supervisa VALUES ('Jose', 'Maria');
INSERT INTO supervisa VALUES ('Maria', 'Ana');
INSERT INTO supervisa VALUES ('Marga', 'Paco');
INSERT INTO supervisa VALUES ('Marga', 'Blas');
INSERT INTO supervisa VALUES ('Blas', 'Antonio');