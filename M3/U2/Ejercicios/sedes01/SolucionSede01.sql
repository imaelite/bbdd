﻿
-- (15) Listar el nombre de la persona y el nombre de su supervisor 

SELECT
  persona,
  supervisor
FROM supervisa;

-- (19) Indicar el nombre de las personas que trabajan para FAGOR

SELECT DISTINCT
  persona
FROM trabaja
WHERE compañia = 'Fagor';

-- (22) Indicar el nombre de las personas que trabajan para FAGOR o para INDRA

SELECT DISTINCT
  persona
FROM trabaja
WHERE compañia = 'Fagor'
OR compañia = 'Indra';

-- (01) Indicar el número de ciudades que hay en la tabla ciudades

SELECT
  COUNT(*)
FROM ciudad;

-- (11) Listar el nombre de las personas y el número de habitantes de la ciudad donde viven

SELECT
  persona.nombre,
  población
FROM persona
  JOIN ciudad
    ON ciudad = ciudad.nombre;

-- (12) Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive

SELECT
  persona.nombre,
  calle,

  población
FROM persona
  JOIN ciudad
    ON ciudad = ciudad.nombre;

-- (17) Indicar el número de ciudades distintas que hay en la tabla compañía

SELECT
  COUNT(DISTINCT ciudad)
FROM compania;

-- (18) Indicar el número de ciudades distintas que hay en la tabla personas

SELECT
  COUNT(DISTINCT ciudad)
FROM persona;

-- 23) Listar la población donde vive cada persona, su salario, su nombre y la compañía para la que trabaja.

-- Ordenar la salida por nombre de la persona y por salario de forma descendente. 

SELECT
  ciudad,
  salario,
  nombre,

  compañia
FROM persona
  JOIN trabaja
    ON nombre = persona
ORDER BY nombre, salario DESC;

    
-- (21) Indicar el número de personas que trabajan para INDRA

  -- Evitaremos duplicados por si la tabla no estuviera normalizada a través de la subconsulta c1 en Access (pegar sólo el último paso).

    SELECT COUNT(DISTINCT persona) FROM trabaja  WHERE compañia='INDRA';


