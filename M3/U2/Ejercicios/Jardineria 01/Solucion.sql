﻿--  (4.07.11) Clientes que no tiene asignado representante de ventas

  SELECT  *
          FROM clientes WHERE CodigoEmpleadoRepVentas IS null;


  --  (4.07.14) Sacar los distintos estados por los que puede pasar un pedido

    SELECT DISTINCT
           Estado
            FROM pedidos;

    --  (4.10.41) ¿De qué ciudades son los clientes?

      SELECT DISTINCT
          
             Ciudad FROM clientes;


      --  (4.07.06) Sacar el nombre de los clientes españoles   ??????????????

        SELECT 
               NombreCliente FROM clientes WHERE Pais='España' OR Pais='Spain'; 


        --  (4.08.01) Obtener el nombre del producto más caro

--  c1
SELECT MAX(PrecioVenta) maximo FROM productos;

SELECT 
       Nombre FROM (SELECT MAX(PrecioVenta) maximo FROM productos) c1 JOIN productos ON c1.maximo=PrecioVenta;


-- david

  SELECT 
       Nombre FROM productos where PrecioVenta=(SELECT MAX(PrecioVenta)  FROM productos);

  --  (4.07.07) Sacar cuántos clientes tiene cada país


    SELECT pais,COUNT(*) FROM clientes GROUP BY Pais;


--  (4.07.10) Sacar el código de empleado y número de clientes al que atiende cada representante de ventas 



  SELECT CodigoEmpleadoRepVentas,COUNT(*) FROM clientes GROUP BY CodigoEmpleadoRepVentas;




  --  (4.09.03) Obtener un listado con el nombre de los empleados junto con el nombre de sus jefes.   

    --  c1
    SELECT DISTINCT
         
           CodigoJefe
            FROM empleados ;
SELECT e0.Nombre,e1.Nombre FROM empleados e0 JOIN empleados e1 ON e0.CodigoJefe=e1.CodigoEmpleado;

--  (4.10.11) Sacar los clientes que residan en la misma ciudad donde hay una oficina, indicando dónde está la oficina.

  --  Ciudad,NombreCliente,LineaDireccion1,LineaDireccion2 (La dirección sería la de la oficina)

  SELECT DISTINCT
        clientes.Ciudad,clientes.NombreCliente, oficinas.LineaDireccion1,oficinas.LineaDireccion2
         FROM clientes JOIN oficinas USING(Ciudad);

  -- david
    select * from (select nombrecliente,ciudad from clientes) c1 join (select ciudad,lineadireccion1,lineadireccion2 from oficinas) c2 using(ciudad);





--  (4.10.17) Sacar cuántos pedidos tiene cada cliente en cada estado.

  SELECT CodigoCliente,Estado,COUNT(*) FROM pedidos GROUP BY CodigoCliente, Estado;