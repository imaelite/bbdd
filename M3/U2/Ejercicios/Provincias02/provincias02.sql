﻿-- Consultas 11 ¿Cuántos caracteres tiene cada nombre de comunidad autónoma? Ordena el resultado por el nombre de la autonomía de forma descendente

SELECT DISTINCT
  autonomia,
  CHAR_LENGTH(autonomia)
FROM provincias
ORDER BY autonomia DESC;


-- Consultas 12 ¿Qué provincias están en autonomías con nombre compuesto?

SELECT DISTINCT
  provincia
FROM provincias
WHERE autonomia LIKE '% %';

-- Consultas 13 ¿Qué provincias tienen nombre compuesto?

SELECT DISTINCT
  provincia
FROM provincias
WHERE provincia LIKE '% %';


-- Consultas 14 ¿Qué provincias tienen nombre simple?

SELECT DISTINCT
  provincia
FROM provincias
WHERE provincia NOT LIKE '% %';


-- Consultas 15 ¿Qué autonomías tienen nombre compuesto? Ordena el resultado alfabéticamente en orden inverso

SELECT DISTINCT
  autonomia
FROM provincias
WHERE autonomia LIKE '% %'
ORDER BY autonomia DESC;


-- Consultas 16 Autonomías que comiencen por 'can' ordenadas alfabéticamente

SELECT DISTINCT
  autonomia
FROM provincias
WHERE autonomia LIKE 'can%'
ORDER BY autonomia;


-- Consultas 17 ¿Qué autonomías tienen provincias de más de un millón de habitantes? Ordénalas alfabéticamente

SELECT DISTINCT
  autonomia
FROM provincias
WHERE poblacion > 1e6
ORDER BY autonomia;

-- Consultas 18 Población del país

SELECT
  SUM(poblacion)
FROM provincias;


-- Consultas 19 ¿Cuántas provincias hay en la tabla?

SELECT
  COUNT(*)
FROM provincias;

-- Consultas 20 ¿Qué comunidades autónomas contienen el nombre de una de sus provincias?



SELECT DISTINCT
  autonomia
FROM provincias
WHERE autonomia LIKE CONCAT('%', provincia, '%');

SELECT DISTINCT
  autonomia
FROM provincias
WHERE LOCATE(provincia,autonomia)>0;


  
  
  
   