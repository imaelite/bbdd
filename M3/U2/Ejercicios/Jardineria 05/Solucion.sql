﻿--  (4.08.08) ¿Qué cliente ha rechazado pedidos por mayor importe?

SELECT
  CodigoPedido
FROM pedidos
WHERE Estado = 'Rechazado';

SELECT
  (Cantidad *
  PrecioUnidad) suma,
  CodigoCliente
FROM detallepedidos
  JOIN (SELECT
      CodigoPedido,
      CodigoCliente
    FROM pedidos
    WHERE Estado = 'Rechazado') c1 USING (CodigoPedido)
GROUP BY c1.CodigoCliente;

SELECT
  MAX(suma)
FROM (SELECT
    (Cantidad *
    PrecioUnidad) suma,
    CodigoCliente
  FROM detallepedidos
    JOIN (SELECT
        CodigoPedido,
        CodigoCliente
      FROM pedidos
      WHERE Estado = 'Rechazado') c1 USING (CodigoPedido)
  GROUP BY c1.CodigoCliente) c1;

--  geruda valley


SELECT
  c1.CodigoCliente
FROM (SELECT
    (Cantidad *
    PrecioUnidad) suma,
    CodigoCliente
  FROM detallepedidos
    JOIN (SELECT
        CodigoPedido,
        CodigoCliente
      FROM pedidos
      WHERE Estado = 'Rechazado') c1 USING (CodigoPedido)
  GROUP BY c1.CodigoCliente) c1
WHERE c1.suma = (SELECT
    MAX(suma)
  FROM (SELECT
      (Cantidad *
      PrecioUnidad) suma,
      CodigoCliente
    FROM detallepedidos
      JOIN (SELECT
          CodigoPedido,
          CodigoCliente
        FROM pedidos
        WHERE Estado = 'Rechazado') c1 USING (CodigoPedido)
    GROUP BY c1.CodigoCliente) c1);

SELECT
  NombreCliente
FROM clientes
  JOIN (SELECT
      c1.CodigoCliente
    FROM (SELECT
        (Cantidad *
        PrecioUnidad) suma,
        CodigoCliente
      FROM detallepedidos
        JOIN (SELECT
            CodigoPedido,
            CodigoCliente
          FROM pedidos
          WHERE Estado = 'Rechazado') c1 USING (CodigoPedido)
      GROUP BY c1.CodigoCliente) c1
    WHERE c1.suma = (SELECT
        MAX(suma)
      FROM (SELECT
          (Cantidad *
          PrecioUnidad) suma,
          CodigoCliente
        FROM detallepedidos
          JOIN (SELECT
              CodigoPedido,
              CodigoCliente
            FROM pedidos
            WHERE Estado = 'Rechazado') c1 USING (CodigoPedido)
        GROUP BY c1.CodigoCliente) c1)) c1 USING (CodigoCliente);


--  (4.10.39) Gama de productos más rechazada

SELECT
  CodigoPedido
FROM pedidos
WHERE Estado = 'rechazado';

SELECT

  CodigoProducto,
  COUNT(*) n
FROM detallepedidos
  JOIN (SELECT
      CodigoPedido
    FROM pedidos
    WHERE Estado = 'rechazado') c1 USING (CodigoPedido)
GROUP BY CodigoProducto
ORDER BY n DESC;


--  david
  select gama from(select codigopedido from pedidos where estado='rechazado') c1 join 
  detallepedidos using(codigopedido) join productos using(codigoproducto) group by gama having sum(cantidad)=(select max(s) from (select max(s) from(select gama, sum(cantidad) s from (select codigopedido from pedidos where estado='rechazado') c1 join detallepedidos using(codigopedido) join productos using(codigoproducto) group by gama) c4));

--  (4.10.43) ¿Cuántos clientes locales y foráneos tiene cada oficina? Indica únicamente el código de la oficina

SELECT
  CodigoOficina,
  COUNT(*)
FROM clientes
  JOIN empleados
    ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado
GROUP BY CodigoOficina;


SELECT
  oficinas.CodigoOficina,
  oficinas.Ciudad,
  clientes.Ciudad



FROM clientes
  JOIN empleados
    ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado
  JOIN oficinas USING (CodigoOficina);



SELECT
  CodigoCliente,
  CodigoPostal
FROM clientes;

SELECT
  CodigoOficina,
  CodigoPostal
FROM oficinas;


SELECT
  *
FROM (SELECT
    CodigoCliente,
    CodigoPostal
  FROM clientes) c1
  JOIN (SELECT
      CodigoOficina,
      CodigoPostal
    FROM oficinas) c2 USING (CodigoPostal);




--  (4.10.37) Código del frutal con el que más se ha facturado

SELECT
  *
FROM productos
WHERE Gama = 'frutales';

--  (02) Mostrar el nombre de aquellos productos que se han pedido menos de 5 veces, ordenados alfabéticamente.


SELECT
  *
FROM pedidos;


SELECT
  CodigoProducto
FROM detallepedidos
GROUP BY CodigoProducto
HAVING COUNT(DISTINCT CodigoPedido) < 5;



SELECT DISTINCT
  Nombre
FROM productos
  JOIN (SELECT
      CodigoProducto
    FROM detallepedidos
    GROUP BY CodigoProducto
    HAVING COUNT(DISTINCT CodigoPedido) < 5) c1 USING (CodigoProducto)
ORDER BY Nombre;


SELECT
  *
FROM productos;

--  david

SELECT DISTINCT
  nombre
FROM (SELECT
    codigoproducto
  FROM detallepedidos
  GROUP BY 1
  HAVING COUNT(*) < 5) c1
  JOIN productos USING (codigoproducto)
ORDER BY 1;



--  (4.10.22) ¿Cuál ha sido el producto más rechazado? Muestra código y nombre


SELECT
  CodigoPedido
FROM pedidos
WHERE Estado = 'rechazado';




SELECT
  MAX(n)
FROM (SELECT
    CodigoProducto,
    COUNT(DISTINCT c1.CodigoPedido) n
  FROM detallepedidos
    JOIN (SELECT
        CodigoPedido
      FROM pedidos
      WHERE Estado = 'rechazado') c1 USING (CodigoPedido)
  GROUP BY CodigoProducto) c1;


      SELECT CodigoProducto,COUNT(DISTINCT c1.CodigoPedido) n FROM detallepedidos JOIN (SELECT CodigoPedido FROM pedidos WHERE Estado='rechazado') c1 USING(CodigoPedido) GROUP BY CodigoProducto WHERE n=(   SELECT MAX(n) FROM (SELECT CodigoProducto,COUNT(DISTINCT c1.CodigoPedido) n FROM detallepedidos JOIN (SELECT CodigoPedido FROM pedidos WHERE Estado='rechazado') c1 USING(CodigoPedido) GROUP BY CodigoProducto)c1);


SELECT
  CodigoProducto,
  COUNT(DISTINCT c1.CodigoPedido) n
FROM detallepedidos
  JOIN (SELECT
      CodigoPedido
    FROM pedidos
    WHERE Estado = 'rechazado') c1 USING (CodigoPedido)
GROUP BY CodigoProducto;

SELECT
  *
FROM productos
  JOIN (SELECT
      CodigoProducto,
      COUNT(DISTINCT c1.CodigoPedido) n
    FROM detallepedidos
      JOIN (SELECT
          CodigoPedido
        FROM pedidos
        WHERE Estado = 'rechazado') c1 USING (CodigoPedido)
    GROUP BY CodigoProducto) c1 USING (CodigoProducto)
WHERE c1.n = 2;




SELECT DISTINCT
  Nombre
FROM productos;



SELECT
  *
FROM clientes;


--  (4.10.24) ¿Con qué producto la empresa gana más dinero? (Sin excluir los pedidos rechazados) Mostrar código y nombre


SELECT
  (Cantidad * PrecioUnidad) s,
  CodigoProducto
FROM detallepedidos
ORDER BY s DESC;



SELECT
  SUM(s) m,
  CodigoProducto
FROM (SELECT
    (Cantidad * PrecioUnidad) s,
    CodigoProducto
  FROM detallepedidos
  ORDER BY s DESC) c1
GROUP BY c1.CodigoProducto
ORDER BY m DESC;

SELECT
  (Cantidad * PrecioUnidad) s,
  CodigoProducto
FROM detallepedidos
GROUP BY CodigoProducto
ORDER BY s DESC;

SELECT
  *
FROM productos
WHERE CodigoProducto = 'OR-247';


SELECT
  codigoproducto,
  nombre
FROM (SELECT
    codigoproducto
  FROM (SELECT
      codigoproducto,
      SUM(cantidad) s
    FROM detallepedidos
    GROUP BY codigoproducto) c1
    JOIN (SELECT
        codigoproducto,
        precioventa - precioproveedor margen
      FROM productos) c2 USING (codigoproducto)
  WHERE s * margen = (SELECT
      MAX(beneficio)
    FROM (SELECT
        codigoproducto,
        s * margen beneficio
      FROM (SELECT
          codigoproducto,
          SUM(cantidad) s
        FROM detallepedidos
        GROUP BY codigoproducto) c1
        JOIN (SELECT
            codigoproducto,
            precioventa - precioproveedor margen
          FROM productos) c2 USING (codigoproducto)) c3)) c4
  JOIN productos USING (codigoproducto);


--  FR-2

SELECT DISTINCT
  NombreCliente,
  CodigoCliente
FROM pedidos
  JOIN detallepedidos USING (codigopedido)
  JOIN clientes USING (CodigoCliente)
WHERE codigoproducto IN ('21636',
'22225',
'30310',
'AR-001',
'AR-002',
'AR-003',
'AR-004',
'AR-005',
'AR-006',
'AR-007',
'AR-008',
'AR-009',
'AR-010',
'FR-17',
'FR-18',
'FR-19',
'FR-20',
'FR-21',
'FR-22',
'OR-001',
'OR-108',
'OR-109',
'OR-110',
'OR-111',
'OR-112',
'OR-113',
'OR-114',
'OR-119',
'OR-120',
'OR-121',
'OR-122',
'OR-123',
'OR-124',
'OR-125',
'OR-126',
'OR-133',
'OR-135',
'OR-137',
'OR-138',
'OR-139',
'OR-140',
'OR-141',
'OR-142',
'OR-145',
'OR-146',
'OR-150',
'OR-198',
'OR-199',
'OR-200',
'OR-201',
'OR-202',
'OR-206',
'OR-207',
'OR-208',
'OR-223',
'OR-248',
'OR-249')
ORDER BY NombreCliente;

--  13

SELECT
  NombreCliente,
  CodigoCliente
FROM clientes
WHERE CodigoCliente IN (19
, 38)
ORDER BY NombreCliente;

SELECT
  MIN(
  PrecioVenta -
  PrecioProveedor)
FROM productos;

SELECT
  PrecioVenta -
  PrecioProveedor,
  CodigoProducto
FROM productos;

/*  21636
22225
30310
AR-001
AR-002
AR-003
AR-004
AR-005
AR-006
AR-007
AR-008
AR-009
AR-010
FR-17
FR-18
FR-19
FR-20
FR-21
FR-22
OR-001
OR-108
OR-109
OR-110
OR-111
OR-112
OR-113
OR-114
OR-119
OR-120
OR-121
OR-122
OR-123
OR-124
OR-125
OR-126
OR-133
OR-135
OR-137
OR-138
OR-139
OR-140
OR-141
OR-142
OR-145
OR-146
OR-150
OR-198
OR-199
OR-200
OR-201
OR-202
OR-206
OR-207
OR-208
OR-223
OR-248
OR-249*/


SELECT DISTINCT
  nombrecliente,
  codigocliente
FROM (SELECT
    codigoproducto
  FROM (SELECT
      codigoproducto,
      precioventa,
      precioproveedor
    FROM productos) productos
  WHERE precioventa - precioproveedor = (SELECT
      MIN(precioventa - precioproveedor)
    FROM productos)) c1
  JOIN (SELECT
      codigoproducto,
      codigopedido
    FROM detallepedidos) detallepedidos USING (codigopreducto)
  JOIN (SELECT
      codigoproducto,
      codigocliente
    FROM pedidos) pedidos USING (codigopedido)
  JOIN (SELECT
      codigocliente,
      nombrecliente
    FROM clientes) clientes USING (codigocliente)
ORDER BY nombrecliente;
SELECT
  *
FROM productos;

CREATE TABLE visitas (
  id_visitas int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(31),
  instante timestamp
);

INSERT INTO visitas (nombre)
  VALUES ('imanol');

UPDATE visitas
SET nombre = 'imanolete'
WHERE nombre = 'imanol';

SELECT
  *
FROM visitas;

CREATE TABLE imanol (
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(30)
);