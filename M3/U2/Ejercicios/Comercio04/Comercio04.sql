﻿--  (48) Calcula la cantidad media de programas por pedido que se distribuyen cuyo código es 7

  SELECT 
        AVG(cantidad)
          FROM distribuye WHERE codigo=7;

  --  (49) Calcula la mínima cantidad de programas de código 7 que se han enviado a los comercios

    --  Recuerda que entendemos que los comercios anotan sus compran en la tabla distribuye y sus ventas en registra. 
    --  Lo distribuido y no registrado, permanece en sus almacenes.
    SELECT 
           MIN(cantidad) FROM distribuye WHERE codigo=7;

    --  (50) Calcula la máxima cantidad de programas de código 7 que se ha distribuido a las tiendas
      SELECT MAX(cantidad) FROM distribuye WHERE codigo=7;
            
--  (51) ¿En cuántos establecimientos se distribuye el programa cuyo código es 7?

  SELECT COUNT(DISTINCT cif) FROM distribuye WHERE codigo=7;

  --  (52) Calcular el número de registros que se han realizado por Internet
    SELECT COUNT(*) FROM registra WHERE medio='internet';

    --  (5) Genera una lista de las ciudades con establecimientos donde se distribuyen programas, sin que aparezcan valores duplicados (utiliza DISTINCT)

      SELECT DISTINCT
             ciudad FROM comercio JOIN distribuye USING(cif);

      --  (5b) Genera una lista de las ciudades con establecimientos donde se han registrado programas, sin que aparezcan valores duplicados (utiliza DISTINCT)

SELECT DISTINCT
            ciudad FROM registra JOIN comercio USING(cif);

--  (38) Genera un listado en el que aparezca cada cliente junto al programa que ha registrado,
  
  --  el medio con el que lo ha hecho y el comercio en el que lo ha adquirido

 SELECT DISTINCT
         cliente.nombre,
    programa.nombre programa,
         
         medio,
         comercio.nombre comercio FROM cliente JOIN registra  USING(dni) JOIN programa USING(codigo) JOIN comercio USING(cif);


--  (41) Nombre de aquellos fabricantes cuyo país es el mismo que Oracle (Subconsulta)

  --  Resuelta con una combinación en vez de una selección (ten en cuenta que para quedarte con Oracle sí que es necesario seleccionar).

  --  Recuerda excluir a Oracle del resultado (el ON admite varias condiciones, no exclusivamente los campos conectados)

  SELECT nombre
         FROM (SELECT id_fab, pais FROM fabricante WHERE nombre='oracle') c1 join fabricante ON c1.pais=fabricante.pais AND c1.id_fab<>fabricante.id_fab;

  --  (42) Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez (Subconsulta)

    SELECT 
           nombre
            FROM cliente WHERE edad =(SELECT edad FROM cliente WHERE nombre='Pepe Pérez') AND nombre <>'Pepe Pérez';

    SELECT 
           nombre
            FROM (SELECT dni,edad FROM cliente WHERE nombre='Pepe Pérez') c1 JOIN cliente ON c1.edad=cliente.edad AND c1.dni<>cliente.dni;

    

 