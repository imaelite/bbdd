﻿--  1- Visualizar el número de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamento. 

SELECT
  dept_no,
  COUNT(*)
FROM emple
GROUP BY dept_no;

--  2. Visualizar los departamentos con más de 5 empleados. Utilizar GROUP BY para agrupar por departamento y HAVING para
--  establecer la condición sobre los grupos.

SELECT
  dept_no,
  COUNT(*)
FROM emple
GROUP BY dept_no
HAVING COUNT(*) > 5;

--  3. Hallar la media de los salarios de cada departamento (utilizar la función avg y GROUP BY). 

SELECT
  AVG(salario),

  dept_no
FROM emple
GROUP BY dept_no;

--  4. Visualizar el nombre de los empleados vendedores del departamento ʻVENTASʼ (Nombre del departamento=ʼVENTASʼ, oficio=ʼVENDEDORʼ). 

SELECT DISTINCT
  emple.apellido
FROM emple
  JOIN depart
    ON emple.dept_no = depart.dept_no
WHERE depart.dnombre = 'VENTAS'
AND oficio = 'VENDEDOR';

--  5. Visualizar el número de vendedores del departamento ʻVENTASʼ (utilizar la función COUNT sobre la consulta anterior). 

SELECT
  COUNT(*)
FROM depart
WHERE dnombre = 'VENTAS';

--  6. Visualizar los oficios de los empleados del departamento ʻVENTASʼ. 

SELECT DISTINCT
  oficio
FROM emple
  JOIN depart
    ON emple.dept_no = depart.dept_no
WHERE dnombre = 'VENTAS';

--  7. A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento cuyo oficio sea ʻEMPLEADOʼ (utilizar GROUP BY para agrupar por departamento.

--  En la cláusula WHERE habrá que indicar que el oficio es ʻEMPLEADOʼ). 

SELECT
  COUNT(*),
  dept_no
FROM emple
WHERE oficio = 'EMPLEADO'
GROUP BY dept_no;

--  8. Visualizar el departamento con más empleados. 

SELECT
  dept_no,
  COUNT(*) a
FROM emple
GROUP BY dept_no
ORDER BY a DESC LIMIT 1;

--  9. Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados. 

-- c1
SELECT
  dept_no,
  SUM(salario) a
FROM emple
GROUP BY dept_no;

SELECT
  AVG(salario)
FROM emple;


SELECT
  c1.dept_no
FROM (SELECT
    dept_no,
    SUM(salario) a
  FROM emple
  GROUP BY dept_no) c1
WHERE c1.a > (SELECT
    AVG(salario)
  FROM emple);

--  10. Para cada oficio obtener la suma de salarios. 

SELECT
  oficio,
  SUM(salario)
FROM emple
GROUP BY oficio;

--  11. Visualizar la suma de salarios de cada oficio del departamento ʻVENTASʼ.

SELECT
  oficio,
  SUM(salario)
FROM emple
  JOIN depart
    ON emple.dept_no = depart.dept_no
WHERE depart.dnombre = 'VENTAS'
GROUP BY oficio;

--  12. Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado. 

SELECT
  COUNT(*) a,
  dept_no
FROM emple
WHERE oficio = 'empleado'
GROUP BY dept_no
ORDER BY a DESC LIMIT 1;

--  13. Mostrar el número de oficios distintos de cada departamento. 

SELECT
  emple.dept_no,
  COUNT(DISTINCT emple.oficio) a
FROM emple
GROUP BY dept_no;

--  14. Mostrar los departamentos que tengan más de dos personas trabajando en el mismo oficio. 

-- c1
SELECT
  dept_no
FROM emple
GROUP BY oficio,
         dept_no
HAVING COUNT(*) > 2;

--  15. Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades. 

SELECT
  estanteria,
  SUM(herramientas.unidades)
FROM herramientas
GROUP BY estanteria;

--  16. Visualizar la estantería con más unidades de la tabla HERRAMIENTAS. (con totales y sin totales) ???????

-- sin totales
SELECT
  estanteria,
  COUNT(*) n_unidades
FROM herramientas
GROUP BY estanteria ORDER BY n_unidades DESC LIMIT 1;

-- con totales
SELECT
  c1.estanteria,
  MAX(c1.n_unidades)
FROM (SELECT
    estanteria,
    COUNT(*) n_unidades
  FROM herramientas
  GROUP BY estanteria) c1;

--  17. Mostrar el número de médicos que pertenecen a cada hospital, ordenado por número descendente de hospital. 

  SELECT cod_hospital,COUNT(*)AS n FROM hospitales GROUP BY cod_hospital ORDER BY n DESC;

  SELECT hospitales.nombre, COUNT(*) n FROM hospitales JOIN medicos ON hospitales.cod_hospital = medicos.cod_hospital GROUP BY hospitales.cod_hospital ORDER BY n DESC;


  --  18. Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene. 

    SELECT 
           nombre,
           especialidad FROM hospitales JOIN medicos ON hospitales.cod_hospital = medicos.cod_hospital GROUP BY nombre , especialidad;

    --  19. Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos 
      
      --  (tendrás que partir de la consulta anterior y utilizar GROUP BY). 
     SELECT 
           nombre,
           especialidad, COUNT(*) n_medicos FROM hospitales JOIN medicos ON hospitales.cod_hospital = medicos.cod_hospital GROUP BY hospitales.cod_hospital , especialidad;




    --  20. Obtener por cada hospital el número de empleados. 

      SELECT 
             nombre, COUNT(*) FROM hospitales JOIN medicos ON hospitales.cod_hospital = medicos.cod_hospital GROUP BY nombre;

      --  21. Obtener por cada especialidad el número de trabajadores

        SELECT especialidad,COUNT(*) FROM medicos GROUP BY especialidad;

        --  22. Visualizar la especialidad que tenga más médicos. 


           SELECT especialidad,COUNT(*) n FROM medicos GROUP BY especialidad;


          SELECT c1.especialidad, MAX(c1.n) FROM (SELECT especialidad,COUNT(*) n FROM medicos GROUP BY especialidad) c1;

          -- 23  ¿Cuál es el nombre del hospital que tiene mayor número de plazas? 
            
            SELECT nombre
                    FROM hospitales HAVING max(num_plazas);
--  24. Visualizar las diferentes estanterías de la tabla HERRAMIENTAS ordenados descendentemente por estantería.


  SELECT DISTINCT
         estanteria
          FROM herramientas ORDER BY estanteria DESC;

  --  25. Averiguar cuántas unidades tiene cada estantería. 

    SELECT 
           estanteria, sum(unidades) 
            FROM herramientas GROUP BY estanteria;


    --  26. Visualizar las estanterías que tengan más de 15 unidades 

      

      SELECT 
           estanteria, sum(unidades) u
            FROM herramientas GROUP BY estanteria HAVING u>15;

      --  27. ¿Cuál es la estantería que tiene más unidades? 

        SELECT MAX(c1.u), c1.estanteria FROM (SELECT 
           estanteria, sum(unidades) u
            FROM herramientas GROUP BY estanteria) c1 ;

        --  28. A partir de las tablas EMPLE y DEPART mostrar los datos del departamento que no tiene ningún empleado. 

   SELECT * FROM depart;
  SELECT * FROM emple;

          SELECT depart.* FROM depart left join emple ON emple.dept_no = depart.dept_no WHERE emp_no IS null;

          --  29. Mostrar el número de empleados de cada departamento. En la salida se debe mostrar también los departamentos que no tienen ningún empleado


            SELECT dept_no, COUNT(*) FROM emple GROUP BY dept_no
            UNION 
            SELECT depart.dept_no, 0 FROM depart left join emple ON emple.dept_no = depart.dept_no WHERE emp_no IS null;


--  30. Obtener la suma de salarios de cada departamento, mostrando las columnas DEPT_NO, SUMA DE SALARIOS y DNOMBRE. 
  
  --  En el resultado también se deben mostrar los departamentos que no tienen asignados empleados. 


  SELECT depart.dept_no,SUM(emple.salario), depart.dnombre FROM depart JOIN emple ON depart.dept_no = emple.dept_no GROUP BY depart.dept_no
   
  UNION

        SELECT depart.dept_no, 0,depart.dnombre FROM depart left join emple ON emple.dept_no = depart.dept_no WHERE emp_no IS null;

  --  31. Utilizar la función IFNULL en la consulta anterior para que en el caso de que un departamento no tenga empleados, aparezca como suma de salarios el valor 0. 


      SELECT depart.dept_no,IFNULL(SUM(emple.salario),0), depart.dnombre FROM depart LEFT JOIN emple ON depart.dept_no = emple.dept_no GROUP BY depart.dept_no;


      --  32. Obtener el número de médicos que pertenecen a cada hospital, mostrando las columnas COD_HOSPITAL, NOMBRE y NÚMERO DE MÉDICOS.
        
        --  En el resultado deben aparecer también los datos de los hospitales que no tienen médicos. 

          SELECT  hospitales.cod_hospital,hospitales.nombre,IFNULL(count(medicos.dni),0) FROM hospitales LEFT JOIN medicos ON hospitales.cod_hospital = medicos.cod_hospital GROUP BY hospitales.cod_hospital;