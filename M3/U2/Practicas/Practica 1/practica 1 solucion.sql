﻿USE practica1;

/*Consulta 1 Mostrar todos los campos y todos los registros de la tabla empleado*/

SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e;


/*Consulta 2  Mostrar todos los campos y todos los registros de la tabla departamento*/

SELECT
  d.dept_no,
  d.dnombre,
  d.loc
FROM depart d;


/*Consulta 3 Mostrar el apellido y oficio de cada empleado.*/

SELECT DISTINCT
  e.apellido,
  e.oficio
FROM emple e;

/*Consulta 4  Mostrar localización y número de cada departamento*/

SELECT
  d.dept_no,
  d.loc
FROM depart d;


/*Consulta 5  Mostrar el número, nombre y localización de cada departamento.
*/

SELECT
  d.dept_no,
  d.dnombre,
  d.loc
FROM depart d;

/*Consulta 6  Indicar el número de empleados que hay*/

SELECT
  COUNT(e.emp_no) AS numeroEmpleados
FROM emple e;


/*Consulta 7  Datos de los empleados ordenados por apellido de forma ascendente*/

SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
ORDER BY e.apellido;


/*Consulta 8 Datos de los empleados ordenados por apellido de forma descendente
*/

SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
ORDER BY e.apellido DESC;


/*Consulta 9  Indicar el numero de departamentos que hay*/


SELECT
  COUNT(*)
FROM depart d;


/*????Consulta 10  Indicar el número de empleados mas el numero de departamentos*/

SELECT
  COUNT(d.dept_no)
FROM depart d;


/*Consulta 11 Datos de los empleados ordenados por número de departamento descendentemente*/


SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
ORDER BY e.dept_no DESC;


/*Consulta 12 Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente*/


SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
ORDER BY e.dept_no DESC,
e.oficio ASC;


/*Consulta 13  Datos de los empleados ordenados por número de departamento descendentemente y por apellido ascendentemente*/


SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
ORDER BY e.dept_no DESC,
e.apellido ASC;


/*Consulta 14 Mostrar los códigos de los empleados cuyo salario sea mayor que 2000.*/

SELECT
  e.emp_no
FROM emple e
WHERE e.salario > 2000;


/*Consulta 15 Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000*/

SELECT
  e.emp_no,
  e.apellido
FROM emple e
WHERE e.salario > 2000;


/*Consulta 16 Mostrar los datos de los empleados cuyo salario este entre 1500 y 2500*/

SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
WHERE e.salario BETWEEN 1500 AND 2500;


/*Consulta 17 Mostrar los datos de los empleados cuyo oficio sea ʻANALISTAʼ.*/

SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
WHERE e.oficio = 'ANALISTA';


/*Consulta 18 Mostrar los datos de los empleados cuyo oficio sea ANALISTA y ganen mas de 2000 €*/


SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
WHERE e.oficio = 'ANALISTA'
AND e.salario > 2000;


/*Consulta 19 Seleccionar el apellido y oficio de los empleados del departamento número 20.*/


SELECT DISTINCT
  e.apellido,
  e.oficio
FROM emple e
WHERE e.dept_no = '20';


/*Consulta 20 Contar el número de empleados cuyo oficio sea VENDEDOR*/


SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
WHERE e.oficio = 'VENDEDOR';


/*Consulta 21 Mostrar todos los datos de los empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma
ascendente.*/

SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
WHERE e.apellido LIKE "M%"
OR e.apellido LIKE "N%"
ORDER BY e.apellido ASC;


/*Consulta 22 Seleccionar los empleados cuyo oficio sea ʻVENDEDORʼ. Mostrar los datos ordenados por apellido de forma ascendente.
*/

SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
WHERE e.oficio = 'VENDEDOR'
ORDER BY e.apellido ASC;


/*Consulta 23 Mostrar los apellidos del empleado que mas gana*/

SELECT
  e.apellido,
  MAX(e.salario)
FROM emple e;


/*Consulta 24 Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ʻANALISTAʼ. Ordenar el resultado por apellido y
oficio de forma ascendente*/

SELECT
  e.emp_no,
  e.apellido,
  e.oficio,
  e.dir,
  e.fecha_alt,
  e.salario,
  e.comision,
  e.dept_no
FROM emple e
WHERE e.dept_no = '10'
AND e.oficio = 'ANALISTA'
ORDER BY e.apellido ASC, e.oficio DESC;

/*Consulta 25 Realizar un listado de los distintos meses en que los empleados se han dado de alta*/

SELECT
DISTINCT
  MONTH(e.fecha_alt)
FROM emple e;

/*Consulta 26  Realizar un listado de los distintos años en que los empleados se han dado de alta*/

SELECT
DISTINCT
  YEAR(e.fecha_alt)
FROM emple e;

/*Consulta 27 Realizar un listado de los distintos días del mes en que los empleados se han dado de alta
*/

SELECT
DISTINCT
  YEAR(e.fecha_alt)
FROM emple e;

/*Consulta 28 Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento
número 20.
*/

SELECT DISTINCT
  e.apellido
FROM emple e
WHERE e.salario > 2000
OR e.dept_no = 20;

/*Consulta 29  Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece*/

SELECT DISTINCT
  e.apellido,
  e.dept_no
FROM emple e;

/*Consulta 30 . Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento
al que pertenece. Ordenar los resultados por apellido de forma descendente*/

SELECT DISTINCT
  e.apellido,
  e.dept_no,
  e.dept_no
FROM emple e
ORDER BY e.apellido DESC;

/*Consulta 31 Listar el número de empleados por departamento. La salida del comando debe ser como la que vemos a continuación*/

SELECT
  e.dept_no,
  COUNT(*) AS numero_de_empleados
FROM emple e
GROUP BY e.dept_no;

/*Consulta 32 Realizar el mismo comando anterior pero obteniendo una salida como la que vemos */

SELECT DISTINCT
  d.dnombre,
  COUNT(*) AS numero_de_empleados
FROM emple e
  JOIN depart d
    ON e.dept_no = d.dept_no
GROUP BY d.dnombre ASC;


/*Consulta 33 Listar el apellido de todos los empleados y ordenarlos por oficio, y por nombre*/

SELECT
  e.apellido
FROM emple e
ORDER BY e.oficio ASC, e.emp_no ASC;


/*Consulta 34 Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ. Listar el apellido de los empleados*/

SELECT
  e.apellido
FROM emple e
WHERE e.apellido LIKE "A%"
GROUP BY e.apellido ASC;


/*Consulta 35 . Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por ʻAʼ o por ’M’. Listar el apellido de los empleados*/


SELECT DISTINCT
  e.apellido
FROM emple e
WHERE e.apellido LIKE "A%"
OR e.apellido LIKE "M%";


/*Consulta 36 Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por ʻZʼ. Listar todos los campos de la tabla
empleados*/


SELECT
  emp_no,
  apellido,
  oficio,
  dir,
  fecha_alt,
  salario,
  comision,
  dept_no
FROM emple e
WHERE e.apellido NOT LIKE "%Z";


/*Consulta 37 Seleccionar de la tabla EMPLE aquellas filas cuyo APELLIDO empiece por ʻAʼ y el OFICIO tenga una ʻEʼ en cualquier
posición. Ordenar la salida por oficio y por salario de forma descendente*/


SELECT
  emp_no,
  apellido,
  oficio,
  dir,
  fecha_alt,
  salario,
  comision,
  dept_no
FROM emple e
WHERE e.apellido LIKE "A%"
AND oficio LIKE "%E%"
ORDER BY oficio, salario DESC;



/*Consulta 38*/


SELECT
  emp_no,
  apellido,
  oficio,
  dir,
  fecha_alt,
  salario,
  comision,
  dept_no
FROM emple e
WHERE e.apellido LIKE "A%"
AND oficio LIKE "%E%"
ORDER BY oficio, salario DESC;