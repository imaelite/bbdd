﻿/* creando las bases de datos */
  DROP DATABASE practica2;
  CREATE DATABASE IF NOT EXISTS practica2;
  USE practica2;

CREATE TABLE Ciudad (
  nombre Varchar(30) PRIMARY KEY,
  población integer
);

CREATE TABLE Persona (
  nombre Varchar(30) PRIMARY KEY,
  calle Varchar(30),
  ciudad Varchar(30),
  CONSTRAINT  FKciudadPersona FOREIGN KEY (ciudad) REFERENCES Ciudad(nombre) on DELETE CASCADE ON UPDATE CASCADE
 );

CREATE TABLE Compañia (
  nombre Varchar(30) PRIMARY KEY,
  ciudad Varchar(30),
   CONSTRAINT  FKcompañiaCiudad FOREIGN KEY (ciudad) REFERENCES ciudad(nombre) on DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE Trabaja (
  persona Varchar(30) PRIMARY KEY,
  compañia Varchar(30),
  salario integer,
  CONSTRAINT  FKtrabajaPersona FOREIGN KEY (persona) REFERENCES persona(nombre) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKtrabajaCompañia FOREIGN KEY (compañia) REFERENCES compañia(nombre) on DELETE CASCADE ON UPDATE CASCADE

);

CREATE TABLE Supervisa (
  supervisor Varchar(30),
  persona Varchar(30),
  PRIMARY KEY (supervisor,persona),
  CONSTRAINT  FKsupervisPersona FOREIGN KEY (persona) REFERENCES persona(nombre) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKsupervisSupervisa FOREIGN KEY (supervisor) REFERENCES persona(nombre) on DELETE CASCADE ON UPDATE CASCADE

);

