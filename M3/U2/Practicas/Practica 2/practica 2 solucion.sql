﻿
-- 1. Indicar el número de ciudades que hay en la tabla ciudades 

SELECT
  COUNT(*)
FROM ciudad;

-- 2. Indicar el nombre de las ciudades que tengan una población por encima de la población media 

SELECT
  nombre
FROM ciudad
WHERE población > (SELECT
    AVG(población)
  FROM ciudad);


-- 3. Indicar el nombre de las ciudades que tengan una población por debajo de la población media 

SELECT
  nombre
FROM ciudad
WHERE población < (SELECT
    AVG(población)
  FROM ciudad);

--  4. Indicar el nombre de la ciudad con la población máxim

SELECT
  nombre
FROM ciudad
WHERE población = (SELECT
    MAX(población)
  FROM ciudad);

-- 5. Indicar el nombre de la ciudad con la población mínima 

SELECT
  nombre
FROM ciudad
WHERE población = (SELECT
    MIN(población)
  FROM ciudad);

--  6. Indicar el número de ciudades que tengan una población por encima de la población media

SELECT
  COUNT(*)
FROM ciudad
WHERE población > (SELECT
    AVG(población)
  FROM ciudad);

-- 7. Indicarme el número de personas que viven en cada ciudad

SELECT
  ciudad,
  COUNT(*)
FROM persona
GROUP BY ciudad;


-- 8. Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañias 

SELECT
  COUNT(persona),
  compañia
FROM trabaja
GROUP BY compañia;


-- 9. Indicarme la compañía que más trabajadores tiene 

-- c1
SELECT
  COUNT(persona) AS e,
  compañia
FROM trabaja
GROUP BY compañia
ORDER BY e DESC LIMIT 1;

-- c2
SELECT
  compañia,
  COUNT(persona) AS k
FROM trabaja
GROUP BY compañia;


SELECT
  compañia,
  COUNT(persona) AS k
FROM trabaja
GROUP BY compañia
HAVING k = (SELECT
    COUNT(persona) AS e
  FROM trabaja
  GROUP BY compañia
  ORDER BY e DESC LIMIT 1);



-- 10. Indicarme el salario medio de cada una de las compañias 

SELECT
  ROUND(AVG(salario)),
  compañia
FROM trabaja
GROUP BY compañia;


--  11. Listarme el nombre de las personas y la población de la ciudad donde viven 

SELECT
  persona.nombre,
  persona.ciudad,
  ciudad.población
FROM persona
  JOIN ciudad
    ON persona.ciudad = ciudad.nombre;


-- 12. Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive 

SELECT
  persona.nombre,
  ciudad,
  población
FROM persona
  JOIN ciudad
    ON persona.ciudad = ciudad.nombre;



-- 13. Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja  


SELECT
  persona.nombre,
  persona.ciudad AS ciudad_persona,
  compañia.ciudad AS ciudad_compania
FROM trabaja
  JOIN persona
    ON trabaja.persona = persona.nombre
  JOIN compañia
    ON compañia.nombre = trabaja.compañia;

-- 14. Realizar el algebra relacional y explicar la siguiente consulta : Listar las personas que trabajan en compañias que están en la misma ciudad de la persona

SELECT
  persona.nombre, persona.ciudad, compañia.ciudad
FROM persona
  JOIN trabaja
  JOIN compañia
    ON persona.nombre = trabaja.persona
    AND trabaja.compañia = compañia.nombre
    AND compañia.ciudad = persona.ciudad
ORDER BY persona.nombre;


--  15. Listarme el nombre de la persona y el nombre de su supervisor 

SELECT
  persona,
  supervisor
FROM supervisa;

-- 16. Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos    

SELECT
  persona,
  supervisor,
  persona.ciudad
FROM supervisa
  JOIN persona
    ON persona.nombre = supervisa.supervisor;

SELECT
  visor.nombre AS nombreDelSupervisor,
  persona.nombre AS nombreDelSupervisado,
  visor.ciudad AS ciudadSupervisor,
  persona.ciudad
FROM persona AS visor
  JOIN supervisa
  JOIN persona
    ON persona = persona.nombre
    AND supervisor = visor.nombre;

--  17 INDICARME EL NUMERO DE CIUDADES DISTINTAS QUE HAY EN LA TABLA COMPAÑIA


SELECT
  COUNT(DISTINCT ciudad)
FROM compañia;

--  18  INDICARME EL NUMERO DE CIUDADES DISTINTAS QUE HAY EN LA TABLA personas

SELECT
  COUNT(DISTINCT ciudad)
FROM persona;

--  19  indicarme el nombre de las personas que trabajan para fagor

SELECT DISTINCT
  persona

FROM trabaja
WHERE compañia = 'fagor';

--  20  indicarme el nombre de las personas que no trabajn en fagor

SELECT DISTINCT
  trabaja.persona

FROM trabaja
  LEFT JOIN (SELECT DISTINCT
      persona

    FROM trabaja
    WHERE compañia = 'fagor') c1
    ON c1.persona = trabaja.persona
WHERE c1.persona IS NULL;


-- 21 Indicarme el número de personas que trabajan para INDRA


SELECT
  COUNT(*)
FROM trabaja
WHERE compañia = 'INDRA';


--  22 iNDICAME EL NOMBRE DE LAS PERSONAS QUE TRABAJAN PARA FAGOR O PARA INDRA

SELECT DISTINCT
  persona
FROM trabaja
WHERE compañia = 'INDRA'
OR compañia = 'fagor';

--  23  listar la población donde vive cada persona, sus salario, su nombre y la compañia para la que trabaja.
--  Ordenar la salida por nombre de la persona y por su salario de froma descendente.

SELECT
  persona.nombre,
  población,
  trabaja.compañia,
  salario
FROM ciudad
  JOIN persona
    ON ciudad.nombre = persona.ciudad
  JOIN trabaja
    ON persona.nombre = trabaja.persona
ORDER BY ciudad.nombre, trabaja.salario DESC;