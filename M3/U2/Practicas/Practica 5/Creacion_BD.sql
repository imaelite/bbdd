﻿DROP DATABASE IF EXISTS practica5;
CREATE DATABASE practica5;
USE practica5;

CREATE TABLE federacion(
nombre char(9) PRIMARY KEY,
  direccion   varchar(50),
  telefono int
);

CREATE TABLE miembro(
  dni char(9) PRIMARY KEY,
  nombre varchar(50),
  titulacion varchar(50)
  )

  CREATE TABLE composicion(
nombre char(9),
    dni char(9),
    cargo varchar(50),
    fecha_inicio date,
    PRIMARY KEY(nombre,dni)
  );

  ALTER TABLE composicion ADD FOREIGN KEY (nombre) REFERENCES federacion(nombre) ON DELETE CASCADE ON UPDATE CASCADE;

    ALTER TABLE composicion ADD FOREIGN KEY (dni) REFERENCES miembro(dni) ON DELETE CASCADE ON UPDATE CASCADE;