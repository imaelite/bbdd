﻿--  1

SELECT
  *
FROM composicion;

SELECT
  miembro.nombre
FROM composicion
  JOIN miembro
    ON composicion.dni = miembro.dni
WHERE cargo = 'A1ECB5F5A2SR5YRZ';

SELECT DISTINCT
  miembro.nombre
FROM (SELECT
    *
  FROM composicion
  WHERE cargo = 'A1ECB5F5A2SR5YRZ') c1
  JOIN miembro USING (dni);

--  2
SELECT
  *
FROM composicion;


SELECT
  direccion
FROM (SELECT
    *
  FROM composicion
  WHERE cargo = '30S') c1
  JOIN federacion USING (nombre);

--  3

SELECT
  *
FROM composicion;

SELECT
  nombre
FROM composicion
WHERE cargo = '30S';

SELECT
  nombre
FROM federacion
  LEFT JOIN (SELECT
      nombre
    FROM composicion
    WHERE cargo = '30S') c1 USING (nombre)
WHERE c1.nombre IS NULL;


--  4

SELECT
  c1.dorsal,
  COUNT(DISTINCT c1.código) n
FROM lleva c1
GROUP BY c1.dorsal
HAVING n = (SELECT
    COUNT(DISTINCT código)
  FROM lleva);

SELECT
  nombre,
  COUNT(DISTINCT cargo) n
FROM composicion
GROUP BY cargo
HAVING n = (SELECT
    COUNT(DISTINCT cargo)
  FROM composicion);




--  5
SELECT
  *
FROM composicion;


--  c1

SELECT
  nombre
FROM composicion
WHERE cargo = 'T330O1H39635QC';

--  c2

SELECT
  nombre
FROM composicion
WHERE cargo = '30S';

SELECT
  nombre
FROM (SELECT
    *
  FROM composicion
  WHERE cargo = 'T330O1H39635QC') c1
  NATURAL JOIN (SELECT
      nombre
    FROM composicion
    WHERE cargo = '30S') c2;