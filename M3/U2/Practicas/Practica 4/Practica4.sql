﻿--  1 Averigua el DNI de todos los clientes.

SELECT dni
        FROM cliente;
--  2 Consulta todos los datos de todos los programas. 

  SELECT * FROM programa;

--  3 Obtén un listado con los nombres de todos los programas. 


  SELECT * FROM programa;

--  4 Genera una lista con todos los comercios. 

  SELECT * FROM comercio;


--  5 Genera una lista de las ciudades con establecimientos donde se venden programas, sin que aparezcan valores duplicados (utiliza DISTINCT). 

SELECT DISTINCT
       ciudad FROM distribuye JOIN comercio ON distribuye.cif = comercio.cif;

--  6 Obtén una lista con los nombres de programas, sin que aparezcan valores duplicados (utiliza DISTINCT). 


  SELECT DISTINCT
         nombre
          FROM programa;

--  7 Obtén el DNI más 4 de todos los clientes. 

  SELECT dni + 4
         
         FROM cliente;


--  8 Haz un listado con los códigos de los programas multiplicados por 7. 

SELECT codigo * 4
       
        FROM programa;

--  9 ¿Cuáles son los programas cuyo código es inferior o igual a 10? 

  SELECT * FROM programa WHERE codigo <=10;


 -- 10 ¿Cuál es el programa cuyo código es 11? 

  SELECT * FROM programa WHERE codigo=11;


--  11 ¿Qué fabricantes son de Estados Unidos? 

  SELECT 
         nombre
         FROM fabricante WHERE pais='Estados Unidos';

 -- 12 ¿Cuáles son los fabricantes no españoles? Utilizar el operador IN. 

  SELECT * FROM fabricante WHERE pais NOT IN ('España');

--  13 Obtén un listado con los códigos de las distintas versiones de Windows. 

SELECT codigo,
       
       version FROM programa WHERE nombre='Windows';



--  14 ¿En qué ciudades comercializa programas El Corte Inglés? 


  SELECT 
         ciudad FROM comercio WHERE nombre='El Corte Inglés';

--  15 ¿Qué otros comercios hay, además de El Corte Inglés? Utilizar el operador IN. 

  SELECT 
         nombre
         FROM comercio WHERE nombre <> 'El Corte Inglés';


--  16 Genera una lista con los códigos de las distintas versiones de Windows y Access. Utilizar el operador IN. 

  SELECT codigo,
       
       version, nombre FROM programa WHERE nombre IN('Windows','Access');


--  17 Obtén un listado que incluya los nombres de los clientes de edades comprendidas entre 10 y 25 y de los mayores de 50 años.
  --  Da una solución con BETWEEN y otra sin BETWEEN. 

SELECT 
       nombre
        FROM cliente WHERE edad BETWEEN 10 AND 25 OR edad > 50;


SELECT 
       nombre
        FROM cliente WHERE (edad >= 10 AND edad <=25) OR edad > 50;


--  18 Saca un listado con los comercios de Sevilla y Madrid. No se admiten valores duplicados. 

  SELECT * FROM comercio WHERE ciudad IN ('Sevilla','Madrid');



--  19 ¿Qué clientes terminan su nombre en la letra “o”? 

SELECT nombre FROM cliente WHERE LEFT(REVERSE(nombre),1)='o';

--  20 ¿Qué clientes terminan su nombre en la letra “o” y, además, son mayores de 30 años? 

 SELECT nombre FROM cliente WHERE LEFT(REVERSE(nombre),1)='o' AND edad>30;

--  21 Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A o por una W. 

  SELECT * FROM programa WHERE LEFT(REVERSE(version),1)='i' OR (LEFT(nombre,1)='A' OR LEFT(nombre,1)='W');

--  22 Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A y termine por una S. 

    SELECT * FROM programa WHERE LEFT(REVERSE(version),1)='i' OR (LEFT(nombre,1)='A' OR LEFT(nombre,1)='S');

--  23 Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, y cuyo nombre no comience por una A. 

   SELECT * FROM programa WHERE LEFT(REVERSE(version),1)='i' OR LEFT(nombre,1)<>'A';

--  24 Obtén una lista de empresas por orden alfabético ascendente. 

  SELECT * FROM fabricante ORDER BY nombre ASC;


 -- 25 Genera un listado de empresas por orden alfabético descendente. 

  SELECT 
         nombre
         FROM fabricante ORDER BY nombre;


--  26 Obtén un listado de programas por orden de versión 

  SELECT * FROM programa GROUP BY nombre,version;


--  27 Genera un listado de los programas que desarrolla Oracle. 
  SELECT * FROM fabricante WHERE nombre='Oracle';


--  28 ¿Qué comercios distribuyen Windows? 

  SELECT 
         comercio.nombre
          FROM distribuye JOIN programa ON distribuye.codigo = programa.codigo JOIN comercio ON distribuye.cif = comercio.cif WHERE programa.nombre='Windows';

 

--  29 Genera un listado de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid. 

  SELECT nombre,COUNT(*) FROM programa JOIN distribuye ON programa.codigo = distribuye.codigo GROUP BY nombre;



--  30 ¿Qué fabricante ha desarrollado Freddy Hardest? 

  SELECT 
         fabricante.nombre FROM fabricante JOIN desarrolla ON fabricante.id_fab = desarrolla.id_fab JOIN programa ON desarrolla.codigo = programa.codigo WHERE programa.nombre='Freddy Hardest';




 -- 31 Selecciona el nombre de los programas que se registran por Internet.
  
  SELECT 
         nombre
          FROM registra JOIN programa ON registra.codigo = programa.codigo WHERE medio='Internet';
     
--  32 Selecciona el nombre de las personas que se registran por Internet. 

SELECT 
       nombre
        FROM cliente JOIN registra ON cliente.dni = registra.dni WHERE medio='Internet';

--  33 ¿Qué medios ha utilizado para registrarse Pepe Pérez? 

  SELECT 
       medio
        FROM cliente JOIN registra ON cliente.dni = registra.dni WHERE nombre='Pepe Pérez';

--  34 ¿Qué usuarios han optado por Internet como medio de registro? 

  SELECT 
       nombre
        FROM cliente JOIN registra ON cliente.dni = registra.dni WHERE medio='Internet';

--  35 ¿Qué programas han recibido registros por tarjeta postal? 

  SELECT * FROM programa JOIN registra ON programa.codigo = registra.codigo WHERE medio='tarjeta postal';


--  36 ¿En qué localidades se han vendido productos que se han registrado por Internet? 

  SELECT DISTINCT
         ciudad FROM registra JOIN distribuye ON registra.cif = distribuye.cif JOIN comercio ON registra.cif = comercio.cif WHERE medio='Internet';



--  37 Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para los que ha efectuado el registro. 


  SELECT DISTINCT cliente.nombre, programa.nombre FROM registra JOIN cliente ON registra.dni = cliente.dni JOIN programa ON registra.codigo = programa.codigo  WHERE registra.medio='Internet';


 -- 38 Genera un listado en el que aparezca cada cliente junto al programa que ha registrado, 
  --  el medio con el que lo ha hecho y el comercio en el que lo ha adquirido. 

SELECT DISTINCT cliente.nombre, programa.nombre, medio, comercio.nombre FROM registra JOIN cliente ON registra.dni = cliente.dni JOIN programa ON registra.codigo = programa.codigo JOIN comercio ON registra.cif = comercio.cif;


--  39 Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle. 

  SELECT 
         DISTINCT
         ciudad FROM programa JOIN distribuye ON programa.codigo = distribuye.codigo JOIN comercio ON distribuye.cif = comercio.cif
  JOIN desarrolla ON programa.codigo = desarrolla.codigo JOIN fabricante ON desarrolla.id_fab = fabricante.id_fab WHERE fabricante.nombre='Oracle';


--  40 Obtén el nombre de los usuarios que han registrado Access XP. 

  SELECT * FROM programa;

  SELECT * FROM registra JOIN programa ON registra.codigo = programa.codigo JOIN cliente ON registra.dni = cliente.dni WHERE programa.nombre='Access';


--  41 Nombre de aquellos fabricantes cuyo país es el mismo que ʻOracleʼ. (Subconsulta). 

SELECT  DISTINCT pais FROM fabricante WHERE fabricante.nombre='Oracle';
SELECT
       nombre
        FROM fabricante WHERE pais=(SELECT  DISTINCT pais FROM fabricante WHERE fabricante.nombre='Oracle');


--  42 Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez. (Subconsulta). 

  SELECT
         edad FROM cliente WHERE nombre='Pepe Pérez';

  SELECT * FROM cliente WHERE edad=(SELECT
         edad FROM cliente WHERE nombre='Pepe Pérez');

--  43 Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio ʻFNACʼ. (Subconsulta). 

  SELECT 
         ciudad FROM comercio WHERE nombre='FNAC';

  SELECT 
         nombre
          FROM comercio WHERE ciudad=(SELECT 
         ciudad FROM comercio WHERE nombre='FNAC');

--  44 Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente ʻPepe Pérezʼ. (Subconsulta). 

  SELECT medio
  FROM cliente JOIN registra ON cliente.dni = registra.dni WHERE nombre='Pepe Pérez';

  SELECT 
         nombre FROM cliente JOIN registra ON cliente.dni = registra.dni WHERE medio IN ( SELECT medio
  FROM cliente JOIN registra ON cliente.dni = registra.dni WHERE nombre='Pepe Pérez');

  
--  45 Obtener el número de programas que hay en la tabla programas. 


  SELECT COUNT(*) FROM programa;

--  46 Calcula el número de clientes cuya edad es mayor de 40 años. 

  SELECT COUNT(*) FROM cliente WHERE edad >40;


--  47 Calcula el número de productos que ha vendido el establecimiento cuyo CIF es 1. 

  SELECT COUNT(programa.codigo)
          FROM programa JOIN distribuye ON programa.codigo = distribuye.codigo WHERE cif=1;
--  48 Calcula la media de programas que se venden cuyo código es 7. 

  SELECT AVG(cantidad)  FROM programa JOIN distribuye ON programa.codigo = distribuye.codigo WHERE distribuye.codigo=7;


--  49 Calcula la mínima cantidad de programas de código 7 que se ha vendido
  
  SELECT min(cantidad)  FROM programa JOIN distribuye ON programa.codigo = distribuye.codigo WHERE distribuye.codigo=7;

--  50 Calcula la máxima cantidad de programas de código 7 que se ha vendido. 

    SELECT MAX(cantidad)  FROM programa JOIN distribuye ON programa.codigo = distribuye.codigo WHERE distribuye.codigo=7;

--  51 ¿En cuántos establecimientos se vende el programa cuyo código es 7? 

  SELECT COUNT(DISTINCT comercio.nombre)
          FROM programa JOIN distribuye ON programa.codigo = distribuye.codigo JOIN comercio ON distribuye.cif = comercio.cif WHERE programa.codigo=7;
--  52 Calcular el número de registros que se han realizado por Internet. 
  SELECT COUNT(*) FROM registra WHERE medio='Internet';

--  53 Obtener el número total de programas que se han vendido en ʻSevillaʼ. 

  SELECT  COUNT(*) FROM programa JOIN distribuye ON programa.codigo = distribuye.codigo JOIN comercio ON distribuye.cif = comercio.cif WHERE ciudad='Sevilla';

--  54 Calcular el número total de programas que han desarrollado los fabricantes cuyo país es ʻEstados Unidosʼ. 

  SELECT COUNT(programa.codigo) FROM programa JOIN desarrolla ON programa.codigo = desarrolla.codigo JOIN fabricante ON desarrolla.id_fab = fabricante.id_fab WHERE pais='Estados Unidos';

--  55 Visualiza el nombre de todos los clientes en mayúscula. En el resultado de la consulta debe aparecer también la longitud de la cadena nombre. 

  SELECT UPPER(nombre), CHAR_LENGTH(nombre) FROM cliente;

  --  56 Con una consulta concatena los campos nombre y versión de la tabla PROGRAMA. 

    SELECT CONCAT(nombre,' ',version)
            FROM programa;