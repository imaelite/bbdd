﻿  --  1

    --  c1
   SELECT * FROM articulo WHERE año='30VISBCD20';

  --  c2

    SELECT * FROM tema WHERE descripcion='ETGZ17WF439CWZ13Z8FIPL1G02';

    SELECT * FROM tema;

    SELECT 
           c1.titulo_art FROM ( SELECT * FROM articulo WHERE año='30VISBCD20') c1 JOIN ( SELECT * FROM tema WHERE descripcion='ETGZ17WF439CWZ13Z8FIPL1G02') c2 USING(codtema);


    --  2

      SELECT * FROM articulo;

      SELECT referencia,codtema FROM articulo; 

      SELECT codtema
              FROM tema;
