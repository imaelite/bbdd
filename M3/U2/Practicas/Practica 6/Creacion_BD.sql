﻿DROP DATABASE IF EXISTS practica6;
CREATE DATABASE practica6;
USE practica6;

CREATE TABLE autor(
dni char(9) PRIMARY KEY,
  nombre   varchar(50),
    universidad   varchar(50)
);


CREATE TABLE tema(
  codtema char(9) PRIMARY KEY,
  descripcion varchar(50)
  )


  CREATE TABLE revista(
referencia char(9)  PRIMARY KEY,
    titulo_rev char(9),
    editorial varchar(50)
  );

    CREATE TABLE articulo(
      referencia char(9),
      dni char(9),
      codtema char(9),
        titulo_art varchar(50),
      año varchar(50),
      volumen  varchar(50),
      numero int,
      paginas int,
      PRIMARY KEY(referencia,dni,codtema));


  ALTER TABLE articulo ADD FOREIGN KEY (referencia) REFERENCES revista(referencia) ON DELETE CASCADE ON UPDATE CASCADE;

    ALTER TABLE articulo ADD FOREIGN KEY (dni) REFERENCES autor(dni) ON DELETE CASCADE ON UPDATE CASCADE;

      ALTER TABLE articulo ADD FOREIGN KEY (codtema) REFERENCES tema(codtema) ON DELETE CASCADE ON UPDATE CASCADE;
