﻿

-- 1.1 nombre y edad de los ciclistas que han ganado etap 
  SELECT DISTINCT
  c2.nombre,
  c2.edad
FROM (SELECT
    dorsal
  FROM etapa) c1
  JOIN (SELECT
      dorsal,
      nombre,
      edad
    FROM ciclista) c2
    ON c1.dorsal = c2.dorsal;


-- 1.2 nombre y edad de los ciclistas que han ganado puertos

SELECT DISTINCT
  c2.nombre,
  c2.edad
FROM (SELECT
    dorsal
  FROM puerto) c1
  JOIN (SELECT
      dorsal,
      nombre,
      edad
    FROM ciclista) c2
    ON c1.dorsal = c2.dorsal;

-- 1.3 nombre y edad de loc ciclistas que han ganado etapas y puertos

SELECT
  c1.nombre,
  c1.edad
FROM (SELECT DISTINCT
    nombre,
    edad
  FROM etapa
    JOIN ciclista
      ON etapa.dorsal = ciclista.dorsal) AS c1
  NATURAL JOIN (SELECT DISTINCT
      nombre,
      edad
    FROM puerto
      JOIN ciclista
        ON puerto.dorsal = ciclista.dorsal) AS c2;

-- idea feliz

SELECT DISTINCT
  nombre,
  edad
FROM ciclista
  JOIN etapa
    ON ciclista.dorsal = etapa.dorsal
  JOIN puerto
    ON ciclista.dorsal = puerto.dorsal;

-- 1.4  listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa

-- con on

SELECT DISTINCT
  director
FROM etapa
  JOIN ciclista
    ON etapa.dorsal = ciclista.dorsal
  JOIN equipo
    ON ciclista.nomequipo = equipo.nomequipo;

-- bien
SELECT
  e.director
FROM equipo e
WHERE e.nomequipo IN (SELECT DISTINCT
    c.nomequipo
  FROM ciclista c
    JOIN (SELECT
        dorsal
      FROM etapa) e1
      ON e1.dorsal = c.dorsal);

-- 1.5 Dorsal y nombre de los ciclistas qua hayan llevado algun maillot

-- con on
SELECT DISTINCT
  ciclista.dorsal,
  nombre
FROM ciclista
  JOIN lleva
    ON ciclista.dorsal = lleva.dorsal;


-- bien

SELECT
  c.dorsal,
  c.nombre
FROM ciclista c
  JOIN (SELECT DISTINCT
      l.dorsal
    FROM lleva l) l
    ON l.dorsal = c.dorsal;

-- 1.6 Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo

-- on
SELECT DISTINCT
  ciclista.dorsal,
  nombre
FROM ciclista
  JOIN lleva
    ON ciclista.dorsal = lleva.dorsal
  JOIN maillot
    ON lleva.código = maillot.código
WHERE color = 'Amarillo';

-- bien 

-- sacamos los amarillos

SELECT DISTINCT
  l.dorsal
FROM lleva l
  JOIN (SELECT
      código
    FROM maillot
    WHERE color = 'Amarillo') c1
    ON l.código = c1.código;

-- total

SELECT
  c.dorsal,
  c.nombre
FROM ciclista c
  JOIN (SELECT DISTINCT
      l.dorsal
    FROM lleva l
      JOIN (SELECT 
          código
        FROM maillot
        WHERE color = 'Amarillo') c1
        ON l.código = c1.código) c1
    ON c.dorsal = c1.dorsal;


-- 1.7 Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas

SELECT DISTINCT
  dorsal
FROM lleva; -- c1

SELECT DISTINCT
  e.dorsal
FROM etapa e; -- c2

SELECT
  *
FROM (SELECT DISTINCT
    dorsal
  FROM lleva) c1
  NATURAL JOIN (SELECT DISTINCT
      e.dorsal
    FROM etapa e) c2;

-- 1.8 indicar el numetapa de etapas que tengan puerto

SELECT DISTINCT
 p.numetapa
FROM puerto p;

-- 1.9 indicar los kms de las etapas que hayan ganado ciclistas del banesto y que tengan puertos

SELECT DISTINCT
  kms
FROM etapa
  JOIN ciclista
    ON etapa.dorsal = ciclista.dorsal
  JOIN puerto
    ON etapa.numetapa = puerto.numetapa
WHERE nomequipo = 'Banesto';


-- bien

-- etapas con puerto

SELECT DISTINCT
  e.numetapa
FROM etapa e
  JOIN puerto p
    ON e.numetapa = p.numetapa; -- c1

SELECT
  c.dorsal
FROM ciclista c
WHERE c.nomequipo = 'Banesto'; -- c2


SELECT DISTINCT
  e.kms
FROM etapa e
  JOIN puerto p
    ON e.numetapa = p.numetapa
WHERE e.dorsal IN (SELECT
    c.dorsal
  FROM ciclista c
  WHERE c.nomequipo = 'Banesto');


-- 1.10 listar el numero de ciclistas que hayan ganado alguna etapa con puerto

SELECT
   COUNT(DISTINCT etapa.dorsal) 
FROM puerto
  JOIN etapa
    ON puerto.numetapa = etapa.numetapa;

-- 1.11 Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto 

-- on

SELECT
  nompuerto
FROM puerto
  JOIN ciclista
    ON puerto.dorsal = ciclista.dorsal
WHERE nomequipo = 'Banesto';

-- bien 

SELECT
  c.dorsal
FROM ciclista c
WHERE c.nomequipo = 'Banesto';


SELECT
  p.nompuerto
FROM puerto p
WHERE p.dorsal IN (SELECT
    c.dorsal
  FROM ciclista c
  WHERE c.nomequipo = 'Banesto');


-- 1.12 Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con mas de 200 km4;

-- on
SELECT
  etapa.numetapa
FROM etapa
  JOIN puerto
    ON etapa.numetapa = puerto.numetapa
  JOIN ciclista
    ON etapa.dorsal = ciclista.dorsal
WHERE nomequipo = 'Banesto'
AND etapa.kms > 200;

-- c1

SELECT
  c.dorsal
FROM ciclista c
WHERE c.nomequipo = 'Banesto';

-- c2

SELECT
  e.numetapa,
  e.dorsal
FROM etapa e
  JOIN puerto
    ON e.numetapa = puerto.numetapa
WHERE e.kms > 200;

-- total

SELECT
 COUNT(DISTINCT c2.numetapa)
FROM (SELECT
    e.numetapa,
    e.dorsal
  FROM etapa e
    JOIN puerto
      ON e.numetapa = puerto.numetapa
  WHERE e.kms > 200) c2
  JOIN (SELECT
      c.dorsal
    FROM ciclista c
    WHERE c.nomequipo = 'Banesto') c1
    ON c1.dorsal = c2.dorsal;


 
