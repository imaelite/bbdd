﻿-- creacion de base de datos

DROP DATABASE IF EXISTS consultasaccion1;
CREATE DATABASE consultasaccion1;

USE consultasaccion1;


CREATE TABLE productos (
  id int AUTO_INCREMENT PRIMARY KEY,
  cantidad int,
  nombre varchar(50),
  precio int,
  peso int,
  grupo char(9)
);

CREATE TABLE cliente (
  id int AUTO_INCREMENT PRIMARY KEY,

  nombre varchar(50),
  apellidos varchar(50),
  nombrecompleto varchar(100),
  cantidad int
);

CREATE TABLE venden (
  productos int,
  cliente int,
  cantidad int,
  fecha date,
  total float,

  PRIMARY KEY (productos, cliente)
);
-- fk
ALTER TABLE venden
ADD CONSTRAINT FKvendenproductos FOREIGN KEY (productos)
REFERENCES productos (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE venden
ADD CONSTRAINT FKvendencliente FOREIGN KEY (cliente)
REFERENCES cliente (id) ON DELETE CASCADE ON UPDATE CASCADE;


-- añadir campo descuento


ALTER TABLE cliente ADD COLUMN descuento int;

UPDATE cliente
SET descuento = cantidad;
-- 3 campos derivados a null

UPDATE productos
SET cantidad = NULL;

UPDATE cliente
SET cantidad = NULL;

UPDATE cliente
SET nombrecompleto = NULL;

-- 4 


--  a
SELECT
  precio,
  venden.cantidad,
  venden.total
FROM venden
  JOIN productos
    ON venden.productos = productos.id;

UPDATE venden
JOIN productos
  ON venden.productos = productos.id
SET venden.total =  venden.cantidad*productos.precio;

--  b

SELECT
  CONCAT(nombre, ' ', apellidos)
FROM cliente;

UPDATE cliente
SET nombrecompleto = CONCAT(nombre, ' ', apellidos);

SELECT
  *
FROM cliente;

--  5

--  5.a
SELECT
  *
FROM cliente;

SELECT
  productos.precio,
  venden.cantidad,
  c.descuento,
  productos.precio * venden.cantidad * (1 - (c.descuento / 100))
FROM productos
  JOIN venden
    ON productos.id = venden.productos
  JOIN cliente c
    ON productos = venden.productos;


UPDATE productos
JOIN venden
  ON productos.id = venden.productos
JOIN cliente c
  ON productos = venden.productos
SET venden.total = productos.precio * venden.cantidad * (1 - c.descuento);

--  5.b 
SELECT
  *
FROM productos;



SELECT
  venden.productos,
  SUM(cantidad) suma
FROM venden
GROUP BY venden.productos;

UPDATE (SELECT
    venden.productos,
    SUM(productos.cantidad) suma
  FROM venden
    JOIN productos
      ON venden.productos = productos.id
  GROUP BY venden.productos) c1
JOIN productos
  ON c1.productos = productos.id
SET productos.cantidad = c1.suma;

-- 5.c



SELECT
  *
FROM cliente;

SELECT
  cliente,
  SUM(cantidad)
FROM venden
GROUP BY cliente;


UPDATE (SELECT
    cliente,
    SUM(cantidad) suma
  FROM venden
  GROUP BY cliente) C1
JOIN cliente
  ON C1.cliente = cliente.id
SET cliente.cantidad = suma;


--  6

ALTER TABLE productos ADD COLUMN precioTotal float;

ALTER TABLE cliente ADD COLUMN precioTotal float;

--  7

--  a
SELECT
  *
FROM productos;

SELECT productos,
       
       SUM(total) total FROM venden GROUP BY productos;


UPDATE (SELECT productos,
       
       SUM(total) total FROM venden GROUP BY productos) c1 JOIN productos ON c1.productos=productos.id SET productos.precioTotal=c1.total;


--  b


SELECT * FROM cliente;
SELECT cliente,
       
       SUM(total) total FROM venden GROUP BY cliente;

UPDATE (SELECT cliente,
       
       SUM(total) total FROM venden GROUP BY cliente) c1 JOIN cliente ON c1.cliente=cliente.id SET cliente.precioTotal=c1.total;