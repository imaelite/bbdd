﻿
-- 1.1 Numero de vendedores cuya fecha de alta sea en febrero de cualquier año

SELECT
  COUNT(*)
FROM ventas
WHERE MONTH(Fecha) = 2;

-- 1.2  Numero de vendedores guapos y feos

SELECT
  `Guap@`,
  COUNT(*)
FROM vendedores
GROUP BY `Guap@`;

-- 1.3 El nombre del producto mas caro


SELECT
  MAX(Precio)
FROM productos;

SELECT
  NomProducto
FROM productos
WHERE Precio = (SELECT
    MAX(Precio)
  FROM productos);

-- 1.4 el precio medio de los productos por grupo

SELECT
  AVG(Precio),
  IdGrupo
FROM productos
GROUP BY IdGrupo;

-- 1.5 Indica que grupo tiene el producto que se haya vendido alguna vez

--  on
SELECT
  IdGrupo,
  IdProducto
FROM productos
  JOIN ventas
    ON productos.IdProducto = ventas.`Cod Producto`
GROUP BY IdGrupo;

-- bien
SELECT DISTINCT

  IdGrupo
FROM productos
  JOIN (SELECT
      `Cod Producto`
    FROM ventas) c1
    ON IdProducto = c1.`Cod Producto`;


-- 1.6 idicar cuales son los grupos de los cuales no se han vendido ningun producto

--  on
SELECT
  IdGrupo
FROM productos
  LEFT JOIN ventas
    ON productos.IdProducto = ventas.`Cod Producto`
WHERE `Cod Vendedor` IS NULL;

-- bien

SELECT
  IdGrupo
FROM productos
  LEFT JOIN (SELECT
      `Cod Producto`
    FROM ventas) c1
    ON IdProducto = c1.`Cod Producto`
WHERE c1.`Cod Producto` IS NULL;

-- 1.7 Numero de poblaciones cuyos vendedores son guapos

SELECT
  COUNT(DISTINCT Poblacion)
FROM vendedores
WHERE `Guap@`;

-- 1.8 Nombre de la población que tiene más vendedores casados

SELECT
  COUNT(*) n,
  Poblacion
FROM vendedores
WHERE EstalCivil = 'casado'
GROUP BY Poblacion;

SELECT
  MAX(c1.n)
FROM (SELECT
    COUNT(*) n,
    Poblacion
  FROM vendedores
  WHERE EstalCivil = 'casado'
  GROUP BY Poblacion) c1;



SELECT
  COUNT(c1.IdVendedor) n,
  c1.Poblacion
FROM (SELECT
    IdVendedor,
    Poblacion
  FROM vendedores
  WHERE EstalCivil = 'casado') c1
GROUP BY c1.Poblacion
ORDER BY n DESC LIMIT 1;

-- ramon

SELECT
  COUNT(*) n,
  Poblacion
FROM vendedores
WHERE EstalCivil = 'casado'
GROUP BY Poblacion
HAVING n = (SELECT
    MAX(c1.n)
  FROM (SELECT
      COUNT(*) n,
      Poblacion
    FROM vendedores
    WHERE EstalCivil = 'casado'
    GROUP BY Poblacion) c1);

-- 1.9 Poblacion que ninguno de sus vendedores etsan casados

SELECT
  IdVendedor
FROM vendedores
WHERE EstalCivil = 'casado';

SELECT DISTINCT
  Poblacion
FROM vendedores
  LEFT JOIN (SELECT
      IdVendedor
    FROM vendedores
    WHERE EstalCivil = 'casado') c1
    ON c1.IdVendedor = vendedores.IdVendedor
WHERE c1.IdVendedor IS NULL;


--  1.10  vendedor que no ha vendido nada

SELECT DISTINCT
  `Cod Vendedor`
FROM ventas;

SELECT
  IdVendedor
FROM vendedores
  LEFT JOIN (SELECT DISTINCT
      `Cod Vendedor`
    FROM ventas) c1
    ON c1.`Cod Vendedor` = IdVendedor
WHERE c1.`Cod Vendedor` IS NULL;

-- 1.11 el vendedor que ha vendido mas kilos. Quiere conocer su nombre

SELECT
  `Cod Vendedor`,
  SUM(Kilos)
FROM ventas
GROUP BY `Cod Vendedor`;

SELECT
  MAX(suma)
FROM (SELECT
    `Cod Vendedor`,
    SUM(Kilos) AS suma
  FROM ventas
  GROUP BY `Cod Vendedor`) c1;

SELECT
  `Cod Vendedor`
FROM ventas
GROUP BY `Cod Vendedor`
HAVING SUM(Kilos) = (SELECT
    MAX(suma)
  FROM (SELECT
      `Cod Vendedor`,
      SUM(Kilos) AS suma
    FROM ventas
    GROUP BY `Cod Vendedor`) c1);

SELECT
  NombreVendedor
FROM vendedores
  JOIN (SELECT
      `Cod Vendedor`
    FROM ventas
    GROUP BY `Cod Vendedor`
    HAVING SUM(Kilos) = (SELECT
        MAX(suma)
      FROM (SELECT
          `Cod Vendedor`,
          SUM(Kilos) AS suma
        FROM ventas
        GROUP BY `Cod Vendedor`) c1)) c1
    ON IdVendedor = c1.`Cod Vendedor`;