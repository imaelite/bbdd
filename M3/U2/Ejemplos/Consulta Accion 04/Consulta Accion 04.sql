﻿
--  

SELECT
  *
FROM bicis;

--  4


ALTER TABLE alquileres
ADD FOREIGN KEY (cliente) REFERENCES clientes (codigo);

ALTER TABLE alquileres
ADD FOREIGN KEY (bici) REFERENCES bicis (bicis_id);


ALTER TABLE averias
ADD FOREIGN KEY (bici) REFERENCES bicis (bicis_id);


UPDATE alquileres
SET precio_total = precio_total * -1;

--  5 
--   ok 

-- 6

-- a
SELECT
  *
FROM bicis;

SELECT
  bici,
  SUM(kms) AS suma_bicis
FROM alquileres
GROUP BY bici;

UPDATE (SELECT
    bici,
   SUM(kms)  AS suma_bicis
  FROM alquileres
  GROUP BY bici) c1
 RIGHT JOIN bicis
  ON c1.bici = bicis_id

SET bicis.kms =IFNULL( c1.suma_bicis,0);

--  b  

SELECT
  *
FROM bicis;
SELECT
  TRUNCATE(DATEDIFF(NOW(), fechaCompra) / 365, 0) AS nueva_fecha
FROM bicis;


UPDATE (SELECT
    TRUNCATE(DATEDIFF(NOW(), fechaCompra) / 365, 0) AS nueva_fecha,
    bicis_id
  FROM bicis) c1
JOIN bicis
  ON c1.bicis_id = bicis.bicis_id
SET bicis.años = c1.nueva_fecha;

--  c
SELECT
  *
FROM bicis;

SELECT
  bici,
  COUNT(*)
FROM averias
GROUP BY bici;

UPDATE (SELECT
    bici,
    COUNT(*) n_averias
  FROM averias
  GROUP BY bici) c1
JOIN bicis
  ON c1.bici = bicis_id
SET bicis.averias = c1.n_averias;

--  d
SELECT
  *
FROM bicis;

SELECT
  bici,
  COUNT(*)
FROM alquileres
GROUP BY bici;
--  d

--  Número de veces que se ha alquidalado esa bici
UPDATE (SELECT
    bici,
    COUNT(*) n_alquileres
  FROM alquileres
  GROUP BY bici) c1
JOIN bicis
  ON c1.bici = bicis_id
SET bicis.alquileres = c1.n_alquileres;

--  e
SELECT
  *
FROM bicis;
SELECT
  bici,
  SUM(coste)
FROM averias
GROUP BY bici;

UPDATE (SELECT
    bici,
    SUM(coste) AS precio
  FROM averias
  GROUP BY bici) c1
JOIN bicis
  ON c1.bici = bicis_id
SET bicis.gastos = c1.precio;

--  h 
SELECT
  *
FROM alquileres;
SELECT
  codigo,
  descuento
FROM clientes;



UPDATE alquileres
JOIN (SELECT
    codigo,
    descuento
  FROM clientes) c1
  ON c1.codigo = alquileres.cliente
SET alquileres.descuento = c1.descuento;


--  i

SELECT
  *
FROM bicis;
SELECT
  bicis_id,
  precio
FROM bicis;

SELECT
  referencia,
  cliente,
  bici,
  (precio * (1 - descuento / 100)) AS precio_total,
  descuento
FROM alquileres
  JOIN bicis
    ON alquileres.bici = bicis.bicis_id;

UPDATE (SELECT
    referencia,
    cliente,
    bici,
    (precio * (1 - descuento / 100)) AS precio_total,
    descuento
  FROM alquileres
    JOIN bicis
      ON alquileres.bici = bicis.bicis_id) c1
JOIN alquileres
  ON c1.referencia = alquileres.referencia
SET alquileres.precio_total = c1.precio_total;

--  f

SELECT
  *
FROM bicis;
SELECT
  *
FROM alquileres;

SELECT
  bici,
  SUM(precio_total) AS suma_bicis
FROM alquileres
GROUP BY bici;

UPDATE (SELECT
    bici,
    SUM(precio_total) AS suma_bicis
  FROM alquileres
  GROUP BY bici) c1
JOIN bicis
  ON c1.bici = bicis_id
SET bicis.beneficios = ROUND(c1.suma_bicis, 2);

--  g

SELECT
  *
FROM alquileres;
SELECT
  *
FROM clientes;
SELECT
  *
FROM bicis;
SELECT
  *
FROM averias;
SELECT
  cliente,
  COUNT(*)
FROM alquileres
GROUP BY cliente;

UPDATE (SELECT
    cliente,
    COUNT(*) n_alquileres
  FROM alquileres
  GROUP BY cliente) c1
JOIN clientes
  ON c1.cliente = codigo
SET clientes.alquileres = c1.n_alquileres;