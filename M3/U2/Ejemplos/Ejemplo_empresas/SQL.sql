﻿DROP DATABASE IF EXISTS empresa;

CREATE DATABASE IF NOT EXISTS empresa;
USE empresa;


CREATE TABLE empresa (
  codigo int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  poblacion varchar(50),
  cp varchar(50),
  trabajadores int
);

CREATE TABLE personas (
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  apellidos varchar(50),
  poblacion varchar(50),
  fecha date,
  dato1 int,
  dato2 int,
  dato3 int,
  fecha_trabajo date,
  empresa int
  --  ,CONSTRAINT FKempreasPersonas FOREIGN KEY (empresa) REFERENCES empresa(codigo) ON DELETE CASCADE ON UPDATE CASCADE
);



ALTER TABLE personas ADD CONSTRAINT FKpersonasEmpresas FOREIGN KEY (empresa) REFERENCES empresa(codigo) ON DELETE CASCADE ON UPDATE CASCADE;

