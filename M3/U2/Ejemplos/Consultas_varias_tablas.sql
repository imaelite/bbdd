﻿/* realizamos la consulta en un paso */

SELECT DISTINCT
  e.*
FROM equipo e
  JOIN ciclista c
    ON e.nomequipo = c.nomequipo
  JOIN etapa e1
    ON c.dorsal = e1.dorsal
  JOIN puerto p
    ON e1.numetapa = p.numetapa;

/*Lo hacemos por etapas*/

-- c1

SELECT DISTINCT
  e.numetapa,
  e.dorsal
FROM etapa e
  JOIN puerto p
    ON e.numetapa = p.numetapa;

-- c2

SELECT
  c.nomequipo
FROM ciclista c
  JOIN (SELECT DISTINCT
      e.numetapa,
      e.dorsal
    FROM etapa e
      JOIN puerto p
        ON e.numetapa = p.numetapa) c1
    ON c.dorsal = c1.dorsal;

-- consulta final

SELECT DISTINCT
  e.*
FROM equipo e
  JOIN (SELECT
      c.nomequipo
    FROM ciclista c
      JOIN (SELECT DISTINCT
          e.numetapa,
          e.dorsal
        FROM etapa e
          JOIN puerto p
            ON e.numetapa = p.numetapa) c1
        ON c.dorsal = c1.dorsal) c2
    ON c2.nomequipo = e.nomequipo;

