﻿-- operaciones con varias vistas

-- c1  ciclistas que han llevado maillot

CREATE OR REPLACE VIEW consulta5c1
AS
SELECT DISTINCT
  l.dorsal
FROM lleva l;

-- c2   ciclistas que no han llevado maillot

CREATE OR REPLACE VIEW consulta5c2
AS
SELECT DISTINCT
  c.dorsal
FROM ciclista c
  LEFT JOIN consulta5c1 c1 USING (dorsal)
WHERE c1.dorsal IS NULL;

-- c3 ciclistas que han ganado etapas

CREATE OR REPLACE VIEW consulta5c3
AS
SELECT DISTINCT
  e.dorsal
FROM etapa e;

-- c4 ciclistas que han ganado puertos

CREATE OR REPLACE VIEW consulta5c4
AS
SELECT DISTINCT
  p.dorsal
FROM puerto p;

-- -----------------Operaciones 

-- c3 - c1

SELECT
  c3.dorsal
FROM consulta5c3 c3
  LEFT JOIN consulta5c1 c1 USING (dorsal)
WHERE c1.dorsal IS NULL;

-- c3 Interseccion C2

SELECT
  c3.dorsal
FROM consulta5c3 c3
WHERE c3.dorsal NOT IN (SELECT
    c2.dorsal
  FROM consulta5c2 c2);


-- c3 - c2

SELECT
  *
FROM consulta5c3 c3
  LEFT JOIN consulta5c2 c2 USING (dorsal)
WHERE c2.dorsal IS NULL;


-- c3 Interseccion c1  

SELECT 
  c3.dorsal
FROM consulta5c3 c3
NATURAL JOIN consulta5c1 c;

-- c4-c3-c2 ciclistas que han ganado puertos y que no han ganado etapas y que llevan maillot

SELECT DISTINCT
  c4.dorsal
FROM consulta5c4 c4
  LEFT JOIN consulta5c3 c3 USING (dorsal)
  LEFT JOIN consulta5c2 c2 USING (dorsal)
WHERE c3.dorsal IS NULL
AND c2.dorsal IS NULL;


-- c1 U c3 ciclistas que han ganado maillot y que han ganado algua etapa

SELECT
  c1.dorsal
FROM consulta5c1 c1
UNION
SELECT
  c3.dorsal
FROM consulta5c3 c3;

-- c1 U c3 U c4

SELECT
  c1.dorsal
FROM consulta5c1 c1
UNION
SELECT
  c3.dorsal
FROM consulta5c3 c3
UNION
SELECT
  c4.dorsal
FROM consulta5c4 c4;
