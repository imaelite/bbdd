﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 27/12/2018 13:54:33
-- Server version: 5.5.5-10.1.36-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS `27_12_2018`;

CREATE DATABASE IF NOT EXISTS `27_12_2018`
CHARACTER SET utf8
COLLATE utf8_spanish2_ci;

--
-- Set default database
--
USE `27_12_2018`;

--
-- Create table `alumnos`
--
CREATE TABLE IF NOT EXISTS alumnos (
  nif varchar(10) NOT NULL,
  nombre varchar(255) DEFAULT NULL,
  ap1 varchar(255) DEFAULT NULL,
  ap2 varchar(255) DEFAULT NULL,
  edad tinyint(4) DEFAULT NULL,
  fnac time DEFAULT NULL,
  PRIMARY KEY (nif)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_spanish2_ci;

--
-- Create table `telefonos`
--
CREATE TABLE IF NOT EXISTS telefonos (
  alumno varchar(10) NOT NULL,
  telefono varchar(255) NOT NULL,
  PRIMARY KEY (alumno, telefono)
)
ENGINE = INNODB,
CHARACTER SET utf8,
COLLATE utf8_spanish2_ci;

--
-- Create foreign key
--
ALTER TABLE telefonos
ADD CONSTRAINT FK_telefonos_alumnos_nif FOREIGN KEY (alumno)
REFERENCES alumnos (nif) ON DELETE CASCADE ON UPDATE CASCADE;

-- 
-- Dumping data for table alumnos
--
-- Table `27_12_2018`.alumnos does not contain any data (it is empty)

-- 
-- Dumping data for table telefonos
--
-- Table `27_12_2018`.telefonos does not contain any data (it is empty)

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;