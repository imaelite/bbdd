﻿CREATE DATABASE consultasaccion3;

USE consultasaccion3;

-- crear tabla importes
DROP TABLE IF EXISTS importes;
CREATE TABLE IF NOT EXISTS importes (
  tipo varchar(12) PRIMARY KEY,
  valor int
);
  
  
 INSERT INTO importes VALUE ('Jornalero',25);
 INSERT INTO importes VALUE ('Efectivo',250);

--  3
ALTER TABLE empleados ADD COLUMN salario_base float DEFAULT 0;

--  4
ALTER TABLE empleados ADD COLUMN premios float  DEFAULT 0;


--  5
ALTER TABLE empleados ADD COLUMN sueldo_nominal float DEFAULT 0;


--  6
ALTER TABLE empleados ADD COLUMN bps float  DEFAULT 0;
ALTER TABLE empleados ADD COLUMN irp float  DEFAULT 0;


--  7
ALTER TABLE empleados ADD COLUMN subtotal_descuentos float  DEFAULT 0;

--  8
ALTER TABLE empleados ADD COLUMN sueldo_liquido float DEFAULT 0;


--  9
ALTER TABLE empleados ADD COLUMN v_transporte float DEFAULT 0;

--  10
ALTER TABLE empleados ADD COLUMN v_alimentacion float DEFAULT 0;
