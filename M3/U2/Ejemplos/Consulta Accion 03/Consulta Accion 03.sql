﻿--  3


SELECT * FROM empleados;

--  no utiliza la tabla importes

UPDATE empleados JOIN importes ON Tipo_trabajo=tipo
  SET salario_base= IF(Tipo_trabajo='Jornalero',Horas_trabajadas,Dias_trabajdos) *IF(tipo='Jornalero',25,250);

--  bien

UPDATE empleados JOIN importes ON Tipo_trabajo=tipo
  SET salario_base= (Horas_trabajadas+Dias_trabajdos)*valor;


--  4

  SELECT * FROM empleados;

--  opcion 1
  UPDATE empleados SET premios=
    CASE 
     WHEN (Tipo_trabajo='Jornalero' AND Horas_trabajadas>200) OR (Tipo_trabajo='Efectivo' AND  Dias_trabajdos >20) AND Rubro IN ('Chofer','Azafata') THEN salario_base * 0.15
     WHEN (Tipo_trabajo='Jornalero' AND Horas_trabajadas>200) OR (Tipo_trabajo='Efectivo' AND  Dias_trabajdos >20) AND Rubro not IN ('Chofer','Azafata') THEN salario_base * 0.05
     ELSE 0
   END;
 
--  opcion 2

    UPDATE empleados SET premios=salario_base*

     IF(Horas_trabajadas>200 OR Dias_trabajdos >20
         ,IF( Rubro IN ('Chofer','Azafata'),0.15,0.05)
              ,0);



--  5

    SELECT * FROM empleados;

  UPDATE empleados SET sueldo_nominal= salario_base + premios ;

--  6
 SELECT * FROM empleados;

  UPDATE empleados SET bps= 0.13;

  UPDATE empleados SET irp= 
   CASE 
     WHEN  sueldo_nominal<(4*1160) THEN 0.03
     WHEN sueldo_nominal<=(10*1160)THEN 0.06
     else 0.09
   END;

  --  7


    UPDATE empleados SET subtotal_descuentos=(sueldo_nominal* irp) + (sueldo_nominal* bps); 

--  8
 SELECT * FROM empleados;
UPDATE empleados SET sueldo_liquido=sueldo_nominal - subtotal_descuentos;

--  9

  SELECT  year(NOW())- year(DATE(`Fecha-nac`)) FROM empleados;
  SELECT * FROM empleados;

UPDATE empleados SET v_transporte=

   CASE 
     WHEN  year(NOW())- year(DATE(`Fecha-nac`))>=40 THEN IF(Barrio IN ('Cordon','Centro'),150,200)  
     WHEN year(NOW())- year(DATE(`Fecha-nac`))<40 THEN 100
   END;

--  10

  SELECT * FROM empleados;

  --  10


ALTER TABLE empleados ADD COLUMN v_alimentacion float DEFAULT 0;

UPDATE empleados SET v_alimentacion=
   CASE 
     WHEN  sueldo_liquido<=5000 THEN 300  
     WHEN  sueldo_liquido<=10000 THEN IF(Tipo_trabajo='Jornalero',200,100)
   END;



-- ojo añadir a la columna tipo de trabajo solo 2 campos


  SELECT  DISTINCT Tipo_trabajo FROM empleados;

UPDATE empleados
  SET Tipo_trabajo='jornalero' WHERE Tipo_trabajo='Blanqueada' OR Tipo_trabajo='Unión' OR Tipo_trabajo='Aguada';

UPDATE empleados
  SET Tipo_trabajo='Efectivo' WHERE Tipo_trabajo='Cordon';

--  quitar los dias a los jornaleros y las horas a los efectivos
UPDATE empleados
  SET empleados.Dias_trabajdos=0  WHERE Tipo_trabajo='jornalero';


UPDATE empleados
  SET empleados.Horas_trabajadas=0  WHERE Tipo_trabajo='Efectivo';