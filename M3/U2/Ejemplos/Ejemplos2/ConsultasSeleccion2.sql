﻿-- 1.1 numero de ciclistas que hay.


SELECT
  COUNT(*) n
FROM ciclista;

-- 1.2 numero de ciclistas que hay del equipo Banesto.

SELECT
  COUNT(*)
FROM ciclista
WHERE nomequipo = 'Banesto';

-- 1.3 la edad media de los ciclistas

SELECT
  AVG(edad)
FROM ciclista;


-- 1.4 la edad media de los ciclistas de Banesto

SELECT
  AVG(edad)
FROM ciclista
WHERE nomequipo = 'Banesto';

-- 1.5 la edad media de los ciclistas por cada equipo.

SELECT
  nomequipo,
  AVG(edad)
FROM ciclista
GROUP BY nomequipo;

-- 1.6 el numero de ciclistas por equipo

SELECT
  COUNT(*),
  nomequipo
FROM ciclista
GROUP BY nomequipo;


-- 1.7 el número total de puertos

SELECT
  COUNT(*)
FROM puerto;

-- 1.8 numero total de puertos mayores de 1500

SELECT
  COUNT(*)
FROM puerto
WHERE altura > 1500;

-- 1.9 listar el nombre de los equipos que tengan más de 4 ciclistas

SELECT
  COUNT(*) AS cuenta,
  nomequipo
FROM ciclista
GROUP BY nomequipo; -- c1

-- con having

SELECT
  nomequipo,
  COUNT(*) nCiclistas
FROM ciclista
GROUP BY nomequipo
HAVING nCiclistas > 4;

-- con subconsulta
SELECT
  c1.nomequipo,
  c1.cuenta
FROM (SELECT
    COUNT(*) AS cuenta,
    nomequipo
  FROM ciclista
  GROUP BY nomequipo) c1
WHERE cuenta > 4;



-- 1.10   listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32

-- having

SELECT
  nomequipo
FROM ciclista
WHERE edad BETWEEN 28 AND 32
GROUP BY nomequipo
HAVING COUNT(*) > 4;

-- con subconsulta
SELECT
  c1.nomequipo
FROM (SELECT
    COUNT(*) AS cuenta,
    nomequipo,
    edad
  FROM ciclista
  WHERE edad BETWEEN 28 AND 32
  GROUP BY nomequipo) c1
WHERE cuenta > 4;



-- 1.11 indicame el numero de etapas que ha ganado cada uno de los ciclistas

SELECT
  COUNT(*) AS numero_de_etapas,
  dorsal
FROM etapa
GROUP BY dorsal;

-- 1.12 indicame el dorsal de los ciclistas que han ganado mas de 1 etapa

-- con having
SELECT
  dorsal
FROM etapa
GROUP BY dorsal
HAVING COUNT(*) > 1;

-- con suconsulta
SELECT
  *
FROM (SELECT
    COUNT(*) AS cuenta,
    dorsal
  FROM etapa
  GROUP BY dorsal) c1
WHERE c1.cuenta > 1;




