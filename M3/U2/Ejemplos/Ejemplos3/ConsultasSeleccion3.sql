﻿-- 1.1 listar las edades de todos los ciclistas de banesto 

SELECT DISTINCT
  edad
FROM ciclista
WHERE nomequipo = 'Banesto';

-- 1.2 listar las edades de los ciclistas que son de banesto o de navigare

SELECT DISTINCT
  edad
FROM ciclista
WHERE nomequipo IN ('Banesto', 'Navigare');

-- 1.3 listar el dorsal de los ciclistas que son Banesto y cuya edad esta entre 25 y 32

SELECT
  dorsal
FROM ciclista
WHERE nomequipo = 'Banesto'
AND edad BETWEEN 25 AND 32;

-- 1.4 listar el dorsal de los ciclistas que son Banesto o cuya edad esta entre 25 y 32

SELECT
  dorsal
FROM ciclista
WHERE nomequipo = 'Banesto'
OR edad BETWEEN 25 AND 32;

-- 1.5 listar la inicial del equipo de loc ciclistas cuyo nombre empieze por r;

SELECT
  SUBSTRING(nomequipo, 1, 1)
FROM ciclista
WHERE LOWER(SUBSTRING(nombre, 1, 1)) = 'r';

-- 1.6 listar el codigo de las etapas que su salida y llegada sea en la misma población.

SELECT
  numetapa
FROM etapa
WHERE salida = llegada;

--  1.7 listar el código de las etapas que su salida y llegada no son en la misma poblacion

-- y que conozcamos el dorsal del ciclista  que ha ganado la etapa

SELECT
  numetapa
FROM etapa
WHERE salida <> llegada
AND dorsal IS NOT NULL;


-- 1.8 listar el nombre de los puertos cuya altura entre 1000 y 2000 o que la altura sea mayor que 2400.

SELECT
  nompuerto
FROM puerto
WHERE altura BETWEEN 1000 AND 2000
OR altura > 2400;

-- 1.9 listar el drosal de los ciclistas que hayan ganado los puertos cuya altura este entre 1000 y 2000 o que la altura sea mayor que 2400.

SELECT DISTINCT
  dorsal
FROM puerto
WHERE altura BETWEEN 1000 AND 2000
OR altura > 2400;

-- 1.10 listar el numero de cilcistas que hayan ganado alguna etapa

SELECT
  COUNT(DISTINCT dorsal)
FROM etapa;

-- 1.11 listar el número de etapas que tengan puertos

SELECT
  COUNT(DISTINCT numetapa)
FROM puerto;

-- 1.12 listar el numero de ciclistas que hayan ganado algun puerto

SELECT
  COUNT(DISTINCT dorsal)
FROM puerto;

-- 1.13 listar el codigo de la etapa con el numero de puertos que tiene

SELECT
  COUNT(*),
  numetapa
FROM puerto
GROUP BY numetapa;

-- 1.14 indicra la altura media de los puertos

SELECT
  AVG(altura)
FROM puerto;

-- 1.15 indica el codigo de etapa cuya altura media de sus puertos está por encim de 1500.


SELECT
  numetapa
FROM puerto
GROUP BY numetapa
HAVING AVG(altura) > 1500;



-- 1.16 indicar el número de etapas que cumplen la condicion anterior


SELECT
  COUNT(*)
FROM (SELECT
    numetapa
  FROM puerto
  GROUP BY numetapa
  HAVING AVG(altura) > 1500) c1;


-- 1.17 listar el dorsal del ciclista con el número de veces que ha llevado algun mayot

SELECT
  dorsal,
  COUNT(DISTINCT código)
FROM lleva
GROUP BY dorsal;

-- 1.18 listar el dorsal del ciclista con el codigo de maillot y cuantas veces ese ciclista ha llevado ese maillot,.

SELECT
  dorsal,
  código,
  COUNT(*) numero
FROM lleva
GROUP BY dorsal,
         código;


-- 1.19 lista el dorsal, el codigo de etapa, el ciclista y el numero de maillots que ese ciclista ha llevado en cada etapa. ????

SELECT DISTINCT
  dorsal,
  numetapa,
  COUNT(*) numeroDorsales
FROM lleva
GROUP BY dorsal,
         numetapa;

