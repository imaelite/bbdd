﻿

-- 1.1 listar las edades de los ciclistas (sin repetidos).

SELECT DISTINCT
  edad
FROM ciclista;

-- 1.2 listar las edades de los ciclistas de Artiach.

SELECT DISTINCT
  edad
FROM ciclista
WHERE nomequipo = 'Artiach';

-- 1.3 listar las edades de los ciclistas de artiach o de amore vita.

SELECT DISTINCT
  edad
FROM ciclista
WHERE nomequipo = 'Artiach'
OR nomequipo = 'Amore Vita';

-- con in
SELECT DISTINCT
  edad
FROM ciclista
WHERE nomequipo IN ('Artiach', 'Amore Vita');

-- con union
SELECT DISTINCT
  edad
FROM ciclista
WHERE nomequipo = 'Artiach'
UNION
SELECT DISTINCT
  edad
FROM ciclista
WHERE nomequipo = 'Amore Vita';



-- 1.4 listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30.

SELECT
  dorsal
FROM ciclista
WHERE edad < 25
OR edad > 30;

-- 1.5 listar los dorsales de los cilistas cuya edad este entre 28 y 32 y que solo sean de Banesto

SELECT
  dorsal
FROM ciclista
WHERE nomequipo = 'Banesto'
AND edad BETWEEN 28 AND 32;


-- ------------
SELECT
  dorsal
FROM ciclista
WHERE edad BETWEEN 28 AND 32;
--   intersect
SELECT
  dorsal
FROM ciclista
WHERE nomequipo = 'Banesto';
-- -------------


-- -- Usando el Join
SELECT
  c2.dorsal
FROM (SELECT
    dorsal
  FROM ciclista c
  WHERE c.edad BETWEEN 28 AND 32) AS c1

  JOIN (SELECT
      dorsal
    FROM ciclista
    WHERE nomequipo = 'Banesto') AS c2

    ON c1.dorsal = c2.dorsal;



-- 1.6 Indicame el nombre de los ciclistas que el nombre de los caracteres sea mayor que 8.

SELECT
DISTINCT
  nombre
FROM ciclista c
WHERE CHAR_LENGTH(nombre) > 8;

-- 1.7 Listarme el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayusculas.

SELECT
  dorsal,
  nombre,
  UPPER(nombre) AS nombre_mayusculas
FROM ciclista;


-- 1.8 listar todos los ciclistas que han llevado el maillot amarillo en alguna etapa.

SELECT DISTINCT
  dorsal
FROM lleva
WHERE código = 'MGE';

-- 1.9 listar el nombre de los puertos cuya altura sea mayor que 1500.

SELECT
  nompuerto
FROM puerto
WHERE altura > 1500;


-- 1.10 listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000.

SELECT DISTINCT
  dorsal
FROM puerto
WHERE pendiente > 8
OR altura BETWEEN 1800 AND 3000;


-- Con Union

SELECT DISTINCT
  dorsal
FROM puerto
WHERE pendiente > 8
UNION
SELECT DISTINCT
  dorsal
FROM puerto
WHERE altura BETWEEN 1800 AND 3000;



-- duda de suma de ganadores de puertos mas etapas ???????????????????????????????????????????
SELECT 
  COUNT(dorsal) + (SELECT COUNT(dorsal) FROM etapa)
FROM puerto
;




-- 1.11 listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000.

SELECT
  dorsal
FROM puerto
WHERE pendiente > 8
AND altura BETWEEN 1800 AND 3000;



 -- union 

     
    SELECT dorsal FROM puerto WHERE pendiente > 8
      union
    SELECT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000;

-- con interseccion, es lo que está en los dos lados

  SELECT *

    FROM 
    
    (SELECT dorsal FROM puerto WHERE pendiente > 8) c1  

    JOIN 

    (SELECT dorsal FROM puerto WHERE altura BETWEEN 1800 AND 3000) c2 

    ON c1.dorsal=c2.dorsal;

 