﻿-- Ejemplo de Uniones



SELECT
  COUNT(nomequipo)
FROM equipo;


-- cartesiano

SELECT
  *
FROM ciclista,
     puerto
WHERE ciclista.dorsal = puerto.dorsal;

-- inner join 

SELECT
  *
FROM ciclista
  JOIN puerto
    ON ciclista.dorsal = puerto.dorsal;

-- natural join

SELECT
  *
FROM ciclista
  NATURAL JOIN puerto;

-- usando using

SELECT
  *
FROM ciclista
  JOIN puerto USING (dorsal);

-- unir 3 tablas , opcion Mejor para Mí

SELECT
  *
FROM equipo
  JOIN ciclista
    ON equipo.nomequipo = ciclista.nomequipo
  JOIN etapa
    ON ciclista.dorsal = etapa.dorsal;

