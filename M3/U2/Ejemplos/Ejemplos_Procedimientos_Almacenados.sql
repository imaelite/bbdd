﻿DROP DATABASE IF EXISTS procedimientosFunciones1;
CREATE DATABASE procedimientosFunciones1;
USE procedimientosFunciones1;


CREATE TABLE datos (
  id int AUTO_INCREMENT PRIMARY KEY,
  dato1 char(9),
  dato2 char(9),
  dato3 char(9),
  suma char(9)
);
/*procedimiento que mustre la fecha de hoy*/

DELIMITER //

DROP PROCEDURE IF EXISTS ej1//
CREATE PROCEDURE ej1 ()
BEGIN
  SELECT
    NOW();
END//
DELIMITER ;


CALL ej1;

/* crear una variable en el interior del procedimiento  */

DELIMITER //

DROP PROCEDURE IF EXISTS ej2//
CREATE PROCEDURE ej2 ()
BEGIN
  DECLARE v1 datetime;
  SET v1 = NOW();
  SELECT
    v1;
END//
DELIMITER ;

--  llamada

CALL ej2();

/*suma de dos numeros*/

DELIMITER //

DROP PROCEDURE IF EXISTS ej2//
CREATE PROCEDURE ej3 (arg1 int, arg2 int)
BEGIN
  DECLARE suma int;
  SET suma = arg1 + arg2;
  SELECT
    suma;
END//
DELIMITER ;


CALL ej3(3, 4);
CALL ej3(5, 7);


/*resta de dos numeros*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej4//
CREATE PROCEDURE ej4 (arg1 int, arg2 int)
BEGIN
  DECLARE resta int;
  SET resta = arg1 - arg2;
  SELECT
    resta;
END//
DELIMITER ;

CALL ej4(3, 4);
CALL ej4(5, 7);


/*mutiplicacion de dos numeros*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej5//
CREATE PROCEDURE ej5 (arg1 int, arg2 int)
BEGIN
  DECLARE multi int;
  SET multi = arg1 * arg2;

  INSERT INTO datos
    VALUES (DEFAULT, arg1, arg2, arg1, multi);
  SELECT
    multi;
END//
DELIMITER ;


CALL ej5(5, 7);


/*mutiplicacion de dos numeros con tabla*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej6//
CREATE PROCEDURE ej6 (arg1 int, arg2 int)
BEGIN
  --  declarar las variables
  DECLARE multi int DEFAULT 0;

  --  crear la tabla para al mcenar los resultados
  CREATE TABLE IF NOT EXISTS datos (
    id int AUTO_INCREMENT PRIMARY KEY,
    dato1 int,
    dato2 int,
    suma int
  );
  SET multi = arg1 * arg2;
  INSERT INTO datos
    VALUES (DEFAULT, arg1, arg2, 0, multi);
END//
DELIMITER ;

CALL ej6(5, 7);

SELECT
  *
FROM datos;


/*El32 que me sume y me multiplique 2 números, crear tabla llamada ej32  */

DELIMITER //
DROP PROCEDURE IF EXISTS ej32//
CREATE PROCEDURE ej32 (arg1 int, arg2 int)
BEGIN
  --  declarar las variables
  DECLARE multi int DEFAULT 0;

  DECLARE suma int DEFAULT 0;

  --  crear la tabla para al mcenar los resultados
  CREATE TABLE IF NOT EXISTS ej32 (
    id int AUTO_INCREMENT PRIMARY KEY,
    dato1 int,
    dato2 int,
    suma int,
    multi int
  );
  SET suma = arg1 + arg2;
  SET multi = arg1 * arg2;

  INSERT INTO ej32
    VALUES (DEFAULT, arg1, arg2, suma, multi);
END//
DELIMITER ;

CALL ej32(5, 7);

SELECT
  *
FROM ej32;


/* en una tabla*/


/*ej33*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej33//
CREATE PROCEDURE ej33 ()
BEGIN

  --  crear la tabla para al mcenar los resultados
  DROP TABLE IF EXISTS ej33;
  CREATE TABLE IF NOT EXISTS ej33 (
    id int AUTO_INCREMENT PRIMARY KEY,
    dato1 int,
    dato2 int,
    resultado float
  );
END//
DELIMITER ;

CALL ej33();

/*ej33a*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej33a//
CREATE PROCEDURE ej33a (arg1 int, arg2 int)
BEGIN

  DECLARE potencia float DEFAULT 0;

  SET potencia = POW(arg1, arg2);
  INSERT INTO ej33
    VALUES (DEFAULT, arg1, arg2, potencia);

END//
DELIMITER ;

CALL ej33a(5, 7);

SELECT
  *
FROM ej33;


/*ej33b*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej33b//
CREATE PROCEDURE ej33b (arg1 int)
BEGIN

  DECLARE raiz float DEFAULT 0;


  SET raiz = SQRT(arg1);



  INSERT INTO ej33
    VALUES (DEFAULT, arg1, 0, raiz);

END//
DELIMITER ;

CALL ej33b(5);

SELECT
  *
FROM ej33;


/*ej34*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej34//
CREATE PROCEDURE ej34 ()
BEGIN

  --  crear la tabla para al mcenar los resultados
  DROP TABLE IF EXISTS ej34;
  CREATE TABLE IF NOT EXISTS ej34 (
    id int AUTO_INCREMENT PRIMARY KEY,
    texto varchar(50),
    longitud int,
    caracteres varchar(50)
  );

END//
DELIMITER ;

CALL ej34();



SELECT
  *
FROM ej34;



/*ej34a*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej34a//
CREATE PROCEDURE ej34a ()
BEGIN

  UPDATE ej34
  SET longitud = LENGTH(texto);
END//
DELIMITER ;

CALL ej34a();

SELECT
  *
FROM ej34;


/*ej34b*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej34b//
CREATE PROCEDURE ej34b (texto char(100))
BEGIN


  INSERT INTO ej34
    VALUES (DEFAULT, texto, 0, 0);

END//
DELIMITER ;

CALL ej34b('frodo');

SELECT
  *
FROM ej34;



/*ej34c*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej34c//
CREATE PROCEDURE ej34c (num_1 int, num_2 int)
BEGIN
  UPDATE (SELECT
      *
    FROM ej34 LIMIT num_2, 100) c1
  JOIN ej34 USING (id)
  SET ej34.caracteres = LEFT(ej34.texto, num_1);

END//
DELIMITER ;

CALL ej34c(3, 4);

SELECT
  *
FROM ej34;

/*ej35*/
/*Le paso u número es >10 el graba grande y si no pequeño*/



DELIMITER //
DROP PROCEDURE IF EXISTS ej35//
CREATE PROCEDURE ej35 (numero int)
BEGIN
  IF numero >= 10 THEN
    SELECT
      'Grande';
  ELSE
    SELECT
      'Pequeño';
  END IF;
END//
DELIMITER ;

CALL ej35(14);

SELECT
  *
FROM ej34;

/*ej4*/
/*le pasa dos numeros losmete en una tabla temporal y los muestra*/

  DELIMITER //
DROP PROCEDURE IF EXISTS ej4//
CREATE PROCEDURE ej4 (numero1 int,numero2 int)
BEGIN
CREATE OR REPLACE TEMPORARY TABLE  ej4(
  id int AUTO_INCREMENT PRIMARY KEY,
  dato1 int
  );
  INSERT INto ej4(dato1) VALUEs (numero1),(numero2);
  SELECT * FROM ej4 ORDER BY dato1 DESC;
END//
DELIMITER ;


CALL ej4(1,233);
CALL ej4(2);
CALL ej4(3);

/*ej5*/
DELIMITER //
DROP PROCEDURE IF EXISTS ej5//
CREATE PROCEDURE ej5 (IN numero1 int,OUT numero2 int, INOUT numero3 int )
BEGIN

SET numero2=50;
SET numero3=numero3*4;
SELECT numero1,numero2,numero3;
END//
DELIMITER ;

SET @numero2=2;
SET @numero3=12;
CALL  ej5(1,@numero2,@numero3);


/*suma*/

DELIMITER //
DROP PROCEDURE IF EXISTS ej6//
CREATE PROCEDURE ej6 (IN numero1 int,in numero2 int, out numero3 int )
BEGIN

SET numero3=numero1+numero2;

SELECT numero1,numero2,numero3;
END//
DELIMITER ;

SET @numero1=1;
SET @numero2=12;
SET @numero3=0;
CALL ej6(@numero1,@numero2,@numero3);