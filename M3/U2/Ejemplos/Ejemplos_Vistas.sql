﻿-- Vistas


-- ciclistas que han ganado puertos de mas de 1200 metros

SELECT
  *
FROM ciclista
  JOIN puerto p
    ON ciclista.dorsal = p.dorsal
WHERE p.altura > 1200;

-- mejorar la consulta mediante las subconsultas

SELECT DISTINCT
  p.dorsal
FROM puerto p
WHERE p.altura > 1200; -- c1

SELECT
  c.*
FROM (SELECT DISTINCT
    p.dorsal
  FROM puerto p
  WHERE p.altura > 1200) c1
  JOIN ciclista c
    ON c1.dorsal = c.dorsal;

-- con vista ciclistas que han ganado puertos de mas de 1200 metros

CREATE OR REPLACE VIEW consulta2c1
AS
SELECT DISTINCT
  p.dorsal
FROM puerto p
WHERE p.altura > 1200; -- c1

CREATE OR REPLACE VIEW consulta2c2
AS
SELECT
  c.*
FROM consulta2c1 c1
  JOIN ciclista c
    ON c1.dorsal = c.dorsal;

SELECT
  *
FROM consulta2c2 c;



-- ciclistas que  no han ganado etapas

SELECT DISTINCT
  e.dorsal
FROM etapa e;  -- c1

SELECT DISTINCT
  *
FROM ciclista c
  LEFT JOIN (SELECT DISTINCT
      e.dorsal
    FROM etapa e) c1
    ON c1.dorsal = c.dorsal
WHERE c1.dorsal IS NULL;

-- la resta pero con not in

SELECT DISTINCT
  *
FROM ciclista c
  LEFT JOIN (SELECT DISTINCT
      e.dorsal
    FROM etapa e) c1
    ON c1.dorsal = c.dorsal
WHERE c1.dorsal IS NULL;

-- con not in
SELECT
  *
FROM ciclista c
WHERE c.dorsal NOT IN (SELECT DISTINCT
    e.dorsal
  FROM etapa e);

-- con vista
CREATE OR REPLACE VIEW consulta3c1
AS
SELECT DISTINCT
  e.dorsal
FROM etapa e;

SELECT DISTINCT
  *
FROM ciclista c
  LEFT JOIN consulta3c1 c1
    ON c1.dorsal = c.dorsal
WHERE c1.dorsal IS NULL;






-- nombre del equipo de los ciclistas que hna ganado etapas con puertos

-- c1

CREATE OR REPLACE VIEW consulta1C1
AS
SELECT DISTINCT
  e.numetapa,
  e.dorsal
FROM etapa e
  JOIN puerto p
    ON e.numetapa = p.numetapa;

-- c2

CREATE OR REPLACE VIEW consulta1C2
AS
SELECT
  c.nomequipo
FROM ciclista c
  JOIN consulta1c1 c1
    ON c.dorsal = c1.dorsal;

SELECT
  *
FROM consulta1c1 c;

-- consulta final

SELECT DISTINCT
  e.*
FROM equipo e
  JOIN consulta1C2 c2
    ON c2.nomequipo = e.nomequipo;

-- ciclistas que hayan ganado alguna etapa y que no hayan ganado ningun puerto

-- no han ganado ningun puerto

SELECT
  c.dorsal
FROM ciclista c
  LEFT JOIN puerto p
    ON p.dorsal = c.dorsal
WHERE p.dorsal IS NULL;

-- han ganado etapas c1

SELECT DISTINCT
  e.dorsal
FROM etapa e;

CREATE OR REPLACE VIEW consulta4c1
AS
SELECT DISTINCT
  e.dorsal
FROM etapa e;

-- cilcistas que han ganado puertos c2

SELECT DISTINCT
  p.dorsal
FROM puerto p;

CREATE OR REPLACE VIEW consulta4c2
AS
SELECT DISTINCT
  p.dorsal
FROM puerto p;


-- ciclistas que no han ganado puertos c3

SELECT
  c.dorsal
FROM ciclista c
  LEFT JOIN puerto p1
    ON c.dorsal = p1.dorsal
WHERE p1.dorsal IS NULL;

CREATE OR REPLACE VIEW consulta4c3
AS
SELECT
  c.dorsal
FROM ciclista c
  LEFT JOIN puerto p1
    ON c.dorsal = p1.dorsal
WHERE p1.dorsal IS NULL;


-- c1 - c3 ciclistas que han ganado  etapas y puertos

SELECT
  c1.dorsal
FROM consulta4c1 c1
  LEFT JOIN consulta4c3 c3
    ON c1.dorsal = c3.dorsal
WHERE c3.dorsal IS NULL;


SELECT
  *
FROM consulta4c1 c1
WHERE c1.dorsal NOT IN (SELECT
    c.dorsal
  FROM consulta4c3 c);


-- c1 - c2 ciclistas que han ganado etapas y no han ganado puertos

SELECT
  c1.dorsal
FROM consulta4c1 c1
WHERE c1.dorsal NOT IN (SELECT
    c2.dorsal
  FROM consulta4c2 c2);


-- Union c1 y c2 con union, cilistas que han ganado etapas + ciclistas que han ganado puertos


SELECT
  c1.dorsal
FROM consulta4c1 c1

UNION

SELECT
  c2.dorsal
FROM consulta4c2 c2;

-- Union c1 y c3 con union, cilistas que han ganado etapas + ciclistas que NO han ganado puertos

SELECT
  c1.dorsal
FROM consulta4c1 c1

UNION

SELECT
  c3.dorsal
FROM consulta4c3 c3;


-- C1 interseccion c3 

SELECT
  *
FROM consulta4c1 c1
  NATURAL JOIN consulta4c3 c3;

-- c1 interseccion c2

SELECT
  *
FROM consulta4c1 c1
  NATURAL JOIN consulta4c2 c2;

-- juntos a pelo
SELECT
  c.dorsal
FROM (SELECT DISTINCT
    e.dorsal
  FROM etapa e) c
  LEFT JOIN puerto p
    ON p.dorsal = c.dorsal
WHERE p.dorsal IS NULL;









