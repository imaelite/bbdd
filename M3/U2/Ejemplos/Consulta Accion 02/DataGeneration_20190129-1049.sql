﻿
--
-- Inserting data into table productos
--
INSERT IGNORE INTO productos(producto, descripcion, rubro, `u/medida`, granja, importe_base) VALUES
(836286570, '0', 'frutas', 'kilo', 'la garota', 60),
(1145565647, '0', 'frutas', 'atado', 'el canuto', 86),
(930817285, 'A', 'semillas', 'unidad', 'la garota', 25),
(398287990, '848', 'verduras', 'unidad', 'el ceibal', 66),
(1908000233, 'KWUQ7', 'semillas', 'atado', 'el canuto', 68),
(1145565648, '4', 'verduras', 'kilo', 'primavera', 50),
(1601867773, 'T3NO', 'semillas', 'unidad', 'litoral', 52),
(183539628, '40JEX3LQ8SI1', 'verduras', 'kilo', 'la pocha', 66),
(621538208, NULL, 'verduras', 'atado', 'la pocha', 14),
(930817286, 'MN5G', 'frutas', 'unidad', 'la siembra', 75),
(398287991, 'Y8VHM71IOJ2', 'verduras', 'atado', 'el ceibal', 89),
(836286571, '32', 'frutas', 'kilo', 'el ceibal', 64),
(183539629, 'X90964', 'verduras', 'kilo', 'la garota', 14),
(1387119411, 'X', 'semillas', 'atado', 'primavera', 88),
(621538209, '5', 'semillas', 'unidad', 'la pocha', 96),
(1601867774, 'P', 'verduras', 'kilo', 'litoral', 100),
(1145565649, '80', 'verduras', 'unidad', 'la pocha', 65),
(836286572, 'G690S', 'verduras', 'atado', 'la garota', 67),
(621538210, '47Y8', 'verduras', 'unidad', 'la pocha', 5),
(836286573, 'F', 'verduras', 'atado', 'el ceibal', 37),
(621538211, '7MAG', 'frutas', 'kilo', 'el ceibal', 88),
(2122748595, '38E1HQ61B', 'verduras', 'kilo', 'la pocha', 14),
(930817287, 'E', 'semillas', 'atado', 'el ceibal', 48),
(1145565650, 'Z038', 'frutas', 'unidad', 'el canuto', 23),
(1387119412, 'S', 'verduras', 'atado', 'la garota', 52),
(930817288, '4F480Y806', 'frutas', 'unidad', 'el canuto', 89),
(1601867775, NULL, 'verduras', 'kilo', 'el ceibal', 69),
(1908000234, '9DCX7', 'semillas', 'atado', 'primavera', 20),
(836286574, 'JHN4N', 'verduras', 'kilo', 'litoral', 67),
(1145565651, '3', 'frutas', 'unidad', 'la siembra', 74),
(930817289, 'US', 'semillas', 'unidad', 'el ceibal', 6),
(1387119413, NULL, 'frutas', 'kilo', 'la garota', 74),
(398287992, 'VD', 'verduras', 'atado', 'la garota', 11),
(1601867776, '714W5147W', 'frutas', 'kilo', 'la pocha', 44),
(1387119414, '6N31G', 'frutas', 'atado', 'la pocha', 91),
(1601867777, 'J9C9H', 'semillas', 'unidad', 'la pocha', 40),
(621538212, 'T', 'verduras', 'unidad', 'primavera', 1),
(836286575, '2W39O', 'verduras', 'atado', 'el ceibal', 19),
(183539630, '2', 'verduras', 'kilo', 'la garota', 4),
(621538213, 'QGS', 'verduras', 'atado', 'el canuto', 89),
(1145565652, 'X', 'verduras', 'kilo', 'la pocha', 100),
(930817290, NULL, 'semillas', 'unidad', 'la pocha', 61),
(1387119415, '5', 'verduras', 'atado', 'la pocha', 62),
(398287993, '5MD8WQ4V48JV', 'verduras', 'kilo', 'litoral', 70),
(2122748596, '6W', 'verduras', 'unidad', 'el ceibal', 77),
(1601867778, '5', 'semillas', 'kilo', 'el canuto', 91),
(1387119416, 'W4CL', 'semillas', 'unidad', 'el ceibal', 5),
(836286576, '4', 'frutas', 'atado', 'el ceibal', 37),
(1601867779, '223', 'semillas', 'kilo', 'el ceibal', 45),
(183539631, 'I7H', 'semillas', 'atado', 'primavera', 66),
(621538214, 'DXG4MZ3F49II', 'verduras', 'unidad', 'litoral', 95),
(1908000235, NULL, 'frutas', 'atado', 'el ceibal', 85),
(836286577, '9N05', 'verduras', 'kilo', 'la pocha', 56),
(1145565653, '9', 'verduras', 'unidad', 'la pocha', 5),
(621538215, 'MTLU9936', 'frutas', 'atado', 'el ceibal', 35),
(930817291, '7KD9', 'verduras', 'kilo', 'primavera', 1),
(398287994, '216JD8VU80', 'verduras', 'unidad', 'el canuto', 18),
(2122748597, 'I5', 'frutas', 'unidad', 'la pocha', 3),
(1145565654, '7QU4', 'verduras', 'atado', 'el canuto', 93),
(183539632, '1H49474I0W', 'verduras', 'kilo', 'el ceibal', 74),
(930817292, 'HIO7SGD', 'verduras', 'kilo', 'la garota', 74),
(1387119417, '3', 'verduras', 'unidad', 'el ceibal', 9),
(398287995, '42', 'frutas', 'atado', 'la garota', 77),
(1908000236, NULL, 'semillas', 'unidad', 'el canuto', 72),
(1145565655, '8T', 'semillas', 'atado', 'el ceibal', 54),
(1601867780, NULL, 'semillas', 'kilo', 'la garota', 6),
(836286578, '1A', 'frutas', 'unidad', 'la siembra', 54),
(183539633, 'FZ', 'semillas', 'kilo', 'la pocha', 40),
(621538216, '1', 'frutas', 'atado', 'la garota', 8),
(836286579, '61240M', 'verduras', 'kilo', 'litoral', 3),
(2122748598, '8', 'verduras', 'unidad', 'la pocha', 1),
(1387119418, NULL, 'verduras', 'atado', 'la pocha', 23),
(1908000237, 'S34I017R29', 'semillas', 'atado', 'la garota', 43),
(930817293, NULL, 'verduras', 'kilo', 'el canuto', 84),
(621538217, '0', 'frutas', 'unidad', 'el ceibal', 27),
(398287996, 'M4', 'frutas', 'unidad', 'litoral', 41),
(836286580, 'UY85', 'frutas', 'atado', 'primavera', 2),
(183539634, 'D', 'semillas', 'kilo', 'la pocha', 60),
(1145565656, 'V1N', 'verduras', 'kilo', 'el ceibal', 100),
(2122748599, '341AA', 'verduras', 'atado', 'la pocha', 78),
(1908000238, 'T', 'frutas', 'unidad', 'el ceibal', 2),
(621538218, 'XQ8', 'frutas', 'atado', 'la siembra', 98),
(930817294, '321I', 'verduras', 'unidad', 'la garota', 41),
(1145565657, NULL, 'verduras', 'kilo', 'el ceibal', 20),
(398287997, 'H0F9QA81CX1', 'semillas', 'kilo', 'el ceibal', 84),
(1601867781, 'WA', 'frutas', 'atado', 'la garota', 58),
(1387119419, '6GII49', 'verduras', 'unidad', 'el canuto', 24),
(836286581, '6454', 'semillas', 'unidad', 'el canuto', 92),
(1601867782, 'I6H8130VZ', 'verduras', 'atado', 'la pocha', 19),
(621538219, 'ZD', 'verduras', 'kilo', 'primavera', 94),
(930817295, '2', 'verduras', 'kilo', 'la pocha', 13),
(183539635, '0VV6I54X', 'verduras', 'unidad', 'el ceibal', 47),
(1145565658, '8K9NB1S20', 'verduras', 'atado', 'la pocha', 14),
(398287998, 'Q9', 'verduras', 'kilo', 'la garota', 39),
(1387119420, 'VP', 'semillas', 'atado', 'la pocha', 20),
(183539636, 'S9M70Y', 'semillas', 'unidad', 'litoral', 24),
(1601867783, '40JK803485', 'verduras', 'atado', 'la garota', 13),
(1387119421, '7428V', 'semillas', 'kilo', 'el ceibal', 80),
(398287999, '0M', 'frutas', 'unidad', 'primavera', 38),
(1601867784, 'S8', 'frutas', 'atado', 'la pocha', 47);