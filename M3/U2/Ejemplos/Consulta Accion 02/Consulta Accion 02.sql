﻿--  4 

ALTER TABLE productos ADD COLUMN IF NOT EXISTS DESCUENTO1 float;

SELECT
  *
FROM productos;

UPDATE productos
  SET DESCUENTO1 = 0.1 *importe_base
  WHERE productos.rubro = 'verduras';

UPDATE productos
  SET DESCUENTO1 = 0
  WHERE productos.rubro <> 'verduras';

-- con if


UPDATE productos
  SET DESCUENTO1 =importe_base*IF(productos.rubro= 'verduras',0.1 ,0 );

--  5

SELECT
  *
FROM productos;




ALTER TABLE productos ADD COLUMN IF NOT EXISTS DESCUENTO2 float;

UPDATE productos
SET DESCUENTO2 = 0.05 *importe_base;


UPDATE productos
SET DESCUENTO2 = 0.2 *importe_base
WHERE `u/medida` = 'atado';

--  con if

UPDATE productos
  SET DESCUENTO2 =importe_base*IF(`u/medida` = 'atado',0.2 ,0.05 );

--  6 



ALTER TABLE productos ADD COLUMN  IF NOT EXISTS DESCUENTO3 float;

SELECT
  *
FROM productos
WHERE rubro = 'frutas'
AND importe_base > 15;

SELECT * FROM productos;

UPDATE productos
SET DESCUENTO3 = 0
WHERE NOT( rubro = 'frutas'
AND importe_base > 15);

UPDATE productos
SET DESCUENTO3 = 0.2 * importe_base
WHERE rubro = 'frutas'
AND importe_base > 15;

--  con 1 update

  UPDATE productos SET DESCUENTO3=importe_base*IF(rubro = 'frutas'
AND importe_base > 15,0.2,0);

--    7

  SELECT * FROM productos;

ALTER TABLE productos ADD COLUMN  IF NOT EXISTS DESCUENTO4 float;



UPDATE productos SET DESCUENTO4=0.25 * importe_base;
UPDATE productos SET DESCUENTO4=0.5 * importe_base WHERE granja='primavera' OR granja='litoral';




--  8


SELECT * FROM productos;


ALTER TABLE productos ADD COLUMN  IF NOT EXISTS AUMENTO1 float DEFAULT 0;


UPDATE productos SET AUMENTO1=0.1 * importe_base WHERE NOT( (rubro='frutas' OR rubro='verduras') AND (granja='la pocha' OR granja='la garota'));


UPDATE productos SET AUMENTO1=0.1 * importe_base WHERE (rubro='frutas' OR rubro='verduras') AND (granja='la pocha' OR granja='la garota');

--  con 1 update

  UPDATE productos SET AUMENTO1=importe_base* IF((rubro='frutas' OR rubro='verduras') AND (granja='la pocha' OR granja='la garota'),0.1,0);




--  9


SELECT * FROM productos;

ALTER TABLE productos ADD COLUMN  IF NOT EXISTS PRESENTACION float DEFAULT 0;




UPDATE productos SET PRESENTACION=1 WHERE  `u/medida`='atado';
UPDATE productos SET PRESENTACION=2 WHERE  `u/medida`='unidad';
UPDATE productos SET PRESENTACION=3 WHERE  `u/medida`='kilo';


-- case


UPDATE productos 
  SET PRESENTACION=
    CASE 
     WHEN `u/medida`='atado' THEN 1
     WHEN `u/medida`='unidad' THEN 2
     ELSE 3
   END;


  -- con IF

--  10

  SELECT * FROM productos;

  ALTER TABLE productos ADD COLUMN  IF NOT EXISTS CATEGORIA char DEFAULT 'x';


  UPDATE productos SET CATEGORIA='A' WHERE importe_base<10;
UPDATE productos SET CATEGORIA='B' WHERE  importe_base BETWEEN 10 AND 20;
UPDATE productos SET CATEGORIA='C' WHERE  importe_base>20;


-- con case

  UPDATE productos 
  SET CATEGORIA=
    CASE 
     WHEN importe_base<10 THEN 'A'
     WHEN importe_base BETWEEN 10 AND 20 THEN 'B'
     ELSE 'C'
   END;



--  11

  SELECT * FROM productos;

  ALTER TABLE productos ADD COLUMN  IF NOT EXISTS AUMENTO2 float DEFAULT 0;

  -- case

    UPDATE productos 
      SET AUMENTO2=importe_base*
        CASE 
          WHEN granja='litoral' AND producto='frutas' THEN 0.1
          WHEN granja='el ceibal' AND producto='verduras' THEN 0.15
          WHEN granja='el canuto' AND producto='semillas' THEN 0.2
        END;


