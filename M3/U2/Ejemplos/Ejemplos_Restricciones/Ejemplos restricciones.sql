﻿DROP DATABASE IF EXISTS restricciones;

CREATE DATABASE restricciones;
USE restricciones;

CREATE TABLE clientes(
id int(11) NOT NULL AUTO_INCREMENT,
  edad int(11) NOT NULL,
  PRIMARY KEY (id)

);

CREATE TABLE ventas(
id int(11) NOT NULL AUTO_INCREMENT,
  cliente int(11),
  fecha date,
  cantidad int,
  PRIMARY KEY (id)
);

  ALTER TABLE ventas ADD FOREIGN KEY (cliente) REFERENCES clientes(id) ON UPDATE CASCADE ON DELETE CASCADE;
--  vistas para restringir la entrada de una edad negativa

  CREATE OR REPLACE VIEW vista1 AS SELECT * FROM clientes WHERE edad>=0 AND edad1>edad WITH LOCAL CHECK OPTION;

  TRUNCATE clientes;

  INSERT IGNORE INTO vista1 VALUE --  con el IGNORE entran los datos  correctos Y los malos no
    (DEFAULT,-10,10) -- la edad no cumple
  ,(DEFAULT,10,0),  -- la edad 1 es menor que la edad
  (DEFAULT,10,20);  --  coreecto

  SELECT * FROM vista1;
  SELECT * FROM clientes;

  ALTER TABLE clientes ADD COLUMN edad1 int;
  
  --  VISTA 2

  --  fecha de ventas menor que hoy

    -- cantidad mayor que 10 y menor que 100

    TRUNCATE ventas;
    CREATE OR REPLACE VIEW vista2 AS SELECT * FROM ventas WHERE fecha<NOW() AND cantidad BETWEEN 10 AND 100 WITH LOCAL CHECK OPTION;

    INSERT IGNORE INTO vista2 VALUES (DEFAULT,NULL,'2020/1/1',0),(DEFAULT,NULL,'2018/1/1',80);
    SELECT * FROM ventas;



  TRUNCATE ventas;

  SELECT * FROM ventas;
  SELECT * FROM clientes;


-- Vista 3

  --  que la cantidad este entre 10 y 100

  CREATE OR REPLACE VIEW vista3 AS SELECT * FROM ventas WHERE cantidad BETWEEN 10 AND 100 WITH LOCAL CHECK OPTION;

     INSERT IGNORE INTO vista3 VALUES (DEFAULT,NULL,'2020/1/1',0),-- no
    (DEFAULT,NULL,'2018/1/1',80); --  si

    --  vista 4

CREATE OR REPLACE VIEW vista4 AS SELECT * FROM vista3 WHERE fecha<NOW() WITH LOCAL CHECK OPTION;

    INSERT IGNORE INTO vista4 VALUES (DEFAULT,NULL,'2020/1/1',0),-- no
                                     (DEFAULT,NULL,'2020/1/1',80), --  no
                                     (DEFAULT,NULL,'2018/1/1',80),  -- si
                                     (DEFAULT,NULL,'2018/1/1',0);   -- no

    CREATE OR REPLACE VIEW vista5 AS SELECT * FROM vista3 WHERE fecha<NOW() WITH CASCADED CHECK OPTION;
    
    INSERT IGNORE INTO vista5 VALUES (DEFAULT,NULL,'2020/1/1',0),-- no
                                     (DEFAULT,NULL,'2020/1/1',80), --  no
                                     (DEFAULT,NULL,'2018/1/1',80),  -- si
                                     (DEFAULT,NULL,'2018/1/1',0);   -- no


    ALTER TABLE clientes ADD COLUMN ventas int;

    SELECT id,COUNT(*) n_ventas, ventas FROM clientes GROUP BY id;
    SELECT * FROM clientes;

    UPDATE  (SELECT clientes.id,COUNT(*) AS n_ventas FROM clientes JOIN ventas ON clientes.id = ventas.id GROUP BY clientes.id) c1 JOIN  clientes ON c1.id=clientes.id  SET ventas=c1.n_ventas;

    SELECT clientes.id,COUNT(*) AS n_ventas FROM clientes JOIN ventas ON clientes.id = ventas.id GROUP BY clientes.id;