﻿-- 1.1 Nombre y edad de los ciclistas que NO han ganado etapas
-- on
SELECT
  nombre,
  edad
FROM ciclista
  LEFT JOIN etapa
    ON ciclista.dorsal = etapa.dorsal
WHERE etapa.dorsal IS NULL;

-- bien

SELECT DISTINCT
  dorsal
FROM etapa;

SELECT DISTINCT
  nombre,
  edad
FROM ciclista
  LEFT JOIN (SELECT DISTINCT
      dorsal
    FROM etapa) c1
    ON c1.dorsal = ciclista.dorsal
WHERE c1.dorsal IS NULL;




-- 1.2 Nombre y edad de los ciclistas que NO han ganado puerto

-- on 
SELECT
  nombre,
  edad
FROM ciclista
  LEFT JOIN puerto
    ON ciclista.dorsal = puerto.dorsal
WHERE puerto.dorsal IS NULL;

-- bien 

SELECT DISTINCT
  dorsal
FROM puerto;

SELECT
  nombre,
  edad
FROM ciclista
  LEFT JOIN (SELECT DISTINCT
      dorsal
    FROM puerto) c1
    ON c1.dorsal = ciclista.dorsal
WHERE c1.dorsal IS NULL;

-- 1.3 listar el director de lso equipos que tengan ciclistas que no hayan ganado ninguna etapa

-- on
SELECT DISTINCT
  director
FROM ciclista
  LEFT JOIN etapa
    ON ciclista.dorsal = etapa.dorsal
  JOIN equipo
    ON ciclista.nomequipo = equipo.nomequipo
WHERE etapa.dorsal IS NULL;


-- bien

-- ganadores de etapas
SELECT DISTINCT
  e.dorsal
FROM etapa e;
-- nombre de equipo que no han ganado ninguna etapa 
SELECT DISTINCT
  c.nomequipo
FROM ciclista c
  LEFT JOIN (SELECT DISTINCT
      e.dorsal
    FROM etapa e) c1
    ON c.dorsal = c1.dorsal
WHERE c1.dorsal IS NULL;

SELECT
  e.director
FROM equipo e
  JOIN (SELECT DISTINCT
      c.nomequipo
    FROM ciclista c
      LEFT JOIN (SELECT DISTINCT
          e.dorsal
        FROM etapa e) c1
        ON c.dorsal = c1.dorsal
    WHERE c1.dorsal IS NULL) c1
    ON e.nomequipo = c1.nomequipo;




-- 1.4 Dorsal y nombre de los ciclistas que NO hayan llevado algún maillott

--  on
SELECT
  ciclista.dorsal,
  nombre
FROM ciclista
  LEFT JOIN lleva
    ON ciclista.dorsal = lleva.dorsal
WHERE lleva.dorsal IS NULL;

--  bien
SELECT DISTINCT
  dorsal
FROM lleva;

SELECT
  nombre,
  edad
FROM ciclista
  LEFT JOIN (SELECT DISTINCT
      dorsal
    FROM lleva) c1
    ON c1.dorsal = ciclista.dorsal
WHERE c1.dorsal IS NULL;


-- 1.5 Dorsal y nombre de los ciclistas que NO hayan llevando el maillot amarillo NUNCA

SELECT DISTINCT
  dorsal
FROM lleva
  JOIN (SELECT código FROM maillot WHERE color = 'Amarillo' ) c1 
    ON lleva.código = c1.código;



SELECT
  c1.dorsal,
  c1.nombre
FROM (SELECT dorsal,
             nombre FROM ciclista) c1
  LEFT JOIN (SELECT DISTINCT
  dorsal
FROM lleva
  JOIN (SELECT código FROM maillot WHERE color = 'Amarillo' ) c1 
    ON lleva.código = c1.código) c2
    ON c2.dorsal = c1.dorsal
WHERE c2.dorsal IS NULL;

-- 1.6 indica el numero de de las etapas que NO tengan puertos

-- on
SELECT
  etapa.numetapa
FROM etapa
  LEFT JOIN puerto
    ON etapa.numetapa = puerto.numetapa
WHERE puerto.numetapa IS NULL;

SELECT DISTINCT
       numetapa
        FROM puerto;

SELECT 
       etapa.numetapa
        FROM etapa LEFT JOIN (SELECT DISTINCT
       numetapa
        FROM puerto)c1 ON c1.numetapa=etapa.numetapa WHERE c1.numetapa IS NULL;

-- 1.7 indica la distancia media de las etapas que NO tengan puerto 

--  on
SELECT
  AVG(etapa.kms)
FROM etapa
  LEFT JOIN puerto
    ON etapa.numetapa = puerto.numetapa
WHERE puerto.numetapa IS NULL;

-- bien 

SELECT DISTINCT
       numetapa
        FROM puerto;

SELECT AVG(kms)
        FROM etapa LEFT JOIN(SELECT DISTINCT
       numetapa
        FROM puerto) c1 ON c1.numetapa=etapa.numetapa WHERE c1.numetapa IS NULL;

-- 1.8 listar el número de ciclistas que NO hayan ganado alguna etapa

-- on
SELECT
  COUNT(*)
FROM ciclista
  LEFT JOIN etapa
    ON ciclista.dorsal = etapa.dorsal
WHERE etapa.dorsal IS NULL;

--  bien  
  SELECT DISTINCT
         dorsal FROM etapa;

  SELECT COUNT(*) FROM ciclista LEFT JOIN ( SELECT DISTINCT
         dorsal FROM etapa) c1 ON ciclista.dorsal=c1.dorsal WHERE c1.dorsal IS NULL; 

-- 1.9 Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerta

--  on
SELECT DISTINCT
  etapa.dorsal
FROM etapa
  LEFT JOIN puerto
    ON etapa.numetapa = puerto.numetapa
WHERE puerto.numetapa IS NULL;

  -- bien 

    SELECT DISTINCT
           numetapa
            FROM puerto;

    SELECT 
           DISTINCT dorsal FROM etapa LEFT JOIN (SELECT DISTINCT
           numetapa
            FROM puerto) c1 ON etapa.numetapa=c1.numetapa WHERE c1.numetapa IS NULL;


 

-- 1.10 listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puerto

  SELECT DISTINCT
         numetapa
          FROM puerto;

-- dorsal de ciclista que ha ganado etapa con puerto
  SELECT DISTINCT
         dorsal FROM etapa 
          LEFT JOIN ( SELECT DISTINCT
          numetapa
          FROM puerto) c1 ON c1.numetapa=etapa.numetapa;

  SELECT 
         ciclista.dorsal FROM ciclista LEFT JOIN (SELECT DISTINCT
         dorsal FROM etapa 
          LEFT JOIN ( SELECT DISTINCT
          numetapa
          FROM puerto) c1 ON c1.numetapa=etapa.numetapa) c1 ON ciclista.dorsal =c1.dorsal WHERE c1.dorsal IS NULL;



  -- equipo y nombre de los ciclistas que han ganado etapas y que no han ganado puertos

    SELECT DISTINCT
           dorsal FROM etapa;

    SELECT DISTINCT
           dorsal FROM puerto;

    SELECT c1.dorsal FROM (SELECT DISTINCT
           dorsal FROM etapa) c1 LEFT JOIN (SELECT DISTINCT
           dorsal FROM puerto) c2 ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL;

    SELECT DISTINCT
           nombre,
          
           nomequipo FROM (SELECT c1.dorsal FROM (SELECT DISTINCT
           dorsal FROM etapa) c1 LEFT JOIN (SELECT DISTINCT
           dorsal FROM puerto) c2 ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL) c1 JOIN ciclista ON c1.dorsal=ciclista.dorsal;