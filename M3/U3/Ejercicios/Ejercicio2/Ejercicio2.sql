﻿--  3- Realizar un procedimiento almacenado que visualice todas las personas cuyo salario sea superior a un valor que se pasara como parámetro de entrada. 

DELIMITER //
DROP PROCEDURE IF EXISTS ej3//
CREATE PROCEDURE ej3 (salario_max int)
BEGIN
      SELECT * FROM personas WHERE salario>salario_max;
END//
DELIMITER ;

CALL ej3(1200);


--  4- Realizar un procedimiento almacenado que visualice todas las personas cuyo salario este entre dos valores que se pasaran como parámetros de entrada. 


  DELIMITER //
DROP PROCEDURE IF EXISTS ej4//
CREATE PROCEDURE ej4 (salario_min int,salario_max int)
BEGIN
      SELECT * FROM personas WHERE salario BETWEEN salario_min AND salario_max ORDER BY salario;
END//
DELIMITER ;

CALL ej4(1200,1900);

--  5- Realizar un procedimiento almacenado que indique cuantos médicos trabajan en un hospital cuyo código se pase como parámetro de entrada. 
  
  --  Además al procedimiento se le pasara un argumento donde se debe almacenar el número de plazas de ese hospital 



 DELIMITER //
DROP PROCEDURE IF EXISTS ej5//
CREATE PROCEDURE ej5 (resultadoEntrada int,OUT resultadoSalida int)
BEGIN
      SELECT COUNT(*) FROM medicos  WHERE cod_hospital=resultadoEntrada;

    SET resultadoSalida= (SELECT num_plazas FROM hospitales  WHERE cod_hospital=resultadoEntrada);
END//
DELIMITER ;

SET @resultadoEntrada = 2;
SET @resultadoSalida = 0;
CALL ej5(@resultadoEntrada,@resultadoSalida);

SELECT @resultadoEntrada,@resultadoSalida;

--  6- Crear una función que calcule el volumen de una esfera cuyo radio de tipo FLOAT se pasara como parámetro.
  --  Realizar después una consulta para calcular el volumen de una esfera de radio 5. 


  DELIMITER //
  CREATE OR REPLACE FUNCTION fun1(r float)
RETURNS float
BEGIN
     DECLARE volumen float DEFAULT 0;
    SET volumen=((4/3*3.14)*POWER(r,3));
    RETURN volumen;
END //

DELIMITER ;

SELECT fun1(5);

--  7- Crear un procedimiento almacenado que cree una tabla denominada esferas. Esta tabla tendrá dos campos de tipo float.
  
  --  Estos campos son radio y volumen. El procedimiento debe crear la tabla aunque esta exista. 


 DELIMITER //
DROP PROCEDURE IF EXISTS ej7//
CREATE PROCEDURE ej7 ()
BEGIN
     CREATE OR REPLACE TABLE esferas (
    id int AUTO_INCREMENT PRIMARY KEY,
    radio float,
       volumen float
  );

END//
DELIMITER ;

CALL ej7();


--  8- Crear un procedimiento almacenado que me permita introducir 100 radios de forma automática en la tabla anterior. 
  
  --  Estos radios deben estar entre dos valores pasados al procedimiento como argumentos de entrada. 

--  9- Crear un procedimiento almacenado que me permita calcular los volúmenes de todos los registros de la tabla esferas creada anteriormente.
  --  Debéis utilizar la función creada para este propósito 

   DELIMITER //
DROP PROCEDURE IF EXISTS ej8//
CREATE PROCEDURE ej8(valor1 int, valor2 int)
BEGIN
  DECLARE contador int DEFAULT 0;
  DECLARE volumen float DEFAULT 0;
  DECLARE r float DEFAULT 0;

    WHILE (contador < 100) DO
  
        SET r = ROUND(((valor2 - valor1 -1) * RAND() + valor1), 0);
    
        SET volumen=fun1(r);

        INSERT INTO esferas VALUE (DEFAULT,r,volumen);
        SET contador=contador+1;
    END WHILE;
END//
DELIMITER ;

CALL ej8(2,6);


TRUNCATE esferas;

SELECT * FROM esferas;


--  10-  Crear un procedimiento que reciba una palabra como argumento y la devuelva en ese mismo argumento escrita al revés. 

DELIMITER //
DROP PROCEDURE IF EXISTS ej10//
CREATE PROCEDURE ej10(inout palabra varchar(50))
BEGIN
  SET palabra=REVERSE(palabra);
END//
DELIMITER ;

SET @entrada='Kevin';
CALL ej10(@entrada);
SELECT @entrada;

--  11- Realizar el ejercicio anterior utilizando una función y el parámetro solamente de entrada


DELIMITER //
CREATE OR REPLACE FUNCTION fun2(entrada varchar(50))
RETURNS varchar(50)
BEGIN
   DECLARE respuesta varchar(50);
   SET respuesta=REVERSE(entrada);

    RETURN respuesta;
END //

DELIMITER ;

SET @entrada='Kevin';
SELECT fun2(@entrada);

--  12- Realizar una función que calcule el factorial de un número. 


  
DELIMITER //
CREATE OR REPLACE FUNCTION fun3(entrada int)
RETURNS int
BEGIN
  
  DECLARE contador int DEFAULT 1;
  DECLARE factorial int DEFAULT 1;
 
  WHILE (contador <= entrada) DO
   
    SET factorial=factorial*contador;

    SET contador=contador+1;
    END WHILE;

    RETURN factorial;
END //

DELIMITER ;

SET @entrada=0;
SELECT fun3(@entrada);