﻿
DROP DATABASE IF EXISTS m3u3e1;
CREATE DATABASE m3u3e1;
USE m3u3e1;
--  Ejercicio 1


    DELIMITER //
DROP PROCEDURE IF EXISTS ejemploProcedimiento1//
CREATE PROCEDURE ejemploProcedimiento1 (IN argumento1 int)
BEGIN
 DECLARE variable1 char(10);
IF (argumento1=17) THEN
  SET variable1='pajaros';
ELSE  
  SET variable1='leones';
END IF;
CREATE TABLE table1(
  nombre varchar(20)
  );
  INSERT INTO table1 VALUES(variable1);
END//
DELIMITER ;

CALL ejemploProcedimiento1(19);

SELECT * FROM table1;
--  ej 2: necesita un argumento
--  ej 3: no puedo crear la tabla 2 veces


  --  ej4

      DELIMITER //
DROP PROCEDURE IF EXISTS ejemploProcedimiento2//
CREATE PROCEDURE ejemploProcedimiento2 (IN argumento1 int)
BEGIN
 DECLARE variable1 char(10);
IF (argumento1=17) THEN
  SET variable1='pajaros';
ELSE  
  SET variable1='leones';
END IF;
CREATE TABLE IF NOT EXISTS table1(
  nombre varchar(20)
  );
  INSERT INTO table1 VALUES(variable1);
END//
DELIMITER ;


CALL ejemploProcedimiento2(10);

SELECT * FROM table1;

--  ej 6

      DELIMITER //
DROP PROCEDURE IF EXISTS ejemploProcedimiento3//
CREATE PROCEDURE ejemploProcedimiento3 ()
BEGIN
SELECT 'hola mundo';
END//
DELIMITER ;


CALL ejemploProcedimiento3();

--  ej 7

        DELIMITER //
DROP PROCEDURE IF EXISTS ejemploProcedimiento4//
CREATE PROCEDURE ejemploProcedimiento4 ()
BEGIN
SELECT 'hola mundo';
  INSERT INTO table1 VALUE ('hola mundo');
END//
DELIMITER ;


CALL ejemploProcedimiento4();

SELECT * FROM table1;

--  ej 8

        DELIMITER //
DROP PROCEDURE IF EXISTS ejemploProcedimiento5//
CREATE PROCEDURE ejemploProcedimiento5()
BEGIN
  DECLARE contador int DEFAULT 1;
 WHILE (contador <= 10) DO
  INSERT INTO table1 VALUE ('hola mundo');
  SET contador=contador+1;
 END WHILE ;
END//
DELIMITER ;


CALL ejemploProcedimiento5();
TRUNCATE table1;
SELECT * FROM table1;


--  ej 9

        DELIMITER //
DROP PROCEDURE IF EXISTS ejemploProcedimiento6//
CREATE PROCEDURE ejemploProcedimiento6()
BEGIN
  DECLARE contador int DEFAULT 0;
  REPEAT
   SET contador=contador+1;
  INSERT INTO table1 VALUE ('hola mundo');
  UNTIL (contador >= 10) 
  END REPEAT;
END//
DELIMITER ;

CALL ejemploProcedimiento6();
TRUNCATE table1;
SELECT * FROM table1;

--  ej  10

  
        DELIMITER //
DROP PROCEDURE IF EXISTS ejemploProcedimiento7//
CREATE PROCEDURE ejemploProcedimiento7(IN parametro int)
BEGIN
  DECLARE contador int DEFAULT 0;
  REPEAT
   SET contador=contador+1;
  INSERT INTO table1 VALUE ('hola mundo');
  UNTIL (contador >= parametro) 
  END REPEAT;
END//
DELIMITER ;

CALL ejemploProcedimiento7(5);
TRUNCATE table1;
SELECT * FROM table1;