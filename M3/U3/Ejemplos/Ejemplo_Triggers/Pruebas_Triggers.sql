﻿

CREATE OR REPLACE TABLE entradas(
id int AUTO_INCREMENT PRIMARY KEY,
  valor int DEFAULT 0,
  total int DEFAULT 0,
  codigo int DEFAULT 0
  );

INSERT INTO entradas (valor,codigo) VALUES(1,5);

SELECT * FROM entradas;


INSERT INTO entradas (valor,codigo) VALUES(1,50);

DELIMITER //
CREATE OR REPLACE TRIGGER TRIGGER1
BEFORE INSERT
  ON entradas
  FOR EACH ROW
  BEGIN
   SET new.total= pow(new.valor,2);
  END //
DELIMITER;

INSERT INTO entradas (valor,codigo) VALUES(3,50);
SELECT * FROM entradas;


--  

  DELIMITER //
CREATE OR REPLACE TRIGGER TRIGGER1
BEFORE INSERT
  ON entradas
  FOR EACH ROW
  BEGIN
IF(new.codigo<50)THEN
   SET new.total= pow(new.valor,2);
  ELSE  
   SET new.total= pow(new.valor,3);
  END IF;
  END //
DELIMITER;

INSERT INTO entradas (valor,codigo) VALUES(2,10);
SELECT * FROM entradas;



