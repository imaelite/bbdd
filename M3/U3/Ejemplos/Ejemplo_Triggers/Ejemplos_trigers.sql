﻿
--  creacion de tablas

CREATE OR REPLACE TABLE salas (
  id int AUTO_INCREMENT PRIMARY KEY,
  butacas int DEFAULT 5,
  fecha date,
  edad int
);


CREATE OR REPLACE TABLE ventas (
  id int AUTO_INCREMENT PRIMARY KEY,
  sala int,
  numero int
);


-- creando el triger
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER salasT1
BEFORE INSERT
ON salas
FOR EACH ROW
BEGIN
  SET new.edad =   TIMESTAMPDIFF(year,new.fecha,now());
END //
DELIMITER;



DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER salasT2
BEFORE UPDATE 
ON salas
FOR EACH ROW
BEGIN
  SET new.edad =   TIMESTAMPDIFF(year,new.fecha,now());
END //
DELIMITER;


SELECT * FROM salas;

UPDATE salas SET fecha='1999/01/01';

INSERT INTO salas (butacas, fecha)
  VALUES ( 150,'2000/01/01');

SELECT * FROM salas;



DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ventasT1
BEFORE UPDATE 
ON ventas
FOR EACH ROW
BEGIN
  DECLARE numero int DEFAULT 0;

  SELECT COUNT(*) INTO numero FROM salas WHERE id=new.sala;


IF (numero=1) THEN
       SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La sala no existe';
  END IF;
END //
DELIMITER;

SELECT * FROM salas;