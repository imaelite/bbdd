﻿
DROP DATABASE IF EXISTS EjemplosProcedimientosFunciones2;
CREATE DATABASE EjemplosProcedimientosFunciones2;
USE EjemplosProcedimientosFunciones2;

--  ej1

  DELIMITER //
DROP PROCEDURE IF EXISTS ej1//
CREATE PROCEDURE ej1 (IN texto1 varchar(50), IN caracter varchar(50))
BEGIN
 DECLARE respuesta int DEFAULT 0;
SET respuesta=LOCATE(caracter,texto1);

  IF respuesta>0 then
    SELECT 'Está';
    ELSE
      SELECT 'No Está';
      END IF;
END//
DELIMITER ;


SET @texto1 = 'imanol';
SET @texto2 = 'x';
CALL ej1(@texto1, @texto2);


-- ej2

  DELIMITER //
DROP PROCEDURE IF EXISTS ej2//
CREATE PROCEDURE ej2 (IN texto1 varchar(50), IN caracter varchar(50))
BEGIN
 DECLARE inicio int DEFAULT 0;
SET inicio=LOCATE(caracter,texto1);

  SELECT LEFT(texto1,inicio-1);

END//
DELIMITER ;

SET @texto1 = 'Luis';
SET @texto2 = 'i';
CALL ej2(@texto1, @texto2);

--  ej3

DELIMITER //
DROP PROCEDURE IF EXISTS ej3//
CREATE PROCEDURE ej3 (IN numero1 int, IN numero2 int, IN numero3 int, OUT arg1 int, OUT arg2 int)
BEGIN


SET arg1 = GREATEST(numero1, numero2, numero3);
SET arg2 = LEAST(numero1, numero2, numero3);

END//
DELIMITER ;

SET @numero1 = 19111;
SET @numero2 = 1211;
SET @numero3 = 12;
SET @numero4 = 0;
SET @numero5 = 0;
CALL ej3(@numero1, @numero2, @numero3, @numero4, @numero5);
SELECT @numero4, @numero5;

--  ej 4

  DELIMITER //
DROP PROCEDURE IF EXISTS ej4//
CREATE PROCEDURE ej4 ()
BEGIN

SELECT CONCAT('Números 1 mayores de 50 hay: ',COUNT(numero1)) FROM datos WHERE numero1>50;

SELECT concat('Números 2 mayores de 50 hay: ',COUNT(numero2)) FROM datos WHERE numero2>50;
END//
DELIMITER ;

CALL ej4();



--  ej5
DELIMITER //
DROP PROCEDURE IF EXISTS ej5//
CREATE PROCEDURE ej5 (IN numero1 int, IN numero2 int)
BEGIN

  SELECT CONCAT('La suma es ',numero1+numero2,' y la resta es ',numero1-numero2);

END//
DELIMITER ;

SET @numero1 = 19;
SET @numero2 = 12;

CALL ej5(@numero1, @numero2);

--  ej6

  DELIMITER //
DROP PROCEDURE IF EXISTS ej6//
CREATE PROCEDURE ej6 (IN numero1 int, IN numero2 int)
BEGIN
DECLARE suma int DEFAULT null;
  DECLARE resta int DEFAULT null;

   IF numero1>numero2 then
    SET suma= numero1+numero2;

    SELECT CONCAT('La suma es ',suma);
   ELSE
   SET resta= numero2-numero1;
   SELECT CONCAT('La resta es ',resta);
   END IF;
END//
DELIMITER ;

SET @numero1 = 10;
SET @numero2 = 15;

CALL ej6(@numero1, @numero2);

SELECT * FROM datos;


--  ej 7
  DELIMITER //
DROP PROCEDURE IF EXISTS ej7//
CREATE PROCEDURE ej7 ()
BEGIN
 UPDATE datos SET junto=(SELECT CONCAT(texto1,' ',texto2));
END//
DELIMITER ;

CALL ej7();
SELECT * FROM datos;

--  ej 8

  DELIMITER //
DROP PROCEDURE IF EXISTS ej8//
CREATE PROCEDURE ej8 ()
BEGIN

UPDATE datos set junto=NULL;

  UPDATE datos SET junto = IF(rango='a',CONCAT(texto1,' - ',texto2),
                                        IF(rango='b',CONCAT(texto1,' + ',texto2)
                                                            ,texto1));

END//
DELIMITER ;

CALL ej8();

SELECT * FROM datos;

SELECT rango FROM datos;