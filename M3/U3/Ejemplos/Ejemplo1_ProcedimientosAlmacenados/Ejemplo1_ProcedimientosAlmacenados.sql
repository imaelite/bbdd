﻿
DROP DATABASE IF EXISTS EjemplosProcedimientosFunciones1;
CREATE DATABASE EjemplosProcedimientosFunciones1;
USE EjemplosProcedimientosFunciones1;
--  ej1

--  con if
DELIMITER //
DROP PROCEDURE IF EXISTS ej1_if//
CREATE PROCEDURE ej1_if (IN numero1 int, IN numero2 int)
BEGIN
  DECLARE respuesta int DEFAULT numero1;
  IF numero2 > numero1 THEN
    SET respuesta = numero2;
  END IF;

  SELECT
    CONCAT('El mayor es ', respuesta);
END//
DELIMITER ;

SET @numero1 = 111;
SET @numero2 = 1222;
CALL ej1_if(@numero1, @numero2);

--  con totales
DELIMITER //
DROP PROCEDURE IF EXISTS ej1_total//
CREATE PROCEDURE ej1_total (IN numero1 int, IN numero2 int)
BEGIN

  CREATE OR REPLACE TEMPORARY TABLE ej1_total (
    id int AUTO_INCREMENT PRIMARY KEY,
    dato1 int
  );
  INSERT INTO ej1_total (dato1)
    VALUES (numero1), (numero2);
  SELECT
    CONCAT('El mayor es ', MAX(dato1))
  FROM ej1_total;

END//
DELIMITER ;

SET @numero1 = 11111;
SET @numero2 = 1211;
CALL ej1_total(@numero1, @numero2);

--  con funcion de mysql

DELIMITER //
DROP PROCEDURE IF EXISTS ej1_mysql//
CREATE PROCEDURE ej1_mysql (IN numero1 int, IN numero2 int)
BEGIN
  DECLARE r int;

  SET r = GREATEST(numero1, numero2);
  SELECT
    CONCAT('El mayor es ', r);

END//
DELIMITER ;

SET @numero1 = 11111;
SET @numero2 = 1211;
CALL ej1_mysql(@numero1, @numero2);


--  ej 2

--  con if
DELIMITER //
DROP PROCEDURE IF EXISTS ej2_if//
CREATE PROCEDURE ej2_if (IN num1 int, IN num2 int, IN num3 int)
BEGIN
 IF (num1>num2) 
      THEN 
      IF (num1>num3) 
        THEN 
        SELECT num1;
      ELSE 
        SELECT num3;
      END IF;
      ELSEIF (num2>num3) 
      THEN
      SELECT num2;
    ELSE 
      SELECT num3;
    END IF;

END//
DELIMITER ;

SET @numero1 = 111;
SET @numero2 = 1222;
SET @numero3 = 12233;
CALL ej2_if(@numero1, @numero2, @numero3);

--  con totales
DELIMITER //
DROP PROCEDURE IF EXISTS ej2_total//
CREATE PROCEDURE ej2_total (IN numero1 int, IN numero2 int, IN numero3 int)
BEGIN

  CREATE OR REPLACE TEMPORARY TABLE ej2_total (
    id int AUTO_INCREMENT PRIMARY KEY,
    dato1 int
  );
  INSERT INTO ej2_total (dato1)
    VALUES (numero1), (numero2), (numero3);
  SELECT
    CONCAT('El mayor es ', MAX(dato1))
  FROM ej2_total;

END//
DELIMITER ;

SET @numero1 = 11111;
SET @numero2 = 1211;
SET @numero3 = 111;
CALL ej2_total(@numero1, @numero2, @numero3);


--  con funcion de mysql

DELIMITER //
DROP PROCEDURE IF EXISTS ej2_mysql//
CREATE PROCEDURE ej2_mysql (IN numero1 int, IN numero2 int, IN numero3 int)
BEGIN
  DECLARE r int;

  SET r = GREATEST(numero1, numero2, numero3);
  SELECT
    CONCAT('El mayor es ', r);

END//
DELIMITER ;

SET @numero1 = 19111;
SET @numero2 = 1211;
SET @numero3 = 12;
CALL ej2_mysql(@numero1, @numero2, @numero3);


--  ej3


DELIMITER //
DROP PROCEDURE IF EXISTS ej3//
CREATE PROCEDURE ej3 (IN numero1 int, IN numero2 int, IN numero3 int, OUT numero4 int, OUT numero5 int)
BEGIN
  CREATE OR REPLACE TEMPORARY TABLE ej3 (
    id int AUTO_INCREMENT PRIMARY KEY,
    dato1 int
  );
  INSERT INTO ej3 (dato1)
    VALUES (numero1), (numero2), (numero3);
  SET numero4 = (SELECT
      MAX(dato1)
    FROM ej3);
  SET numero5 = (SELECT
      MIN(dato1)
    FROM ej3);

END//
DELIMITER ;

SET @numero1 = 19111;
SET @numero2 = 1211;
SET @numero3 = 12;
SET @numero4 = 0;
SET @numero5 = 0;
  CALL ej3(@numero1, @numero2, @numero3, @numero4, @numero5);
SELECT @numero4, @numero5;


--  ej 4

DELIMITER //
DROP PROCEDURE IF EXISTS ej4//
CREATE PROCEDURE ej4 (IN fecha1 date, IN fecha2 date)
BEGIN
  SELECT
    CONCAT('La diferencia es de ',DAY(fecha1) - DAY(fecha2),' días.');
END//
DELIMITER ;

SET @numero1 = '2019/01/12';
SET @numero2 = '2019/01/02';
CALL ej4(@numero1, @numero2);


--  ej 5

DELIMITER //
DROP PROCEDURE IF EXISTS ej5//
CREATE PROCEDURE ej5 (IN fecha1 date, IN fecha2 date)
BEGIN
  SELECT
    concat('La diferencia es de ',MONTH(fecha1) - MONTH(fecha2),' meses');
END//
DELIMITER ;

SET @numero2 = '2018/01/12';
SET @numero1 = '2018/07/02';
CALL ej5(@numero1, @numero2);


  --  ej 6

DELIMITER //
DROP PROCEDURE IF EXISTS ej6//
CREATE PROCEDURE ej6 (IN fecha1 date, IN fecha2 date, OUT num1 int,OUT num2 int,OUT num3 int)
BEGIN


SET num1=DAY(fecha1)-DAY(fecha2);
SET num2=MONTH(fecha1)-MONTH(fecha2);
SET num3=YEAR(fecha1)-YEAR(fecha2);

select   CONCAT('La diferencia de días es ', num1,', de meses es ' ,num2,' y de años es ',num3);

END//
DELIMITER ;

SET @numero2 = '2018/01/12';
SET @numero1 = '2018/07/02';
SET @numero3 = 0;
SET @numero4 = 0;
SET @numero5 = 0;
CALL ej6(@numero1, @numero2, @numero3, @numero4, @numero5);


--  ej 7

  DELIMITER //
DROP PROCEDURE IF EXISTS ej7//
CREATE PROCEDURE ej7 (IN frase varchar(50))
BEGIN

  SELECT concat('El número de caracteres es: ',CHAR_LENGTH(frase));

END//
DELIMITER ;

SET @frase = 'Hola, qué tal?';
CALL ej7(@frase);