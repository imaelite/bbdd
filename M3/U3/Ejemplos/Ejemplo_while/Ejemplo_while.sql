﻿
--  crear tabla de prueba

CREATE OR REPLACE TABLE ej_bucle_1 (
  id int AUTO_INCREMENT PRIMARY KEY,
  dato int
);


--  ej1

--  con iterate
DELIMITER //
DROP PROCEDURE IF EXISTS ej_bucle_1//
CREATE PROCEDURE ej_bucle_1 (a1 int, a2 int)
BEGIN
  DECLARE contador int DEFAULT a1;
e1:
  WHILE (contador <= a2) DO
    IF (contador = 8
      OR contador = 12) THEN
      SET contador = contador + 1;
      ITERATE e1;
    END IF;
    INSERT INTO ej_bucle_1
      VALUE (DEFAULT, contador);
    SET contador = contador + 1;
  END WHILE e1;
END//
DELIMITER ;

--  sin iterate

DELIMITER //
DROP PROCEDURE IF EXISTS ej_bucle_1//
CREATE PROCEDURE ej_bucle_1 (a1 int, a2 int)
BEGIN
  DECLARE contador int DEFAULT a1;
  WHILE (contador <= a2) DO
    IF (contador <> 8
      AND contador <> 12) THEN
      INSERT INTO ej_bucle_1
        VALUE (DEFAULT, contador);
    END IF;
    SET contador = contador + 1;
  END WHILE;
END//
DELIMITER ;


SET @numero1 = 5;
SET @numero2 = 20;
CALL ej_bucle_1(@numero1, @numero2);

SELECT
  *
FROM ej_bucle_1;

TRUNCATE ej_bucle_1;


--  funcion
DELIMITER //
  CREATE OR REPLACE FUNCTION fun1(n1 int,n2 int,n3 int)
    RETURNS int
BEGIN
  DECLARE suma int DEFAULT null;
    IF(n1>0 AND n2>2 AND n3>0) THEN 
      set suma=n1+n2+n3;
    END IF; 
  RETURN suma;
END //

DELIMITER ;

 
SET @numero1 = 5;
SET @numero2 = 20;
SET @numero3 = 30;
select fun1(@numero1,@numero2,@numero3);