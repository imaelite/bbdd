﻿


-- Ej1  Crear un disparador para que cuando inserte un registro en clientes me compruebe si la edad esta entre 18 y 70.

--   En caso de que no esté produzca una excepción con el mensaje “La edad no es válida”. 

-- 

--  añadir campo edad
ALTER TABLE clientes ADD COLUMN edad int DEFAULT 0;

DROP TRIGGER ej1;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej1
BEFORE INSERT
ON clientes
FOR EACH ROW
BEGIN
  DECLARE s CONDITION FOR SQLSTATE '45000';

  IF (new.edad NOT BETWEEN 18 AND 70) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La edad no es valida';
  END IF;

END//
DELIMITER;

INSERT INTO clientes (CodCli, NombCli, Direccion, CP, CodLoc, edad)
  VALUES (1999, 'Imanol', 'Calle Falsa', '33455', '87654', 1);


SELECT
  *
FROM clientes;



--  modificar un poco le primero


DROP TRIGGER ej1;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej1
BEFORE INSERT
ON clientes
FOR EACH ROW
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLSTATE '45000'
  BEGIN
    RESIGNAL SET MESSAGE_TEXT = 'La edad no es valida';
  END;
  IF (new.edad NOT BETWEEN 18 AND 70) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La edad no es valida';
  END IF;
END//
DELIMITER;


-- Ej2  Además, cada vez que un usuario introduzca un registro con una edad no valida debe grabar ese registro en una tabla denominada errores. 

DROP TRIGGER ej1;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej1
BEFORE INSERT
ON clientes
FOR EACH ROW
BEGIN
  DECLARE s CONDITION FOR SQLSTATE '45000';
  IF (new.edad NOT BETWEEN 18 AND 70) THEN
    INSERT INTO errores (id, mensaje, fecha, hora)
      VALUES (new.CodCli, 'La edad no es valida', date (NOW()), time (NOW()));
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La edad no es valida';
  --  no entra aki
  END IF;
END//
DELIMITER;

INSERT INTO clientes (CodCli, NombCli, Direccion, CP, CodLoc, edad)
  VALUES (1999, 'Imanol', 'Calle Falsa', '33455', '87654', 1);

SELECT
  *
FROM clientes;

SELECT
  *
FROM errores;

DESCRIBE errores;

-- ej 3 Crear un disparador para que cuando modifique un registro de la tabla clientes me compruebe si la edad esta entre 18 y 70.
--  En caso de que no esté produzca una excepción con el mensaje “La edad no es válida”. 

DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej3
BEFORE UPDATE
ON clientes
FOR EACH ROW
BEGIN
  DECLARE s CONDITION FOR SQLSTATE '45000';
  IF (new.edad NOT BETWEEN 18 AND 70) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La edad no es valida';
  END IF;

END//
DELIMITER;


SELECT
  *
FROM clientes;

UPDATE clientes
SET edad = 1
WHERE CodCli = 3;


-- ej4 Crear disparadores en la tabla localidades que comprueben que cuando introduzcas o modifiques una localidad el código de la provincia exista en la tabla provincias.

--  En caso de que no exista que cree la provincia. El resto de campos de la tabla provincias se colocarán a valor por defecto.  


DROP TRIGGER ej4;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej4
BEFORE INSERT
ON localidades
FOR EACH ROW
BEGIN
  IF (new.CodPro NOT IN (SELECT
        CodPro
      FROM provincias)) THEN
    INSERT INTO provincias (CodPro)
      VALUES (new.CodPro);
  END IF;
END//
DELIMITER;

--  con excepciones

DROP TRIGGER ej4;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej4
BEFORE INSERT
ON localidades
FOR EACH ROW
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLSTATE '45000'
  BEGIN
    INSERT INTO provincias (CodPro)
      VALUES (new.CodPro);
  END;

  IF (new.CodPro NOT IN (SELECT
        CodPro
      FROM provincias)) THEN
    SIGNAL SQLSTATE '45000';
  END IF;
END//
DELIMITER;

SELECT
  *
FROM localidades;

SELECT
  *
FROM provincias
WHERE CodPro = '9117';   -- 54

INSERT localidades (CodLoc, NombLoc, CodPro)
  VALUES ('993389', 'Nueva Localidad', '9117');


--  actualizacion

DROP TRIGGER ej4_update;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej4_update
BEFORE UPDATE
ON localidades
FOR EACH ROW
BEGIN
  IF (new.CodPro NOT IN (SELECT
        CodPro
      FROM provincias)) THEN
    UPDATE provincias
    SET CodPro = new.CodPro;
  END IF;
END//
DELIMITER;


--  ej 5  Modificar los disparadores de la tabla clientes para que cuando introduzcas o modifiques un cliente compruebe si la localidad del cliente exista.

--  En caso que no exista mostrar el mensaje de error “La localidad no existe”. Debe insertar ese registro de error en la tabla errores. 

DROP TRIGGER ej2;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej2
BEFORE INSERT
ON clientes
FOR EACH ROW
BEGIN
  DECLARE s CONDITION FOR SQLSTATE '45000';
  IF (new.edad NOT BETWEEN 18 AND 70) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La edad no es valida';
    
INSERT INTO errores ( mensaje, fecha, hora)
      VALUES ('La edad no es valida', date (NOW()), time (NOW()));
  END IF;
  IF (new.CodLoc NOT IN (SELECT
        CodLoc
      FROM localidades)) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La localidad no existe';
    INSERT INTO errores (mensaje, fecha, hora)
      VALUES ('La localidad no existe', date (NOW()), time (NOW()));
  END IF;

END//
DELIMITER;



SELECT
  *
FROM clientes;

INSERT INTO clientes (CodCli,CodLoc,edad)
  VALUES (1999,'sss',50);

SELECT
  CodLoc

FROM localidades;

--  ej 6 

ALTER TABLE provincias ADD COLUMN iniciales char(1) DEFAULT '';



DROP TRIGGER ej61;

DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej61
BEFORE INSERT
ON provincias
FOR EACH ROW
BEGIN
  DECLARE numPro int DEFAULT 0;
  DECLARE inicial char(1) DEFAULT '';

  SET new.iniciales=LEFT(new.NombPro,1);

  IF (new.CodPro IN (SELECT
        CodPro
      FROM localidades)) THEN

    SET numPro = (SELECT
        COUNT(*)
      FROM localidades
      WHERE CodPro = new.CodPro);
  END IF;

  SET new.cantidad=numPro;

END//
DELIMITER;

SELECT
  *
FROM provincias;
SELECT
  *
FROM localidades;
SELECT
  *
FROM clientes;

--  ej 7


ALTER TABLE provincias ADD COLUMN cantidad int DEFAULT 0;


 
DROP TRIGGER ej7;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej7
AFTER INSERT
ON localidades
FOR EACH ROW
BEGIN
  DECLARE cantidad_nueva int DEFAULT 0;
  SET cantidad_nueva = (SELECT
      COUNT(*)
    FROM localidades
    WHERE CodPro = new.CodPro);

  UPDATE provincias
  SET provincias.cantidad = cantidad_nueva
  WHERE CodPro = new.CodPro;

END//
DELIMITER;


INSERT localidades (CodLoc, NombLoc, CodPro)
  VALUES ('41786', 'Localidad Imanol', 90);

SELECT
  *
FROM provincias;
SELECT
  *
FROM localidades;


  
