﻿--  llamada del procedimento
CALL p1();

CREATE OR REPLACE TABLE numeros (
  valor int
);

INSERT INTO numeros
  VALUES (1), (100), (220), (400), (1000);

SELECT
  *
FROM numeros;

SELECT
  MOD(10, 2) AS n
FROM numeros
WHERE n LIKE 0;


SELECT
  COUNT(*)
FROM numeros
WHERE MOD(valor, 2) = 0;


DELIMITER //
DROP PROCEDURE IF EXISTS pares_cursor//
CREATE PROCEDURE pares_cursor ()
BEGIN
  DECLARE final int DEFAULT 0;
  DECLARE numero int DEFAULT 0;
  DECLARE contadorPares int DEFAULT 0;

  DECLARE cursor1 CURSOR FOR
  SELECT
    *
  FROM numeros;

  DECLARE CONTINUE HANDLER FOR NOT FOUND
  SET final = 1;

  OPEN cursor1;
  --  leo el primer registro
  FETCH cursor1 INTO numero;
  WHILE (final = 0) DO
    FETCH cursor1 INTO numero;
    IF (MOD(numero, 2) = 0) THEN
      SET contadorPares = contadorPares + 1;
    END IF;
  END WHILE;
  /* CERRAR EL CURSOR */
  CLOSE cursor1;

  SELECT
    contadorPares;

END//
DELIMITER ;

CALL pares_cursor();

--  quiero encontarr en la tabal numeros los registros que seanmayores que 5 y almancenar en una tabala nueva


DELIMITER //
DROP PROCEDURE IF EXISTS mayores_cinco//
CREATE PROCEDURE mayores_cinco ()
BEGIN
  DECLARE final int DEFAULT 0;
  DECLARE numero int DEFAULT 0;
  DECLARE contadorPares int DEFAULT 0;
  DECLARE cursor1 CURSOR FOR
  SELECT
    *
  FROM numeros;
  DECLARE CONTINUE HANDLER FOR NOT FOUND
  SET final = 1;
  --  creacion de tabal
  CREATE OR REPLACE TEMPORARY TABLE mayores (
    valor int
  );
  OPEN cursor1;
  --  leo el primer registro
  FETCH cursor1 INTO numero;
  WHILE (final = 0) DO
    IF (numero > 5) THEN
      INSERT INTO mayores
        VALUE (numero);
    END IF;
    FETCH cursor1 INTO numero;
  END WHILE;
  /* CERRAR EL CURSOR */
  CLOSE cursor1;
  SELECT
    *
  FROM mayores;
END//
DELIMITER ;

CALL mayores_cinco();


CREATE OR REPLACE TEMPORARY TABLE notas (
  id int AUTO_INCREMENT PRIMARY KEY,
  nota int,
  repeticiones int,
  valor varchar(50)
);


--  en repeticiones hay que poner el número de veces que se repite esa  nota haste el valor leido
-- la nota pero en texto
--  0,5 suspenso
--  5,6 suficiente
--  6,7 bien
--  7,9 notable
--  9,10 sobresaliente

DELIMITER //
DROP PROCEDURE IF EXISTS leer_repetidos//
CREATE PROCEDURE leer_repetidos ()
BEGIN
  -- variables
  DECLARE final int DEFAULT 0;
  DECLARE numero int DEFAULT 0;
  DECLARE resultado varchar(30) DEFAULT 'sobresaliente';
  DECLARE repeticiones varchar(30);
  DECLARE registro int DEFAULT 0;
  --  repeticiones
  DECLARE r0 int DEFAULT 0;
  DECLARE r1 int DEFAULT 0;
  DECLARE r2 int DEFAULT 0;
  DECLARE r3 int DEFAULT 0;
  DECLARE r4 int DEFAULT 0;
  DECLARE r5 int DEFAULT 0;
  DECLARE r6 int DEFAULT 0;
  DECLARE r7 int DEFAULT 0;
  DECLARE r8 int DEFAULT 0;
  DECLARE r9 int DEFAULT 0;
  DECLARE r10 int DEFAULT 0;


  DECLARE cursor1 CURSOR FOR
  SELECT
    id,
    nota
  FROM notas;
  DECLARE CONTINUE HANDLER FOR NOT FOUND
  SET final = 1;
  OPEN cursor1;
  --  leo el primer registro
  FETCH cursor1 INTO registro, numero;
  WHILE (final = 0) DO

    --  calculo el texto de la nota
    SET resultado = notaTexto(numero);

    --  calculo las repeticiones

    CASE
      WHEN (numero = 0) THEN SET r0 = r0 + 1;
          SET repeticiones = r0;
      WHEN (numero = 1) THEN SET r1 = r1 + 1;
          SET repeticiones = r1;

      WHEN (numero = 2) THEN SET r2 = r2 + 1;
          SET repeticiones = r2;
      WHEN (numero = 3) THEN SET r3 = r3 + 1;
          SET repeticiones = r3;
      WHEN (numero = 4) THEN SET r4 = r4 + 1;
          SET repeticiones = r4;
      WHEN (numero = 5) THEN SET r5 = r5 + 1;
          SET repeticiones = r5;
      WHEN (numero = 6) THEN SET r6 = r6 + 1;
          SET repeticiones = r6;
      WHEN (numero = 7) THEN SET r7 = r7 + 1;
          SET repeticiones = r7;
      WHEN (numero = 8) THEN SET r8 = r8 + 1;
          SET repeticiones = r8;
      WHEN (numero = 9) THEN SET r9 = r9 + 1;
          SET repeticiones = r9;
      ELSE SET r10 = r10 + 1;
        SET repeticiones = r10;
    END CASE;

    --  Actualizacion
    UPDATE notas
    SET valor = resultado
    WHERE nota = numero;



    UPDATE notas
    SET repeticiones = repeticiones
    WHERE notas.id = registro;

    FETCH cursor1 INTO registro, numero;




  END WHILE;
  /* CERRAR EL CURSOR */
  CLOSE cursor1;
  SELECT
    *
  FROM notas;
END//
DELIMITER;

TRUNCATE notas;
INSERT INTO notas (nota)
  VALUES (5), (5), (3), (7), (9), (9), (10), (5), (1);

CALL leer_repetidos();


--  Funcion de sacar la nota en palabra
DELIMITER //
CREATE OR REPLACE FUNCTION notaTexto (valor int)
RETURNS varchar(50)
BEGIN
  DECLARE resultado varchar(50);
  CASE
    WHEN (valor < 5) THEN SET resultado = 'Suspenso';
    WHEN (valor < 6) THEN SET resultado = 'Suficiente';
    WHEN (valor < 7) THEN SET resultado = 'Bien';
    WHEN (valor < 9) THEN SET resultado = 'Notable';
    ELSE SET resultado = 'Sobresaliente';
  END CASE;
  RETURN resultado;
END//
DELIMITER ;


SELECT
  notaTexto(10);