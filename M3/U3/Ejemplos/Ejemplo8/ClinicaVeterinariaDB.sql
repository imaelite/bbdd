﻿DROP DATABASE IF EXISTS clinicaVeterinaria;
CREATE DATABASE IF NOT EXISTS clinicaVeterinaria;
USE clinicaVeterinaria;


CREATE TABLE cliente (
  idCliente int AUTO_INCREMENT PRIMARY KEY,
  edad int DEFAULT 0,
  fNacimiento date,
  nombre varchar(50)
);


CREATE TABLE mascota (
  idMascota int AUTO_INCREMENT,
  idCliente int,
  edad int DEFAULT 0,
  fNacimiento date,
  nombre varchar(50),
  PRIMARY KEY (idMascota, idCliente)
);

CREATE TABLE producto (
  idProducto int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50),
  cantidad int,
  precio float,
  idCliente int
);

CREATE TABLE veterinario (
  idVet int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(50)
);

CREATE TABLE atiende (
  idMascota int,
  idVet int,
  PRIMARY KEY (idMascota, idVet)
);

-- Insercion -----------------


--  clientes
INSERT INTO cliente (idCliente, edad, fNacimiento, nombre)
  VALUES (1, 0, '1987/09/03', 'imanol');

INSERT INTO cliente (idCliente, edad, fNacimiento, nombre)
  VALUES (DEFAULT, 0, '1995/03/08', 'kevin');

INSERT INTO cliente (idCliente, edad, fNacimiento, nombre)
  VALUES (DEFAULT, 0, '1958/12/25', 'luis');

--  mascotas

INSERT INTO mascota (idMascota, idCliente, edad, fNacimiento, nombre)
  VALUES (DEFAULT, 1, 0, '2005/09/03', 'mascota1');


INSERT INTO mascota (idMascota, idCliente, edad, fNacimiento, nombre)
  VALUES (DEFAULT, 1, 0, '2002/09/03', 'mascota2');
INSERT INTO mascota (idMascota, idCliente, edad, fNacimiento, nombre)
  VALUES (DEFAULT, 1, 0, '2007/10/03', 'mascota3');


--   productos

INSERT INTO producto (idProducto, nombre, cantidad, precio, idCliente)
  VALUES (DEFAULT, 'huesoGoma', 10, 10.50, 1);

INSERT INTO producto (idProducto, nombre, cantidad, precio, idCliente)
  VALUES (DEFAULT, 'correa', 15, 7.50, 1);

INSERT INTO producto (idProducto, nombre, cantidad, precio, idCliente)
  VALUES (DEFAULT, 'pelotaGoma', 3, 4.50, 2);


--  veterinario

INSERT INTO veterinario (idVet, nombre)
  VALUES (DEFAULT, 'ramon');

INSERT INTO veterinario (idVet, nombre)
  VALUES (DEFAULT, 'david');


--  atiende

INSERT INTO atiende (idMascota, idVet)
  VALUES (20, 1);
INSERT INTO atiende (idMascota, idVet)
  VALUES (1, 2);

INSERT INTO atiende (idMascota, idVet)
  VALUES (2, 2);

INSERT INTO atiende (idMascota, idVet)
  VALUES (3, 2);