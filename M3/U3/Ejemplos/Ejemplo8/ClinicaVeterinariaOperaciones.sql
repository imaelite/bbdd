﻿-- procedimientos

--  1 introducir clientes
DELIMITER //
DROP PROCEDURE IF EXISTS altaCliente//
CREATE PROCEDURE altaCliente (f_nac date, nombre varchar(50))
BEGIN
  DECLARE edad int DEFAULT 0;

  --  
  SET edad = calcEdad(f_nac);


  INSERT INTO cliente (edad, fNacimiento, nombre)
    VALUES (edad, f_nac, nombre);

END//
DELIMITER ;

CALL altaCliente('1987/09/03', 'ladislao');
CALL altaCliente('1992/09/03', 'maria');


-- 2 borrar mascota


DELIMITER //
DROP PROCEDURE IF EXISTS bajaMascota//
CREATE PROCEDURE bajaMascota (idCliente_para int, idMascota_para int)
BEGIN
  --  borrar mascota de tabla mascotas
  DELETE
    FROM mascota
  WHERE idMascota = idMascota_para
    AND idCliente = idCliente_para;
  --  borrar mascota de tabla atiende
  DELETE
    FROM atiende
  WHERE idMascota = idMascota_para;

END//
DELIMITER ;

CALL bajaMascota(1, 1);

--  3 introducir mascota


DELIMITER //
DROP PROCEDURE IF EXISTS altaMascota//
CREATE PROCEDURE altaMascota (idCliente_para int, fecha_para date, nombre_masc varchar(50))
BEGIN
  DECLARE esta int DEFAULT 0;

  DECLARE edad_var int DEFAULT 0;

  SELECT
    COUNT(*) INTO esta
  FROM cliente
  WHERE idCliente = idCliente_para;

  IF (esta <> 0) THEN
    SET edad_var = calcEdad(fecha_para);
    INSERT INTO mascota (idMascota, idCliente, edad, fNacimiento, nombre)
      VALUES (DEFAULT, idCliente_para, edad_var, fecha_para, nombre_masc);
  END IF;

END//
DELIMITER ;

CALL altaMascota(1, '2012/12/02', 'petter');

CALL altaMascota(11, '2016/12/02', 'juanito');






--  funciones

-- 1 calcular edad

DELIMITER //
CREATE OR REPLACE FUNCTION calcEdad (fecha_para date)
RETURNS int
BEGIN
  DECLARE edad int DEFAULT 0;
  SET edad = TIMESTAMPDIFF(year, fecha_para, NOW());
  RETURN edad;
END//
DELIMITER ;

--  2 devolver nombre de mascotas de un cliente concreto

DELIMITER //
CREATE OR REPLACE FUNCTION mostrarMascotas (idCliente_para int)
RETURNS varchar(200)
BEGIN
  DECLARE listaPerros varchar(200) DEFAULT '';
  DECLARE final int DEFAULT 0;
  DECLARE perroConcreto varchar(50) DEFAULT '';
  DECLARE cursor1 CURSOR FOR
  SELECT
    nombre
  FROM mascota
  WHERE idCliente = idCliente_para;
  DECLARE CONTINUE HANDLER FOR NOT FOUND
  SET final = 1;

  OPEN cursor1;

  FETCH cursor1 INTO perroConcreto;

  WHILE (final = 0) DO
    SET listaPerros = CONCAT(listaPerros, ' ', perroConcreto);
    FETCH cursor1 INTO perroConcreto;
  END WHILE;
  CLOSE cursor1;
  RETURN listaPerros;
END//
DELIMITER ;

SELECT
  mostrarMascotas(1);


--  3 calcular el precio total de lo que ha comprado un cliente



DELIMITER //
CREATE OR REPLACE FUNCTION calcPrecioTotal (idCliente_para int)
RETURNS float
BEGIN
  DECLARE precioTotal float DEFAULT 0;

  SELECT
    SUM(cantidad * precio) INTO precioTotal
  FROM producto
  WHERE idCliente = idCliente_para;


  RETURN precioTotal;
END//
DELIMITER ;

SELECT
  calcPrecioTotal(2);

--  disparadores

-- 1  actualizar el campo totalFacturado, cada vez que se introduce un producto vendido
DROP TRIGGER IF EXISTS productoAI;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER productoAI
AFTER INSERT
ON producto
FOR EACH ROW
BEGIN
  DECLARE precioTotalCalculado float DEFAULT 0;
  SET precioTotalCalculado = calcPrecioTotal(new.idCliente);

  UPDATE cliente
  SET totalFacturado = precioTotalCalculado
  WHERE idCliente = new.idCliente;
END//
DELIMITER;

INSERT INTO producto (idProducto, nombre, cantidad, precio, idCliente)
  VALUES (DEFAULT, 'correaMax', 10, 5, 3);

INSERT INTO producto (idProducto, nombre, cantidad, precio, idCliente)
  VALUES (DEFAULT, 'correaMin', 5, 5, 2);

INSERT INTO producto (idProducto, nombre, cantidad, precio, idCliente)
  VALUES (DEFAULT, 'bozal Max', 3, 5, 1);


--  2 actualize el precio tatal de cada cliente cuando actualizemos la cantidad o precio de cada producto

DROP TRIGGER IF EXISTS productosAU;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER productosAU
AFTER UPDATE
ON producto
FOR EACH ROW
BEGIN
  DECLARE precioTotalCalculado float DEFAULT 0;

  --  calcular el total gastado de cada cliente en la tabla productos
  SET precioTotalCalculado = calcPrecioTotal(new.idCliente);

  --  actualizamos la tabla cliente con la variable anterior
  UPDATE cliente
  SET totalFacturado = precioTotalCalculado
  WHERE idCliente = new.idCliente;
END//
DELIMITER;


--  3 comprobar que la edad de un perro sea correcta, y si no dar un aviso

DROP TRIGGER mascotaBI;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER mascotaBI
BEFORE INSERT
ON mascota
FOR EACH ROW
BEGIN
  DECLARE edad_numero int DEFAULT 0;
  DECLARE esta int DEFAULT 0;

  --  limira edad
  SET edad_numero = calcEdad(new.fNacimiento);

  -- check
  IF (edad_numero < 1) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha debe ser anterior que la fecha actual';
  END IF;
--  que el cliente exista

    SELECT
    COUNT(*) INTO esta
  FROM cliente
  WHERE idCliente = new.idCliente;

  IF (esta = 0) THEN

  SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El cliente introducido no existe';
    END IF;
END//
DELIMITER;


CALL altaMascota (1,'2012/03/15','flafy');

--  4 cuando borro un cliente, borro sus mascotas y sus productos asociados

DROP TRIGGER IF EXISTS clienteAD;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER clienteAD
AFTER DELETE 
ON cliente
FOR EACH ROW
BEGIN
DECLARE idClienteVar int DEFAULT 0;

  --  borrar de la tabla atiende
  DELETE FROM atiende WHERE idMascota IN (SELECT idMascota FROM mascota WHERE idCliente=OLD.idCliente);


--  borra de la tabla mascota
 DELETE FROM mascota WHERE idCliente=OLD.idCliente;


  --  borrar de la tabla productos
  DELETE FROM producto WHERE idCliente=OLD.idCliente;
END//
DELIMITER;


-- 5 actualice la edad de una cliente tras actualizar la fecha


  DROP TRIGGER IF EXISTS clienteBU;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER clienteBU
BEFORE UPDATE  
ON cliente
FOR EACH ROW
BEGIN
DECLARE edad_nueva int DEFAULT 0;

  SET edad_nueva=calcEdad(new.fNacimiento);

  SET new.edad=edad_nueva;


END//
DELIMITER;


-- 6 actualice la edad de una mascota tras actualizar la fecha

    DROP TRIGGER IF EXISTS mascotaBU;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER mascotaBU
BEFORE UPDATE  
ON mascota
FOR EACH ROW
BEGIN
DECLARE edad_nueva int DEFAULT 0;

  SET edad_nueva=calcEdad(new.fNacimiento);

  SET new.edad=edad_nueva;

END//
DELIMITER;


--  7 cuando borro una mascotase borran los registros de atiende


  DROP TRIGGER IF EXISTS mascotaAD;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER mascotaAD
AFTER DELETE   
ON mascota
FOR EACH ROW
BEGIN
DECLARE edad_nueva int DEFAULT 0;

  DELETE
    FROM atiende
  WHERE idMascota = OLD.idMascota;

END//
DELIMITER;


--  8 cuando borro un VETERINARIO borran los registros de atiende


  DROP TRIGGER IF EXISTS vetAD;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER vetAD
AFTER DELETE   
ON veterinario
FOR EACH ROW
BEGIN

  DELETE
    FROM atiende
  WHERE idVet = OLD.idVet;

END//
DELIMITER;




















