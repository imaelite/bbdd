﻿
--  ejemplo 9

--  1.a
DROP DATABASE IF EXISTS ejemplo9;
CREATE DATABASE ejemplo9;
USE ejemplo9;

--  apartado 1.b

CALL crearTablaEmpleados(1);

SELECT
  *
FROM empleados;


--  apartado 1.c


CALL crearTablaEmpresa(1);

SELECT
  *
FROM empresa;


--  apartado 1.d

CALL crearTablaTrabajan(1);

SELECT
  *
FROM trabajan;


--  apartado 1.e

--  debe dar error
INSERT INTO trabajan (empleado, empresa, fechaInicio, fechaFin, baja)
  VALUES (3, 3, '2011/1/1', '2010/1/1', NULL);

--  no debe dar error
INSERT INTO trabajan (empleado, empresa, fechaInicio, fechaFin, baja)
  VALUES (2, 2, '2011/1/1', '2016/1/1', NULL);



--  si la fecha de fin no está vacia el campo baja pasa a null

  INSERT INTO trabajan (empleado, empresa, fechaInicio, fechaFin)
  VALUES (3, 3, '2014/1/1', '2016/1/1');

   INSERT INTO trabajan (empleado, empresa, fechaInicio)
  VALUES (2, 2, '2014/1/1');

--  poner la fecha a null y la baja tendría que ponerse de nuevo a false

  UPDATE trabajan SET fechaFin=null WHERE id=2;

TRUNCATE trabajan;
SELECT
  *
FROM trabajan;

UPDATE trabajan SET fechaFin='2016/1/1' WHERE id=2;
UPDATE trabajan SET fechaFin=NULL WHERE id=2;



--  si no existe el empleado en la tabla empleado o la empresa impedir introducir un dato en la tabla trabaja



   INSERT INTO trabajan (empleado, empresa, fechaInicio)
  VALUES (2, 7, '2014/1/1');

  UPDATE trabajan SET empresa=77 WHERE id=1;



  --  crear una funcion que reciba una id de empresa y me deviuelva si la empresa existe y otra pa aempleado


    SELECT empleadoEsta(11);

    SELECT empresaEsta(21);

    --  comprobar de nuevo si las funciones creadas en el  trigger funcionan

SELECT * FROM empleados;
SELECT * FROM empresa;
SELECT * FROM trabajan;

   INSERT INTO trabajan (empleado, empresa, fechaInicio)
  VALUES (4,1,'2014/1/1');


  -- probar el trigger de insercion, 

       INSERT INTO empleados (id,nombre,fechaNacimiento)
  VALUES (8,'luis','2012/1/1');

SELECT * FROM empleados;


SELECT empresa, COUNT(*) n FROM trabajan WHERE empleado=2 GROUP BY empresa;

SELECT MAX(n) FROM (SELECT empresa, COUNT(*) n FROM trabajan WHERE empleado=2 GROUP BY empresa) c1;

SELECT c1.empresa FROM (SELECT empresa, COUNT(*) n FROM trabajan WHERE empleado=2 GROUP BY empresa) c1 WHERE c1.n=(SELECT MAX(n) FROM (SELECT empresa, COUNT(*) n FROM trabajan WHERE empleado=2 GROUP BY empresa) c1) limit 1;

--  mirara la empresa en la que mas ha trabajado un empleado
SELECT 
       nombre
       
        FROM empresa WHERE id=(SELECT c1.empresa FROM (SELECT empresa, COUNT(*) n FROM trabajan WHERE empleado=2 GROUP BY empresa) c1 WHERE c1.n=(SELECT MAX(n) FROM (SELECT empresa, COUNT(*) n FROM trabajan WHERE empleado=2 GROUP BY empresa) c1) limit 1);



