﻿
--  tabla empleados
DELIMITER //
DROP PROCEDURE IF EXISTS crearTablaEmpleados//
CREATE PROCEDURE crearTablaEmpleados (test int)
BEGIN
  CREATE OR REPLACE TABLE empleados (
    id int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(50),
    fechaNacimiento date,
    correo varchar(20),
    edad int
  );
  IF (test = 1) THEN
    INSERT INTO empleados (nombre, fechaNacimiento)
      VALUES ('pedro', '2000/1/1'),
      ('ana', '1980/2/1'),
      ('jose', '1990/3/1'),
      ('luis', '2004/5/1'), ('pedro', '2000/1/1')
    ;
  END IF;

END//
DELIMITER ;



--  tabla Empresas

DELIMITER //
DROP PROCEDURE IF EXISTS crearTablaEmpresa//
CREATE PROCEDURE crearTablaEmpresa (test int)
BEGIN
  CREATE OR REPLACE TABLE empresa (
    id int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(50),
    direccion varchar(50),
    numero_empleados int
  );
  IF (test = 1) THEN
    INSERT INTO empresa (nombre, direccion)
      VALUES ('empresa1', 'direccion1'),
      ('empresa2', 'direccion2'),
      ('empresa3', 'direccion3'),
      ('empresa4', 'direccion4')
    ;
  END IF;

END//
DELIMITER ;


--  tabla trabajan

DELIMITER //
DROP PROCEDURE IF EXISTS crearTablaTrabajan//
CREATE PROCEDURE crearTablaTrabajan (test int)
BEGIN
  CREATE OR REPLACE TABLE trabajan (
    id int AUTO_INCREMENT PRIMARY KEY,
    empleado int,
    empresa int,
    fechaInicio date,
    fechaFin date,
    baja bool DEFAULT FALSE,
    CONSTRAINT trabajanUK UNIQUE KEY (empleado, empresa)
  ) ENGINE MYISAM;
  IF (test = 1) THEN
    INSERT INTO trabajan (empleado, empresa, fechaInicio, fechaFin)
      VALUES (1, 1, '2011/1/1', NULL);
  END IF;
END//
DELIMITER ;



--  trigger para insertar fecha inciio > fin no te deje

DROP TRIGGER IF EXISTS inicio_fin;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER inicio_fin
BEFORE INSERT
ON trabajan
FOR EACH ROW
BEGIN
  IF (new.fechaInicio > new.fechaFin) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor que la fecha de fin';
  END IF;

END//
DELIMITER;

--  campo baja se pone a true si fecha de fin tiene algo



DROP TRIGGER IF EXISTS trabajanBI;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER trabajanBI
BEFORE INSERT
ON trabajan
FOR EACH ROW
BEGIN
  --  limitar la entrada de fecha inferior de inicio a la final
  IF (new.fechaInicio > new.fechaFin) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor que la fecha de fin';
  END IF;

  --  si la fecha final no esta vacioa baja pasa a true
  IF (new.fechaFin IS NOT NULL) THEN
    SET new.baja = TRUE;
  END IF;
END//
DELIMITER;


--  cuando actualice la fecha fin y sea inferior al inicio (fallo), y cuando la fecha fin sea diferente de vacio baja pas aa true

DROP TRIGGER IF EXISTS trabajanBU;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER trabajanBU
BEFORE UPDATE
ON trabajan
FOR EACH ROW
BEGIN
 -- comprobar si el empleado existe tabla empleado
    IF (new.empleado NOT IN (SELECT
          id
        FROM empleados)) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Debe introducir previamente el empleado en la tabla empleado, antes de actualizar';
    END IF;

   -- comprobar si la emplresa existe tabla empleado
    IF (new.empresa NOT IN (SELECT
          id
        FROM empresa)) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Debe introducir previamente la empresa en la tabla empresa, antes de actualizar';
    END IF;



  IF (new.fechaInicio > new.fechaFin) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor que la fecha de fin';
  END IF;

  --  si la fecha final no esta vacioa baja pasa a true
  IF (new.fechaFin IS NOT NULL) THEN
    SET new.baja = TRUE;
  ELSE
    SET new.baja = FALSE;
  END IF;


END//
DELIMITER;


--  cuando metan un dato en trabajn, compruebe que ese empleado exista


DROP TRIGGER IF EXISTS trabajanBI;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER trabajanBI
BEFORE INSERT
ON trabajan
FOR EACH ROW
BEGIN

  --  compruebo que ese empleado exista

  IF (new.empleado NOT IN (SELECT
        id
      FROM empleados)) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Debe introducir previamente el empleado en la tabla empleado';
  END IF;


   --  compruebo que la empresa exista

  IF (new.empresa NOT IN (SELECT
        id
      FROM empresa)) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Debe introducir previamente la empresa en la tabla empresa';
  END IF;

  --  limitar la entrada de fecha inferior de inicio a la final
  IF (new.fechaInicio > new.fechaFin) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor que la fecha de fin';
  END IF;

  --  si la fecha final no esta vacioa baja pasa a true
  IF (new.fechaFin IS NOT NULL) THEN
    SET new.baja = TRUE;
  END IF;
END//
DELIMITER;


  --  funciones que reciba una id de empresa y me deviuelva si la empresa existe y otra pa aempleado
DELIMITER //
CREATE OR REPLACE FUNCTION empleadoEsta(id_emple int)
RETURNS int
BEGIN
  DECLARE esta int DEFAULT 0;
  SELECT COUNT(*) INTO esta FROM empleados WHERE id=id_emple;

  RETURN esta;
END//
DELIMITER ;


DELIMITER //
CREATE OR REPLACE FUNCTION empresaEsta(id_empre int)
RETURNS int
BEGIN
  DECLARE esta int DEFAULT 0;
  SELECT COUNT(*) INTO esta FROM empresa WHERE id=id_empre;

  RETURN esta;
END//
DELIMITER ;


--  este trigger utiliza las funciones de arriba

DROP TRIGGER IF EXISTS trabajanBI;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER trabajanBI
BEFORE INSERT
ON trabajan
FOR EACH ROW
BEGIN

  --  compruebo que ese empleado exista

  IF (empleadoEsta(new.empleado)=0) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Debe introducir previamente el empleado en la tabla empleado';
  END IF;


   --  compruebo que la empresa exista

  IF (empresaEsta(new.empresa)=0) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Debe introducir previamente la empresa en la tabla empresa';
  END IF;

  --  limitar la entrada de fecha inferior de inicio a la final
  IF (new.fechaInicio > new.fechaFin) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor que la fecha de fin';
  END IF;

  --  si la fecha final no esta vacioa baja pasa a true
  IF (new.fechaFin IS NOT NULL) THEN
    SET new.baja = TRUE;
  END IF;
END//
DELIMITER;



--  modificar el trigger de actualizacion con las funciones

  DROP TRIGGER IF EXISTS trabajanBU;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER trabajanBU
BEFORE UPDATE
ON trabajan
FOR EACH ROW
BEGIN
 -- comprobar si el empleado existe tabla empleado
    IF (empleadoEsta(new.empleado)=0) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Debe introducir previamente el empleado en la tabla empleado, antes de actualizar';
    END IF;

   -- comprobar si la emplresa existe tabla empleado
    IF (empresaEsta(new.empresa)=0) THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Debe introducir previamente la empresa en la tabla empresa, antes de actualizar';
    END IF;



  IF (new.fechaInicio > new.fechaFin) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio debe ser menor que la fecha de fin';
  END IF;

  --  si la fecha final no esta vacioa baja pasa a true
  IF (new.fechaFin IS NOT NULL) THEN
    SET new.baja = TRUE;
  ELSE
    SET new.baja = FALSE;
  END IF;


END//
DELIMITER;

--  funcion de edad

  DELIMITER //
CREATE OR REPLACE FUNCTION calcEdad(fecha_para date)
RETURNS int
BEGIN
  DECLARE edad int DEFAULT 0;
  SET edad = TIMESTAMPDIFF(year, fecha_para, NOW());
  RETURN edad;
END//
DELIMITER ;


--  disparador de introduccion de empleados

  DROP TRIGGER IF EXISTS empleadosBI;
DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER empleadosBI
BEFORE INSERT
ON empleados
FOR EACH ROW
BEGIN

  DECLARE edad_calculada int DEFAULT 0;
  DECLARE correo_calculado varchar(300) DEFAULT '';

  --  calcular la edad
  
  set edad_calculada=calcEdad(new.fechaNacimiento);
  SET NEW.edad=edad_calculada;
--  calcular el correo
-- empresa para la que más a trabajado


  SET correo_calculado=CONCAT(correo_calculado,new.nombre,'@');


END//
DELIMITER;