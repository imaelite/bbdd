﻿
DROP DATABASE IF EXISTS m3u3ej3;
CREATE DATABASE m3u3ej3;
USE m3u3ej3;

--  ej2 funcion area de triangulo


DELIMITER //
CREATE OR REPLACE FUNCTION ej2 (base float, altura float)
RETURNS float
BEGIN
  DECLARE area float DEFAULT 0;
  SET area = (base * altura) / 2;
  RETURN area;
END//

DELIMITER ;

SELECT
  ej2(5, 6);


--  ej3 funcion perímetro triangulo

DELIMITER //
CREATE OR REPLACE FUNCTION ej3 (base float, lado2 float, lado3 float)
RETURNS float
BEGIN
  DECLARE perimetro float DEFAULT 0;
  SET perimetro = base + lado2 + lado3;
  RETURN perimetro;
END//

DELIMITER ;

SELECT
  ej3(5, 6, 9);

--  ej4 procedimiento almacenado triangulos   ??????????????

DELIMITER //
DROP PROCEDURE IF EXISTS ej4//
CREATE PROCEDURE ej4 (id1 int, id2 int)
BEGIN

  UPDATE triangulos
  SET area = ej2(base, altura),
      perimetro = ej3(base, lado2, lado3)
  WHERE id BETWEEN id1 AND id2;
END//
DELIMITER ;

SELECT
  *
FROM triangulos;
CALL ej4(3, 6);


--  ej  5 función area cuadrado

DELIMITER //
CREATE OR REPLACE FUNCTION ej5 (lado float)
RETURNS float
BEGIN
  DECLARE area float DEFAULT 0;
  SET area = POW(lado, 2);
  RETURN area;
END//

DELIMITER ;

SELECT
  ej5(5);


--  ej 6 funcion perimetro cuadrado

DELIMITER //
CREATE OR REPLACE FUNCTION ej6 (lado float)
RETURNS float
BEGIN
  DECLARE perimetro float DEFAULT 0;
  SET perimetro = lado * 4;
  RETURN perimetro;
END//

DELIMITER ;

SELECT
  ej6(5);

--  ej 7 procedimiento almacenadpo cuadrados


DELIMITER //
DROP PROCEDURE IF EXISTS ej7//
CREATE PROCEDURE ej7 (id1 int, id2 int)
BEGIN
  UPDATE cuadrados
  SET perimetro = ej6(lado),
      area = ej5(lado)
  WHERE id BETWEEN id1 AND id2;
END//
DELIMITER ;

SELECT
  *
FROM cuadrados;
CALL ej7(3, 6);


--  ej 8 funcion area circulo

DELIMITER //
CREATE OR REPLACE FUNCTION ej8 (radio float)
RETURNS float
BEGIN
  DECLARE area float DEFAULT 0;
  SET area = 3.1416 * POW(radio, 2);
  RETURN area;
END//

DELIMITER ;

SELECT
  ej8(5);

--  ej 9 funcion perimetro de un circulo

DELIMITER //
CREATE OR REPLACE FUNCTION ej9 (radio float)
RETURNS float
BEGIN


  DECLARE perimetro float DEFAULT 0;

  SET perimetro = 2 * 3.1416 * radio;

  RETURN perimetro;
END//

DELIMITER ;

SELECT
  ej9(5);

SELECT
  *
FROM circulo;

--  ej 10 perimetro almacenado de círculos


DELIMITER//
DROP PROCEDURE IF EXISTS ej10//
CREATE PROCEDURE ej10 (id1 int, id2 int, tipo_a char)
BEGIN
  IF tipo_a IS NULL THEN
    UPDATE circulo
    SET area = ej8(radio),
        perimetro = ej9(radio)
    WHERE id BETWEEN id1 AND id2;
  ELSE
    UPDATE circulo
    SET area = ej8(radio),
        perimetro = ej9(radio)
    WHERE id BETWEEN id1 AND id2
    AND tipo = tipo_a;
  END IF;
END//
DELIMITER;

UPDATE circulo
SET area = '',
    perimetro = '';
SELECT
  *
FROM circulo;
CALL ej10(1, 5, NULL);


--  ej 11 realiza una funcion que reciba como argumentos 4 notas y saque la media
DELIMITER //
CREATE OR REPLACE FUNCTION ej11 (nota1 float, nota2 float, nota3 float, nota4 float)
RETURNS float
BEGIN
  DECLARE media float DEFAULT 0;
  SET media = (nota1 + nota2 + nota3 + nota4) / 4;
  RETURN media;
END//
DELIMITER ;

SELECT
  ej11(5, 3, 5, 7);

SELECT
  *
FROM circulo;


--  ej 12 realiza una funcion que reciba 4 notas y devuelva la mínima
DELIMITER //
CREATE OR REPLACE FUNCTION ej12 (nota1 float, nota2 float, nota3 float, nota4 float)
RETURNS float
BEGIN
  DECLARE minima float DEFAULT 0;

  SET minima = LEAST(nota1, nota2, nota3, nota4);
  RETURN minima;
END//
DELIMITER ;

SELECT
  ej12(5, 3, 5, 7);

--  ej 13 realiza una funcion que reciba 4 notas y devuelva la maxima
DELIMITER //
CREATE OR REPLACE FUNCTION ej13 (nota1 float, nota2 float, nota3 float, nota4 float)
RETURNS float
BEGIN
  DECLARE maxima float DEFAULT 0;

  SET maxima = GREATEST(nota1, nota2, nota3, nota4);
  RETURN maxima;
END//
DELIMITER ;

SELECT
  ej13(5, 3, 5, 7);

--  ej 14 realiza una funcion que reciba 4 notas y devuelva la moda
DELIMITER //
CREATE OR REPLACE FUNCTION ej14 (nota1 float, nota2 float, nota3 float, nota4 float)
RETURNS float
BEGIN
  DECLARE r float DEFAULT 0;
  DECLARE mayor float DEFAULT 0;
  CREATE TEMPORARY TABLE IF NOT EXISTS moda (
    id int AUTO_INCREMENT PRIMARY KEY,
    numero int DEFAULT 0
  );
  INSERT INTO moda
    VALUEs (DEFAULT,nota1),(DEFAULT,nota2),(DEFAULT,nota3),(DEFAULT,nota4);

  SELECT numero INTO r FROM moda GROUP BY numero order BY count(*) DESC LIMIT 1; 

  RETURN r;
END//
DELIMITER;

SELECT
  ej14(1, 1, 2, 2);



--

  /**
 Ejercicio 16

 Realizar una función que reciba como argumentos 4 notas.
 Debe devolver el cálculo de la nota maxima
 
 **/

  DELIMITER //
  CREATE OR REPLACE FUNCTION maximo
    (n1 int, n2 int, n3 int, n4 int)
  RETURNS int
    COMMENT 'Realizar una función que reciba como argumentos 4 notas. Debe devolver el cálculo de la nota maxima'
  BEGIN
    DECLARE r int;
    set r=GREATEST(n1,n2,n3,n4);
    RETURN r;
  END //
  DELIMITER ;
  
SELECT maximo(4,12,8,-9);

CREATE OR REPLACE PROCEDURE actualizarAlumnos(
 IN id1 int,
 IN id2 int)
COMMENT ' Realizar un procedimiento almacenado que cuando le llames como argumentos:
-	Id1: id inicial
-	Id2: id final
Actualice las notas mínima, máxima, media y moda de los alumnos (utilizando las funciones realizadas) que estén comprendidos entre los id pasados.
Además, el mismo procedimiento debe actualizar la tabla grupos colocando la nota media de los alumnos que estén comprendidos entre los id pasados y por grupo.'
BEGIN
  UPDATE alumnos 
    SET 
    media=media(nota1,nota2,nota3,nota4),
    minimo=minimo(nota1,nota2,nota3,nota4),
    maximo=maximo(nota1,nota2,nota3,nota4),
    moda=moda(nota1,nota2,nota3,nota4)
    WHERE 
      id BETWEEN id1 AND id2;

  UPDATE grupos
    SET media=(SELECT avg(media) FROM alumnos WHERE grupo=1)
    WHERE id=1;
  
  UPDATE grupos
    SET media=(SELECT avg(media) FROM alumnos WHERE grupo=2)
    WHERE id=2;

END;







