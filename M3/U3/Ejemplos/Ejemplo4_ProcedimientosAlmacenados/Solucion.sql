﻿DROP DATABASE IF EXISTS m3u3p4;
CREATE DATABASE m3u3p4;
USE m3u3p4;

--  ej1

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio1//
CREATE PROCEDURE ejercicio1 ()
BEGIN

  SELECT
    'esto es un ejmplo de procedimiento' AS mensaje;
END//
DELIMITER ;


CALL ejercicio1();

--  ej2


DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio1//
CREATE PROCEDURE ejercicio1 ()
BEGIN
  DECLARE var1 int DEFAULT 1;
  DECLARE var2 varchar(10) DEFAULT NULL;
  DECLARE var3 double(4, 2) DEFAULT 10.48;



  SELECT
    'esto es un ejmplo de procedimiento' AS mensaje;
END//
DELIMITER ;


--  ej3

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo3//
CREATE PROCEDURE ejemplo3 ()
BEGIN
  DECLARE v_caracter1 char(1);
  DECLARE forma_pago enum ('metalico', 'tarjeta', 'transferencia');

  SET v_caracter1 = 'hola'; -- tiene que ser 1 caracter
  SET forma_pago = 1; -- no aparece en la lista
  SET forma_pago = 'cheque'; -- no existe
  SET forma_pago = 4; -- no aparece en la lista
  SET forma_pago = 'TARJETA';
  SELECT
    forma_pago;

END//
DELIMITER ;

CALL ejemplo3();

--  ej4

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo4//
CREATE PROCEDURE ejemplo4 (num double)
BEGIN

  SELECT
    num;

END//
DELIMITER ;

CALL ejemplo4(5.5);

--  ej5
DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo5//
CREATE PROCEDURE ejemplo5 (num double)
BEGIN
  DECLARE var1 double;

  SET var1 = num;

  SELECT
    var1;

END//
DELIMITER ;

CALL ejemplo5(5.5);

--  ej6

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo6//
CREATE PROCEDURE ejemplo6 (num double)
BEGIN

  IF (num = 0) THEN
    SELECT
      'cero';
  ELSEIF num > 0 THEN
    SELECT
      'Es positivo';
  ELSE
    SELECT
      'Es Negativo';
  END IF;

END//
DELIMITER ;

CALL ejemplo6(0.5);

--  ej7  

DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio7 (num int)
RETURNS boolean
BEGIN
  DECLARE respuesta boolean DEFAULT TRUE;

  IF (MOD(num, 2) = 0) THEN
    SET respuesta = TRUE;
  ELSE
    SET respuesta = FALSE;
  END IF;

  RETURN respuesta;
END//
DELIMITER ;


SELECT
  ejercicio7(8);

--  ej 8


DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio8 (lado1 double, lado2 double)
RETURNS double
BEGIN
  DECLARE hipotenusa double DEFAULT 0;

  SET hipotenusa = POW(lado1, 2) + POW(lado2, 2);

  SET hipotenusa = SQRT(hipotenusa);


  RETURN hipotenusa;
END//
DELIMITER ;


SELECT
  ejercicio8(9, 11);

--  ej 9

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo9//
CREATE PROCEDURE ejemplo9 (num double, OUT salida varchar(30))
BEGIN

  IF (num = 0) THEN
    SET salida =
    'cero';
  ELSEIF num > 0 THEN
    SET salida =
    'Es positivo';
  ELSE
    SET salida =
    'Es Negativo';
  END IF;

END//
DELIMITER ;

SET @salida = 0;
CALL ejemplo9(0, @salida);
SELECT
  @salida;

--  ej 10
DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo10//
CREATE PROCEDURE ejemplo10 (nota double)
BEGIN
  CASE
    WHEN nota < 5 THEN SELECT
          'Insuficiente';
    WHEN nota < 6 THEN SELECT
          'Suficiente';
    WHEN nota < 7 THEN SELECT
          'Bien';
    WHEN nota < 9 THEN SELECT
          'Notable';
    WHEN nota <= 10 THEN SELECT
          'Sobresaliente';
  END CASE;
END//
DELIMITER ;

CALL ejemplo10(10);

--  ej 11

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo11//
CREATE PROCEDURE ejemplo11 (nombre varchar(50), nota real)
BEGIN
  CREATE TABLE IF NOT EXISTS notas (
    id int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(50),
    nota real
  );

  INSERT INTO notas
    VALUES (DEFAULT, nombre, nota);
END//
DELIMITER ;

CALL ejemplo11('imanol', 8);

SELECT
  *
FROM notas;

--  ej12

DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio7 (num int)
RETURNS varchar(50)
BEGIN
  DECLARE respuesta varchar(50);

  CASE
    WHEN num = 1 THEN SET respuesta = 'Lunes';
    WHEN num = 2 THEN SET respuesta = 'Martes';
    WHEN num = 3 THEN SET respuesta = 'Miércoles';
    WHEN num = 4 THEN SET respuesta = 'Jueves';
    WHEN num = 5 THEN SET respuesta = 'Vernes';
    WHEN num = 6 THEN SET respuesta = 'Sabado';
    WHEN num = 7 THEN SET respuesta = 'Domingo';
  END CASE;
  RETURN respuesta;
END//
DELIMITER ;

SELECT
  ejercicio7(8);

--  ej13

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo13//
CREATE PROCEDURE ejemplo13 (nombre varchar(50))
BEGIN
  DECLARE dato varchar(50);
  DECLARE respuesta int;

  SET respuesta = (SELECT
      COUNT(*)
    FROM notas
    WHERE notas.nombre = nombre);


  SELECT
    CONCAT('El número de alumnos es ', respuesta);
END//
DELIMITER ;

SELECT
  *
FROM notas;
CALL ejemplo13('imanol');


--  ej14
DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo14//
CREATE PROCEDURE ejemplo14 (nombre varchar(50))
BEGIN
  DECLARE respuesta int;
  DECLARE dato varchar(50);

  DECLARE contador int DEFAULT (SELECT
      COUNT(*)
    FROM notas);


  DECLARE contador_nombres int DEFAULT 0;
  DECLARE cursor1 CURSOR FOR
  SELECT
    notas.nombre
  FROM notas;

  OPEN cursor1;

  WHILE contador > 0 DO

    FETCH cursor1 INTO dato;


    IF (dato = nombre) THEN
      SET contador_nombres = contador_nombres + 1;
    END IF;

    SET contador = contador - 1;
  END WHILE;

  SELECT
    CONCAT('El número de alumnos es ', contador_nombres);
END//
DELIMITER ;



SELECT
  *
FROM notas;
CALL ejemplo14('iman');



--  ej15
DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo15//
CREATE PROCEDURE ejemplo15 (nombre varchar(50))
BEGIN
  --  declaracion de variales
  DECLARE final int DEFAULT 0;
  DECLARE dato varchar(50);
  DECLARE contador_nombres int DEFAULT 0;

  -- declaracion del cursos
  DECLARE cursor1 CURSOR FOR
  SELECT
    notas.nombre
  FROM notas;

  -- declaracion de la excepcion
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET final = 1;

  --  abrimos el cursos
  OPEN cursor1;

  --  apuntamos al primer registro
  FETCH cursor1 INTO dato;

  WHILE final = 0 DO
    -- mirar si el nonbre esiguel que el parámetro
    IF (dato = nombre) THEN
      --  en ese caso lo suma
      SET contador_nombres = contador_nombres + 1;
    END IF;
    --  pasamos de registro en registro
    FETCH cursor1 INTO dato;
  END WHILE;

  SELECT
    CONCAT('El número de alumnos es ', contador_nombres);
END//
DELIMITER ;

CALL ejemplo15('imanol');

--  ej 16

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo16//
CREATE PROCEDURE ejemplo16 ()
BEGIN
  CREATE TABLE IF NOT EXISTS usuarios (
    id int AUTO_INCREMENT PRIMARY KEY,
    nombre varchar(20),
    contraseña varchar(20),
    nombreUsuario varchar(20),
    CONSTRAINT uniqueNombreUsuario UNIQUE INDEX (nombreUsuario)
  );

END//
DELIMITER ;

CALL ejemplo16();


--  ej 17
DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo17//
CREATE PROCEDURE ejemplo17 (nombre varchar(50), contraseña varchar(50), nombreUsuario varchar(50))
BEGIN
  INSERT INTO usuarios
    VALUES (DEFAULT, nombre, contraseña, nombreUsuario);

END//
DELIMITER ;

CALL ejemplo17('mikel', '123', 'mikel123');

SELECT
  *
FROM usuarios;

--  ej 18

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo18//
CREATE PROCEDURE ejemplo18 (nombre varchar(50), contraseña varchar(50), nombreUsuario varchar(50))
BEGIN
  CALL ejemplo16();
  CALL ejemplo17(nombre, contraseña, nombreUsuario);

END//
DELIMITER ;


CALL ejemplo18('martita', '123', 'marta');

SELECT
  *
FROM usuarios;


--  ej19

DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio19 (nombre_entrada varchar(30))
RETURNS varchar(50)
BEGIN
  DECLARE respuesta varchar(50) DEFAULT 'Falso';

  IF (nombre_entrada IN (SELECT
        nombreUsuario
      FROM usuarios)) THEN
    SET respuesta = 'Verdadero';
  END IF;
  RETURN respuesta;
END//
DELIMITER ;


SELECT
  ejercicio19();
SELECT
  *
FROM usuarios;