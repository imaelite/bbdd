﻿
--  1


DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej1
BEFORE INSERT
ON ventas
FOR EACH ROW
BEGIN
  SET new.total =(new.precio * new.unidades);
END //
DELIMITER;

INSERT INTO ventas (id, producto, precio, unidades, total)
  VALUES (0, 'Pipas', 12, 10, 0);
SELECT * FROM ventas;

--  2

  DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej1
after INSERT
ON ventas
FOR EACH ROW
BEGIN

--  
--  DECLARE total_var float DEFAULT 0;
-- 
--  SET total_var=(new.precio * new.unidades);
-- 
--  SET new.total =total_var;

  
  UPDATE productos SET cantidad=cantidad+new.total WHERE producto=new.producto;
END //
DELIMITER;


INSERT INTO ventas (id, producto, precio, unidades, total)
  VALUES (0, 'P1', 12, 10, 0);

--  ej3

DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej3
BEFORE UPDATE 
ON ventas
FOR EACH ROW
BEGIN
SET new.total=new.unidades*new.precio;
--  ej 8
  UPDATE productos SET cantidad= old.total WHERE productos.producto=old.producto;
END //
DELIMITER;


SELECT * FROM ventas;

SELECT * FROM productos;

UPDATE ventas SET precio=10 WHERE ID=4;

--  ej 4

DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej4
AFTER UPDATE 
ON ventas
FOR EACH ROW
BEGIN
  --  sumar suma
    UPDATE productos SET cantidad=(cantidad+new.total)-old.total WHERE producto=new.producto;
END //
DELIMITER;


UPDATE ventas SET precio=12 WHERE ID=1;

SELECT * FROM productos;
SELECT * FROM ventas;


--  ej 5

DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej5
BEFORE UPDATE 
ON productos
FOR EACH ROW
BEGIN
 
    UPDATE ventas SET total=unidades*precio WHERE ventas.producto=new.producto;
END //
DELIMITER;

UPDATE productos SET producto='p11' WHERE producto='p1';

  SELECT * FROM  productos;

SELECT * FROM ventas;

--  e6 

  DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej6
after DELETE 
ON productos
FOR EACH ROW
BEGIN
  DELETE FROM ventas WHERE producto=old.producto;
END //
DELIMITER;

DELETE FROM productos WHERE producto='p3';

  SELECT * FROM  productos;

SELECT * FROM ventas;

--  ej 7

    DELIMITER //
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER ej7
AFTER DELETE 
ON ventas
FOR EACH ROW
BEGIN
 UPDATE productos SET cantidad=cantidad-old.total WHERE producto=old.producto;
END //
DELIMITER;

  SELECT * FROM  productos;

SELECT * FROM ventas;

DELETE FROM ventas WHERE id=4; 
