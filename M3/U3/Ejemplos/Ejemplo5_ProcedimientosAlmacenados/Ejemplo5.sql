﻿--  ej 1


DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo1//
CREATE PROCEDURE ejemplo1 (var_pais varchar(40))
BEGIN
  SELECT
    nombre_cliente,
    ciudad
  FROM cliente
  WHERE cliente.pais = var_pais;

END//
DELIMITER ;

SELECT
  *
FROM cliente;
CALL ejemplo1('Spain');

--  ej2

DELIMITER //
CREATE OR REPLACE FUNCTION ejemplo2 (var_pais varchar(40))
RETURNS int
BEGIN

  RETURN (SELECT
      COUNT(*)
    FROM cliente
    WHERE pais = var_pais);
END//
DELIMITER ;

SELECT
  ejemplo2('Spain');



--  ej3

DELIMITER //
CREATE OR REPLACE FUNCTION ejemplo3 (var_pais varchar(40))
RETURNS int
BEGIN
  --  declaracion de variales
  DECLARE final int DEFAULT 0;
  DECLARE dato int DEFAULT 0;
  DECLARE contador int DEFAULT 0;

  --  declaracion del cursos
  DECLARE cursor1 CURSOR FOR
  SELECT
    codigo_cliente
  FROM cliente
  WHERE pais = var_pais;

  -- declaracion de la excepcion
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET final = 1;


  OPEN cursor1;


  FETCH cursor1 INTO dato;

  WHILE final = 0 DO
    SET contador = contador + 1;
    FETCH cursor1 INTO dato;
  END WHILE;

  CLOSE cursor1;

  RETURN contador;
END//
DELIMITER ;

SELECT
  ejemplo3('USA');

SELECT
  *
FROM cliente;


--  ej 4

DELIMITER //
CREATE OR REPLACE FUNCTION ejemplo4 (var_cantidad int)
RETURNS int
BEGIN

  RETURN (SELECT
      COUNT(*)
    FROM producto
    WHERE cantidad_en_stock > var_cantidad);
END//
DELIMITER ;

SELECT
  ejemplo4(200);

SELECT
  *
FROM producto
WHERE cantidad_en_stock > 200;

-- ej5
DELIMITER //
CREATE OR REPLACE FUNCTION ejemplo5 (cantidad int)
RETURNS int
BEGIN
  --  declaracion de variales
  DECLARE final int DEFAULT 0;
  DECLARE dato int DEFAULT 0;
  DECLARE contador int DEFAULT 0;

  --  declaracion del cursos
  DECLARE cursor1 CURSOR FOR
  SELECT
    codigo_producto
  FROM producto
  WHERE cantidad_en_stock > cantidad;

  -- declaracion de la excepcion
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET final = 1;

  OPEN cursor1;
  FETCH cursor1 INTO dato;

  WHILE final = 0 DO
    SET contador = contador + 1;
    FETCH cursor1 INTO dato;
  END WHILE;

  CLOSE cursor1;

  RETURN contador;
END//
DELIMITER ;

SELECT
  ejemplo5(200);

--  ej6

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo6//
CREATE PROCEDURE ejemplo6 (var_formapago varchar(40))
BEGIN
  SELECT
    MAX(total)
  FROM pago
  WHERE forma_pago = var_formapago;

END//
DELIMITER ;

SELECT
  *
FROM pago;
CALL ejemplo6('Cheque');

--  ej 7

DELIMITER //
DROP PROCEDURE IF EXISTS ejemplo7//
CREATE PROCEDURE ejemplo7 (var_formapago varchar(40))
BEGIN

  SELECT
    MAX(total),
    MIN(total),
    COUNT(*),
    AVG(total),
    SUM(total)
  FROM pago
  WHERE forma_pago = var_formapago;

END//
DELIMITER ;


CALL ejemplo7('PayPal');


--  ej 8

DELIMITER //
CREATE OR REPLACE FUNCTION ejemplo8 (nom_fab varchar(40))
RETURNS float
BEGIN

  RETURN (SELECT
      AVG(precio_venta)
    FROM producto
    WHERE proveedor = nom_fab);
END//
DELIMITER ;

SELECT
  *
FROM producto;
SELECT
  ejemplo8('NaranjasValencianas.com');

--  ej9
DELIMITER //
CREATE OR REPLACE FUNCTION ejemplo9(nom_fab varchar(40))
RETURNS float
BEGIN
  DECLARE contador int DEFAULT 0;
  DECLARE salida CONDITION FOR SQLSTATE '45000';


  SET contador = (SELECT
      COUNT(*)
    FROM producto
    WHERE proveedor = nom_fab);

  IF (contador = 0) THEN
    SIGNAL salida SET MESSAGE_TEXT = 'El fabricante no existe';
  END IF;

  RETURN (SELECT
      AVG(precio_venta)
    FROM producto
    WHERE proveedor = nom_fab);
END//
DELIMITER ;
SELECT
  *
FROM producto;
SELECT   ejemplo9('imanol');

--  ej10
