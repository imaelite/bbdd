﻿--  creacion de base de datos
DROP DATABASE IF EXISTS m3u3p3;
CREATE DATABASE m3u3p3;
USE m3u3p3;

--  1- Generar un script denominado practica7 para generar el siguiente esquema relacional 


CREATE TABLE zonaUrbana (
  NombreZona int PRIMARY KEY,
  categoria char(1)
);
CREATE TABLE bloqueCasas (
  calle int,
  Numero int,
  Npisos int,
  NombreZona int,
  PRIMARY KEY (calle, Numero)

);

CREATE TABLE casaParticular (
  NombreZona int,
  numero int,
  Mcuadrados float,
  PRIMARY KEY (NombreZona, numero)
);

CREATE TABLE piso (
  calle int,
  Numero int,
  Planta int,
  Puerta int,
  Mcuadrados float,
  PRIMARY KEY (calle, Numero, Planta, Puerta)
);


CREATE TABLE persona (
  dni char(9),
  nombe varchar(40),
  edad int,
  NombreZona int,
  NumCasa int,
  calle int,
  NumBloque int,
  Planta int,
  Puerta int
);


CREATE TABLE PoseeC (
  dni char(9),
  NombreZona int,
  NumCasa int,
  PRIMARY KEY (dni, NombreZona, NumCasa)
);


CREATE TABLE PoseeP (
  dni char(9),
  calle int,
  NumBloque int,
  Planta int,
  Puerta int,
  PRIMARY KEY (dni, calle, NumBloque, Planta, Puerta)
);



--  2- Crear un procedimiento al cual le pasas un numero. Debe comprobar si ese valor pasado se encuentra en el campo nombrezona en la tabla zona urbana. 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio1//
CREATE PROCEDURE ejercicio1 (numero int)
BEGIN
 IF numero IN (SELECT
        NombreZona
      FROM zonaUrbana) THEN
    SELECT
      'El valor se encuentra en la tabla';
 ELSE
    SELECT
      'El valor NO se encuentra en la tabla';
 END IF;
END//
DELIMITER ;


CALL ejercicio1(7);

INSERT INTO zonaUrbana
  VALUE (1, 2);

SELECT
  NombreZona
FROM zonaUrbana;

--  3- Crear un procedimiento, llamarlo p2, que introduzca valores en la tabla zona_urbana 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio2//
CREATE PROCEDURE ejercicio2 (nombreZona int, categoria int)
BEGIN
  INSERT INTO zonaUrbana
    VALUE (nombreZona, categoria);
END//
DELIMITER;
CALL ejercicio2(9, 7);
SELECT
  *
FROM zonaUrbana;

--  4- Crear una función, llamarla f3, que cuente el numero de registros de la tabla ZonaUrbana 

DELIMITER //
CREATE OR REPLACE FUNCTION f3 ()
RETURNS int
BEGIN
  RETURN (SELECT
      COUNT(*)
    FROM zonaUrbana);
END//
DELIMITER ;
SELECT
  f3();


--  5- Crear un procedimiento que utilice el valor devuelto por la funcion anterior.

--  Este procedimiento inserta un registro en casaparticular siempre que haya algun registro en la tabla zonaurbana 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio5//
CREATE PROCEDURE ejercicio5 (nombreZona int, numero_entrada int, metros int)
BEGIN
  DECLARE numero int DEFAULT 0;

  SET numero = f3();

  IF numero > 0 THEN
    INSERT INTO casaParticular
      VALUE (nombreZona, numero_entrada, metros);
  END IF;
END//
DELIMITER ;

TRUNCATE zonaUrbana;

CALL ejercicio5(55, 6, 120);

SELECT
  *
FROM casaParticular;



--    Ejercicipo 5 de ramon


-- otra version
DELIMITER //
CREATE OR REPLACE PROCEDURE p5 ()
BEGIN

  -- creando variables
  DECLARE v1 int;

  DECLARE i int DEFAULT 1;
  -- DECLARE llave int DEFAULT 1;

  -- creo un cursor para la tabla zona urbana
  DECLARE c1 CURSOR FOR
    SELECT
      zu.NombreZona
    FROM zonaUrbana zu;

  -- control de salida para el cursor
  /*DECLARE CONTINUE HANDLER FOR NOT FOUND 
    SET llave = 0;*/
  
  -- abrimos el cursor
  OPEN c1;
  WHILE (i <= f3()) DO
    -- leo el primer registro 
    -- almaceno en v1 la primera zona urbana
    FETCH c1 INTO v1;
    -- introduzco el registro en casaParticular
    INSERT INTO CasaParticular
      VALUES (v1, ROUND(RAND() * 100), ROUND(RAND() * 300) + 200);
    -- sumar 1 al contador
    SET i = i + 1;
  END WHILE;
  -- cierro el cursor
  CLOSE c1;
  SELECT
    CONCAT('Ha insertado ', f3(), ' registros.') salida;
END //
DELIMITER ;

CALL p5();

SELECT f3();


SELECT * FROM CasaParticular;

--  7- Crear un procedimiento, llamarlo p6, para que introduzca de forma automatica 100 registros en la tabla ZonaUrbana. 
--  Para crear valores aleatorios podeis utilizar la funcion predefinida llamada RAND() (esta devuelve un numero entre 0 y 1).

-- Para redondear un numero utilizar la funcion ROUND(numero). 


DELIMITER //
DROP PROCEDURE IF EXISTS p6//
CREATE PROCEDURE p6 ()
BEGIN
  DECLARE nombreZona_creada int DEFAULT 0;
  DECLARE categoria int DEFAULT 0;
  DECLARE contador int DEFAULT 0;

  -- introducir datos aleatorios
  WHILE contador < 100 DO

    SET nombreZona_creada = ROUND(RAND() * 10000);

    IF nombreZona_creada IN (SELECT
          NombreZona
        FROM zonaUrbana) THEN
      SET nombreZona_creada = nombreZona_creada + 1;
    END IF;

    SET categoria = ROUND(RAND() * 10000);

    INSERT INTO zonaUrbana
      VALUE (nombreZona_creada, categoria);
    SET contador = contador + 1;
  END WHILE;
END//
DELIMITER;

CALL p6();

SELECT
  *
FROM zonaUrbana;


--  8- Crear un procedimiento que introduzca 10 registros en la tabla Casa particular. 
--  Tendreis que realizar una función que devuelva un valor de nombre de zona valido.
--  Para ello posicionaros en un registro de forma aleatoria en la tabla Zona Urbana 


--  funcion

DELIMITER //
CREATE OR REPLACE FUNCTION nombre_zona_valido ()
RETURNS int
BEGIN
  RETURN (SELECT
      NombreZona
    FROM zonaUrbana
    ORDER BY RAND() LIMIT 1);
END//
DELIMITER ;

DELIMITER //
DROP PROCEDURE IF EXISTS p8//
CREATE PROCEDURE p8 ()
BEGIN
  DECLARE nombreZona int DEFAULT 0;
  DECLARE numero int DEFAULT 0;
  DECLARE metros int DEFAULT 0;
  DECLARE contador int DEFAULT 0;

  -- introducir datos aleatorios
  WHILE contador < 10 DO
    SET nombreZona = nombre_zona_valido();
    SET numero = ROUND(RAND() * 10000);
    SET metros = ROUND(RAND() * 10000);

    INSERT INTO casaParticular
      VALUE (nombreZona, numero, metros);
    SET contador = contador + 1;
  END WHILE;



END//
DELIMITER ;


TRUNCATE casaParticular;

CALL p8();

SELECT
  *
FROM casaParticular;

--  9- Crear un procedimiento que utilizando un cursor te muestre los registros del 1 al 10. 


DELIMITER //
DROP PROCEDURE IF EXISTS p9//
CREATE PROCEDURE p9 ()
BEGIN
  DECLARE muestra int DEFAULT 0;
  DECLARE contador int DEFAULT 0;


  /* declarar el cursor */
  DECLARE cursor1 CURSOR FOR
  SELECT
    NombreZona
  FROM zonaUrbana;

  /* ABRIR EL CURSOR */
  OPEN cursor1;

  --  creo una tabla tempotal para almacenar los registros del cursor

  CREATE OR REPLACE TEMPORARY TABLE temporal (
    numero int
  );

  WHILE contador < 10 DO
    /* LEO EL PRIMER REGISTRO UTILIZANDO EL CURSOR */
    FETCH cursor1 INTO muestra;

    INSERT INTO temporal
      VALUE (muestra);

    SET contador = contador + 1;
  END WHILE;

  /* CERRAR EL CURSOR */
  CLOSE cursor1;

  /* MUESTRO resultado */
  SELECT
    *
  FROM temporal;
END//
DELIMITER ;

CALL p9();

SELECT
  *
FROM zonaUrbana;

--  11- Crear un procedimiento, llamarlo p10, donde podamos introducir datos de forma automática en la tabla piso.  
--  Tener en cuenta que los campos numero y calle tenéis que sacarlos de la tabla bloque casas. Introducir 23 registros.
DELIMITER //
DROP PROCEDURE IF EXISTS p10//
CREATE PROCEDURE p10 ()
BEGIN
  DECLARE numero1 int DEFAULT 0;
  DECLARE numero2 int DEFAULT 0;
  DECLARE contador int DEFAULT 0;

  DECLARE cursor1 CURSOR FOR
  SELECT
    calle,
    Numero
  FROM bloqueCasas;

  OPEN cursor1;

  WHILE contador < 6 DO

    FETCH cursor1 INTO numero1, numero2;

    INSERT INTO piso
      VALUE (numero1, numero2, ROUND(RAND() * 10), ROUND(RAND() * 10), ROUND(RAND() * 10));

    SET contador = contador + 1;
  END WHILE;
  CLOSE cursor1;

END//
DELIMITER ;

CALL p10();

SELECT * FROM piso;
SELECT * FROM bloqueCasas;

--  12- Crear un procedimiento al cual le pasas una fecha en el primer argumento y 
  --  que te retorna tres argumentos (lógicamente de salida) con el día, mes y año de la fecha generada. 

  DELIMITER //
DROP PROCEDURE IF EXISTS p12//
CREATE PROCEDURE p12 (fecha date,out dia int,OUT mes int, OUT annio int)
BEGIN
  SET dia=DAY(fecha);
  SET mes=MONTH(fecha);
  SET annio=YEAR(fecha);
END//
DELIMITER ;

 set @mes=0;
 set @dia=0;
 set @annio=0;
CALL p12('2019/02/19',@dia,@mes,@annio);

SELECT @dia,@mes,@annio;

--  13- Crear una función a la cual le pasas una fecha y te devuelve el numero de años que tiene esa persona teniendo en cuenta la fecha actual. 


DELIMITER //
CREATE OR REPLACE FUNCTION f13 (fecha date)
RETURNS int
BEGIN
  RETURN year(NOW())-year(fecha);
END//
DELIMITER ;

SELECT f13('1987/09/03');

--  14- Crear una función a la cual le pasas un nombre y te devuelve el nombre sin espacios en blanco en la derecha y en la izquierda (utilizar las funciones RTRIM y LTRIM). 


  DELIMITER //
CREATE OR REPLACE FUNCTION f14 (nombre varchar(40))
RETURNS varchar(40)
BEGIN
    DECLARE nombre_bien varchar(30);
SET nombre_bien= LTRIM(nombre);
  SET nombre_bien= RTRIM(nombre);

  RETURN nombre_bien;
END//
DELIMITER ;

SELECT f14('   imanol         ');
  