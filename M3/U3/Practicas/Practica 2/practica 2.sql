﻿
DROP DATABASE IF EXISTS m3u3p2;
CREATE DATABASE m3u3p2;
USE m3u3p2;

--  1- Explicar el funcionamiento del siguiente procedimiento y después ejecutarle 
--  crea una tabla con 3 campos sin pk

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio1//
CREATE PROCEDURE ejercicio1 ()
BEGIN


  CREATE TABLE IF NOT EXISTS tabla1 (
    nombre varchar(10) DEFAULT NULL,
    edad int(3) DEFAULT 0,
    fecha_nacimiento date DEFAULT NULL

  );


END//
DELIMITER ;

CALL ejercicio1();


-- ej 2 Modificar el procedimiento anterior para crear una tabla con los siguientes campos: 

DELIMITER //
DROP PROCEDURE IF EXISTS crear_tabla//
CREATE PROCEDURE crear_tabla ()
BEGIN

  CREATE TABLE IF NOT EXISTS personas (
    dni decimal(8),
    nombre varchar(30),

    edad integer,
    apellidos varchar(50),
    fecha_nacimiento date
  );
END//
DELIMITER ;

CALL crear_tabla();
SELECT
  *
FROM personas;


--  3- Explicar el funcionamiento del siguiente procedimiento y después ejecutarle 
DELIMITER //
DROP PROCEDURE IF EXISTS f1//
CREATE PROCEDURE f1 ()
BEGIN
  DECLARE v1 int;
  SET v1 = (SELECT
      COUNT(*)
    FROM tabla1);

  IF v1 = 0 THEN
    SELECT
      'La tabla está vacia';
  ELSE
    SELECT
      'La tabla NO está vacia';
  END IF;

END//
DELIMITER ;

CALL f1();
SELECT
  *
FROM tabla1;


--  4- Modificar el procedimiento anterior para que si le pasamos un 0 te evalua la tabla denominada tabla1, si es un 1 evalúa la tabla denominada personas. 

--  Si le pasa cualquier otro valor entonces debe indicar valor incorrecto. 

DELIMITER //
DROP PROCEDURE IF EXISTS f2//
CREATE PROCEDURE f2 (entrada int)
BEGIN


  CASE entrada
    WHEN 0 THEN SELECT
          COUNT(*)
        FROM tabla1;
    WHEN 1 THEN SELECT
          COUNT(*)
        FROM personas;
    ELSE SELECT
        'Valor Incorrecto';
  END CASE;


END//
DELIMITER ;

-- introducimos un dato en la tabla personas

INSERT INTO tabla1
  VALUES ('ima', 31, '1987/09/03');
CALL f2(0);
SELECT
  *
FROM tabla1;

--  5- Explicar el funcionamiento del siguiente procedimiento y después ejecutarle 

--  el procedimiento nos devuelve un resultado diferente en funcion del numero de registros de la tabla 1

DELIMITER //
DROP PROCEDURE IF EXISTS f3//
CREATE PROCEDURE f3 ()
BEGIN
  DECLARE v1 int;
  SET v1 = (SELECT
      COUNT(*)
    FROM tabla1);

  IF v1 = 0 THEN
    SELECT
      'La tabla está vacia';

    INSERT INTO tabla1
      VALUES (DEFAULT, DEFAULT, DEFAULT);

    ELSEIF (v1 BETWEEN 10 AND 100) THEN
    SELECT
      *
    FROM tabla1;
  ELSE
    SELECT
      *
    FROM tabla1 LIMIT 1, 100;
  END IF;

END//
DELIMITER ;

TRUNCATE tabla1;
CALL f3();


--  6- Transformar el procedimiento anterior en una función. ¿Qué problemas tenemos? Hacer que devuelva el numero de registros de la tabla. ¿Seguimos teniendo problemas? 

  -- no puedo mostrar en una variable el resultado de un select *


DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio6 ()
RETURNS int
BEGIN

  DECLARE salida int DEFAULT 0;

  DECLARE v1 int DEFAULT 0;
  SET v1 = (SELECT
      COUNT(*)
    FROM tabla1);

  IF v1 = 0 THEN
    SET salida = 'La tabla está vacia';
       INSERT INTO tabla1
      VALUES (DEFAULT, DEFAULT, DEFAULT);
  ELSEIF (v1 BETWEEN 10 AND 100) THEN
    SET salida = (SELECT
        COUNT(*)
      FROM tabla1);
  ELSE
    SET salida = (SELECT
        COUNT(*)-1
      FROM tabla1);
  END IF;
  RETURN salida;
END//

DELIMITER ;


TRUNCATE tabla1;

SELECT ejercicio6();

INSERT INTO tabla1
  VALUES ('ima', 31, '2005/09/03');

SELECT * FROM tabla1;


--  7- Realizar un procedimiento, denominado crear_claves, que genere las siguientes claves y restricciones en la tabla personas: 
  
  --  a. Clave principal : dni b. Clave única: nombre y apellidos c. Indexado con duplicados: fecha_nacimiento. 

  DELIMITER //
DROP PROCEDURE IF EXISTS crear_claves//
CREATE PROCEDURE crear_claves ()
BEGIN
  DECLARE v1 int;
 
  ALTER TABLE personas ADD PRIMARY KEY (dni);

  ALTER TABLE personas ADD CONSTRAINT uni_nombre_apellidos UNIQUE (nombre,apellidos);

  ALTER TABLE personas ADD index (fecha_nacimiento);

END//
DELIMITER ;

CALL crear_claves();

DESCRIBE personas;

--  8- Crear un procedimiento que actualice todos los campos edad calculándolos en función de la fecha de nacimiento. 

  DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio8//
CREATE PROCEDURE ejercicio8 ()
BEGIN

UPDATE personas SET edad=(SELECT  year(NOW())-year(fecha_nacimiento));

END//
DELIMITER ;


CALL ejercicio8();
SELECT * FROM personas;

  SELECT  year(NOW())-year(fecha_nacimiento) FROM personas;

  INSERT INTO personas
  VALUES (3,'juan', 31,'osun','1990/09/03');


  --  10- Crear una vista denominada nombres con los nombres y apellidos de todas las personas. Esta vista debe tener un solo campo denomina nombre_completo con el siguiente formato  
  --  apellidos, nombre. 

      CREATE OR REPLACE VIEW nombres  AS
        SELECT CONCAT(nombre,', ',apellidos) AS nombre_completo
      FROM personas;

      SELECT * FROM nombres;

--  11- Crear un procedimiento el cual te rellene una tabla denominada alfabeto con todas las letras de la ‘a’ a la ‘z’.
  --  Crear otro procedimiento que te rellene una tabla denominada números con todos los números del 1 al 1000. Estos procedimientos llamarlos crear_alfabeto y crear_numero. 
  

  -- numeros
    DELIMITER //
    DROP PROCEDURE IF EXISTS crear_numero//
    CREATE PROCEDURE crear_numero()
    BEGIN
      DECLARE contador int DEFAULT 1;

      CREATE TABLE IF NOT EXISTS numeros(
      numero int  );

      WHILE contador<=1000 DO
      INSERT INTO numeros VALUE(contador);
      SET contador=contador+1;
      END WHILE;

    END//
    DELIMITER ;

CALL crear_numero();

SELECT * FROM numeros;


--  letras
    DELIMITER //
    DROP PROCEDURE IF EXISTS crear_alfabeto//
    CREATE PROCEDURE crear_alfabeto()
    BEGIN
      DECLARE contador int DEFAULT 97;
      
      CREATE TABLE IF NOT EXISTS letras(
       letra char(3)  );

      WHILE contador<123 DO
      INSERT INTO letras VALUE(char(contador));
      SET contador=contador+1;
      END WHILE;
    END//
    DELIMITER ;

TRUNCATE letras;
CALL crear_alfabeto();

SELECT * FROM letras;
