﻿--  9- Crear una vista que nos permita sacar todas las personas menores de 18 años. Sacar nombre, apellidos y edad.
  
  --  Grabar los registros en una tabla denominada menores. El resto de registros grabarlos en una tabla denominada no_menores. Esto realizarlo en un guion denominado los_menores.sql 


  --  menores de edad
  CREATE OR REPLACE VIEW menores_edad  AS
    SELECT 
        nombre,
        edad,
        apellidos 
  FROM personas WHERE edad<18;

  --  tablas de menores y mayoresç

  -- opcion 1
  CREATE OR REPLACE TABLE menores(
  nombre varchar(50),
    edad int,
    apellidos varchar(50)
  ) AS SELECT * FROM menores_edad;

  -- opcion 2
  CREATE OR REPLACE TABLE no_menores(
  nombre varchar(50),
    edad int,
    apellidos varchar(50)
  );

  --  insercion de datos
  INSERT INTO menores SELECT * FROM menores_edad;

  INSERT INTO no_menores  SELECT personas.nombre,personas.edad,personas.apellidos FROM personas LEFT JOIN menores_edad USING(nombre) WHERE menores_edad.nombre IS NULL;

  --  mirar si ha introducido los datos

  SELECT * FROM menores;

  SELECT * FROM no_menores;

   






