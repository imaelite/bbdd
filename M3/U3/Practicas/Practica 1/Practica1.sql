﻿
DROP DATABASE IF EXISTS m3u3p1;
CREATE DATABASE m3u3p1;
USE m3u3p1;

--  Llamar al procedimiento ejercicio1 y probarle con varios lados 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio1//
CREATE PROCEDURE ejercicio1 (lado float)
BEGIN
  
  DECLARE area float;    
  DECLARE superficie float;
  
  
  SET area=POW(lado,2);
  set superficie=lado*4;
  
  CREATE TABLE IF NOT EXISTS ejercicio1(
  numero int AUTO_INCREMENT PRIMARY KEY,
    lado float,
    area float,
    superficie float
  );    

INSERT INTO ejercicio1 VALUES(DEFAULT,lado,area,superficie);

END//
DELIMITER ;

CALL ejercicio1(8);

SELECT * FROM ejercicio1;

--  2. En el procedimiento ejercicio2 quiero que lo modifiquéis para que calcule el volumen de un cubo. 


  DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio2//
CREATE PROCEDURE ejercicio2 (lado float)
BEGIN
  
  DECLARE volumen float;    

  SET volumen=POW(lado,3);

  CREATE TABLE IF NOT EXISTS ejercicio2(
  numero int AUTO_INCREMENT PRIMARY KEY,
    lado float,
    volumen float
  );    

INSERT INTO ejercicio2 VALUES(DEFAULT,lado,volumen);
END//
DELIMITER ;

CALL ejercicio2(8);

SELECT * FROM ejercicio2;

--  3. Modificar el siguiente procedimiento para que calcule la superficie total y el volumen de un cubo. 


    DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio3//
CREATE PROCEDURE ejercicio3 (lado float)
BEGIN
  
  DECLARE volumen float;    
  DECLARE superficie float;   

  SET volumen=POW(lado,3);
   SET superficie=POW(lado,2)*6;

  CREATE TABLE IF NOT EXISTS ejercicio3(
  numero int AUTO_INCREMENT PRIMARY KEY,
    lado float,
    volumen float,
    superficie float
  );    

INSERT INTO ejercicio3 VALUES(DEFAULT,lado,volumen,superficie);
END//
DELIMITER ;

CALL ejercicio3(9);

SELECT * FROM ejercicio3;

--  4. Mediante la siguiente función calculamos el volumen. Llamar la función y probarla.

DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio4(lado int)
RETURNS int
BEGIN
   DECLARE volumen float;
   SET volumen=POW(lado,3);

    RETURN volumen;
END //

DELIMITER ;

select ejercicio4(9);

--  5 La siguiente función debe calcular el área total de un cubo 

  DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio5(lado int)
RETURNS float
BEGIN
   DECLARE volumen float;
   SET volumen=POW(lado,2)*6;

    RETURN volumen;
END //

DELIMITER ;

select ejercicio5(9);

--  6. En el siguiente procedimiento almacenado vamos a utilizar las funciones anteriores. 
 -- Llamar al procedimiento y probarlo.
  
   
    DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio6//
CREATE PROCEDURE ejercicio6 (lado float)
BEGIN
  
  DECLARE volumen float;    
  DECLARE areatotal float;   

  SET volumen=ejercicio4(lado);
    SET areatotal=ejercicio5(lado);
  CREATE TABLE IF NOT EXISTS ejercicio6(
  numero int AUTO_INCREMENT PRIMARY KEY,
    lado float,
    volumen float,
    superficie float
  );    

INSERT INTO ejercicio6 VALUES(DEFAULT,lado,volumen,areatotal);
END//
DELIMITER ;

CALL ejercicio6(9);
SELECT * FROM  ejercicio6;

--  7. La siguiente función calcula el volumen de un cilindro. La función recibe como argumentos el radio y la altura. Llamarla ejercicio7. 

  DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio7(radio float,altura float)
RETURNS float
BEGIN
   DECLARE volumen float;
   SET volumen=PI()* POW(radio,2) * altura;

    RETURN volumen;
END //

DELIMITER ;

select ejercicio7(5,5);

--  8. Crear una función que calcule el área lateral de un cilindro. La función recibe como argumentos el radio y la altura. Llamarla ejercicio8 
  
  
  DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio8(radio float,altura float)
RETURNS float
BEGIN
   DECLARE area_l float;
   SET area_l=2*PI()*radio*altura;

    RETURN area_l;
END //
DELIMITER ;

select ejercicio8(25,15); 


--  9. Crear una función que calcule el área total de un cilindro. La función recibe como argumentos el radio y la altura. Llamarla ejercicio9.  

   DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio9(radio float,altura float)
RETURNS float
BEGIN
   DECLARE area_l float;
  DECLARE area_superior float;
  DECLARE area_total float;


   SET area_l=2*PI()*radio*altura;

  set area_superior=2*PI()*POW(radio,2);

 set area_total=area_l+area_superior;
  

    RETURN area_total;
END //
DELIMITER ;

select ejercicio9(15,25); 

--  10. Modificar el procedimiento siguiente para que utilice las funciones de los ejercicios anteriores para calcular el volumen, el área lateral y el área total de un cilindro. 
 
 
    DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio10//
CREATE PROCEDURE ejercicio10 (radio float, altura float)
BEGIN
  
  DECLARE volumen float;    
  DECLARE arealateral float; 
  DECLARE areatotal float;   

  --  llamada a funciones
  SET volumen=ejercicio7(radio,altura);
  SET arealateral=ejercicio8(radio,altura);
  SET areatotal=ejercicio9(radio,altura);
--  -----------

--  tabla temporal
  CREATE TEMPORARY TABLE IF NOT EXISTS ejercicio10(
  numero int AUTO_INCREMENT PRIMARY KEY,
     radio float,
     altura float,
    volumen float,
    arealateral float,
    areatotal float
  );    

INSERT INTO ejercicio10 VALUES(DEFAULT,radio,altura,volumen,arealateral,areatotal);
END//
DELIMITER ;



CALL ejercicio10(15,25);
TRUNCATE ejercicio10;

SELECT * FROM ejercicio10;