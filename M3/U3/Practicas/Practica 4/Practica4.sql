﻿--  creacion de base de datos
DROP DATABASE IF EXISTS m3u3p4;
CREATE DATABASE m3u3p4;
USE m3u3p4;

--  1- Crea un procedimiento que reciba una cadena que puede contener letras y números y sustituya los números por *.
--  El argumento debe ser único y debe almacenar en el, el resultado. 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio1//
CREATE PROCEDURE ejercicio1 (INOUT cadena varchar(30))
BEGIN

  DECLARE longitud int DEFAULT 0;
  DECLARE contador int DEFAULT 1;
  DECLARE salida varchar(30) DEFAULT '';
  DECLARE letra varchar(30) DEFAULT 0;

  SET longitud = CHAR_LENGTH(cadena);

  WHILE contador <= longitud DO

    SET letra = SUBSTRING(cadena, contador, 1);
    IF letra IN ('1', '2', '3', '4', '5', '6', '7', '8', '9', '0') THEN
      SET letra = '*';
    END IF;

    SET salida = CONCAT(salida, letra);
    SET contador = contador + 1;
  END WHILE;

  SET cadena = salida;
END//
DELIMITER ;


SET @palabra = '1ima1n1ol1';
CALL ejercicio1(@palabra);
SELECT
  @palabra;


--  2- Crea una función que recibe como argumento una cadena que puede contener letras y número y

--  debe devolver la cadena con el primer carácter en mayúsculas y el resto en minúsculas

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio2//
CREATE PROCEDURE ejercicio2 (INOUT cadena varchar(30))
BEGIN

  DECLARE longitud int DEFAULT 0;
  DECLARE contador int DEFAULT 1;
  DECLARE salida varchar(30) DEFAULT '';
  DECLARE letra varchar(30) DEFAULT 0;
  DECLARE salida_if varchar(30) DEFAULT 0;

  SET longitud = CHAR_LENGTH(cadena);

  WHILE contador <= longitud DO

    SET letra = SUBSTRING(cadena, contador, 1);
    IF letra NOT IN ('1', '2', '3', '4', '5', '6', '7', '8', '9', '0') THEN

      IF salida_if = 0 THEN
        SET letra = UPPER(letra);
        SET salida_if = 11;
      ELSE
        SET letra = LOWER(letra);
      END IF;


    END IF;

    SET salida = CONCAT(salida, letra);
    SET contador = contador + 1;
  END WHILE;

  SET cadena = salida;
END//
DELIMITER ;

SET @palabra = '123ivanIMANOL123KeViN';
CALL ejercicio2(@palabra);
SELECT
  @palabra;

--  3- Desarrollar una función que devuelva el número de años completos que hay entre dos fechas que se pasan como parámetros. Utiliza la función DATEDIFF


DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio3 (fecha1 date, fecha2 date)
RETURNS int
BEGIN
  RETURN (SELECT
      FLOOR(DATEDIFF(fecha1, fecha2) / 365));
END//

DELIMITER ;

SELECT
  ejercicio3('2000/11/20', '1980/01/01');


--  4- Realiza el ejercicio anterior pero utilizando la función TIMESTAMPDIFF 



DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio4 (fecha1 date, fecha2 date)
RETURNS int
BEGIN
  RETURN (TIMESTAMPDIFF(year, fecha1, fecha2));
END//

DELIMITER ;

SELECT
  ejercicio4('2000/11/20', '1980/01/01');

--  5- Crear un procedimiento que genere una tabla llamada números (un solo campo de tipo int) la rellene de 50 números y

--  después devuelva su suma, media, valor máximo y valor mínimo. 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio5//
CREATE PROCEDURE ejercicio5 ()
BEGIN
  DECLARE contador int DEFAULT 0;

  CREATE TABLE IF NOT EXISTS numeros (
    id int AUTO_INCREMENT PRIMARY KEY,
    dato int
  );

  TRUNCATE numeros;

  WHILE contador < 1000 DO

    INSERT INTO numeros
      VALUE (DEFAULT, contador);

    SET contador = contador + 1;
  END WHILE;

  --  --  después devuelva su suma, media, valor máximo y valor mínimo. 
  SELECT

    SUM(dato),
    AVG(dato),
    MAX(dato),
    MIN(dato)
  FROM numeros;

END//

DELIMITER ;

CALL ejercicio5();

SELECT
  *
FROM numeros;

--  6- Crear una función que reciba 5 números y devuelva su valor máximo, mínimo, media y suma. 

DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio6 (num1 int, num2 int, num3 int, num4 int, num5 int)
RETURNS varchar(300)
BEGIN
  CREATE OR REPLACE TEMPORARY TABLE numeros (
    dato int
  );

  INSERT INTO numeros
    VALUE (num1), (num2), (num3), (num4), (num5);

  RETURN (SELECT
      CONCAT('El maximo es ', MAX(dato), ', el minimo es ', MIN(dato), ', la media es ', ROUND(AVG(dato)), ', la suma de ', SUM(dato))
    FROM numeros);
END//
DELIMITER ;

SELECT
  ejercicio6(3, 4, 5, 6, 7);

--  7- Realizar un procedimiento que genere una tabla con dos campos (nota de tipo int, resultado de tipo varchar).

--  Debe rellenar esa tabla con 10 números entre 0 y 10 (con el procedimiento). 


DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio7//
CREATE PROCEDURE ejercicio7 ()
BEGIN
  DECLARE contador int DEFAULT 0;
  DECLARE valor int DEFAULT 0;
  CREATE OR REPLACE TABLE numeros (
    id int AUTO_INCREMENT PRIMARY KEY,
    nota int,
    resultado varchar(30)
  );

  TRUNCATE numeros;

  WHILE contador < 10 DO

    SET valor = RAND() * 10;
    INSERT INTO numeros (nota)
      VALUE (valor);

    SET contador = contador + 1;
  END WHILE;
END//
DELIMITER ;

CALL ejercicio7();

SELECT
  *
FROM numeros;

-- --  8 8- Realizar un procedimiento almacenado almacene en el campo resultado el siguiente valor en función de nota a. 
--   Nota entre 0 y 4:
--   Suspenso b. Nota 5: 
--   Suficiente c. Nota 6: 
--   Bien d. Nota entre 7 y 8: 
--   Notable e. Nota 
--   entre 9 y 10: Sobresaliente 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio8//
CREATE PROCEDURE ejercicio8 ()
BEGIN
  DECLARE idLeido int DEFAULT 0;
  DECLARE valor int DEFAULT 0;
  DECLARE fin int DEFAULT 0;
  DECLARE nota_letras varchar(50) DEFAULT 'Sobresaliente';
  DECLARE dato int DEFAULT 0;

  --  declaracion del cursos
  DECLARE cursor1 CURSOR FOR
  SELECT
    id,
    nota
  FROM numeros;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin = 1;
  OPEN cursor1;

  FETCH cursor1 INTO idLeido, dato;
  WHILE fin = 0 DO

    CASE

      WHEN dato < 5 THEN SET nota_letras = 'Suspenso';

      WHEN dato < 6 THEN SET nota_letras = 'Suficiente';

      WHEN dato < 7 THEN SET nota_letras = 'Bien';

      WHEN dato < 9 THEN SET nota_letras = 'Notable';

      WHEN dato >= 9 THEN SET nota_letras = 'Sobresaliente';

    END CASE;
    UPDATE numeros
    SET numeros.resultado = nota_letras
    WHERE numeros.id = idLeido;

    FETCH cursor1 INTO idLeido, dato;
  END WHILE;
END//
DELIMITER ;

CALL ejercicio8();
SELECT
  *
FROM numeros;


--  9- Crear un procedimiento almacenado que genere estas dos tablas 


DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio9//
CREATE PROCEDURE ejercicio9 ()
BEGIN

  CREATE TABLE IF NOT EXISTS comisiones (
    vendedor int,
    comision float
  );

  CREATE TABLE IF NOT EXISTS ventas (
    vendedor int,
    producto int,
    importe float
  );

END//
DELIMITER ;

CALL ejercicio9();

SELECT
  *
FROM ventas;

--  10- Analizar el siguiente procedimiento almacenado 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio10//
CREATE PROCEDURE ejercicio10 (IN mivendedor int)
BEGIN

  DECLARE micomision int DEFAULT 0;
  DECLARE suma int;
  DECLARE existe bool;

  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 1
  AND vendedor = mivendedor;

  SET micomision = micomision + (suma * 0.15);

  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 2
  AND vendedor = mivendedor;

  SET micomision = micomision + (suma * 0.1);
  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 3
  AND vendedor = mivendedor;

  SET micomision = micomision + (suma * 0.2);

  SELECT
    COUNT(1) > 0 INTO existe
  FROM comisiones
  WHERE vendedor = mivendedor;


  IF existe THEN
    UPDATE comisiones
    SET comision = comision + micomision
    WHERE vendedor = mivendedor;
  ELSE
    INSERT INTO comisiones (vendedor, comision)
      VALUES (mivendedor, micomision);
  END IF;

END//
DELIMITER ;


--  otra forma

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio10//
CREATE PROCEDURE ejercicio10 (IN mivendedor int)
BEGIN

  DECLARE micomision int DEFAULT 0;
  DECLARE suma int;
  
  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 1
  AND vendedor = mivendedor;

  SET micomision = micomision + (suma * 0.15);

  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 2
  AND vendedor = mivendedor;

  SET micomision = micomision + (suma * 0.1);
  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 3
  AND vendedor = mivendedor;

  SET micomision = micomision + (suma * 0.2);

  --  esto hace la función del if de arriba
  INSERT INTO comisiones(vendedor, comision)
  VALUES(mivendedor, micomision)
  ON DUPLICATE KEY UPDATE 
  comision = micomision;
 
END//
DELIMITER ;

CALL ejercicio10(2);

SELECT * FROM ventas;

SELECT * FROM comisiones;

DESCRIBE ventas;

--  Introducir datos en la tabla ventas

INSERT INTO ventas VALUEs(1,1,05);
INSERT INTO ventas VALUEs(2,2,15);
INSERT INTO ventas VALUEs(3,3,25);
INSERT INTO ventas VALUEs(4,4,35);
INSERT INTO ventas VALUEs(5,5,45);
INSERT INTO ventas VALUEs(6,6,65);


--  12 Modificar el procedimiento almacenado para que en caso de que sea un producto de tipo 4 le coloquemos una comisión del 30% de sus ventas sobre ese producto. 

  DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio12//
CREATE PROCEDURE ejercicio12(IN mivendedor int)
BEGIN

  DECLARE micomision int DEFAULT 0;
  DECLARE suma int;
  
  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 1
  AND vendedor = mivendedor;

  SET micomision = micomision + (suma * 0.15);

  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 2
  AND vendedor = mivendedor;

  SET micomision = micomision + (suma * 0.1);
  SELECT
    IFNULL(SUM(importe), 0) INTO suma
  FROM ventas
  WHERE producto = 3
  AND vendedor = mivendedor;

  SET micomision = micomision + (suma * 0.2);

  --  esto hace la función del if de arriba
  INSERT INTO comisiones(vendedor, comision)
  VALUES(mivendedor, micomision)
  ON DUPLICATE KEY UPDATE 
  comision = micomision;
 
END//
DELIMITER ;


--  13- Introducir una serie de productos y comprobar que funciona correctamente este procedimiento.


  SELECT * FROM ventas;

INSERT INTO ventas VALUEs(7,7,105);
INSERT INTO ventas VALUEs(8,8,115);
INSERT INTO ventas VALUEs(9,9,215);
INSERT INTO ventas VALUEs(10,10,135);
INSERT INTO ventas VALUEs(11,11,415);
INSERT INTO ventas VALUEs(11,11,165);