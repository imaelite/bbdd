﻿--  creacion de base de datos

DROP DATABASE IF EXISTS m3u3p6;
CREATE DATABASE m3u3p6;
USE m3u3p6;


--  1- Convertir en el siguiente procedimiento almacenado el LOOP en un While 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio1//
CREATE PROCEDURE ejercicio1 ()
BEGIN
  DECLARE counter bigint DEFAULT 0;

  WHILE (counter < 10) DO
    SET counter = counter + 1;

    SELECT
      counter;
  END WHILE;

END//
DELIMITER ;

CALL ejercicio1();

--  2- Realizar el ejercicio anterior con un REPEAT 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio2//
CREATE PROCEDURE ejercicio2 ()
BEGIN
  DECLARE counter bigint DEFAULT 0;
  REPEAT
    SELECT
      counter;
    SET counter = counter + 1;

  UNTIL (counter > 10)
  END REPEAT;
END//
DELIMITER ;

CALL ejercicio2();

--  3- Importar la tabla Runners (descargar el runners.sql del servidor) a vuestra base de datos. 


  --  ?????????????????? donde está?


  --  4- Analizar el siguiente procedimiento. Implementarle y probarle.
USE p6m3u3;


DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio4//
CREATE PROCEDURE ejercicio4()
BEGIN
  DECLARE v_name varchar(120);
  DECLARE v_time int DEFAULT 0;
  DECLARE v_penalty1 int DEFAULT 0;
    DECLARE v_penalty2 int DEFAULT 0;

--  fin del bucle
DECLARE fin int DEFAULT 0;

--  el selec que vamos a ejecutar

  DECLARE runners_cursor CURSOR FOR

    SELECT name,time,penalty1,penalty2  FROM runners;

  --  condicion de salida
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin=1;


  OPEN runners_cursor;
-- El bulcecon while

get_runners: LOOP
   FETCH runners_cursor INTO v_name,v_time,v_penalty1,v_penalty2;
IF fin= 1 THEN 

  LEAVE get_runners;

  END IF;

  END LOOP get_runners;
  
  SELECT name,time,penalty1,penalty2  FROM runners;

  CLOSE runners_cursor;
END//
DELIMITER ;


CALL ejercicio4();