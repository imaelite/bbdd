﻿--  creacion de base de datos
DROP DATABASE IF EXISTS m3u3p5;
CREATE DATABASE m3u3p5;
USE m3u3p5;

--  1- Crear un procedimiento almacenado que genere una tabla con los campos a. Id : entero, clave principal b. Nombre: texto 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio1//
CREATE PROCEDURE ejercicio1 ()
BEGIN
  CREATE TABLE IF NOT EXISTS tabla1 (
    id int PRIMARY KEY,
    nombre varchar(200)

  );
--  http://127.0.0.1/ejemplo2yiiCopia/web/index.php/gii/form
END//
DELIMITER ;

CALL ejercicio1();


--  2- Colocar en el procedimiento anterior un control de errores de tal forma que si la tabla ya existe en vez de crearla la vacía. 
DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio2//
CREATE PROCEDURE ejercicio2 ()
BEGIN
  -- en caso de existir la tabla la vacia 
  DECLARE EXIT HANDLER FOR 1050
  TRUNCATE tabla1
  ;
  CREATE TABLE tabla1 (
    id int PRIMARY KEY,
    nombre varchar(200)

  );
END//
DELIMITER ;

CALL ejercicio2();


--  3- Crea una función que la pasamos dos argumentos (id, nombre). La función debe introducir el dato en la tabla en caso de que el id no exista.

--  Si el Id existe entonces lo que realizara es actualizarle con el nombre que pasemos. Realizar el ejercicio sin control de errores 

DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio3//
CREATE PROCEDURE ejercicio3 (id_arg int, nombre_arg varchar(50))
BEGIN

  INSERT INTO tabla1 (id, nombre)
    VALUES (id_arg, nombre_arg)
  ON DUPLICATE KEY UPDATE
  nombre = nombre_arg;

END//
DELIMITER ;

CALL ejercicio3(3, 'lolete');

SELECT
  *
FROM tabla1;

INSERT INTO tabla1
  VALUES (1, 'ima');
INSERT INTO tabla1
  VALUES (2, 'izas');
INSERT INTO tabla1
  VALUES (3, 'imanol');
INSERT INTO tabla1
  VALUES (4, 'mikel');


--  4- Realizar el ejercicio anterior pero utilizando el control de errores. (Crear un función nueva).  


DELIMITER //
DROP PROCEDURE IF EXISTS ejercicio4//
CREATE PROCEDURE ejercicio4 (id_arg int, nombre_arg varchar(50))
BEGIN
  -- declaracion de variables
  DECLARE mal boolean DEFAULT FALSE;
  --  Excepciones
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01000'
  BEGIN
    UPDATE tabla1
    SET tabla1.nombre = nombre_arg
    WHERE id = id_arg;
  END;

  SELECT
    COUNT(1) > 0 INTO mal
  FROM tabla1
  WHERE id = id_arg;

  IF (mal) THEN
    SIGNAL SQLSTATE '01000';
  ELSE
    INSERT INTO tabla1 (id, nombre)
      VALUES (id_arg, nombre_arg);
  END IF;
END//
DELIMITER ;

CALL ejercicio4(9, 'tomy');

SELECT
  *
FROM tabla1;

--  5- Crear un script que añada un campo a la tabla denominado fecha (tipo DATE) 


ALTER TABLE tabla1 ADD COLUMN fecha date;

DESCRIBE tabla1;


--  6- Crear una función a la cual le pasas un fecha y te devuelve el día de la semana en castellano al que pertenece. 

--  Por ejemplo si le paso 05/05/2015 me indicara que es martes. Probarla con la tabla creada. Utilizar la instrucción CASE 


DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio6 (fecha1 date)
RETURNS varchar(50)
BEGIN
  DECLARE dia_numero int DEFAULT 0;
  DECLARE respuesta varchar(50);

  SELECT
    DAYOFWEEK(fecha1) INTO dia_numero;

  CASE dia_numero

    WHEN 1 THEN SET respuesta = 'Domingo';
    WHEN 2 THEN SET respuesta = 'Lunes';
    WHEN 3 THEN SET respuesta = 'Martes';
    WHEN 4 THEN SET respuesta = 'Miércoles';
    WHEN 5 THEN SET respuesta = 'Jueves';
    WHEN 6 THEN SET respuesta = 'Viernes';
    WHEN 7 THEN SET respuesta = 'Sabado';
  END CASE;
  RETURN respuesta;
END//
DELIMITER ;


SELECT
  DAYOFWEEK('2015/05/05');

SELECT
  ejercicio6('2015/05/07');


--  7- Modificar la función anterior para que cuando el dia de la semana sea de lunes a viernes (incluidos) lance una señal que me grabe 

--  en una nueva tabla denominada fechas la fecha y el texto laboral. Si el dia de la semana es sábado o domingo me lance 

--  otra señal que me grabe en la tabla fechas la fecha y el texto festivo. 

--  8. Quiero que lo modifiquéis para poder realizar únicamente un return

  CREATE TABLE IF NOT EXISTS fechas (
    fecha date,
    texto varchar(200)
  );


DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio7 (fecha1 date)
RETURNS varchar(50)
BEGIN
  DECLARE dia_numero int DEFAULT 0;
  DECLARE respuesta varchar(50);
  --  Excepciones
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01001'
  BEGIN
    INSERT INTO fechas
      VALUES (fecha1,'festivo');
  END;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '01002'
  BEGIN
    INSERT INTO fechas
      VALUES (fecha1,'laboral');
  END;


  SELECT
    DAYOFWEEK(fecha1) INTO dia_numero;
  CASE dia_numero
    WHEN 1 THEN SET respuesta = 'Domingo';
    WHEN 2 THEN SET respuesta = 'Lunes';
    WHEN 3 THEN SET respuesta = 'Martes';
    WHEN 4 THEN SET respuesta = 'Miércoles';
    WHEN 5 THEN SET respuesta = 'Jueves';
    WHEN 6 THEN SET respuesta = 'Viernes';
    WHEN 7 THEN SET respuesta = 'Sabado';
  END CASE;


  IF (dia_numero = 1
    OR dia_numero = 7) THEN
    SIGNAL SQLSTATE '01001';
  END IF;

  IF (dia_numero BETWEEN 2 AND 6) THEN
    SIGNAL SQLSTATE '01002';
  END IF;

  RETURN respuesta;
END//
DELIMITER ;


SELECT
  ejercicio7('2019/03/10');

SELECT * FROM fechas;

--  9 Modificar el ejercicio del procedimiento almacenado solicitado en el ejercicio 6
  
  --  para realizarlo utilizando una tabla temporal como la que vemos en la imagen. 

  DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio7 (fecha1 date)
RETURNS varchar(50)
BEGIN
  DECLARE dia_numero int DEFAULT 0;
  DECLARE respuesta varchar(50);
  --  Excepciones
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01001'
  BEGIN
    INSERT INTO fechas
      VALUES (fecha1,'festivo');
  END;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '01002'
  BEGIN
    INSERT INTO fechas
      VALUES (fecha1,'laboral');
  END;

  DROP TEMPORARY TABLE IF EXISTS temporal;
  CREATE TEMPORARY TABLE IF NOT EXISTS temporal (
    fecha int,
    texto varchar(200)
  );

   SELECT
    DAYOFWEEK(fecha1) INTO dia_numero;

  INSERT INTO temporal (fecha, texto)
  VALUES 
    (1,'Domingo'),
    (2,'Lunes'),
    (3,'Martes'),
    (4,'Miércoles'),
    (5,'Jueves'),
    (6,'Viernes'),
    (7,'Sabado');


SELECT texto INTO respuesta FROM temporal WHERE temporal.fecha=dia_numero;

  IF (dia_numero = 1
    OR dia_numero = 7) THEN
    SIGNAL SQLSTATE '01001';
  END IF;

  IF (dia_numero BETWEEN 2 AND 6) THEN
    SIGNAL SQLSTATE '01002';
  END IF;

  RETURN respuesta;
END//
DELIMITER ;

SELECT
  ejercicio7('2019/03/7');

SELECT * FROM temporal;

SELECT * FROM fechas;


    