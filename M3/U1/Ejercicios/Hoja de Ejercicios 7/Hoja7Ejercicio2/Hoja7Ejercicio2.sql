﻿--  eliminar la bd si existe
DROP  DATABASE IF EXISTS hoja7unidad1modulo3Ejercicio2;

CREATE DATABASE hoja7unidad1modulo3Ejercicio2;

USE hoja7unidad1modulo3Ejercicio2;

-- tabla compañia
DROP TABLE IF EXISTS compañia;

CREATE TABLE IF NOT EXISTS compañia(
  numero char(9),
  actividad varchar(30),
  PRIMARY KEY (numero)
  );

INSERT INTO compañia VALUES ('comp_01','limpiar');



-- tabla soldado
  DROP TABLE IF EXISTS soldado;

CREATE TABLE IF NOT EXISTS soldado(
  s char(9),
  nombre varchar(30),
  apellido varchar(30),
  grado varchar(30),
  PRIMARY KEY (s)
  );

INSERT INTO  soldado VALUES ('sol1','ima','osuna','oficial');




--  tabla pertenece

DROP TABLE IF EXISTS pertenece;

CREATE TABLE IF NOT EXISTS pertenece(
  compañia char(9), 
  soldado char(9),
  PRIMARY KEY(compañia,soldado),
  CONSTRAINT  FKPerteneceCompañia FOREIGN KEY (compañia) REFERENCES compañia(numero) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKPerteneceSoldado FOREIGN KEY (soldado) REFERENCES soldado(s) on DELETE CASCADE ON UPDATE CASCADE
  );

INSERT INTO  pertenece VALUES ('comp_01','sol1');


-- tabla servicio
  DROP TABLE IF EXISTS servicio;

CREATE TABLE IF NOT EXISTS servicio(
  se char(9),
  descripcion varchar(30),
  PRIMARY KEY (se)
  );
INSERT INTO  servicio VALUES ('ser1','duro');

-- tabla realiza
  DROP TABLE IF EXISTS realiza;

CREATE TABLE IF NOT EXISTS realiza(
  soldado char(9),
   servicio char(9),
  fecha date,
  PRIMARY KEY (soldado,servicio),
  CONSTRAINT  FKrealizaSoldado FOREIGN KEY (soldado) REFERENCES soldado(s) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKrealizaServicio FOREIGN KEY (servicio) REFERENCES servicio(se) on DELETE CASCADE ON UPDATE CASCADE
  );


INSERT INTO  realiza VALUES ('sol1','ser1','1988/01/11');

--  tabla cuartel
CREATE TABLE IF NOT EXISTS cuartel(
  cu char(9),
  nombre varchar(30),
  direccion varchar(30),
  PRIMARY KEY (cu)
  );
INSERT INTO  cuartel VALUES ('cuartel1','cuartel principal','Santander');


-- tabla esta
CREATE TABLE IF NOT EXISTS esta(
  soldado char(9),
   cuartel char(9),
  PRIMARY KEY (soldado,cuartel),
  CONSTRAINT  FKestaSoldado FOREIGN KEY (soldado) REFERENCES soldado(s) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKestaCuartel FOREIGN KEY (cuartel) REFERENCES cuartel(cu) on DELETE CASCADE ON UPDATE CASCADE
  );


INSERT INTO  esta VALUES ('sol1','cuartel1');

--  tabla cuerpo

CREATE TABLE IF NOT EXISTS cuerpo(
  c char(9),
   denominacio varchar(30),
  PRIMARY KEY (c)
  );

INSERT INTO  cuerpo VALUES ('cuerpo1','cuerpo bueno');

--  tabla pertenecen

CREATE TABLE IF NOT EXISTS pertenecen(
  cuerpo char(9),
   soldado char(9),
  PRIMARY KEY (cuerpo,soldado),
  CONSTRAINT  FKpertenecenCuerpo FOREIGN KEY (cuerpo) REFERENCES cuerpo(c) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKpertenecenSoldado FOREIGN KEY (soldado) REFERENCES soldado(s) on DELETE CASCADE ON UPDATE CASCADE
  );

INSERT INTO  pertenecen VALUES ('cuerpo1','sol1');





