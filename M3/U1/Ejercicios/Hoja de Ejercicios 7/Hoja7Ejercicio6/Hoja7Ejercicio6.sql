﻿--  eliminar la bd si existe
DROP  DATABASE IF EXISTS hoja7unidad1modulo3Ejercicio6;

CREATE DATABASE hoja7unidad1modulo3Ejercicio6;

USE hoja7unidad1modulo3Ejercicio6;

-- tabla opcion
DROP TABLE IF EXISTS opcion;

CREATE TABLE IF NOT EXISTS opcion(
  nombre char(20),
  descripcion varchar(30),
  PRIMARY KEY (nombre)
  );


--  tabla tiene
DROP TABLE IF EXISTS tiene;

CREATE TABLE IF NOT EXISTS tiene(
  marca char(20),
  cil char(20),
  modelo char(20),
  opcion  char(20),
  precio float,
  PRIMARY KEY (marca,cil,modelo,opcion),
  CONSTRAINT  FKtieneOpcion FOREIGN KEY (opcion) REFERENCES opcion(nombre) on DELETE CASCADE ON UPDATE CASCADE
  );


-- tabla modelo
DROP TABLE IF EXISTS modelo;

CREATE TABLE IF NOT EXISTS modelo(
  marca char(20),
  cil char(20),
  modelo char(20),
 precio float,
  PRIMARY KEY (marca,cil,modelo),
  CHECK(precio>=0)
  );

SELECT * FROM modelo WHERE precio >=0;

-- tabla cliente

DROP TABLE IF EXISTS cliente;

CREATE TABLE IF NOT EXISTS cliente(
  dni char(20),
   nombre char(20),
  dir char(20),
  tel char(20),
  PRIMARY KEY (dni)
  );


-- tabla vehiculo
DROP TABLE IF EXISTS vehiculo;
CREATE TABLE IF NOT EXISTS vehiculo(
  matricula char(20),
  precio int,
  marca char(20),
  modelo char(20),
  PRIMARY KEY (matricula)
  );



-- tabla vendedor
DROP TABLE IF EXISTS vendedor;

CREATE TABLE IF NOT EXISTS vendedor(
  dni char(20),
  nombre char(20),
  dir char(20),
  tel char(20),
  PRIMARY KEY (dni)
  );

-- tabla cede opcion 1
DROP TABLE IF EXISTS cede;
CREATE TABLE IF NOT EXISTS cede(
  cliente char(20),
  vehiculo char(20),
  fecha date,
  PRIMARY KEY (cliente,vehiculo),
    CONSTRAINT cedeUnica1 UNIQUE KEY (vehiculo),
   CONSTRAINT  FKcedeCliente FOREIGN KEY (cliente) REFERENCES cliente(dni) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKcedeVehiculo FOREIGN KEY (vehiculo) REFERENCES vehiculo(matricula) on DELETE CASCADE ON UPDATE CASCADE
  );

-- tabla compra
DROP TABLE IF EXISTS compra;
CREATE TABLE IF NOT EXISTS compra(
  --  tabla modelo
  marca char(20),
  cil char(20),
  modelo char(20),
  -- cliente
  cliente char(20),
  --  vendedor
  vendedor char(20),
  --  opcion
  opcion char(20),

  matricula char(20),
  fecha date,
  PRIMARY KEY (marca,cil,modelo,cliente,vendedor,opcion),
  CONSTRAINT  FKcomproModelo FOREIGN KEY (marca,cil,modelo) REFERENCES modelo(marca,cil,modelo) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKcomproCliente FOREIGN KEY (cliente) REFERENCES cliente(dni) on DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT  FKcomproVendedor FOREIGN KEY (vendedor) REFERENCES vendedor(dni) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKcomproOpcion FOREIGN KEY (opcion) REFERENCES opcion(nombre) on DELETE CASCADE ON UPDATE CASCADE
  );



