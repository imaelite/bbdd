﻿--  eliminar la bd si existe
DROP  DATABASE IF EXISTS hoja7unidad1modulo3;

CREATE DATABASE hoja7unidad1modulo3;

USE hoja7unidad1modulo3;

-- tabla empleados
DROP TABLE IF EXISTS empleado;

CREATE TABLE IF NOT EXISTS empleado(
  dni char(9),
  PRIMARY KEY (dni)
  );

INSERT INTO  hoja7unidad1modulo3.empleado VALUES ('dni1'),('dni2');



-- tabla departamento
  DROP TABLE IF EXISTS departamento;

CREATE TABLE IF NOT EXISTS departamento(
  codigo varchar(15),
  PRIMARY KEY (codigo)
  );

INSERT INTO  departamento VALUES ('cod_dept1'),('cod_dept2');



--  tabla pertenecen

DROP TABLE IF EXISTS pertenece;

CREATE TABLE IF NOT EXISTS pertenece(
  departamento varchar(15), 
  empleado char(9),
  PRIMARY KEY(departamento,empleado),
   CONSTRAINT uniqueEmpleado UNIQUE KEY (empleado),
  CONSTRAINT  FKPerteneceEmpleado FOREIGN KEY (empleado) REFERENCES empleado(dni) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKPerteneceDepartamento FOREIGN KEY (departamento) REFERENCES departamento(codigo) on DELETE CASCADE ON UPDATE CASCADE
  );

INSERT INTO  pertenece VALUES ('cod_dept1','dni1'),('cod_dept2','dni2');


-- tabla proyecto
  DROP TABLE IF EXISTS proyecto;

CREATE TABLE IF NOT EXISTS proyecto(
  codigo varchar(15),
  PRIMARY KEY (codigo)
  );
INSERT INTO  proyecto VALUES ('pro1'),('pro2');

-- tabla trabaja
  DROP TABLE IF EXISTS trabaja;

CREATE TABLE IF NOT EXISTS trabaja(
  empleado char(9),
   proyecto varchar(15),
  fecha date,
  PRIMARY KEY (empleado,proyecto),
  CONSTRAINT  FKtrabajaEmpleado FOREIGN KEY (empleado) REFERENCES empleado(dni) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKtrabajaProyecto FOREIGN KEY (proyecto) REFERENCES proyecto(codigo) on DELETE CASCADE ON UPDATE CASCADE
  );


INSERT INTO  trabaja VALUES ('dni1','pro1','1988/01/11');