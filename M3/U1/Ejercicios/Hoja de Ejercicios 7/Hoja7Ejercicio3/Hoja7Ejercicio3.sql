﻿--  eliminar la bd si existe
DROP  DATABASE IF EXISTS hoja7unidad1modulo3Ejercicio3;

CREATE DATABASE hoja7unidad1modulo3Ejercicio3;

USE hoja7unidad1modulo3Ejercicio3;

-- tabla turista
DROP TABLE IF EXISTS turista;

CREATE TABLE IF NOT EXISTS turista(
  t char(9),
  nombre varchar(30),
  apellidos varchar(30),
  direccion varchar(30),
  telefono varchar(30),
  PRIMARY KEY (t)
  );

INSERT INTO turista VALUES ('t_01','imanol','osuna','calle falsa','999888777');

-- tabla hotel
DROP TABLE IF EXISTS hotel;

CREATE TABLE IF NOT EXISTS hotel(
  h char(9),
  nombre varchar(30),
  direccion varchar(30),
  plazas int,
  ciudad varchar(30),
  PRIMARY KEY (h)
  );

INSERT INTO hotel VALUES ('h_01','imanol','calle falsa',9,'Santander');

-- tabla reserva
DROP TABLE IF EXISTS reserva;

CREATE TABLE IF NOT EXISTS reserva(
  t char(9),
  h char(9),
  pensión varchar(30),
  fecha_e date,
   fecha_s date,
  PRIMARY KEY (t,h),
    CONSTRAINT  FKReservaTurista FOREIGN KEY (t) REFERENCES turista(t) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT   FKReservaHotel FOREIGN KEY (h) REFERENCES hotel(h) on DELETE CASCADE ON UPDATE CASCADE
  );

INSERT INTO reserva VALUES ('t_01','h_01','completa','2019/11/01','2019/11/08');

-- tabla vuelo

DROP TABLE IF EXISTS vuelo;

CREATE TABLE IF NOT EXISTS vuelo(
  n char(9),
   fecha date,
  hora varchar(30),
   origen varchar(30),
  n_turista int,
  n_total int,
  destino varchar(30),
  PRIMARY KEY (n)
  );

INSERT INTO vuelo VALUES ('v_01','2019/11/01','15:30','Burgos',3,11,'Salamanca');


-- tabla toma
DROP TABLE IF EXISTS toma;

CREATE TABLE IF NOT EXISTS toma(
  t char(9),
  v char(9),
  clase varchar(30),
  PRIMARY KEY (t,v),
  CONSTRAINT  FKTomaTurista FOREIGN KEY (t) REFERENCES turista(t) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT   FKRTomaVuelo FOREIGN KEY (v) REFERENCES vuelo(n) on DELETE CASCADE ON UPDATE CASCADE
  );

INSERT INTO toma VALUES ('t_01','v_01','turista');

-- tabla agencia
DROP TABLE IF EXISTS toma;

CREATE TABLE IF NOT EXISTS toma(
  t char(9),
  v char(9),
  clase varchar(30),
  PRIMARY KEY (t,v),
  CONSTRAINT  FKTomaTurista FOREIGN KEY (t) REFERENCES turista(t) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT   FKRTomaVuelo FOREIGN KEY (v) REFERENCES vuelo(n) on DELETE CASCADE ON UPDATE CASCADE
  );

INSERT INTO toma VALUES ('t_01','v_01','turista');

-- tabla agencia

DROP TABLE IF EXISTS agencia;

CREATE TABLE IF NOT EXISTS agencia(
  s char(9),
  direccion varchar(30),
   tel varchar(30),
  PRIMARY KEY (s)
  );

INSERT INTO agencia VALUES ('a_01','Burgos','999666333');

-- tabla contrata

DROP TABLE IF EXISTS contrata;

CREATE TABLE IF NOT EXISTS contrata(
  t char(9),
  a char(9),
  PRIMARY KEY (t,a),
  CONSTRAINT  FKcontrataTurista FOREIGN KEY (t) REFERENCES turista(t) on DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT  FKRcontrataAgencia FOREIGN KEY (a) REFERENCES agencia(s) on DELETE CASCADE ON UPDATE CASCADE
  );

INSERT INTO contrata VALUES ('t_01','a_01');

